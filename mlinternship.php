<!DOCTYPE html>
<html lang="en">
<head>
	<title>Internship At Makers Lab</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/mlinternship"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/mlinternship">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/internship.jpg">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/internship-bg.jpg">
	<meta property="og:description" content="An initiative of Makers Lab is hosting a month internship program, A platform to Ideate, Design and Build your Product. Internship is a building stone to develop a community of engineers (students, graduates, professionals and others) to collaborate on building an innovative solutions for challenging problems."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="An initiative of Makers Lab is hosting a month internship program, A platform to Ideate, Design and Build your Product. Internship is a building stone to develop a community of engineers (students, graduates, professionals and others) to collaborate on building an innovative solutions for challenging problems."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Internship at Makers Lab">
	<link rel="canonical" href="https://www.deshpandestartups.org/mlinternship">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/internship-bg.jpg" width="1349" height="198" alt="Deshpande Startups, events, Makers Makeathon">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Internship At Makers Lab</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">1 MONTH INTERNSHIP AT</span><br> MAKERS LAB</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-4 p-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/internship.jpg" width="474" height="237" alt="Deshpande startups Makers Makeathon">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center text-truncate">Internship At Makers Lab</h5>
									<p><b>Date : </b>January 07<sup>th</sup> 2019</p>
									<!-- <p><b>Time : </b>09:00 AM</p> -->
									<p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
										E:<a href="mailto:makerslab&#064;dfmail&#046;org"> makerslab&#064;dfmail&#046;org</a>
									</p>
								</div>
								<div class="card-footer">
									<p class="text-yellow">The registrations has been closed.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<p class="pt-4 text-yellow"><b>Event description:</b></p>
						<p class="text-justify wow slideInRight">An initiative of <b>Makers Lab</b> is hosting a month internship program, A platform to <b>Ideate, Design and Build your Product.</b> Internship is a building stone to develop a community of engineers (students, graduates, professionals and others) to collaborate on building an innovative solutions for challenging problems. Also, feel the vibrant startup ecosystem in a tier 2 city like Hubballi.
						</p>
						<p class="text-yellow"><b>6 reasons why you should participate:</b></p>
						<ul class="wow slideInRight text-justify">
							<li>Gain practical exposure and build knowledge on multidisciplinary technologies</li>
							<li>Hands on experience on utilizing cutting edge machines available at the Makers lab</li>
							<li>Exposure to Startup Ecosystem, Makers Lab, Yuva Entrepreneurship & ESDM and build entrepreneurial mindset</li>
							<li>Network with exciting startups, join the enthusiastic multidisciplinary makers and developers community, explore career opportunities</li>
							<li>Learn how to prototype innovative ideas efficiently while reducing the product development cycle</li>
							<li>Creating a culture of innovation and problem solving through collaboration</li>
						</ul>
						<!-- <p class="text-yellow"><b>Note:</b></p>
							<i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> For 4<sup>th</sup> year students batch starts from January 07<sup>th</sup> 2019<br>
							<i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> For 2<sup>nd</sup> & 3<sup>rd</sup> year students batch starts from January 17<sup>th</sup> 2019 -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- <br> -->

	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>