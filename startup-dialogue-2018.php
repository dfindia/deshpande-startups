<!DOCTYPE html>
<html lang="en">
<head>
   <title>Startup Dialogue 2018</title>
   <?php
   require_once 'essentials/meta.php';
   ?>
   <meta name="linkage" content="https://www.deshpandestartups.org/startup-dialogue-2018"/>
   <meta property="og:site_name" content="Deshpande Startups"/>
   <meta property="og:type" content="website">
   <meta property="og:url" content="https://www.deshpandestartups.org/startup-dialogue-2018">
   <meta property="og:title" content="Deshpande Startups, Startup Dialogue 2018">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/deshpande-bg-new.jpg">
   <meta property="og:description" content="Startup Dialogue intends to create an exciting platform for all the stakeholders (Enablers, Social & for profit Startups, VC's/Investors, Startup enthusiasts, Leaders) of the Indian startup ecosystem to come together and celebrate the spirit of entrepreneurship."/>
   <meta name="author" content="Deshpande Startups"/>
   <meta name="description" content="Startup Dialogue intends to create an exciting platform for all the stakeholders (Enablers, Social & for profit Startups, VC's/Investors, Startup enthusiasts, Leaders) of the Indian startup ecosystem to come together and celebrate the spirit of entrepreneurship."/>
   <!-- <meta name="keywords" content="Current openings, Business development executive, Business development manager, technical manager, Hubballi Karnatak India."/> -->
   <link rel="canonical" href="https://www.deshpandestartups.org/startup-dialogue-2018">
   
<?php
         // $title = 'Deshpande Startups';
require_once 'essentials/bundle.php';
?>
</head>
<body>
   <?php
   require_once 'essentials/title_bar.php';
   require_once 'essentials/menus.php';
   ?>

         <!-- <a href="inauguration-form.php" target="_blank"><img class="carousel-inner img-fluid" src="img/inauguration/inauguration-slider.png" width="1349" height="320" alt="Deshpande Startups, inaugural"></a>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb justify-content-end">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="events.php">Events</a></li>
            <li class="breadcrumb-item"><a href="inauguration.php">Inauguration</a></li>
            <li class="breadcrumb-item active" aria-current="page">Agenda</li>
         </ol>
      </nav> -->
      <div class="container">
         <div class="center wow fadeInDown">
            <h2 class="text-yellow pt-2 text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted"> STARTUP </span>DIALOGUE 2018</h2>
            <div class="divider b-y text-yellow content-middle"></div>
            <!-- <br> -->
            <!-- <p>Mentors will enrich your ideas, train you the current trends, navigate the right approach, help you in strategizing your business and also monitor your journey.</p> -->
         </div>
      </div>
      <!-- <br> -->

      <div class="container">
         <div class="row">
            <!-- <div class="col-md-1"></div> -->
            <div class="col-md-12 pl-5">
               <!-- <embed src="Agenda-for-inauguration-of-Sandbox-Startups.pdf" width="1050px" height="900px" /> -->
                  <!-- <div class='embed-responsive' style='padding-bottom:150%'>
    <object data='Agenda-for-inauguration-of-Sandbox-Startups.pdf' type='application/pdf' width='100%' height='100%'></object>
 </div> -->
 <!-- <iframe src="Agenda-for-inauguration-of-Sandbox-Startups.pdf" width="100%" height="800px"></iframe> -->
 <!-- <iframe src="http://docs.google.com/gview?url=https://www.deshpandestartups.org/Agenda-for-inauguration-of-Sandbox-Startups.pdf&embedded=true" style="width:100%; height:1000px;" frameborder="0"></iframe> -->
 <!-- <iframe src="http://docs.google.com/gview?url=https://www.deshpandestartups.org/Agenda-for-inauguration-of-Sandbox-Startups.pdf&embedded=true" width="100%" height="800px" frameborder="0"></iframe> -->
 <iframe src="https://docs.google.com/viewer?url=https://www.deshpandestartups.org/startup-dialogue-2018.pdf&embedded=true" width="100%" height="800px" frameborder="0"></iframe>
</div>
</div>
</div>
<br>
<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>