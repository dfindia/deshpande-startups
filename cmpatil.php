<!DOCTYPE html>
<html lang="en">
<head>
	<title>C. M. Patil - CEO, Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/cmpatil"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/cmpatil">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/team/c-m-patil.jpg">
	<meta property="og:description" content="C. M. Patil is an engineering graduate and brings a decade & half of entrepreneurial experience, and have held positions like Director, COO, National Technical-Head in his professional career."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="C. M. Patil is an engineering graduate and brings a decade & half of entrepreneurial experience, and have held positions like Director, COO, National Technical-Head in his professional career."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Makers Lab Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="C. M. Patil - CEO, Deshpande Startups">
	<link rel="canonical" href="https://www.deshpandestartups.org/cmpatil">
	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	/*p{text-align:justify;}*/
	.cal{
		font-family: calibri;
	}
    .modal-title {
    	text-align: left;
    	margin: 0;
    	line-height: 1;
    }
    .follow-us{
    	margin:20px 0 0;
    }
    .follow-us li{ 
    	display:inline-block; 
    	width:auto; 
    	margin:0 5px;
    }
    .follow-us li .fa{ 
    	font-size:25px; 
    	/*color:#767676;*/
    	color: #e7572a;
    }
    .follow-us li .fa:hover{ 
    	color:#025a8e;
    }
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>

	<div class="container cal pt-4">
		<div class="row wow fadeInLeft" data-wow-duration="1s" data-wow-offset="50">
			<div class="col-md-12 team-main">
				<div>
					<div class="modal-header">
						<!-- <a type="button" class="close" data-dismiss="modal" aria-label="Close" href="#"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> back </a> -->
						<h4 class="modal-title modal-title-cust text-yellow" id="myModalLabel">C. M. PATIL<br><small class="text-muted">Chief Executive Officer - Deshpande Startups</small></h4>
						<ul class="follow-us clearfix">
							<li><a href="https://twitter.com/cmpatil7" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="https://www.linkedin.com/in/cmpatil/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="modal-body">
						<p class="text-justify">
							<img src="img/team/c-m-patil.jpg" width="165" height="160" class="img img-fluid mr-2 rounded-circle" alt="C. M. Patil, CEO-Deshpande Startups" align="left">
							C. M. Patil is an engineering graduate and brings a decade & half of entrepreneurial experience, and have held positions like Director, COO, National Technical-Head in his professional career.<br><br>
							He was on the board member for 6+yrs, the company grew from 12 ppl team to 350+ team, rendering their services for the fortune 500 companies like Toyota, Mahindra, Bosch, HAL, HCL, etc. both in Aero and Auto-domain. He was then regarded as the youngest corporate trainer certified for a few major MNC’s on CAD tools. To get the global experience join an MNC & lead a large team, and being passionate to solve the problems co-founded another startup a year later which is into a hiring platform which was regarded among top 50 Startups of the country by IIMA and was awarded grant money. Their platform was used by industry leaders.<br><br>
							He was passionate to build products from India and joined a startup which was in Machine vision space as a Delivery Head, their 1st product itself won a global award at Texas, the USA competing against global players. His strong technical foundation was pretty helpful in excelling his skills in business operations, currently, the CEO of Deshpande Startups of Deshpande Foundation. The company envisions to create a Hub for mission-driven entrepreneurs in the region. Since he comes from tier 2 city the vision was strong enough to make him relocate from Bangalore to Hubli to nurture the entrepreneurial ecosystem in non-metros.  Today they have guided more than 80+ startups.<br><br>
							More than 20+ startups have received external funding (Angel investments and grants from State Govt.)
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>