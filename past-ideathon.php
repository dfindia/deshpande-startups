<!DOCTYPE html>
<html lang="en">
<head>
	<title> Ideathon | Events, Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/past-ideathon"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/past-ideathon">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/ideathon.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/ideathon-bg.png">
	<meta property="og:description" content="Do you have an innovative idea that can translate into a cutting-edge product or service? Participate in the IDEATHON, a Platform to Present Your Idea to Solve Real World Problems and get selected to YUVA ENTREPRENEURSHIP PROGRAM to Kick Start Your Startup Journey with the startup ecosystem and Win 1 Lakh Worth Rewards."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Do you have an innovative idea that can translate into a cutting-edge product or service? Participate in the IDEATHON, a Platform to Present Your Idea to Solve Real World Problems and get selected to YUVA ENTREPRENEURSHIP PROGRAM to Kick Start Your Startup Journey with the startup ecosystem and Win 1 Lakh Worth Rewards."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Ideathon">
	<link rel="canonical" href="https://www.deshpandestartups.org/past-ideathon">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/ideathon-bg.png" width="1349" height="198" alt="Deshpande Startups, events IDEATHON">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Ideathon</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown">IDEATHON</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/ideathon.png" width="474" height="237" alt="Deshpande startups IDEATHON">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center">Ideathon </h5>
									<p><b>Date : </b>April 30<sup>th</sup> 2019</p>
									<!-- <p><b>Last Date to Register : </b>April 25<sup>th</sup> 2019</p> -->
									<p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village,<br> Gokul Road, Hubballi, Karnataka.
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
										E:<a href="mailto:makerslab&#064;dfmail&#046;org"> makerslab&#064;dfmail&#046;org</a>
									</p>
								</div>
								<div class="card-footer">
									<p class="text-yellow">The registrations has been closed.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-4 text-yellow"><b>Event Description:</b></p>
						<p class="text-justify wow slideInRight"> As an Initiative of <b>Yuva Entrepreneurship</b> Program of <b>Deshpande Startups</b> we are extending an opportunity for the student community & graduates aged between <b>15 - 23</b> to be a part of this exclusive event and sense the vibrant startup ecosystem to become India's propelling true growth story.</p>

						<p class="text-justify wow slideInRight">Do you have an innovative idea that can translate into a cutting-edge product or service? Participate in the <b>IDEATHON</b>, a platform to present your idea to solve real world problems and get selected for <b>Yuva Entrepreneurship Program</b> to kick start your startup journey with the startup ecosystem and win 1 Lakh worth rewards.</p>

						<p class="text-justify wow slideInRight text-yellow"><b> As a part of the winning team you will receive support as mentioned below:</b></p>
						<ul class="text-justify wow slideInRight">
							<li><b>Makers Lab</b> - A platform to ideate, design and build your product</li>
							<li>Full fledge automated <b>ESDM Lab</b></li>
							<li>Furnished  co-working space with Wi-Fi facility</li>
							<li>Mentorship and coaching</li>
							<li>Networking opportunities</li>
							<li>Training/workshop</li>
							<li>Knowledge sessions</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<br>

	<br>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>