	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Apply for Deshpande Startups Visit</title>
		<?php
		require_once 'essentials/meta.php';
		?>
		<meta name="linkage" content="https://www.deshpandestartups.org/fssi-visit-form"/>
		<meta property="og:site_name" content="Deshpande Startups"/>
		<meta property="og:type" content="website">
		<meta property="og:url" content="https://www.deshpandestartups.org/fssi-visit-form">
		<!-- <meta property="og:image" content="https://www.deshpandestartups.org/img/events/alexathon.jpg"> -->
		<meta property="og:description" content="Deshpande startups is a living laboratory of Resources, Connections, Knowledge & Talent to support mission driven entrepreneurs to scale in their venture."/>
		<meta name="author" content="Deshpande Startups"/>
		<meta name="description" content="Deshpande startups is a living laboratory of Resources, Connections, Knowledge & Talent to support mission driven entrepreneurs to scale in their venture."/>
		<!-- <meta name="keywords" content=""/> -->
		<meta property="og:title" content="Deshpande Startups Visit">
		<link rel="canonical" href="https://www.deshpandestartups.org/fssi-visit-form">
		<?php
			// $title = 'Deshpande Startups';
		require_once 'essentials/bundle.php';
		?>
		<style type="text/css">
		.tdwidth
		{width: 200px;}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<br>
	<div class="container text-center">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">APPLY FOR EXPOSURE VISIT</span><br> TO DESHPANDE STARTUPS</h2>
		<div class="divider b-y text-yellow content-middle"></div>
	</div>
	<br>
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-md-9">
				<iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
				</iframe>
				<div class="p-3 w3-card">
					<form role="form" action="https://docs.google.com/forms/d/e/1FAIpQLSdhzc7NzpZOolNzIv4y_ONlQXhahGImiyUtqOFUei14UVOJow/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">
						<div class="row">
							<div class="col-md-12 pad">
								<!-- <div class="row"> -->
									<div class="col-md-12">
										<div class="row">
											<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
												<label for="input1"><b>Name<span class="text-yellow">*</span></b></label>
												<input type="text" name="entry.164745598" class="box2 form-control" maxlength="50" pattern="^(?![ .]+$)[a-zA-Z .]*$" placeholder="Mention your name" title="Mention your name" required="required">
											</div>
											<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
												<label for="input3"><b>Mobile number<span class="text-yellow">*</span></b></label>
												<input type="phone" name="entry.1762696389" class="box2 form-control" pattern="\d*" min="12" placeholder="Mention your mobile number" maxlength="10" minlength="10" title="Your mobile number" required="required">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="row">
											<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
												<label for="input2"><b>Email-Id<span class="text-yellow">*</span></b></label>
												<input type="email" name="entry.1806706054" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
											</div>
											<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="input4"><b>College/Organization/Company<span class="text-yellow">*</span></b></label>
												<input type="text" name="entry.1217847706" class="box2 form-control" placeholder="College/Organization/Company" required="required">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="row">
											<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="input4"><b>City/Place<span class="text-yellow">*</span></b></label>
												<input type="text" name="entry.1387792052" pattern="^(?![ .]+$)[a-zA-Z .]*$" class="box2 form-control" placeholder="Your location" required="required">
											</div>
											<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
												<label for="input3"><b>State<span class="text-yellow">*</span></b></label>
												<input type="text" name="entry.1025668952" pattern="^(?![ .]+$)[a-zA-Z .]*$" placeholder="Your state" class="box2 form-control" required="required">
											</div>
										</div>
									</div>

									<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
										<label for="input7"><b>Are You?<span class="text-yellow">*</span></b></label><br>
									</div>
									<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="student"><input type="radio" name="entry.169493007" value="student" required="required" checked="checked"> Student</label>
											</div>
											<div class="form-group col-md-6">
												<label for="startup"><input type="radio" name="entry.169493007" value="Startup/Professional"> Startup/Professional</label>
											</div>
											<!-- <div class="form-group col-md-4">
												<label for="Academician"><input type="radio" name="entry.169493007" value="Academician"> Academician</label>
											</div>
											<div class="form-group col-md-4">
												<label for="Professional"><input type="radio" name="entry.169493007" value="professional"> Professional</label>
											</div> -->
										</div>
									</div>
									<!-- <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
										<label for="input4"><b>Enter the number of people want to visit<span class="text-yellow">*</span></b></label>
										<input type="number" name="entry.38970015" class="box2 form-control" placeholder="Number of people want to visit" required="required">
									</div> -->
									<!-- <div class="col-md-12">
										<div class="row">
											<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="input4"><b>Select the date you would like to visit<span class="text-yellow">*</span></b></label>
												<br>
												<br>
												<input type="date" name="entry.2065240515" class="box2 form-control" placeholder="Date you would like to visit" required="required">
											</div>
											<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
												<label for="appt-time"><b>Enter time to visit (visiting hours 10:00am to 06:00pm)<span class="text-yellow">*</span></b></label>
												<input type="time" name="entry.271257944" id="appt-time" min="10:00" max="18:00" class="box2 form-control" required="required">
											</div>
										</div>
									</div> -->
									<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
										<label for="input9"><b>Do you have any questions</b></label>
										<textarea name="entry.1957566567" class="box2 form-control" rows="5" min="10" maxlength="350" placeholder="Your queries" title="Your Text Here"></textarea>
									</div>

									<div class="form-group col-lg-12">
									<!-- <span class="text-yellow"><b>*</b></span>
										<div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div><br> -->
										<span class="text-yellow"><h6><b>*</b> Fields are mandatory</h6></span>
										<input type="submit" class="btn btn-warning" id="ss-submit" name="submit" value="Submit">
									</div>
									<!-- </div> -->
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<br>

		<script src='https://www.google.com/recaptcha/api.js'></script>
		<?php
		require_once 'essentials/footer.php';
		require_once 'essentials/copyright.php';
		require_once 'essentials/js.php';
		?>

		<script>
			window.onload = function() {
				var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
				recaptcha.required = true;
				recaptcha.oninvalid = function(e) {
	 // do something
	 alert("Please complete the captcha");
	}
}
</script>
</body>
</html>