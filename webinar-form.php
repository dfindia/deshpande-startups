	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Apply now | Webinar - Agri NEXT: Future of Innovation & Entrepreneurship in Agriculture</title>
		<?php
		require_once 'essentials/meta.php';
		?>
		<meta name="linkage" content="https://www.deshpandestartups.org/webinar-form"/>
		<meta property="og:site_name" content="Deshpande Startups"/>
		<meta property="og:type" content="website">
		<meta property="og:url" content="https://www.deshpandestartups.org/webinar-form">
		<meta property="og:image" content="https://www.deshpandestartups.org/img/events/webinar3.jpg">
		<meta property="og:image" content="https://www.deshpandestartups.org/img/events/webinar3-bg.jpg">
		<meta property="og:description" content="India is an agrarian economy and the majority of the individuals are engaged in this sector for the sustenance of better livelihoods. It renders a significant contribution to the growth and development of the country’s economy."/>
		<meta name="author" content="Deshpande Startups"/>
		<meta name="description" content="India is an agrarian economy and the majority of the individuals are engaged in this sector for the sustenance of better livelihoods. It renders a significant contribution to the growth and development of the country’s economy."/>
		<!-- <meta name="keywords" content=""/> -->
		<meta property="og:title" content="Apply now for Webinar">
		<link rel="canonical" href="https://www.deshpandestartups.org/webinar-form">
		<?php
		// $title = 'Deshpande Startups';
		require_once 'essentials/bundle.php';
		?>
	</head>
	<body>
		<?php
		require_once 'essentials/title_bar.php';
		require_once 'essentials/menus.php'; 	
		?>
		<br>
		<!-- <div class="container text-center">
			<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">APPLY FOR</span> WEBINAR SERIES</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div> -->
		<br>
		<div class="container pt-5">
			<div class="row justify-content-md-center">
				<br>
				<div class="col-md-10">
					<h4 class="text-center">Registration for the webinar has been closed because we have reached the maximum number of registrations for the event.<br><br> Thank you for your interest!</h4>
					
				</div>
			</div>
		</div>
		<br>
		<br>
		<br>
		<br>
		<br>

		<script src='https://www.google.com/recaptcha/api.js'></script>
		<?php
		require_once 'essentials/footer.php';
		require_once 'essentials/copyright.php';
		require_once 'essentials/js.php';
		?>


		<script type="text/javascript">
			$(function() {
				$('[name="entry.257744942"]').on('click', function (e) {
					var val = $(this).val();
					if (val == "Startup founder") {
						$('.txbx1').show('fade');
						$('.txbx2').hide();
								// $('.txbx3').hide();
							}else if (val == "Aspiring entrepreneur") {
								$('.txbx1').show('fade');
								$('.txbx2').hide();
								// $('.txbx3').hide();
							}else if (val == "Others") {
								$('.txbx1').hide();
								$('.txbx2').show('fade');
								// $('.txbx3').hide();
							}else {
								$('.txbx1').hide();
								$('.txbx2').hide();
								// $('.txbx3').hide();
							};
						});
			});
		</script>

		<script>
			window.onload = function() {
				var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
				recaptcha.required = true;
				recaptcha.oninvalid = function(e) {
	 // do something
	 alert("Please complete the captcha");
	}
}
</script>
</body>
</html>