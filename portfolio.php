<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Portfolio - Deshpande Startups</title>
      <?php
         require_once 'essentials/meta.php';
         ?>
      <meta name="linkage" content="https://www.deshpandestartups.org/portfolio"/>
      <meta property="og:site_name" content="Deshpande Startups"/>
      <meta property="og:type" content="website">
      <meta property="og:url" content="https://www.deshpandestartups.org/portfolio">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/portfolio/klonec.jpg">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/portfolio/fluxgentech.jpg">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/portfolio/lifetrons.jpg">
      <meta property="og:description" content="Deshpande Startups Portfolio | Agritech | HealthTech | EduTech| SME Tech | RetailTech | DeepTech | Hyperlocal | ConsumerTech | Travel & Tourism."/>
      <meta name="author" content="Deshpande Startups"/>
      <meta name="description" content= "Deshpande Startups Portfolio | Agritech | HealthTech | EduTech| SME Tech | RetailTech | DeepTech | Hyperlocal | ConsumerTech | Travel & Tourism."/>
      <!-- <meta name="keywords" content="incubation, esdm cluster, pcb prototyping, Testing &amp; certification, arduino, raspberry pi boards, sandbox startups"/> -->
      <meta property="og:title" content="Deshpande Startups - Portfolio">
      <link rel="canonical" href="https://www.deshpandestartups.org/portfolio">
      <link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">
      <?php
         require_once 'essentials/bundle.php';
         ?>
   </head>
   <body>
      <?php
         require_once 'essentials/title_bar.php';
         require_once 'essentials/menus.php';
         ?>
      <!-- Generator: Jssor Slider Maker -->
      <!-- <div id="jssor_1" class="slide1"> -->
      <!-- Loading Screen -->
      <!-- <div data-u="loading" class="jssorl-009-spin load"> -->
      <!-- <img class="spin1" src="img/spin.svg" alt="spin" /> -->
      <!-- </div> -->
      <!-- <div data-u="slides" class="slide2"> -->
      <!-- <div data-p="170.00"> -->
      <!-- <img data-u="image" src="img/bg-home/edge-bg.png" class="img-fluid" alt="Deshpande Startups, EDGE Program"> -->
      <!-- </div> -->
      <!-- <div data-p="170.00"> -->
      <!-- <a href="edge-form" target="_blank"><img data-u="image" src="img/bg-home/edge-theme.png" class="img-fluid" alt="EDGE theme"></a> -->
      <!-- </div> -->
      <!-- <div data-p="170.00"> -->
      <!-- <img data-u="image" src="img/bg-home/startups-launched.png" class="img-fluid" alt="Deshpande Startups, EDGE Program, startups launched"> -->
      <!-- </div> -->
      <!-- <div data-p="170.00"> -->
      <!-- <img data-u="image" src="img/edge/impact-of-edge.png" class="img-fluid" alt="Deshpande Startups, EDGE Program, Impact of EDGE Program"> -->
      <!-- </div> -->
      <!-- </div> -->
      <!-- Bullet Navigator -->
      <!-- <div data-u="navigator" class="jssorb051 bull" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75"> -->
      <!-- <div data-u="prototype" class="i bulletnav"> -->
      <!-- <svg viewbox="0 0 16000 16000" class="pos"> -->
      <!-- <circle class="b" cx="8000" cy="8000" r="5800"></circle> -->
      <!-- </svg> -->
      <!-- </div> -->
      <!-- </div> -->
      <!-- </div> -->
      <!-- <nav aria-label="breadcrumb">
         <ol class="breadcrumb justify-content-end">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"> EDGE</li>
         </ol>
         </nav> -->
      <!-- <br> --> <br>
      <div class="container">
         <div class="text-center">
            <h2 class="text-yellow  wow fadeInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted"> </span>PORTFOLIO</h2>
            <div class="divider b-y text-yellow content-middle"></div>
         </div>
         <div class="text-center">
            <div id="myBtnContainer">
               <button class="btn p-1 btnactive btn-lg btn-warning" onclick="filterSelection('all')"> All</button>
               <button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('agritech')"> AgriTech</button>
               <button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('healthtech')"> HealthTech</button>
               <button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('edutech')"> EduTech</button>
               <button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('smetech')"> SME Tech</button>
               <button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('retailtech')"> RetailTech</button>
               <button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('deeptech')"> DeepTech</button>
               <button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('hyperlocal')"> Hyperlocal</button>
               <button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('consumertech')"> ConsumerTech</button>               
               <button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('travel&tourism')"> Travel & Tourism</button>
            </div>
         </div>
      </div>
      <br> 
      <div class="tz-gallery">
         <div class="container">
            <div class="row">
               <div class="col-xs-12 col-sm-6 col-md-4 gal agritech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/klonec.jpg" alt="klonec-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Krishi Tantra</h3>
                           <p class="text-center">Krishi Tantra provides an automated soil analysis system which provide results of soil nutrients and customized agronomy on the go. Krishi Tantra addresses difficulties in the current system of farming with indigenously designed instruments and tools to improve farming practices.</p>
                           <a href="https://krishitantra.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!--1-->
               <div class="col-xs-12 col-sm-6 col-md-4 gal smetech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/fluxgentech.jpg" alt="fluxgentech-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">FluxGen Engineering Technologies</h3>
                           <p class="text-left">Fluxgen offers AI and IoT based platform for Industrial water management for efficient and sustainable usage of water resources.</p>
                           <a href="http://www.fluxgentech.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!--2-->
               <div class="col-xs-12 col-sm-6 col-md-4 gal healthtech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/lifetrons.jpg" alt="lifetrons-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Lifetrons Inno Equipments Pvt. Ltd.</h3>
                           <p class="text-center">Lifetrons provides cost-effective solution for combating Neonatal Jaundice/Infant Mortality, with A BESIDE PORTABLE low cost NEONATAL PHOTOTHERAPY UNIT. Lifetrons Aura Can be used in most of Rural and Remote Health setup.</p>
                           <a href="https://lifetronshealth.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 3 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal smetech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/linkEz-tech.jpg" alt="linkEz-tech-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">LinkEz Technologies Pvt. Ltd.</h3>
                           <p class="text-center">LinkEZ’s state-of-the-art MATS Engine is core to building a network of interconnected devices, processes, and people, all working towards making manufacturing process smart and efcient.</p>
                           <a href="http://www.linkeztech.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 4 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal healthtech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/nautilus-hearing.jpg" alt="nautilus-hearing-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Nautilus Hearing Solutions Pvt. Ltd.</h3>
                           <p class="text-center">Nautilus Hearing is a new kind of hearing care company which built smart hearing testing devices by using best technologies to make Hearing Care Simple, Accessible and Reliable.</p>
                           <a href="http://www.nautilushearing.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 1 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal healthtech deeptech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/skyfire-applied.jpg" alt="skyfire-applied-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Atom360</h3>
                           <p class="text-center">Atom 360 is developing an Artificial Intelligence algorithm which identify potentially malignant disorders like precancerous and cancerous oral lesions by analyzing images captured by the smartphone's camera. This helps the early detection and reduces the cost and treatment time.</p>
                           <a href="http://www.atom360.io/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 2 -->
               <!-- 3 -->
               <!-- </div> -->
               <!-- end of row -->
               <!-- row 2 -->
               <!-- <div class="row"> -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal healthtech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/sparcolife-tech.jpg" alt="sparcolife-tech-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Sparcolife Technologies (OPC) Private Limited</h3>
                           <p class="text-center">Sparcolife Technologies is an evidence based Mind - Body Intervention (MBI) platform for bringing certainity and sanity to fertility care. Vyana Life is a platform for Couples seeking to have a Complete Family either naturally or through Assisted Reproductive Technology (ART)</p>
                           <a href="https://www.sparclife.co/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 6 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal hyperlocal">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/electreps.jpg" alt="electreps-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">ElectReps Pvt Ltd</h3>
                           <p class="text-center">PublicNext offers a mobile App which connects people in neighborhood with hyperlocal news, jobs, offers, Q&A, events and provides an options for people to create content</p>
                           <a href="https://publicnext.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 7 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal travel&tourism">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/digitour-tech.jpg" alt="digitour-tech-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Digi Tour</h3>
                           <p class="text-center">Digitour provides interactive audio-visual guided tour exclusively for historical monuments which can be accessed on any smart devices. It provides authentic information narrated by experts at lowest price. </p>
                           <a href="https://www.dgtour.in" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 5 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal healthtech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/ascentia.jpg" alt="ascentia-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">ResearchNeeds</h3>
                           <p class="text-center">ResearchNeeds is a clinical research data management platform with associated services, consists of 4 modules (DataKapt, StatX, MedRite, LabTech) which will ease the entire research process from research design to publishing.</p>
                           <a href="https://researchneeds.in/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 6 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal healthtech deeptech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/autoyos.jpg" alt="autoyos-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Autoyos</h3>
                           <p class="text-center">Autoyos provide accessible and affordable healthcare by reducing the dependability on resources through Autonomous Systems. Their solution, iRobo is a Smart and intelligent autonomous system to provided comprehensive eye care in a low resource settings without requiring skilled manpower. </p>
                           <a href="http://autoyos.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 7 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal agritech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/fala-tech.jpg" alt="fala-tech-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Fala</h3>
                           <p class="text-center">Fala provides a Decentralised, Organised and Cloud tech based Vegetable growing system, which is placed in the customers location to grow and deliver vegetables of customers choice on demand with clean water and without chemicals.</p>
                           <!-- <a href="vivek-pawar" target="_blank" class="btn btn-outline-secondary">Know More</a> -->
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 8 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal edutech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/scital-talent.jpg" alt="scital-talent-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Scital Talent Services Pvt. Ltd.</h3>
                           <p class="text-center">SciTal provides a Peer Learning Platform, designed for group based learning (guided by a facilitator), Activity based content for Peer engagement and Training to help teachers across Schools and Tuition centers transition to adopt the Peer learning model.</p>
                           <a href="https://scital.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 9 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal agritech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/transity.jpg" alt="transity-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Transity</h3>
                           <p class="text-center">Transity is building a technology platform, freshFLO, that can digitize all the operations involved in movement of agriculture produce from the farm to markets and end consumers. Their platform is broadly categorized into SaaS Platform & Digital Horticulture Value Chain Ecosystem. </p>
                           <a href="https://transity.co/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 10 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal healthtech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/dento-matrix.jpg" alt="dento-matrix-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Dento Matrix</h3>
                           <p class="text-center">DentoMatrix offers a seamless experience to dentists through the entire orthodontal treatment process. They have an Image based scanner that uses a normal CMOS sensor and 3D imaging techniques to produce 3D printable dental impressions.</p>
                           <a href="https://dentomatrix.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 11 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal agritech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/kakud.jpg" alt="kakud-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Kakud</h3>
                           <p class="text-center">Kakud provides a affordable storage and drying facilities to farmers for their respective crops. Kakud is with a vision of Improving the financial status of Indian farmers by providing Farmer’s friendly post-harvest solutions.</p>
                           <a href="http://www.kakud.in/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 12 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal deeptech retailtech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/docketrun-tech.jpg" alt="docketrun-tech-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Docketrun</h3>
                           <p class="text-center">Docketrun provides Artificial Intelligence Catalyst to Business for their Growth by providing plug and play solution to build next generation AI. It takes care of Infrastructure,Training Custom Models and Code, providing easily deployable models in Multiple Platforms like Web, Application and Hardware.</p>
                           <a href="https://www.docketrun.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 13 -->
               <!-- 15 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal healthtech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/ckelp-tech.jpg" alt="ckelp-tech-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">C-Kelp </h3>
                           <p class="text-center">CKelp is a digital Health Care platform for neighbourhood Residents and LOCAL Health care service providers. Enabling local Doctors, Diagnostic Centers and Pharmacies to provide Health care services to the needy local residents.</p>
                           <a href="https://ckelp.in/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 18 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal smetech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/nebeskie-labs.jpg" alt="nebeskie-labs-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Nebeskie Labs Pvt. Ltd</h3>
                           <p class="text-center">Nebeskie Labs is an Electricity Management Company which helps its customers in saving business energy cost. The customer can monitor and control their energy usage through Enture (web/mobile app) and pay through monthly or annual subscription plans.</p>
                           <a href="https://www.nebeskie.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 19 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal agritech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/oscillo-machines.jpg" alt="oscillo-machines-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Oscillo Machines Pvt. Ltd.</h3>
                           <p class="text-center">Oscillo Provides manual and electric operated root washed paddy transplanter. Addressing 95% root washed paddy seedling growers in country where our solution will increase the yield upto 12.8%.</p>
                           <a href="http://www.oscillomachines.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 20 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal deeptech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/nturing-tech.jpg" alt="nturing-tech-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Nturing</h3>
                           <p class="text-center">nTuring is an Artificial Intelligence and Machine Learning based product company into linguistic solutions. We enable businesses in education, media & broadcasting, communications and print media to leverage our linguistic voice and text solutions to enrich their businesses.</p>
                           <a href="https://nturing.com/#/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 21 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal hyperlocal retailtech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/shopgro.jpg" alt="shopgro-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Shopgro Pvt. Ltd.</h3>
                           <p class="text-center"> Shopgro create an automated hyperlocal search marketing platform which log and optimise each POS-scanned product and make the store’s digital presence in a matter of weeks. Shopgro enables a digital storefront for any mom and pop store with simple one time hardware installation.</p>
                           <a href="https://www.shopgro.in/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 22 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal agritech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/symgrow-tech.jpg" alt="symgrow-tech-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Symgrow Technolpgies Pvt. Ltd.</h3>
                           <p class="text-center">Symgrow provides SMART FAULT PREVENTER an electronic stabilizerwhich monitors power supply, fluctuations and internal damage in motor. The device which not only prevents burning of motors but also safeguards farmers/farm-workers against electrical shocks.</p>
                           <a href="http://symgrow.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 23 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal agritech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/wide-mobilty.jpg" alt="wide-mobilty-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Wide Mobilty Mechatronics Pvt. Ltd.</h3>
                           <p class="text-center">Wide mobility has built CONRAD-G that is a X-Ray based non-destructive inspection system to identify fruit fly infections in gherkins (post-harvest) by enterprise class of image processing software and applications.</p>
                           <a href="http://www.widemobility.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 24 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal smetech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/printalytix.jpg" alt="printalytix-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Printalytix Pvt Ltd</h3>
                           <p class="text-center">One-Stop-One-Shop Solution Provider for Additive, Subtractive & Formative – Manufacturing Technologies on a platter with the help of our network of growth-hungry, striving SMEs, utilizing their latent manufacturing potential, capacity & skills.</p>
                           <a href="https://www.printalytix.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!--25-->
               <div class="col-xs-12 col-sm-6 col-md-4 gal consumertech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/artiTech-innovations.jpg" alt="artiTech-innovations-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">KOSHA DESIGNS</h3>
                           <p class="text-center">KOSHA provides technology based authentication to the customers so that they buy the authentic handicrafts. The solution captures the product origin, crafts person details and production process which can be traced/verified by customers using their smart phones.</p>
                           <a href="https://www.koshadesigns.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 26 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal consumertech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/rapture.jpg" alt="rapture-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Rapture Innovation Labs Pvt. Ltd</h3>
                           <p class="text-center">Rapture built a headphone "Aura"; a multi-utility, multi-genre headphone that offers best-in-industry bass response and highly immersive listening experience. The USP lies in the patent-pending audio and bass reproduction system thereby overcoming limitation experience by conventional headphones.</p>
                           <a href="https://rapture.works/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!-- 27 -->
               <div class="col-xs-12 col-sm-6 col-md-4 gal agritech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/lmgk-agro.jpg" alt="lmgk-agro-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center"> MGK Solar Insect Trap</h3>
                           <p class="text-center">LMGK Solar Insect Trap a automated solar device, eco-friendly, non-polluting insect killer, reduces the usage of poisonous chemical pesticide in the agriculture field.</p>
                           <a href="https://www.mgksolartrap.com/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!--28-->
               <div class="col-xs-12 col-sm-6 col-md-4 gal edutech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/kotumb.jpg" alt="kotumb-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Kotumb </h3>
                           <p class="text-center">Kotumb is a professional networking Platform which enabling and empowering youth of bharath to manage their employability and employment.</p>
                           <a href="https://www.kotumb.in/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!--29-->
               <div class="col-xs-12 col-sm-6 col-md-4 gal smetech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/dailygate-informix.jpg" alt="dailygate-informix-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">Daily gate Informix LLP</h3>
                           <p class="text-center">Dailygate creates a comprehensive platform for MSMEs that enables structured organizational information flow Using Messenger, Calendar, Decisions, Notices, Tasks, Projects, Processes and Forms, Integrated to enable Delegation and Decision Making.</p>
                           <a href="https://dailygate.in/" target="_blank" class="btn btn-warning">Know More</a>
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!--30-->
               <div class="col-xs-12 col-sm-6 col-md-4 gal agritech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/jivanah.jpg" alt="jivanah-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">AgSenses </h3>
                           <p class="text-center">AgSenses in a business of farm mechanization services to small and marginal farmers. AgSenses vision is to create a platform based model with a network of connected farms and Ag robots that would deliver precise and quality services at farm level.</p>
                           <!-- <a href="vivek-pawar" target="_blank" class="btn btn-outline-secondary">Know More</a> -->
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!--31-->
               <div class="col-xs-12 col-sm-6 col-md-4 gal agritech">
                  <!-- <div class="pad"> -->
                  <div class="flip-card">
                     <div class="flip-card-inner">
                        <div class="flip-card-front">
                           <img src="img/portfolio/super-cane.jpg" alt="super-cane-logo" style="width:350px;height:317px;">
                        </div>
                        <div class="flip-card-back">
                           <h3 class="text-yellow text-center">SuperCane Harvester Machines Pvt Ltd</h3>
                           <p class="text-center">Supercane has developed indigenous technology for Sugarcane Harvesting to suit Indian Farming condition and cut the cane in whole stalk. They envision to Revolutionize the harvesting process in sugarcane industry and reduce ill-impact due to labor scarcity issues.</p>
                           <!-- <a href="vivek-pawar" target="_blank" class="btn btn-outline-secondary">Know More</a> -->
                        </div>
                     </div>
                  </div>
                  <!-- </div> -->
               </div>
               <!--32-->
            </div>
            <!-- end of row -->
            <!-- end row 2-->
         </div>
      </div>
      <!-- end of container -->
      <br><br><br><br><br>
      <!-- <div id="feedback">
         <a href="edge-form" target="_blank">Apply Now</a>
         </div> -->
      <script src="js/jssor.slider-27.1.0.min.js"></script>
      <script>
         jssor_1_slider_init = function() {
         
         	var jssor_1_SlideshowTransitions = [
         	{$Duration:800,$Opacity:2}
         	];
         
         	var jssor_1_options = {
         		$AutoPlay: 1,
         		$SlideshowOptions: {
         			$Class: $JssorSlideshowRunner$,
         			$Transitions: jssor_1_SlideshowTransitions,
         			$TransitionsOrder: 1
         		},
         		$ArrowNavigatorOptions: {
         			$Class: $JssorArrowNavigator$
         		},
         		$BulletNavigatorOptions: {
         			$Class: $JssorBulletNavigator$
         		}
         	};
         
         	var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
         
         	/*#region responsive code begin*/
         
         	var MAX_WIDTH = 1920;
         
         	function ScaleSlider() {
         		var containerElement = jssor_1_slider.$Elmt.parentNode;
         		var containerWidth = containerElement.clientWidth;
         
         		if (containerWidth) {
         
         			var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
         
         			jssor_1_slider.$ScaleWidth(expectedWidth);
         		}
         		else {
         			window.setTimeout(ScaleSlider, 30);
         		}
         	}
         
         	ScaleSlider();
         
         	$Jssor$.$AddEvent(window, "load", ScaleSlider);
         	$Jssor$.$AddEvent(window, "resize", ScaleSlider);
         	$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
         	/*#endregion responsive code end*/
         };
      </script>
      <script>jssor_1_slider_init();</script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
      <script>
         baguetteBox.run('.tz-gallery');
      </script>
      <script>
         filterSelection("all")
         function filterSelection(c) {
         	var x, i;
            x = document.getElementsByClassName("gal");
            // console.log({x,c});
         	if (c == "all") c = "";
         	for (i = 0; i < x.length; i++) {
         		w3RemoveClass(x[i], "show");
         		if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
         	}
         }
         
         function w3AddClass(element, name) {
         	var i, arr1, arr2;
         	arr1 = element.className.split(" ");
         	arr2 = name.split(" ");
         	for (i = 0; i < arr2.length; i++) {
         		if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
         	}
         }
         
         function w3RemoveClass(element, name) {
         	var i, arr1, arr2;
         	arr1 = element.className.split(" ");
         	arr2 = name.split(" ");
         	for (i = 0; i < arr2.length; i++) {
         		while (arr1.indexOf(arr2[i]) > -1) {
         			arr1.splice(arr1.indexOf(arr2[i]), 1);     
         		}
         	}
         	element.className = arr1.join(" ");
         }
         
         
         // Add active class to the current button (highlight it)
         var btnContainer = document.getElementById("myBtnContainer");
         var btns = btnContainer.getElementsByClassName("btn");
         
         for (var i = 0; i < btns.length; i++) {
         btns[i].addEventListener("click", function(){
         var current = document.getElementsByClassName("btnactive");
         console.log("Init all buttons")

         current[0].className = current[0].className.replace(" btnactive", "");
;
         console.log(current)
         
         this.className += " btnactive";
         });
         }
      </script>
      <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once 'essentials/js.php';
         ?>
   </body>
</html>