	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Apply now - GOOGLE I/O EXTENDED Form</title>
		<?php
		require_once 'essentials/meta.php';
		?>
		<meta name="linkage" content="https://www.deshpandestartups.org/googleio"/>
		<meta property="og:site_name" content="Deshpande Startups"/>
		<meta property="og:type" content="website">
		<meta property="og:url" content="https://www.deshpandestartups.org/googleio">
		<!-- <meta property="og:image" content="https://www.deshpandestartups.org/img/events/google-io.jpg"> -->
		<meta property="og:description" content="Makers Lab Presents, DevMeetup + Google I/O Extended, An initiative of Makers Lab, Deshpande Startups, for the first time hosting Google I/O 2018 Live Stream, a gathering of curious and like minded 100 Hackers"/>
		<meta name="author" content="Deshpande Startups"/>
		<meta name="description" content="Makers Lab Presents, DevMeetup + Google I/O Extended, An initiative of Makers Lab, Deshpande Startups, for the first time hosting Google I/O 2018 Live Stream, a gathering of curious and like minded 100 Hackers"/>
		<!-- <meta name="keywords" content=""/> -->
		<meta property="og:title" content="Register now for GOOGLE I/O EXTENDED">
		<link rel="canonical" href="https://www.deshpandestartups.org/googleio">
		<?php
			// $title = 'Deshpande Startups';
		require_once 'essentials/bundle.php';
		?>
	</head>
	<body>
		<?php
		require_once 'essentials/title_bar.php';
		require_once 'essentials/menus.php';
		?>
	<!-- <img class="carousel-inner img-fluid" src="img/events/alexathon-bg.jpg" width="1349" height="198" alt="Deshpande Startups, events, ALEXATHON">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="index">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item"><a href="alexathon">DEVMEETUP</a></li>
			<li class="breadcrumb-item active" aria-current="page">Apply for DEVMEETUP</li>
		</ol>
	</nav> -->
	<br>
	<div class="container text-center">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">APPLY FOR</span> GOOGLE I/O EXTENDED</h2>
		<div class="divider b-y text-yellow content-middle"></div>
		<!-- <h5 class="text-yellow">The registration has been closed</h5> -->
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-lg-2">
				<!-- <h5 class="text-center">Request base registration contact us: M: +91-951-331-5791  E: makerslab@dfmail.org</h5><br> -->
				<iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
				</iframe>
				<div class="p-3 w3-card">
					<form role="form" action="https://docs.google.com/forms/d/e/1FAIpQLScg3e7tqBMVK-CmTCqLPeuHefX_33dzgVRjNDQiWxmGW4X55A/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">
						<div class="row">
							<div class="col-md-12 pad">
								<!-- <div class="row"> -->
									<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
										<div class="row">
											<div class="form-group col-md-6">
												<label for="input1"><b>Name<span class="text-yellow">*</span></b></label>
												<input type="text" name="entry.2081427371" class="box2 form-control" maxlength="50" pattern="[A-Za-z\s]{1,50}" placeholder="Mention your name" title="Mention your name" required="required">
											</div>
											<div class="form-group col-md-6">
												<label for="input3"><b>Mobile Number<span class="text-yellow">*</span></b></label>
												<input type="phone" name="entry.886208732" class="box2 form-control" pattern="\d*" min="12" placeholder="Mention your mobile number" maxlength="10" minlength="10" title="Your mobile number" required="required">
											</div>
										</div>
									</div>
									<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
										<div class="row">
											<div class="form-group col-md-6">
												<label for="input3"><b>Alternative Mobile Number<span class="text-yellow">*</span></b></label>
												<input type="phone" name="entry.281787724" class="box2 form-control" pattern="\d*" min="12" placeholder="Mention your mobile number" maxlength="10" minlength="10" title="Your mobile number" required="required">
											</div>
											<div class="form-group col-md-6">
												<label for="input2"><b>Email-Id<span class="text-yellow">*</span></b></label>
												<input type="email" name="entry.2147439578" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
											</div>
										</div>
									</div>
									<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
										<div class="row">
											<div class="form-group col-md-6">
												<label for="input5"><b>City<span class="text-yellow">*</span></b></label>
												<input type="text" name="entry.1827052086" class="box2 form-control" placeholder="Your location" required="required">
											</div>
											<div class="form-group col-md-6">
												<label for="input5"><b>State<span class="text-yellow">*</span></b></label>
												<input type="text" name="entry.16909213" class="box2 form-control" placeholder="Your state" required="required">
											</div>
										</div>
									</div>

									<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
										<label for="input4"><b>College/Institute<span class="text-yellow">*</span></b></label>
										<input type="text" name="entry.510341228" class="box2 form-control" placeholder="Name of your College/Institute" required="required">
									</div>

									<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
										<label for="input7"><b>Are You?<span class="text-yellow">*</span></b></label><br>
									</div>
									<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
										<div class="row">
											<div class="form-group col-md-6">
												<label for="student"><input type="radio" name="entry.133609176" value="student" required="required"> Student</label>
											</div>
											<div class="form-group col-md-6">
												<label for="Graduates"><input type="radio" name="entry.133609176" value="graduate"> Graduate</label>
											</div>
											<!-- <div class="form-group col-md-4">
												<label for="Professionals"><input type="radio" name="entry.133609176" value="professional"> Professional</label>
											</div> -->
										</div>

										<div class="row txbx4" style="display: none">
											<div class="form-group col-md-12 m-0">
												<div class="row">
													<div class="form-group col-md-6">
														<label for="input5"><b>Academic Qualification<span class="text-yellow">*</span></b></label>
														<input type="text" name="entry.325346653" class="box2 form-control" placeholder="Academic qualification" title="Academic qualification">
													</div>
													<div class="form-group col-md-6">
														<label for="input45"><b>Stream/ Branch<span class="text-yellow">*</span></b></label>
														<input type="text" name="entry.2011770784" class="box2 form-control" title="Mention your Stream/ branch" placeholder="Your Stream/ branch">
														<!-- <label for="input5"><b>Semester<span class="text-yellow">*</span></b></label>
															<input type="text" name="entry.1150049576" class="box2 form-control" placeholder="Your semester" title="Your semester"> -->
														</div>
													</div>
												</div>
												<div class="form-group col-md-12 m-0">
													<label for="input7"><b>Year?<span class="text-yellow">*</span></b></label>
												</div>
												<div class="form-group col-md-12 m-0">
													<div class="row">
														<div class="form-group col-md-4">
															<label for="2ndyear"><input type="radio" name="entry.1150049576" value="2nd Year" checked="checked"> 2<sup>nd</sup></label>
														</div>
														<div class="form-group col-md-4">
															<label for="3rdyear"><input type="radio" name="entry.1150049576" value="3rd Year"> 3<sup>rd</sup></label>
														</div>
														<div class="form-group col-md-4">
															<label for="4thyear"><input type="radio" name="entry.1150049576" value="4th Year"> 4<sup>th</sup></label>
														</div>
													</div>
												<!-- <label for="input45"><b>Stream/ Branch<span class="text-yellow">*</span></b></label>
													<input type="text" name="entry.2011770784" class="box2 form-control" title="Mention your Stream/ branch" placeholder="Your Stream/ branch"> -->
												</div>
											</div>

											<div class="row txbx5" style="display: none">
												<div class="form-group col-md-12">
													<div class="row">
														<div class="form-group col-md-6">
															<label for="input5"><b>Academic Qualification<span class="text-yellow">*</span></b></label>
															<input type="text" name="entry.325346653" class="box2 form-control" placeholder="Academic qualification" title="Academic qualification">
														</div>
														<div class="form-group col-md-6">
															<label for="input45"><b>Stream/ Branch<span class="text-yellow">*</span></b></label>
															<input type="text" name="entry.2011770784" class="box2 form-control" title="Mention your Stream/ branch" placeholder="Your Stream/ branch">
														</div>
													</div>
												</div>
											</div>

										<!-- <div class="row txbx6" style="display: none">
											<div class="form-group col-md-12">
												<div class="row">
													<div class="form-group col-md-6">
														<label for="input5"><b>Academic Qualification<span class="text-yellow">*</span></b></label>
														<input type="text" name="entry.325346653" class="box2 form-control" placeholder="Your course" title="Your course">
													</div>
													<div class="form-group col-md-6">
														<label for="input45"><b>Stream/ Branch<span class="text-yellow">*</span></b></label>
														<input type="text" name="entry.2011770784" class="box2 form-control" title="Mention your Stream/ branch" placeholder="Your Stream/ branch">
													</div>
												</div>
												<label for="input45"><b>Professional In?<span class="text-yellow">*</span></b></label>
												<input type="text" name="entry.466835864" class="box2 form-control" title="You are professional in?" placeholder="Your Stream/ branch">
											</div>
										</div> -->
									</div>

									<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
										<label for="input7"><b>You want to participate in?<span class="text-yellow">*</span></b></label><br>
									</div>
									<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
										<div class="row">
											<div class="form-group col-md-6">
												<label for="appscript"><input type="radio" name="entry.405707819" value="Google Apps script workshop" checked="checked"> Google Apps script workshop</label>
											</div>
											<div class="form-group col-md-6">
												<label for="drones"><input type="radio" name="entry.405707819" value="Workshop on Electronics of Drones"> Workshop on Electronics of Drones</label>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-6">
												<label for="3dprinting"><input type="radio" name="entry.405707819" value="3D Printing Workshop"> 3D Printing Workshop</label>
											</div>
											<div class="form-group col-md-6">
												<label for="hackathon"><input type="radio" name="entry.405707819" value="Hackathon"> Hackathon</label>
											</div>
										</div>
									</div>

									<div class="row txbx7" style="display: none">
										<div class="form-group col-md-12">
											<label for="input45"><b>Team Name<span class="text-yellow">*</span></b></label>
											<input type="text" name="entry.345090684" class="box2 form-control" placeholder="Your team name">
										</div>

										<div class="form-group col-md-12 m-0">
											<label for="input50"><b>Number of Participants<span class="text-yellow">*</span></b></label>
										</div>
										<div class="col-md-12">
											<div class="row" >
												<div class="col-md-6 form-group">
													<label for="one"><input type="radio" name="entry.2055934401" id="one" value="1" checked="checked"> 1 - Rs.500/-</label>
												</div>
												<div class="col-md-6 form-group">
													<label for="two"><input type="radio" name="entry.2055934401" id="two" value="2"> 2 - Rs.1000/-</label>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 form-group">
													<label for="three"><input type="radio" name="entry.2055934401" id="three" value="3"> 3 - Rs.1500/-</label>
												</div>
												<div class="col-md-6 form-group">
													<label for="four"><input type="radio" name="entry.2055934401" id="four" value="4"> 4 - Rs.2000/-</label>
												</div>
											</div>
											<div class="row txbx" style="display: none">
												<div class="form-group col-md-12">
													<input type="text" class=" box2 form-control" name="entry.1747160493" title="mention name" pattern="[A-Za-z\s]{1,50}" placeholder="Name of participant 2" />
												</div>
												<div class="form-group col-md-12">
													<input type="text" class="box2 form-control" pattern="\d*" maxlength="10" minlength="10" name="entry.1935901279" placeholder="Mobile number of participant 2"/>
												</div>
												<div class="form-group col-md-12">	
													<input type="email" class="box2 form-control" name="entry.1007644022" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email-Id of participant 2"/>
												</div>
											</div>
											<div class="row txbx2" style="display: none">
												<div class="form-group col-md-12">
													<input type="text" class="box2 form-control" name="entry.1746488500" title="mention name" pattern="[A-Za-z\s]{1,50}" placeholder="Name of participant 3" />
												</div>
												<div class="form-group col-md-12">
													<input type="text" class="box2 form-control" name="entry.1786059712" pattern="\d*" maxlength="10" minlength="10" placeholder="Mobile number of participant 3"/>
												</div>
												<div class="form-group col-md-12">	
													<input type="email" class="box2 form-control" name="entry.1845872074" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email-Id of participant 3"/>
												</div>
											</div>
											<div class="row txbx3" style="display: none">
												<div class="form-group col-md-12">
													<input type="text" class="box2 form-control" name="entry.1058456775" title="mention name" pattern="[A-Za-z\s]{1,50}" placeholder="Name of participant 4" />
												</div>
												<div class="form-group col-md-12">
													<input type="text" class="box2 form-control" name="entry.2129745297" pattern="\d*" maxlength="10" minlength="10" placeholder="Mobile number of participant 4"/>
												</div>
												<div class="form-group col-md-12">	
													<input type="email" class="box2 form-control" name="entry.451559944" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email-Id of participant 4"/>
												</div>
											</div>
										</div>

										<div class="form-group col-md-12">
											<label for="input7"><b>Skill set<span class="text-yellow">*</span></b></label><br>
											<div class="row">
												<div class="col-md-6">
													<label for="Web Development"><input type="checkbox" name="entry.452133172" value="Web Development" checked="checked"> Web Development</label>
												</div>
												<div class="col-md-6">
													<label for="Web Application"><input type="checkbox" name="entry.452133172" value="Web Application"> Web Application</label>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<label for="Android Application"><input type="checkbox" name="entry.452133172" value="Android Application"> Android Application</label>
												</div>
												<div class="col-md-6">
													<label for="IoT"><input type="checkbox" name="entry.452133172" value="IoT"> IoT</label>
												</div>
											</div>
										</div>

										<div class="form-group col-md-12">
											<label for="input45"><b>What are you interested/passionate about?</b></label>
											<input type="text" name="entry.886328242" class="box2 form-control" placeholder="What are you interested/passionate about?">
										</div>

										<div class="form-group col-md-12">
											<label for="input45"><b>Please explain your project/ mini project you have worked on?</b></label>
											<textarea name="entry.630943394" class="box2 form-control" rows="4" maxlength="500" placeholder="Explain about your project/ mini project you have worked on"></textarea>
										</div>

										<div class="form-group col-md-12">
											<label for="input45"><b>What are your top 2 achievements?</b></label>
											<input type="text" name="entry.1916854500" class="box2 form-control" placeholder="Your top 2 achievements">
										</div>

									</div>

									<div class="form-group col-lg-12">
									<!-- <span class="text-yellow"><b>*</b></span>
										<div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div><br> -->
										<div class="form-group">
											<label for="agreement"><input type="checkbox" name="entry.1651875507" value="YES" required="required"> I agree to make payment for the Google I/O Extended Participation.<span class="text-yellow"><b>*</b></span></label>
										</div>
										<span class="text-yellow"><h6><b>*</b> Fields are mandatory</h6></span>
										<input type="submit" class="btn btn-warning" id="ss-submit" name="submit" value="Submit">
									</div>
									<!-- </div> -->
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<br>

		<script src='https://www.google.com/recaptcha/api.js'></script>
		<?php
		require_once 'essentials/footer.php';
		require_once 'essentials/copyright.php';
		require_once 'essentials/js.php';
		?>
		<!-- <script type="text/javascript">
			$(function() {
				$('[name="entry.133609176"]').on('click', function (e) {
					var val = $(this).val();
					if (val == "student") {
						$('.txbx4').show('fade');
						$('.txbx5').hide();
						$('.txbx6').hide();
					}
					else if (val == "graduate") {
						$('.txbx4').hide();
						$('.txbx5').show('fade');
						$('.txbx6').hide();
					} else {
						$('.txbx4').hide();
						$('.txbx5').hide();
						$('.txbx6').show('fade');
					};
				});
			});
		</script> -->
		<script type="text/javascript">
			$(function() {
				$('[name="entry.133609176"]').on('click', function (e) {
					var val = $(this).val();
					if (val == "student") {
						$('.txbx4').show('fade');
						$('.txbx5').hide();
					}
					else {
						$('.txbx4').hide();
						$('.txbx5').show('fade');
					};
				});
			});
		</script>
		<script type="text/javascript">
			$(function() {
				$('[name="entry.405707819"]').on('click', function (e) {
					var val = $(this).val();
					if (val == "Hackathon") {
						$('.txbx7').show('fade');
					}else {
						$('.txbx7').hide();
					};
				});
			});
		</script>
		<script type="text/javascript">
			$(function() {
				$('[name="entry.2055934401"]').on('click', function (e) {
					var val = $(this).val();
					if (val == 1) {
						$('.txbx').hide();
						$('.txbx2').hide();
						$('.txbx3').hide();
					}
					else if (val == 2) {
						$('.txbx').show('fade');
						$('.txbx2').hide();
						$('.txbx3').hide();
					} else if (val == 3) {
						$('.txbx').show('fade');
						$('.txbx2').show('fade');
						$('.txbx3').hide();
					} else {
						$('.txbx').show('fade');
						$('.txbx2').show('fade');
						$('.txbx3').show('fade');
					};
				});
			});
		</script>
		<script>
			window.onload = function() {
				var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
				recaptcha.required = true;
				recaptcha.oninvalid = function(e) {
					alert("Please complete the captcha");
				}
			}
		</script>
	</body>
	</html>