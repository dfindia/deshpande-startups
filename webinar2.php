<!DOCTYPE html>
<html lang="en">
<head>
	<title>Webinar - Healthcare NEXT: Opportunities in Bharat Healthcare</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/webinar2"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/webinar2">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/webinar2.jpg">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/webinar2-bg.jpg">
	<meta property="og:description" content="Healthcare has become one of India’s largest and fastest-growing sectors and it is expected to reach $372 billion by 2022. The Indian healthcare sector is growing at a brisk pace due to its strengthening coverage, services and increasing expenditure by public as well as private players."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Healthcare has become one of India’s largest and fastest-growing sectors and it is expected to reach $372 billion by 2022. The Indian healthcare sector is growing at a brisk pace due to its strengthening coverage, services and increasing expenditure by public as well as private players."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Webinar - Healthcare NEXT: Opportunities in Bharat Healthcare">
	<link rel="canonical" href="https://www.deshpandestartups.org/webinar2">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		small {
			font-size: 70%!important;
			font-weight: 340!important;
		}
		#feedback a {
			display: block;
			background: #E66425;
			height: 45px;
			padding-top: 5px;
			width: 191px;
			text-align: center;
			color: #fff;
			font-family: Arial, sans-serif;
			font-size: 20px;
			font-weight: bold;
			text-decoration: none;
		}
		@media only screen and (max-width: 360px){
			#feedback a {
				display: block;
				height: 40px;
				padding-top: 5px;
				width: 155px;
				text-align: center;
			}
		}

		#feedback {
			height: 0px;
			width: 0px;
			/*width: 85px;*/
			position: fixed;
			/*right: 0;*/
			top: 50%;
			z-index: 998;
			transform: rotate(-90deg);
			-webkit-transform: rotate(-90deg);
			-moz-transform: rotate(-90deg);
			-o-transform: rotate(-90deg);
		}

		#telegram a {
			display: block;
			background: #0088cc;
			height: 45px;
			width: 45px;
			text-align: center;
			color: #fff;
			font-size: 30px;
		}

		@media only screen and (max-width: 360px){
			#telegram a {
				display: block;
				height: 45px;
				/*padding-top: 5px;*/
				width: 40px;
				text-align: center;
				color: #fff;
			}
		}

		#telegram {
			height: 0px;
			width: 0px;
			/* width: 85px; */
			position: fixed;
			/* right: 0; */
			top: 52%;
			z-index: 998;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/webinar2-bg.jpg" width="1349" height="198" alt="Deshpande Startups, events, Webinar - Healthcare NEXT: Opportunities in Bharat Healthcare">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Webinar</li>
		</ol>
	</nav>

	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center text-uppercase wow slideInDown"><span class="text-muted">Healthcare NEXT</span><br> Opportunities in Bharat Healthcare</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<p class="text-yellow"><b>Event Description:</b></p>
				<p class="text-justify wow slideInRight">Healthcare has become one of India’s largest and fastest-growing sectors and it is expected to reach $372 billion by 2022. The Indian healthcare sector is growing at a brisk pace due to its strengthening coverage, services and increasing expenditure by public as well as private players.</p>

				<p class="text-justify wow slideInRight">The prevailing pandemic is adding a new dimension to the health care system which has already been seeing dramatically changes over the past decade because of the introduction of new technologies, and enhancement of health service infrastructure, resources for rural health remain relatively insufficient. With 75% of the healthcare infrastructure and the majority of healthcare professionals are concentrated in urban areas delivery of health care in rural India is an open opportunity. With new trend of telemedicine platforms, applications and portable devices one can expect to see affordability and accessibility barriers being broken.</p>
				
				<p class="text-justify wow slideInRight">COVID-19 has firmly established the need for active action and the establishment of a robust, collaborative, scalable, and agile healthcare infrastructure. Apart from this the ever growing need for health care also presents opportunities for startups and companies to make real accessible, affordable and quality health care.</p>

				<p class="text-justify wow slideInRight">Our second webinar will host veterans from Industry and Government bodies in Health and Biotech care. The webinar will have discussions and focus on the future of healthcare in the Bharat market and what it takes to build a health care enterprise in the Bharat ecosystem presenting some of the challenges which will be opportunities for entrepreneurs with interest in the sector.</p>
				<!-- <br> -->

				<!-- <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
					<div class="row text-center justify-content-md-center">
						<div class="col-md-4 col-sm-12 col-lg-4 col-xs-12">
							<div class="text-center pt-2">
								<a href="https://www.youtube.com/watch?v=AOB6gSLDirY&feature=youtu.be" class="btn btn-warning px-5" target="_blank">Register now</a>
							</div>
							</div>
							<div class="col-md-4 col-sm-12 col-lg-4 col-xs-12">
									<div class="text-center pt-2">
										<a href="https://youtu.be/bSkSneSozQw" class="btn btn-warning px-5" target="_blank">Click here for video</a>
									</div>
									</div>
								</div>
								</div>
								<br> -->

							</div>
						</div>
					</div>


					<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
						<h2 class=" text-yellow text-center text-uppercase wow slideInDown"><span class="text-muted">OUR </span> SPEAKERS</h2>
						<div class="divider b-y text-yellow content-middle"></div>
					</div>
					<div class="featured-bg-container">
						<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
							<div class="row text-center justify-content-md-center">
								<div class="col-md-2">
									<a href="https://www.linkedin.com/in/kmazumdarshaw/" target="_blank"><img src="img/speakers/kiran-mazumdar.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Kiran Mazumdar Shaw, Founder & Chairperson, BIOCON LIMITED, Speaker"></a>
									<h5 class="text-center text-yellow mb-0"><b>Kiran Mazumdar Shaw</b></h5>
									<small>Founder & Chairperson,<br> BIOCON LIMITED</small>
								</div>
								<div class="col-md-2">
									<a href="https://www.linkedin.com/in/kris-gopalakrishnan-10b49950/" target="_blank"><img src="img/speakers/kris-gopalakrishnan.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s" alt="Kris Gopalakrishnan, Chairman - Axilor Ventures, Co-founder - Infosys, Speaker"></a>
									<h5 class="text-center text-yellow mb-0"><b>Kris Gopalakrishnan</b></h5>
									<small>Chairman - Axilor Ventures,<br> Co-founder - Infosys</small>
								</div>
								<div class="col-md-2">
									<a href="https://www.linkedin.com/in/manish-diwan-a618761/" target="_blank"><img src="img/speakers/manish-diwan.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" alt="Dr. Manish Diwan, Head - Strategy Partnership & Entrepreneurship Development, BIRAC, Speaker"></a>
									<h5 class="text-center text-yellow mb-0"><b>Dr. Manish Diwan</b></h5>
									<small>Head - Strategy Partnership & Entrepreneurship Development, BIRAC</small>
								</div>
								<div class="col-md-2">
									<a href="https://www.linkedin.com/in/susethi/" target="_blank"><img src="img/speakers/sudhir-sethi.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.7s" alt="Sudhir Sethi, Founder & Chairman - Chiratae Ventures, Speaker"></a>
									<h5 class="text-yellow text-center mb-0"><b>Sudhir Sethi</b></h5>
									<small>Founder & Chairman,<br> Chiratae Ventures</small>
								</div>
								<div class="col-md-2">
									<a href="https://www.linkedin.com/in/cmpatil/" target="_blank"><img src="img/team/c-m-patil.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.9s" alt="C. M. Patil, CEO - Deshpande Startups, Speaker"></a>
									<h5 class="text-yellow text-center mb-0"><b>C. M. Patil</b></h5>
									<small>CEO,<br> Deshpande Startups</small>
								</div>
							</div>
						</div>

					</div>
					<br/>

					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<h4 class="text-center wow slideInRight"><b>Our second webinar on “Healthcare NEXT: Opportunities in Bharat Healthcare” is scheduled on 21<sup>st</sup> June 2020 from 5:00pm to 6:00pm</b></h4>
								<!-- <br> -->
								<div class="row text-center justify-content-md-center">
									<div class="text-center pt-2">
										<a href="https://www.youtube.com/watch?v=AOB6gSLDirY&feature=youtu.be" class="btn btn-warning px-5" target="_blank">Click here for video</a>
									</div>
								</div>
								<br>
								<p class="text-yellow"><b>The webinars will focus on the following aspects:</b></p>
								<p class="text-justify wow slideInRight">The country has also become one of the leading destinations for high-end diagnostic services with tremendous capital investment for advanced diagnostic facilities, thus catering to a greater proportion of the population. Besides, Indian medical service consumers have become more conscious of their healthcare upkeep.</p>

								<p class="text-justify wow slideInRight">While the healthcare system has changed dramatically over the past decade because of the introduction of new technologies, and enhancement of health service infrastructure, resources for rural health remain relatively insufficient. 75% of the healthcare infrastructure and the majority of healthcare professionals are concentrated in urban areas. Apart from these, there are other constraints that impede the rural healthcare sector including sub-optimal infrastructure & accessibility, insurance & affordability, and changing disease profile.</p>
								<p class="text-justify wow slideInRight">The Indian healthcare sector is much diversified and is full of opportunities in every segment which includes providers, payers and medical technology. With the increase in the competition, businesses are looking to explore the latest dynamics and trends which will have a positive impact on their business.</p>
								<ul class="wow slideInRight">
									<li>Opportunities & challenges in Bharat healthcare</li>
									<li>The focus of investments and infrastructure in Bharat healthcare</li>
									<li>How newer advancement in medical and ICT field will provide golden opportunities for health tech startups to venture into Bharat Market</li>
									<li>What does it take to build a healthcare enterprise in Bharat market?</li>
								</ul>
								<!-- <br> -->
						<!-- <div class="text-center pt-3">
							<h4 class="text-yellow pb-2">Apply now for the webinar</h4>
							<a href="#" class="btn btn-warning px-5" target="_blank">Register now</a>
						</div> -->
					</div>
				</div>
			</div>
			<br>

			
			<!-- <br> -->
			<br>
			<br>
			<!-- <div id="feedback">
				<a href="https://www.youtube.com/watch?v=AOB6gSLDirY&feature=youtu.be" target="_blank">Register now</a>
			</div> -->
			<div id="telegram">
				<a href="https://t.me/joinchat/AAAAAEWlzrvTlsP5RdP8qw" target="_blank" title="Join Chat Now"><i class="fa fa-telegram" aria-hidden="true"></i></a>
			</div>
			
			<?php
			require_once 'essentials/footer.php';
			require_once 'essentials/copyright.php';
			require_once 'essentials/js.php';
			?>
		</body>
		</html>