<!DOCTYPE html>
<html lang="en">
<head>
	<title>Kuppulakshmi Krishnamoorthy - product Evangelist, Zoho</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/kuppulakshmi"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/kuppulakshmi">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/edge/kuppulakshmi.png">
	<meta property="og:description" content="Kuppulakshmi, fondly known as Kuppu, heads the Zoho for Startups program globally. She has been part of Zoho's growth story for about a decade now, and bridging the gap between businesses and technology has been her core area of work."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Kuppulakshmi, fondly known as Kuppu, heads the Zoho for Startups program globally. She has been part of Zoho's growth story for about a decade now, and bridging the gap between businesses and technology has been her core area of work."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Makers Lab Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Kuppulakshmi Krishnamoorthy - product Evangelist, Zoho">
	<link rel="canonical" href="https://www.deshpandestartups.org/kuppulakshmi">
	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	/*p{text-align:justify;}*/
	.cal{
		font-family: calibri;
	}
	.modal-title {
		text-align: left;
		margin: 0;
		line-height: 1;
	}
	.follow-us{
		margin:20px 0 0;
	}
	.follow-us li{ 
		display:inline-block; 
		width:auto; 
		margin:0 5px;
	}
	.follow-us li .fa{ 
		font-size:25px; 
		/*color:#767676;*/
		color: #e7572a;
	}
	.follow-us li .fa:hover{ 
		color:#025a8e;
	}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>

	<div class="container cal pt-4">
		<div class="row wow fadeInLeft" data-wow-duration="1s" data-wow-offset="50">
			<div class="col-md-12 team-main">
				<div>
					<div class="modal-header">
						<!-- <a type="button" class="close" data-dismiss="modal" aria-label="Close" href="#"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> back </a> -->
						<h4 class="modal-title modal-title-cust text-yellow" id="myModalLabel">KUPPULAKSHMI KRISHNAMOORTHY<br><small class="text-muted">Product Evangelist - Zoho</small></h4>
						<ul class="follow-us clearfix">
							<li><a href="https://twitter.com/Kuppulakshmi" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="https://www.linkedin.com/in/kuppukrish/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="modal-body">
						<p class="text-justify">
							<img src="img/edge/kuppulakshmi.png" width="165" height="160" class="img img-fluid mr-2 rounded-circle" alt="Kuppulakshmi, product Evangelist-zoho, EDGE Mentor" align="left">
							Kuppulakshmi, fondly known as Kuppu, heads the Zoho for Startups program globally. She has been part of Zoho's growth story for about a decade now, and bridging the gap between businesses and technology has been her core area of work. She focuses on strategic alliances between Zoho and various startup bodies such as State Governments, Incubators, Accelarators, and other stakeholders; and also is mentors startups on pitching, design thinking, and on using emotional intelligence while hiring new talents and while setting up the vision and culture of the startup. Her previous experience from being a softskills and communication trainer and a voice and accent specialist, strengthen her expertise in story telling, and she is a great fan of "explain-it-like-I am-five" concept.<br><br>
							Kuppu believes that the best way to live life is to be useful to others, and she strives to find a balance between giving and taking. She is an ambivert, reads a lot of non-fiction, writes, sings, paints, dances, and is also a foodie who can cook. Is a hands-on mom to a super-active 7-year-old, and has plants for pets.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>