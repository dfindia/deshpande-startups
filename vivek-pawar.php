<!DOCTYPE html>
<html lang="en">
<head>
	<title>Vivek Pawar - Chief Executive Officer - Deshpande Foundation</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/vivek-pawar"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/vivek-pawar">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/mentors/vivek-pawar.jpg">
	<meta property="og:description" content="Vivek Pawar is currently the CEO of Deshpande Foundation. He had earlier served the foundation the last 8 years as a Managing Trustee. He is also the Chairman Emeritus & Founder of Sankalp Semiconductor."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Vivek Pawar is currently the CEO of Deshpande Foundation. He had earlier served the foundation the last 8 years as a Managing Trustee. He is also the Chairman Emeritus & Founder of Sankalp Semiconductor."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Makers Lab Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Vivek Pawar - Chief Executive Officer - Deshpande Foundation">
	<link rel="canonical" href="https://www.deshpandestartups.org/vivek-pawar">
	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	/*p{text-align:justify;}*/
	.cal{
		font-family: calibri;
	}
	.modal-title {
		text-align: left;
		margin: 0;
		line-height: 1;
	}
	.follow-us{
		margin:20px 0 0;
	}
	.follow-us li{ 
		display:inline-block; 
		width:auto; 
		margin:0 5px;
	}
	.follow-us li .fa{ 
		font-size:25px; 
		/*color:#767676;*/
		color: #e7572a;
	}
	.follow-us li .fa:hover{ 
		color:#025a8e;
	}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>

	<div class="container cal pt-4">
		<div class="row wow fadeInLeft" data-wow-duration="1s" data-wow-offset="50">
			<div class="col-md-12 team-main">
				<div>
					<div class="modal-header">
						<!-- <a type="button" class="close" data-dismiss="modal" aria-label="Close" href="#"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> back </a> -->
						<h4 class="modal-title modal-title-cust text-yellow" id="myModalLabel">VIVEK PAWAR<br><small class="text-muted">Chief Executive Officer - Deshpande Foundation</small></h4>
						<ul class="follow-us clearfix">
							<li><a href="https://twitter.com/VGPawar1203" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="https://www.linkedin.com/in/vivekgpawar" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-2">
							<img src="img/mentors/vivek-pawar.jpg" width="165" height="160" class="img img-fluid mr-2 rounded-circle" alt="Vivek Pawar - Chief Executive Officer - Deshpande Foundation, EDGE Mentor" align="left">
						</div>
						<div class="col-md-10">
						<p class="text-justify">
							Vivek Pawar is currently the CEO of Deshpande Foundation. He had earlier served the foundation the last 8 years as a Managing Trustee. He is also the Chairman Emeritus & Founder of Sankalp Semiconductor.<br><br>
							An alumni of IIT Kharagpur, Vivek served Texas Instruments India and USA, for 15+ years in various management positions. During his stint at TI, he is credited with development of innovative chip products with many rewards and recognitions, few of them are:</p>
							<ul>
								<li>World’s first integrated DSL that won the EDN Asia award in 1999</li>
								<li>World’s first single chip DSL that won EDN world Award in 2003</li>
								<li>Vivek has 3 patents and multiple international publications to his credit</li>
							</ul>
							<p class="text-justify">
							He founded Sankalp Semiconductors (an advanced technology services provider offering comprehensive solutions from concept to prototype, in the semiconductor space) in 2005 and over a span of 10 years he has established multiple development centres across the globe in Germany, Canada, California and Texas USA apart from 4 centres in India. Vivek was inducted into Deshpande Foundation in April 2018. He has been tasked with a mission to work on strategies to adopt a scalable model for the foundation, lower the cost per impact, improve quality and improve execution.
						</p>
					</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>