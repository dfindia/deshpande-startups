<!DOCTYPE html>
<html lang="en">
<head>
	<title>Android UI UX Designer - Nautilus</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/nautilus-android-designer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/nautilus-android-designer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/nautilus-big.png">
	<meta property="og:description" content="We are looking for Android UI UX Designer . Job Position: Android UI UX Designer , Experience: Minimum 1 year."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Android UI UX Designer . Job Position: Android UI UX Designer , Experience: Minimum 1 year."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Android UI UX Designer, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/nautilus-android-designer">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Android</span> UI UX Designer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Android UI UX Designer <br>
					<strong>Startup:</strong> Nautilus Hearing Solutions Pvt. Ltd.<br>
					<!-- <strong>Qualification:</strong> Bachelor’s degree or equivalent<br> -->
					<strong>Experience:</strong> Minimum 1 year of experience required<br>
					<!-- <strong>Career Level:</strong> Senior<br> -->
					<strong>Industry Type:</strong> IT - software/Software services<br>
					<strong>Functional Area:</strong> IT software - Application programming, Maintenance<br>
					<strong>Role Category:</strong> Programming & design<br>
					<strong>Job Location:</strong> Hubballi
				</p>
			</div>
			<div class="col-md-6">
				<a href="http://www.nautilushearing.com/" target="_blank" rel="nofollow"><img src="img/career/nautilus-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, Nautilus Hearing Solutions Pvt. Ltd."></a>
			</div>
		</div>
		<h3 class="text-yellow">Job Description:</h3>
		<p class="text-justify pt-1">We are looking for an exceptional UI/UX designer who can contribute to building an equally exceptional design oriented software product for our company.</p>
		<p class="text-justify">The person should have a good understanding and hold in designing enterprise software applications. The candidate should have designed highly dynamic and interactive android applications for mobile devices. The candidate will be working very closely with Product management and Product development teams.</p>
		<div class="row pt-2">
			<div class="col-md-12">
				<h3 class="text-yellow">Job Responsibilities:</h3>
				<ul class="text-justify">
					<li>Gather and evaluate user requirements in collaboration with product manager and business analyst</li>
					<li>Design user experience flows and interfaces supporting the same</li>
					<li>Illustrate design ideas using Wireframes, Storyboards, Process flows and Sitemaps</li>
					<li>Design graphic user interface elements for mobile screen</li>
					<li>Build Navigation options, Filter lists and Search fields</li>
					<li>Create original graphic designs (e.g. Images, Charts, Tables, Menus and Widgets)</li>
					<li>Identify and troubleshoot UX problems (e.g. Responsiveness)</li>
					<li>Conduct layout adjustments based on user feedback</li>
				</ul>
			</div>
		</div>
		
	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to<br> E:<a href="mailto: uday&#064;nautilushearing&#046;com"> uday&#064;nautilushearing&#046;com</a></b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>