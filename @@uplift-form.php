<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Apply now for Sandbox Uplift</title>
      <?php
         require_once 'essentials/meta.php';
         ?>
      <meta name="linkage" content="https://www.deshpandestartups.org/uplift-form"/>
      <meta property="og:site_name" content="Deshpande Startups"/>
      <meta property="og:type" content="website">
      <meta property="og:url" content="https://www.deshpandestartups.org/uplift-form">
      <!-- <meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/bg-new.jpg"> -->
      <meta property="og:image" content="https://www.deshpandestartups.org/img/events/uplift.jpg">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/events/uplift-bg.jpg">
      <meta property="og:description" content="We are hosting a platform for all the mission driven entrepreneurs across India to pitch their business idea to access support from Deshpande Startups in scaling their business ventures."/>
      <meta name="author" content="Deshpande Startups"/>
      <meta name="description" content="We are hosting a platform for all the mission driven entrepreneurs across India to pitch their business idea to access support from Deshpande Startups in scaling their business ventures."/>
      <!-- <meta name="keywords" content=""/> -->
      <meta property="og:title" content="Apply for Sandbox Uplift">
      <!-- <link rel="canonical" href="https://www.deshpandestartups.org/edge-form"> -->
      <?php
         // $title = 'Deshpande Startups';
         require_once 'essentials/bundle.php';
         ?>
   </head>
   <body>
      <?php
         require_once 'essentials/title_bar.php';
         require_once 'essentials/menus.php';
         ?>
      <br>
      <div class="container text-center">
         <h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted"> UPLIFT</span> FORM</h2>
         <div class="divider b-y text-yellow content-middle"></div>
         <!-- <h4 class="pt-5">The registrations has been closed.<br> To be updated with our upcoming events kindly subscribe to us.</h4> -->
      </div>
      <br>
      <div class="container">
         <div class="row">
            <div class="col-md-8 offset-lg-2">
               <!-- <h5 class="text-center">Request base registration contact us: M: +91-951-331-5791  E: makerslab@dfmail.org</h5><br> -->
               <iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
               </iframe>
               <form role="form" action="https://docs.google.com/forms/u/1/d/e/1FAIpQLSepA3l3RIXt3qFnmQSXKyPss1KKNLXFrKpVrbwXfU8Fb4JMdg/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">
                  <div class="row w3-card p-3">
                     <div class="col-md-12 pad">
                        <div class="row">
                           <div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
                              <label for="input1"><b>Entrepreneur Name<span class="text-yellow">*</span></b></label>
                              <input type="text" name="entry.228668796" class="box2 form-control" maxlength="50" pattern="[A-Za-z\s]{1,50}" placeholder="Mention your name" title="Mention your name" required="required">
                           </div>
                           <div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
                              <label for="input2"><b>Mobile Number<span class="text-yellow">*</span></b></label>
                              <input type="phone" name="entry.1452688954" class="box2 form-control" pattern="\d*" min="12" placeholder="Mention your mobile number" maxlength="10" minlength="10" title="Your mobile number" required="required">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="row">
                           <div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
                              <label for="input3"><b>Email-Id<span class="text-yellow">*</span></b></label>
                              <input type="email" name="entry.1358103933" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
                           </div>
                           <div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                              <label for="input4"><b>City/Place<span class="text-yellow">*</span></b></label>
                              <input type="text" name="entry.1986024006" class="box2 form-control" placeholder="Your location" required="required">
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                        <label for="input6"><b>Are You?<span class="text-yellow">*</span></b></label><br>
                     </div>
                     <div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                        <div class="row">
                           <div class="form-group col-md-6">
                              <label for="Startup Founder"><input type="radio" name="entry.1462877651" value="Startup Founder" required="required" checked="checked"> Startup Founder</label>
                           </div>
                           <div class="form-group col-md-6">
                              <label for="Aspiring Entrepreneur"><input type="radio" name="entry.1462877651" value="Aspiring Entrepreneur"> Aspiring Entrepreneur</label>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                        <label for="input5"><b>Startup Name<span class="text-yellow">*</span></b></label>
                        <input type="text" name="entry.939423999" class="box2 form-control" placeholder="Startup Name" required="required">
                     </div>
                     <div class="form-group col-md-12 m-0">
                        <div class="row">
                           <div class="form-group col-md-12 wow fadeInLeft">
                              <label for="input8"><b>Sector<span class="text-yellow">*</span></b></label>
                              <select class="form-control" name="entry.1181620906">
                                 <option value="Agri-tech" selected>Agri-tech</option>
                                 <option value="Medtech">Medtech</option>
                                 <option value="Edu-tech">Edu-tech</option>
                                 <option value="Rural Innovations">Rural Innovations</option>
                                 <option value="Rural Retail/Consumption">Rural Retail/Consumption</option>
                                 <option value="Rural Fintech">Rural Fintech</option>
                                 <option value="Others">Others</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <!-- <div class="form-group col-md-12 m-0"> -->
                     <div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                        <label for="input10"><b>Stage<span class="text-yellow">*</span></b></label>
                     </div>
                     <div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                        <div class="row">
                           <div class="col-md-12 form-group">
                              <label for="Concept stage/Idea stage "><input type="radio" name="entry.1066385" value="Concept stage/Idea stage" required="required" checked="checked"> Concept stage/Idea stage</label>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12 form-group">
                              <label for="Product ready: We have commercially pilot-tested product"><input type="radio" name="entry.1066385" value="Product ready: We have commercially pilot-tested product"> Product ready: We have commercially pilot-tested product</label>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12 form-group">
                              <label for="Revenue positive: Early transaction"><input type="radio" name="entry.1066385" value="Revenue positive: Early transaction"> Revenue positive: Early transaction</label>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12 form-group">
                              <label for="Revenue positive: Growth stage"><input type="radio" name="entry.1066385" value="Revenue positive: Growth stage"> Revenue positive: Growth stage</label>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                        <label for="input5"><b>LinkedIn:</b></label>
                        <input type="url" name="entry.659565809" class="box2 form-control" placeholder="LinkedIn">
                     </div>
                     <!-- <div class="row">
                        <div class="form-group col-md-12 wow fadeInLeft">
                           <label for="input8"><b>Stage<span class="text-yellow">*</span></b></label>
                           <select class="form-control" name="entry.1066385">
                              <option value="Ideathon stage" selected>Ideathon stage</option>
                              <option value="MVP stage">MVP stage</option>
                              <option value="Early revenue">Early revenue</option>
                              <option value="Revenue positive">Revenue positive</option>
                             
                           </select>
                        </div>
                        </div> -->
                     <!-- </div>                       -->
                     <div class="form-group col-lg-12">
                        <!-- <span class="text-yellow"><b>*</b></span>
                           <div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div><br> -->
                        <!-- <div class="form-group">
                           <label for="agreement"><input type="checkbox" name="entry.782003103" value="I agree to make payment for the IDEATHON Participation" required="required"> I agree to make payment of Rs.500/- for the IDEATHON Participation.<span class="text-yellow"><b>*</b></span></label>
                           </div> -->
                        <span class="text-yellow">
                           <h6><b>*</b> Fields are mandatory</h6>
                        </span>
                        <input type="submit" class="btn custom-btn2 btn-warning" id="ss-submit" name="submit" value="Submit">
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <br>
      <!-- <div class="container text-center">
         <h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted"> SANDBOX</span> UPLIFT</h2>
         <div class="divider b-y text-yellow content-middle"></div>
         <h4 class="pt-5">The registrations has been closed.<br> To be updated with our upcoming events kindly subscribe to us.</h4>
         </div> -->
      <br>
      <br>
      <br>
      <br>
      <script src='https://www.google.com/recaptcha/api.js'></script>
      <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once 'essentials/js.php';
         ?>
   </body>
</html>