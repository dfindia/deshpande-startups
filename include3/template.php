<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="x-apple-disable-message-reformatting">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Deshpande Startups</title>

	<style>
		h1,h2,h3,h4,h5,h6 {line-height: 100%;}
		.spacer,.divider {mso-line-height-rule:exactly;}
		td,th,div,p,a {font-size: 13px; line-height: 22px;}
		td,th,div,p,a,h1,h2,h3,h4,h5,h6 {font-family:"Segoe UI",Arial,sans-serif;}

		@media only screen {
			.col,td,th,div,p,a,h1,h2,h3,h4,h5,h6 {font-family: "Open Sans",-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI","Roboto","Helvetica Neue",Arial,sans-serif!important;}
			.webfont, .webfont a {font-family: "Lato",-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI","Roboto","Helvetica Neue",Arial,sans-serif!important;}
		}

		a {text-decoration: none;}
		img {border: 0; line-height: 100%; max-width: 100%; vertical-align: middle;}
		.col {font-size: 13px; line-height: 22px; vertical-align: top;}

		.hover-scale {transition: all 0.3s ease-in-out 0s;}
		.hover-scale:hover {transform: scale(1.2);}
		.video {display: block; height: auto; object-fit: cover;}
		.star:hover a, .star:hover ~ .star a {color: #FFCF0F!important;}

		@media only screen and (max-width: 600px) {
			.video {width: 100%;}
			img {height: auto!important;}
			u ~ div .wrapper {min-width: 100vw;}
			.container {width: 100%!important; -webkit-text-size-adjust: 100%;}
		}

		@media only screen and (max-width: 480px) {
			.col {
				box-sizing: border-box;
				display: inline-block!important;
				line-height: 20px;
				width: 100%!important;
			}
			.col-sm-1 {max-width: 25%;}
			.col-sm-2 {max-width: 50%;}
			.col-sm-3 {max-width: 75%;}
			.col-sm-third {max-width: 33.33333%;}
			.col-sm-auto {width: auto!important;}
			.col-sm-push-1 {margin-left: 25%;}
			.col-sm-push-2 {margin-left: 50%;}
			.col-sm-push-3 {margin-left: 75%;}
			.col-sm-push-third {margin-left: 33.33333%;}

			.full-width-sm {display: table!important; width: 100%!important;}
			.stack-sm-first {display: table-header-group!important;}
			.stack-sm-last {display: table-footer-group!important;}
			.stack-sm-top {display: table-caption!important; max-width: 100%; padding-left: 0!important;}

			.toggle-content {
				max-height: 0;
				overflow: auto;
				transition: max-height .4s linear;
				-webkit-transition: max-height .4s linear;
			}
			.toggle-trigger:hover + .toggle-content,
			.toggle-content:hover {max-height: 999px!important;}

			.show-sm {
				display: inherit!important;
				font-size: inherit!important;
				line-height: inherit!important;
				max-height: none!important;
			}
			.hide-sm {display: none!important;}

			.align-sm-center {
				display: table!important;
				float: none;
				margin-left: auto!important;
				margin-right: auto!important;
			}
			.align-sm-left {float: left;}
			.align-sm-right {float: right;}

			.text-sm-center {text-align: center!important;}
			.text-sm-left {text-align: left!important;}
			.text-sm-right {text-align: right!important;}

			.nav-sm-vertical .nav-item {display: block!important;}
			.nav-sm-vertical .nav-item a {display: inline-block; padding: 5px 0!important;}

			.h1 {font-size: 32px!important;}
			.h2 {font-size: 24px!important;}
			.h3 {font-size: 16px!important;}

			.borderless-sm {border: none!important;}
			.height-sm-auto {height: auto!important;}
			.line-height-sm-0 {line-height: 0!important;}
			.overlay-sm-bg {background: #232323; background: rgba(0,0,0,0.4);}

			u ~ div .wrapper .toggle-trigger {display: none!important;}
			u ~ div .wrapper .toggle-content {max-height: none;}
			u ~ div .wrapper .nav-item {display: inline-block!important; padding: 0 10px!important;}
			u ~ div .wrapper .nav-sm-vertical .nav-item {display: block!important;}

			.p-sm-0 {padding: 0!important;}
			.p-sm-8 {padding: 8px!important;}
			.p-sm-16 {padding: 16px!important;}
			.p-sm-24 {padding: 24px!important;}
			.pt-sm-0 {padding-top: 0!important;}
			.pt-sm-8 {padding-top: 8px!important;}
			.pt-sm-16 {padding-top: 16px!important;}
			.pt-sm-24 {padding-top: 24px!important;}
			.pr-sm-0 {padding-right: 0!important;}
			.pr-sm-8 {padding-right: 8px!important;}
			.pr-sm-16 {padding-right: 16px!important;}
			.pr-sm-24 {padding-right: 24px!important;}
			.pb-sm-0 {padding-bottom: 0!important;}
			.pb-sm-8 {padding-bottom: 8px!important;}
			.pb-sm-16 {padding-bottom: 16px!important;}
			.pb-sm-24 {padding-bottom: 24px!important;}
			.pl-sm-0 {padding-left: 0!important;}
			.pl-sm-8 {padding-left: 8px!important;}
			.pl-sm-16 {padding-left: 16px!important;}
			.pl-sm-24 {padding-left: 24px!important;}
			.px-sm-0 {padding-right: 0!important; padding-left: 0!important;}
			.px-sm-8 {padding-right: 8px!important; padding-left: 8px!important;}
			.px-sm-16 {padding-right: 16px!important; padding-left: 16px!important;}
			.px-sm-24 {padding-right: 24px!important; padding-left: 24px!important;}
			.py-sm-0 {padding-top: 0!important; padding-bottom: 0!important;}
			.py-sm-8 {padding-top: 8px!important; padding-bottom: 8px!important;}
			.py-sm-16 {padding-top: 16px!important; padding-bottom: 16px!important;}
			.py-sm-24 {padding-top: 24px!important; padding-bottom: 24px!important;}
		}
	</style>
</head>
<body marginwidth="0" marginheight="0" style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" offset="0" topmargin="0" leftmargin="0">
	<table class="wrapper" role="presentation" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="spacer height-sm-auto py-sm-8" height="24" bgcolor="#EEEEEE"></td>
		</tr>
	</table>
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="px-sm-16" bgcolor="#EEEEEE" align="center">
				<table class="container" width="600" cellspacing="0" cellpadding="0">
					<tr>
						<td class="px-sm-8" style="padding: 0 24px;" bgcolor="#FFFFFF" align="left">
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
							<table width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="col pb-sm-16" style="padding: 0 8px;" width="122">
										<p class="webfont" style="color: #000000; font-size: 18px; font-weight: 700; margin: 0;">
											Hey there,
										</p>
									</td>
									<td class="col" style="padding: 0 8px;" width="398">
										<p style="color: #666666; margin: 0;">
											You have an email from <a href="http://www.deshpandestartups.org">deshpandestartups.org</a> mentors online form.<br><br>Don't panic. This is an automatically generated email!. This email was generated from the link below<br>
											<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
											<br>
											Regarding the mentors online form, the details below and photo attached will help you through!
										</p>
									</td>
								</tr>
							</table>
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table class="wrapper" role="presentation" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="spacer height-sm-auto py-sm-8" height="24" bgcolor="#EEEEEE"></td>
		</tr>
	</table>
	<table class="wrapper" role="presentation" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="spacer height-sm-auto py-sm-8" height="24" bgcolor="#EEEEEE"></td>
		</tr>
	</table>
	<table class="wrapper" role="presentation" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="px-sm-16" bgcolor="#EEEEEE" align="center">
				<table class="container" role="presentation" width="600" cellspacing="0" cellpadding="0">
					<tr>
						<td class="spacer height-sm-auto py-sm-8" height="24" bgcolor="#FFFFFF"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="px-sm-16" bgcolor="#EEEEEE" align="center">
				<table class="container" width="600" cellspacing="0" cellpadding="0">
					<tr>
						<td class="px-sm-8" style="padding: 0 24px;" bgcolor="#FFFFFF" align="left">
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
							<table width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="col pb-sm-16" style="padding: 0 8px; vertical-align: middle;" width="20%">
										<p class="webfont" style="color: #888888; font-weight: 700; margin: 0;text-align: center;">Name</p>
									</td>
									<td class="col pb-sm-8 text-lg-center" style="padding: 0 8px;" width="80%">
										<h4 class="webfont" style="color: #232323; font-size: 16px;margin: 0;text-align: center;">
											<?php echo $mentorname; ?>
										</h4>
									</td>
								</tr>
							</table>
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="px-sm-16" bgcolor="#EEEEEE" align="center">
				<table class="container" width="600" cellspacing="0" cellpadding="0">
					<tr>
						<td class="px-sm-8" style="padding: 0 24px;" bgcolor="#FFFFFF" align="left">
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
							<table width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="col pb-sm-16" style="padding: 0 8px; vertical-align: middle;" width="20%">
										<p class="webfont" style="color: #888888; font-weight: 700; margin: 0;text-align: center;">Email</p>
									</td>
									<td class="col pb-sm-8 text-lg-center" style="padding: 0 8px;" width="80%">
										<h4 class="webfont" style="color: #232323; font-size: 16px;margin: 0;text-align: center;">
											<?php echo $emailaddress; ?>
										</h4>
									</td>
								</tr>
							</table>
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="px-sm-16" bgcolor="#EEEEEE" align="center">
				<table class="container" width="600" cellspacing="0" cellpadding="0">
					<tr>
						<td class="px-sm-8" style="padding: 0 24px;" bgcolor="#FFFFFF" align="left">
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
							<table width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="col pb-sm-16" style="padding: 0 8px; vertical-align: middle;" width="20%">
										<p class="webfont" style="color: #888888; font-weight: 700; margin: 0;text-align: center;">Phone Number</p>
									</td>
									<td class="col pb-sm-8 text-lg-center" style="padding: 0 8px;" width="80%">
										<h4 class="webfont" style="color: #232323; font-size: 16px;margin: 0;text-align: center;">
											<?php echo $mobilenumber; ?>
										</h4>
									</td>
								</tr>
							</table>
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- <table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="px-sm-16" bgcolor="#EEEEEE" align="center">
				<table class="container" width="600" cellspacing="0" cellpadding="0">
					<tr>
						<td class="px-sm-8" style="padding: 0 24px;" bgcolor="#FFFFFF" align="left">
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
							<table width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="col pb-sm-16" style="padding: 0 8px; vertical-align: middle;" width="20%">
										<p class="webfont" style="color: #888888; font-weight: 700; margin: 0;text-align: center;">Is Interested In</p>
									</td>
									<td class="col pb-sm-8 text-lg-center" style="padding: 0 8px;" width="80%">
										<h4 class="webfont" style="color: #232323; font-size: 16px;margin: 0;text-align: center;">
											<?php //echo $select; ?>
										</h4>
									</td>
								</tr>
							</table>
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table> -->
	<?php if(isset($mentorbio)){ ?>
		<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="px-sm-16" bgcolor="#EEEEEE" align="center">
					<table class="container" width="600" cellspacing="0" cellpadding="0">
						<tr>
							<td class="px-sm-8" style="padding: 0 24px;" bgcolor="#FFFFFF" align="left">
								<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td class="col pb-sm-16" style="padding: 0 8px; vertical-align: middle;" width="20%">
											<p class="webfont" style="color: #888888; font-weight: 700; margin: 0;text-align: center;">Here's his/her Bio</p>
										</td>
										<td class="col pb-sm-8 text-lg-center" style="padding: 0 8px;" width="80%">
											<h4 class="webfont" style="color: #232323; font-size: 16px;margin: 0;text-align: center;">
												<?php echo $mentorbio; ?>
											</h4>
										</td>
									</tr>
								</table>
								<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<?php } ?>
	<table class="wrapper" role="presentation" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="px-sm-16" bgcolor="#EEEEEE" align="center">
				<table class="container" role="presentation" width="600" cellspacing="0" cellpadding="0">
					<tr>
						<td class="spacer height-sm-auto py-sm-8" height="24" bgcolor="#FFFFFF">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="px-sm-16" bgcolor="#EEEEEE" align="center">
				<table class="container" width="600" cellspacing="0" cellpadding="0">
					<tr>
						<td class="px-sm-8" style="padding: 0 24px;" bgcolor="#FFFFFF" align="left">
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
							<table width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="col pb-sm-16" style="padding: 0 8px;" width="240">
										<div class="align-sm-center">
											<a href="https://www.deshpandestartups.org/" target="_blank">
												<img src="https://www.deshpandestartups.org/img/deshpande-startup-logo.png" alt="Footer Logo" class="" width="150">
											</a>
										</div>
									</td>
									<td class="col text-sm-center" style="padding: 0 8px;" width="310" align="right">
										<p style="color: #888888; margin: 0;">
											© 2019 <a href="http://www.deshpandestartups.org">deshpandestartups.org</a>. All Rights Reserved.
										</p>
										<p class="links-inherit-color" style="color: #888888; margin: 0;">
											Deshpande Startups, Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi - 580 030
										</p>
										<div class="spacer" style="line-height: 16px;">
											‌
										</div>
									</td>
								</tr>
							</table>
							<div class="spacer line-height-sm-0 py-sm-8" style="line-height: 24px;">&nbsp;</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>