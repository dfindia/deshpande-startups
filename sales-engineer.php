<!DOCTYPE html>
<html lang="en">
<head>
	<title>Technical Sales Engineer | Electronics & Communication</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/sales-engineer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/sales-engineer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/team-image.png">
	<meta property="og:description" content="Job Position: Technical Sales Engineer, Experience:  2 - 3 years, Education: BE (Electronics & Communication) + MBA"/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Job Position: Technical Sales Engineer, Experience:  2 - 3 years, Education: BE (Electronics & Communication) + MBA"/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Technical Sales Engineer, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Technical Sales Engineer, Current openings at Deshpande startups">
	<link rel="canonical" href="https://www.deshpandestartups.org/sales-engineer">
	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	/*p{text-align:justify;}*/
	.cal{
		font-family: calibri;
	}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Technical Sales</span> Engineer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="pull-right"><a href="career-opportunities" class="btn btn-warning btn-md" target="_blank">Apply Now</a></div>
		<p class="text-justify"><strong>Job Position:</strong> Technical Sales Engineer<br>
			<strong>Organization:</strong> Deshpande Startups<br>
			<strong>Department Name:</strong> ESDM Cluster<br>
			<strong>Experience:</strong> 2 - 3 years of experience<br>
			<strong>Education:</strong> BE (Electronics & Communication) + MBA<br>
			<strong>Job Location:</strong> Bengaluru<br>
			<strong>Tentative Date of Joining:</strong> Immediate
		</p>
		<h3  class="text-yellow">Job Description:</h3>
		<ul>
			<li>Generating new customer base for our scope of operation</li>
			<li>Customer follow-up for new orders</li>
			<li>Work closely with product management & engineering in understanding product features set, develop technical product library, includes best practices and design guidelines</li>
			<li>Represent the customer’s technical needs during new service developments and create competitive analysis from technical perspective</li>
			<li>Maintain cordial relations with existing customer and generate new orders from them</li>
			<li>Providing pre-sales and post-sales support</li>
			<li>Perform such other duties as required and appropriate to the objectives of this position and the needs of the organization</li>
		</ul>
		<br>
		<h3  class="text-yellow">Knowledge, Skills & Abilities:</h3>
		<ul>
			<li>Sound technical knowledge of the Electronics industry</li>
			<li>Experience of sales/business development in the EMS sector</li>
			<li>Ability to handle pressure and meet deadlines</li>
			<li>Good presentation skills</li>
			<li>Excellent communication skills</li>
			<li>Good negotiation skills</li>
			<li>Able to work as part of wide and varied team</li>
		</ul>
	</div>
	<br>
	<!-- <div class="container cal">
		<p class="text-center">For any further enquiries, please email E: <a href="mailto:hr.sandbox@dfmail.org">hr.sandbox@dfmail.org</a> or call us M: +91-889-297-0077</p>
	</div> -->
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>