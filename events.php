<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Events - Deshpande Startups</title>
      <?php
         require_once 'essentials/meta.php';
         ?>
      <meta name="linkage" content="https://www.deshpandestartups.org/events"/>
      <meta property="og:site_name" content="Deshpande Startups"/>
      <meta property="og:type" content="website">
      <meta property="og:url" content="https://www.deshpandestartups.org/events">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/events/makeathon-event.png">
      <meta property="og:description" content="Deahpande Startups events"/>
      <meta name="author" content="Deshpande Startups"/>
      <meta name="description" content="Deshpande Startups events"/>
      <!-- <meta name="keywords" content=""/> -->
      <meta property="og:title" content="Deshpande Startups Events">
      <!-- <link rel="canonical" href="https://www.deshpandestartups.org/events"> -->
      <?php
         require_once 'essentials/bundle.php';
         ?>
   </head>
   <body>
      <?php
         require_once 'essentials/title_bar.php';
         require_once 'essentials/menus.php';
         ?>
      <!-- <img class="carousel-inner img-fluid" src="img/events/events-bg.png" width="1349" height="198" alt="Deshpande Startups, events, ESDM, Electronic Symposium">
         <nav aria-label="breadcrumb">
         	<ol class="breadcrumb justify-content-end">
         		<li class="breadcrumb-item"><a href="./">Home</a></li>
         		<li class="breadcrumb-item active" aria-current="page">Events</li>
         	</ol>
         </nav> -->
      <div class="container pt-3">
         <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
            <h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">UPCOMING</span> EVENTS</h2>
            <div class="divider b-y text-yellow content-middle"></div>
            <!-- <br> -->
            <!-- <h3 class="text-center" pt-5> Coming Soon</h3> -->
         </div>
         <div class="row">
            <div class="col-md-12 px-5">
               <div class="row justify-content-md-center">
                  <!-- <h3> Coming Soon</h3><br><br><br><br><br> -->
                  <!-- <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="ideathon" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/events-ideathon.jpg" width="474" height="237" alt="Deshpande startups, Ideathon, Yuva Entrepreneurship"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Ideathon</h5>
                              <p><b>Last Date to Apply: <br></b>Dec 07<sup>th</sup> 2020</p> -->
                  <!-- <p><b>Last date to register : </b><br>March 18<sup>th</sup> 2020</p> -->
                  <!-- <p><b>Venue: </b> Deshpande Startups,<br> Next to Airport Opp to Gokul Village Gokul Road Hubballi, Karnataka.
                     </p>
                     <p class="text-truncate mb-2"><b>Contact details:</b><br>
                        M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
                        E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a>
                     </p> -->
                  <!-- <br> -->
                  <!-- </div>
                     <div class="card-footer">
                        <a href="ideathon" class="btn btn-primary p-1" target="_blank">Know More</a>
                        <a href="ideathon-form" class="btn btn-primary p-1" target="_blank">Apply Now</a>
                     </div>
                     </div>
                     </div>
                     </div> -->
                  <!-- <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                        <a href="uplift-form" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/uplift1.jpg" width="474" height="237" alt="Deshpande startups event, Sandbox Uplift"></a>
                           <div class="card-body">
                           <h5 class="card-title text-yellow text-center">Sandbox Uplift</h5>
                               <p><b>Date : </b>Sep 26<sup>th</sup> & 27<sup>th</sup> 2020</p> -->
                  <!-- <p><b>Last date to apply: </b><br>Dec 31<sup>st</sup> 2020</p> -->
                  <!-- <p><b>Time :</b> 05:00pm - 06:00pm</p>  -->
                  <!-- <p><b>Venue:</b> Deshpande Startups, <br> Next to Airport Opp to Gokul Village Gokul Road Hubballi, Karnataka.</p> -->
                  <!-- <p class=""><b>Contact details:</b><br> -->
                  <!-- M:<a href="tel:+91-951-331-5791"> +91-968-665-4749</a><br> -->
                  <!-- E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a> -->
                  <!-- </p>   -->
                  <!-- <br> -->
                  <!-- </div>
                     <div class="card-footer">
                     <a href="uplift" class="btn btn-primary px-2" target="_blank">Know More</a>
                        <a href="uplift-form" class="btn btn-primary p-1" target="_blank">Register Now</a>
                     </div>
                     </div>
                     </div>
                     </div> -->
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="makeathon" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/makeathon-event.png" width="474" height="237" alt="Makers Makeathon, Yuva Entrepreneurship, Deshpande Startups"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Makers Makeathon 2020</h5>
                              <p><b>Date : </b>Dec 28<sup>th</sup>, 29<sup>th</sup> & 30<sup>th</sup> 2020</p>
                              <p><b>Last date to apply : </b><br>Dec 25<sup>th</sup> 2020</p>
                              <!-- <p><b>Time :</b> 05:00pm - 06:00pm</p> --> 
                              <p><b>Venue :</b> Deshpande Startups, Next to Airport Opp to Gokul Village, <br> Gokul Road, Hubballi, <br> Karnataka - 580030</p>
                              <p class=""><b>Contact details:</b><br>
                                 M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
                                 E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="makeathon" class="btn p-1 btn-primary" target="_blank">Know More</a>
                              <a href="mm-register" class="btn btn-primary p-1" target="_blank">Apply Now</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <br>
      <div class="container">
         <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
            <h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">PAST</span> EVENTS</h2>
            <div class="divider b-y text-yellow content-middle"></div>
         </div>
         <!-- 1 row-->
         <div class="row">
            <div class="col-md-12 px-5">
               <div class="row">
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="ideathon" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/events-ideathon.jpg" width="474" height="237" alt="Deshpande startups, Ideathon, Yuva Entrepreneurship"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Ideathon</h5>
                              <p><b>Last Date to Apply: <br></b>Dec 07<sup>th</sup> 2020</p>
                              <!-- <p><b>Last date to register : </b><br>March 18<sup>th</sup> 2020</p> -->
                              <p><b>Venue: </b> Deshpande Startups,<br> Next to Airport Opp to Gokul Village Gokul Road Hubballi, Karnataka.
                              </p>
                              <p class="text-truncate mb-2"><b>Contact details:</b><br>
                                 M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
                                 E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="ideathon" class="btn btn-primary p-1" target="_blank">Know More</a>
                              <!-- <a href="ideathon-form" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="hackathon" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/hackathon.png" width="474" height="237" alt="Virtual Hackathon, Yuva Entrepreneurship, Deshpande Startups"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Virtual Hackathon</h5>
                              <p><b>Date : </b>Sep 26<sup>th</sup> & 27<sup>th</sup> 2020</p>
                              <p><b>Last date to apply : </b><br>Sep 23<sup>rd</sup> 2020</p>
                              <!-- <p><b>Time :</b> 05:00pm - 06:00pm</p> -->
                              <p><b>Venue :</b> Virtual</p>
                              <p class=""><b>Contact details:</b><br>
                                 M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
                                 E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="hackathon" class="btn p-1 btn-primary" target="_blank">Know More</a>
                              <!--<a href="hackathon-form" class="btn btn-primary p-1" target="_blank">Apply Now</a>-->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="nidhi-prayas" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/nidhi-prayas.jpg" width="474" height="237" alt="Funding, NIDHI Prayas 1.0"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">NIDHI PRAYAS 1.0</h5>
                              <p><b>Applications open : </b><br>July 06<sup>th</sup> 2020</p>
                              <br>
                              <!-- <p class="text-yellow">The registrations has been closed.</p> -->
                              <!-- <p><b>Last date to apply : </b><br>July 31<sup>st</sup> 2020</p> -->
                              <!-- <p><b>Time :</b> 05:00pm - 06:00pm</p> -->
                              <!-- <p><b>Venue :</b> Virtual</p> -->
                              <p class="mb-4">
                                 <b>Contact details:</b><br>
                                 <!-- M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br> -->
                                 M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a> / <br><a href="tel:+91-951-331-5287"> +91-951-331-5287</a><br>
                                 E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a> / 
                                 <a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="nidhi-prayas" class="btn p-1 btn-primary" target="_blank">Know More</a>
                              <!-- <a href="nidhi-prayas" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- 1 row-->
         <!-- 2 row-->
         <div class="row">
            <div class="col-md-12 px-5">
               <div class="row">
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="webinar" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/webinar3.jpg" width="474" height="237" alt="Webinar - Agri NEXT: Future of Innovation & Entrepreneurship in Agriculture"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Agri NEXT</h5>
                              <p><b>Date : </b>July 18<sup>th</sup> 2020</p>
                              <!-- <p class="text-yellow">The registrations has been closed.</p> -->
                              <!-- <p><b>Last date to apply : </b><br>July 17<sup>th</sup> 2020</p> -->
                              <!--<p><b>Time :</b> 05:00pm - 06:00pm</p>--><br><br>
                              <p><b>Venue :</b> Virtual</p>
                              <p class="text-truncate mb-4">
                                 <b>Contact details:</b><br>
                                 <!-- M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br> -->
                                 M:<a href="tel:+91-951-331-5287"> +91-951-331-5287</a><br>
                                 E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="webinar" class="btn p-1 btn-primary" target="_blank">Know More</a>
                              <!-- <a href="webinar" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="webinar2" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/webinar2.jpg" width="474" height="237" alt="Webinar - Healthcare NEXT, Deshpande startups"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Healthcare NEXT</h5>
                              <p><b>Date : </b>June 21<sup>st</sup> 2020</p>
                              <!-- <p><b>Last date to apply : </b><br>June 20<sup>th</sup> 2020</p> -->
                              <p><b>Time :</b> 05:00pm - 06:00pm</p>
                              <p><b>Venue :</b> Virtual</p>
                              <p class="text-truncate mb-4">
                                 <b>Contact details:</b><br>
                                 <!-- M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br> -->
                                 M:<a href="tel:+91-951-331-5287"> +91-951-331-5287</a><br>
                                 E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="webinar2" class="btn p-1 btn-primary" target="_blank">Know More</a>
                              <!-- <a href="#" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="workshop" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/3d-arduino-workshop.jpg" width="474" height="237" alt="Deshpande startups, Arduino & 3D printing Workshops"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Workshops</h5>
                              <p><b>Date : </b>June 24<sup>th</sup> & 25<sup>th</sup> 2020</p>
                              <!-- <p><b>Last date to register : </b><br>June 20<sup>th</sup> 2020</p> -->
                              <p><b>Registration fee : </b>Rs.100/- per workshop</p>
                              <p><b>Venue :</b> Virtual
                              </p>
                              <p class="text-truncate mb-0"><b>Contact details:</b><br>
                                 M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
                                 E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="workshop" class="btn btn-primary p-1" target="_blank">Know More</a>
                              <!-- <a href="workshop" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- 2 row-->
         <!-- 3 row-->
         <div class="row">
            <div class="col-md-12 px-5">
               <div class="row">
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="sandbox-uplift" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/sandbox-uplift.png" width="474" height="237" alt="Sandbox Uplift, Deshpande startups"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Sandbox Uplift</h5>
                              <!-- <p><b>Date : </b>June 13<sup>th</sup> 2020</p> -->
                              <p><b>Last date to apply : </b><br>June 13<sup>th</sup> 2020</p>
                              <p><b>Venue :</b> Virtual
                              </p>
                              <p class="text-truncate mb-4">
                                 <b>Contact details:</b><br>
                                 <!-- M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br>	 -->
                                 M:<a href="tel:+91-951-331-5287"> +91-951-331-5287</a><br>
                                 E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br>
                                 <br> -->
                              <br>
                           </div>
                           <div class="card-footer">
                              <br>
                              <!-- <a href="sandbox-uplift" class="btn p-1 btn-primary" target="_blank">Know More</a> -->
                              <!-- <a href="#" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="past-webinar" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/webinar.jpg" width="474" height="237" alt="Webinar - Envisioning Bharat Opportunities, Deshpande startups"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Envisioning Bharat Opportunities</h5>
                              <p><b>Date : </b>May 31<sup>st</sup> 2020</p>
                              <p><b>Last date to apply : </b><br>May 30<sup>th</sup> 2020</p>
                              <!-- <p><b>Time :</b> 05:00pm - 06:00pm</p> -->
                              <p><b>Venue :</b> Virtual</p>
                              <p class="text-truncate"><b>Contact details:</b><br>
                                 M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br>
                                 E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="past-webinar" class="btn p-1 btn-primary" target="_blank">Know More</a>
                              <!-- <a href="#" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="3d-contest" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/3d-contest.png" width="474" height="237" alt="Deshpande startups, 3D Design Contest, Yuva Entrepreneurship"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">3D Design Contest</h5>
                              <p><b>Date : </b>May 10<sup>th</sup> 2020</p>
                              <p><b>Last date to register : </b><br>May 07<sup>th</sup> 2020</p>
                              <p><b>Venue :</b> Virtual
                              </p>
                              <p class="text-truncate"><b>Contact details:</b><br>
                                 M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
                                 E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="3d-contest" class="btn btn-primary p-1" target="_blank">Know More</a>
                              <!-- <a href="#" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- 3 row-->
         <!-- 4 row-->
         <div class="row">
            <div class="col-md-12 px-5">
               <div class="row">
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="ideathon" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/ideathon-3.png" width="474" height="237" alt="Deshpande startups, Ideathon, Yuva Entrepreneurship"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Ideathon</h5>
                              <p><b>Date : </b>March 21<sup>st</sup> 2020</p>
                              <!-- <p><b>Last date to register : </b><br>March 18<sup>th</sup> 2020</p> -->
                              <p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
                              </p>
                              <p class="text-truncate mb-2"><b>Contact details:</b><br>
                                 M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
                                 E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="ideathon" class="btn btn-primary p-1" target="_blank">Know More</a>
                              <!-- <a href="#" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="sandbox-uplift" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/sandbox-uplift.png" width="474" height="237" alt="Sandbox Uplift, Deshpande startups"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Sandbox Uplift</h5>
                              <p><b>Date : </b>March 21<sup>st</sup> 2020</p>
                              <!-- <p><b>Last date to apply : </b><br>March 15<sup>th</sup> 2020</p> -->
                              <p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
                              </p>
                              <p class="text-truncate"><b>Contact details:</b><br>
                                 M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br>
                                 E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer pb-3">
                              <br>
                              <!-- <a href="sandbox-uplift" class="btn p-1 btn-primary" target="_blank">Know More</a> -->
                              <!-- <a href="#" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="developthon" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/developthon.png" width="474" height="237" alt="Developthon, Deshpande startups"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Developthon</h5>
                              <!-- <p class="timr mb-1" id="demo"></p> -->
                              <!-- <h4 class="text-yellow font-weight-bold text-center pt-3">Coming soon...</h4><br> -->
                              <p><b>Date : </b>February 1<sup>st</sup> & 2<sup>nd</sup> 2020</p>
                              <!-- <p><b>Last date to apply : </b><br>January 25<sup>th</sup> 2020</p> -->
                              <p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
                              </p>
                              <p class="text-truncate"><b>Contact details:</b><br>
                                 M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
                                 E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="developthon" class="btn p-1 btn-primary" target="_blank">Know More</a>
                              <!-- <a href="#" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- 4 row-->
         <!-- 5 row-->
         <div class="row">
            <div class="col-md-12 px-5">
               <div class="row">
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="http://www.startupdialogue.org/" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/startup-dialogue-2020.png" width="474" height="237" alt="Startup Dialogue 2020, Deshpande startups"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center">Startup Dialogue 2020</h5>
                              <p><b>Date : </b>February 02<sup>nd</sup> 2020</p>
                              <!-- <p><b>Time : </b>09:00 AM</p> -->
                              <!-- <p><b>Last date to apply : </b><br>January 25<sup>th</sup> 2020</p> -->
                              <p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
                              </p>
                              <p class="text-truncate"><b>Contact details:</b><br>
                                 M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br>
                                 E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="http://www.startupdialogue.org/" class="btn p-1 btn-primary" target="_blank">Know More</a>
                              <!-- <a href="http://www.startupdialogue.org/registration-form" class="btn p-1 btn-primary" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 p-4">
                     <div class="card-deck">
                        <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                           <a href="internship" target="_blank"><img class="card-img-top img-fluid wow zoomIn" src="img/events/internship2.png" width="474" height="237" alt="Internship, Deshpande startups"></a>
                           <div class="card-body">
                              <h5 class="card-title text-yellow text-center text-truncate">Internship</h5>
                              <p><b>Date : </b>January 12<sup>th</sup> to 27<sup>th</sup> 2020</p>
                              <!-- <p><b>Time : </b>09:00 AM</p> -->
                              <!-- <p><b>Last date to apply : </b><br>January 10<sup>th</sup> 2020</p> -->
                              <p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
                              </p>
                              <p class="text-truncate"><b>Contact details:</b><br>
                                 M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
                                 E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a>
                              </p>
                              <!-- <br> -->
                           </div>
                           <div class="card-footer">
                              <a href="internship" class="btn p-1 btn-primary" target="_blank">Know More</a>
                              <!-- <a href="internship-form" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <br>
      <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once 'essentials/js.php';
         ?>
   </body>
</html>