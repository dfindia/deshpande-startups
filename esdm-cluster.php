<!DOCTYPE html>
<html lang="en">
   <head>
      <title>ESDM Cluster | An initiative of Deshpande Startups</title>
      <?php
         require_once 'essentials/meta.php';
         ?>
      <meta name="linkage" content="https://www.deshpandestartups.org/esdm-cluster"/>
      <meta property="og:site_name" content="Deshpande Startups"/>
      <meta property="og:type" content="website">
      <meta property="og:url" content="https://www.deshpandestartups.org/esdm-cluster">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/deshpande-startups-esdm.jpg">
      <meta property="og:description" content="Deshpande ESDM Cluster is an initiative of Deshpande Startups along with Govt of Karnataka &amp; IESA with a vision to promote the ESDM industry in the North Karnataka"/>
      <meta name="author" content="Deshpande Startups"/>
      <meta name="description" content= "Deshpande ESDM Cluster is an initiative of Deshpande Startups along with Govt of Karnataka &amp; IESA with a vision to promote the ESDM industry"/>
      <!-- <meta name="keywords" content="incubation, esdm cluster, pcb prototyping, Testing &amp; certification, arduino, raspberry pi boards, Deshpande startups"/> -->
      <meta property="og:title" content="Deshpande Startups ESDM Cluster">
      <link rel="canonical" href="https://www.deshpandestartups.org/esdm-cluster">
	  
      <?php
         // $title = 'Deshpande Startups';
         require_once 'essentials/bundle.php';
         ?>
      <style type="text/css">
         .gray{background-color: #403b3b;}
         .white{color:#fff;}
		 label{
			 font-size: 14px;
		 }
      </style>
   </head>
   <body>
      <?php
         require_once 'essentials/title_bar.php';
         require_once 'essentials/menus.php';
         ?>
      <img class="carousel-inner img-fluid" src="img/esdm/deshpande-startups-esdm-1.jpg" width="1349" height="395" alt="ESDM Cluster Deshpande Startups, deshpande foundation">
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb justify-content-end">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">ESDM Cluster</li>
         </ol>
      </nav>
      <div class="container">
         <div class="center wow fadeInDown">
            <h2 class="text-yellow text-center slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">ABOUT</span> ESDM CLUSTER</h2>
            <div class="divider b-y text-yellow content-middle"></div>
            <br>
            <p class="text-justify">
               ESDM cluster offers <b>“State-of-the-art infrastructure and modern-day technological aid along with a testing facility to support the electronics startups ideas.”</b>
            </p>
            <p class="text-justify">
               The cluster is an initiative of Deshpande Startups in association with the Government of Karnataka & India Electronic and Semiconductor Association (IESA).<br><br>
               <!-- <p class="text-justify"> -->
               It is strategically based in Hubballi, with a vision to promote the ESDM cluster and nurture the young electronics startup community in the North Karnataka region.<br><br>
               <!-- </p> -->
               <!-- <p class="text-justify"> -->
               ESDM is professionally managed to cater to the Electronics Manufacturing solutions.<br> 
            </p>
         </div>
      </div>
	  
      <div class="container">
         <div class="row featurette">
            <div class="col-md-12 bg-grey1 w3-card">
               <br>
               <p><b>It has the capability to serve various verticals includes:</b></p>
               <div class="row">
                  <div class="col-md-3">
                     <ul>
                        <li>Automotive </li>
                        <li>Consumer Durables</li>
                     </ul>
                  </div>
                  <div class="col-md-3">
                     <ul>
                        <li>Instrumentation</li>
                        <li>Medical</li>
                     </ul>
                  </div>
                  <div class="col-md-3">
                     <ul>
                        <li>Telecommunication</li>
                        <li>Power Transmission</li>
                     </ul>
                  </div>
                  <div class="col-md-3">
                     <ul>
                        <li>Weighing solutions</li>
                        <li>Industry applications product like UPS, Energy Meters etc</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <br>			
      <div class="container">
         <h5 class="text-yellow"><b>What it offers</b></h5>
         <p class="text-justify">
            ESDM Cluster offers Design, Prototype development, Batch production, Final assembly testing facility and Modern day Technological Aids. Deshpande Startups has installed new generation fully Automated SMT facilities with the capability to handle Lead-Free (pb) Processes.
         </p>
      </div>
      <!-- </div> -->
      <div class="container">
         <br>
         <div class="center wow fadeInDown">
            <h2 class="text-yellow text-center slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">HIGHLIGHTS OF</span> THE FACILITY</h2>
            <div class="divider b-y text-yellow content-middle"></div>
            <br>
            <div class="row">
               <div class="col-md-5">
                  <img src="img/esdm/deshpande-startups-esdm.gif" class="pr-1 text-center">
               </div>
               <div class="col-md-7">
                  <ul>
                     <li>8500 sq ft of state-of-art hardware manufacturing facility</li>
                     <li>ESD protected class 10000 shop floor</li>
                     <li>State-of-the-art Lead-free wave-soldering facility for bulk production of PCB assemblies</li>
                  </ul>
                  <h5 class="pl-3"><b>New generation fully-Automated SMT lines from Fuji<br> consisting of</b></h5>
                  <ul>
                     <li>Fuji solder paste screen printer</li>
                     <li>Fuji pick & place machine with tray feeder</li>
                     <li>JT RS800II Lead-free reflow oven</li>
                  </ul>
                  <h5 class="pl-3"><b>Advanced testing and rework facilities consisting of</b></h5>
                  <ul>
                     <li>Environmental testing chambers</li>
                     <li>Vibration testing chambers</li>
                     <li>Dust chambers</li>
                     <li>Rain test chambers</li>
                     <li>Various testing measuring equipment’s like Spectrum analyzer, Logic Analyzer etc</li>
                  </ul>
               </div>
            </div>
            <br>
            <div class="text-center">
               <!-- <a href="contact-us" class="btn btn-warning button4" target="_blank">Contact Us </a> -->
			      <a href="sop-for-esdm-smt-process-input" class="btn btn-warning button4"  target="_blank">Download SMT SOP</a>
               <a href="" class="btn btn-warning button4" data-toggle="modal" data-target="#quotation-modal" target="_blank">Get Your PCB Assembly Quotation</a>
               <a href="contact-us" class="btn btn-warning button4" target="_blank">Contact Us </a>  
            </div>
         </div>
      </div>
      <br>
      <div class="container-fluid">
         <div class="row featurette text-white">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 bg-blue p-4">
               <h2 class=" wow slideInLeft text-center">ESDM Cluster Advantages</h2>
               <div class="divider b-w content-middle"><span class="fs-11">...</span></div>
               <ul class="wow slideInLeft">
                  <li>Offers a work environment of coherent  culture, process & values</li>
                  <li>Comprehensive turnkey services under one roof</li>
                  <li>Capable of supporting rapid growth and change</li>
                  <li>Single-minded focus on quality</li>
                  <li>Short lead times and reliable on-time delivery</li>
                  <li>99.9% yield capability</li>
                  <li>Strong commitment to supporting customers R&D prototypes</li>
               </ul>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 bg-yel p-4">
               <div class="text-center">
                  <h2 class="wow slideInRight">Capabilities</h2>
               </div>
               <div class="divider b-w content-middle text-yellow"><span class="fs-11">...</span></div>
               <ul class="wow slideInRight">
                  <li> Lead-Free Process Capability</li>
                  <br>
                  <li>0201 Component Placement</li>
                  <br>
                  <li>BGA & Micro BGA Capability</li>
               </ul>
            </div>
         </div>
      </div>
      <!-- <br> -->
	

  <!-- Modal -->
  <div class="modal fade" id="quotation-modal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Get Your Quotation</h4>
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <div class="modal-body">
       <iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(typeof submitted != 'undefined' && submitted){alert('Thanks for reaching out! We will soon contact you'); document.getElementById('enquiry-form').reset();}"></iframe>
        <form action="https://docs.google.com/forms/u/0/d/e/1FAIpQLSeEsoB-c2ZBkUes2cVMPcg5S1tavhJga9Ksb2U2fxxtJaMEUQ/formResponse" method="POST" target="hidden_iframe" id="enquiry-form" onSubmit="submitted=true;" id="contact-form">
                     <div class="form-row">
                        <div class="form-group col-lg-4 fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
                           <label for="input1"><b>Name<span class="text-yellow">*</span></b></label>
                           <input type="text" name="entry.82677880" class="box2 form-control" maxlength="25" placeholder="Your name" required="required">
                        </div>
                        <div class="form-group col-lg-4  fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
                           <label for="input2"><b>Email-id<span class="text-yellow">*</span></b></label>
                           <input type="email" name="entry.1959259248" placeholder="Your Email-id" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
                        </div>
                        <div class="form-group col-lg-4  fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
                           <label for="input2"><b>Company Name<span class="text-yellow">*</span></b></label>
                           <input type="text" name="entry.33188390" placeholder="Company Name" maxlength="50" class="box2 form-control" required="required">
                        </div>
					</div>
					<div class="form-row">
                    
                     
                     
                        <div class="form-group col-lg-4 fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
                           <label for="input3"><b>Contact number<span class="text-yellow">*</span></b></label>
                           <input type="phone" name="entry.1162151663" class="box2 form-control" pattern="\d*" min="10" placeholder="Contact number" maxlength="12" title="Contact number" required="required">
                        </div>
                        <div class="form-row form-group col-lg-4 fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label><b>Type of PCB</b></label>
                           <select class="form-control" name="entry.1187495074">
						   <option value="None" selected>--Select option--</option>
                              <option value="rigid" selected>Rigid</option>
                              <option value="flex">Flex</option>
                           </select>
                        </div>
                        <div class="form-group col-lg-4 fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
                           <label for="input3"><b>Quantity of PCB</b></label>
                           <input type="text" name="entry.1672591216" class="box2 form-control" placeholder="Quantity of PCB" maxlength="6" title="Quantity of PCB" >
                        </div>
					</div>
				
                     <div class="form-row">
                        <div class="form-row form-group col-lg-4  fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label><b>Process</b></label>
                           <select class="form-control" name="entry.1969083845">
                               <option value="None">--Select option--</option>
                              <option value="rohs" selected>ROHS</option>
                              <option value="non-rohs">Non-ROHS</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
                           <label for="input3"><b>No. of Components on PCB</b></label>
                           <input type="text" name="entry.352756311" class="box2 form-control" pattern="\d*" placeholder="Components" maxlength="6" title="No. of Components on PCB" >
                        </div>
                        <div class="form-group col-lg-4  fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
                           <label for="input3"><b>No. of Solder Joints</b></label>
                           <input type="text" name="entry.2018111838" class="box2 form-control" pattern="\d*" placeholder="Solder Joints" maxlength="6" title="No. of Solder Joints" >
                        </div>
					</div>
					<div class="form-row">
                        
                     
                        <div class="form-row form-group col-lg-6  fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label><b>No. of Assembly Layers</b></label>
                           <select class="form-control" name="entry.1645947089">
                               <option value="None" selected>--Select option--</option>
                              <option value="single" selected>Single</option>
                              <option value="double">Double</option>
                           </select>
                        </div>
                        <div class="form-group col-lg-6  fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
                           <label for="input3"><b>PCB/Panel Dimensions</b></label>
                           <input type="text" name="entry.401936111" class="box2 form-control" pattern="\d*" placeholder="PCB/Panel Dimensions" maxlength="6" title="PCB/Panel Dimensions">
                        </div>
                     
					</div> 
               <div class="form-row">
               <div class="form-group col-lg-12">
               <label for="input4"><b>If you like to mention additional details, Please specify.<span class="text-yellow"></span></b></label>
							<textarea name="entry.2136065186" class="box2 form-control" rows="3" min="10" maxlength="500" placeholder="Message Short & sweet please (Maximum 500 Characters)" title="Your Text Here"></textarea>
               </div>
               </div>
			
                     <div class="form-row">
                        <div class="form-group col-lg-12">
                        
						   <input type="submit" class="btn btn-warning button4 g-recaptcha" name="Submit" id="button" value="Submit to get a call" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" >
   
                        </div>
                     </div>
                  </form>
               </div>
               <!--modal body end-->
        
     
      </div>
      
    </div>
  </div>
  

      <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once 'essentials/js.php';
         ?>
</body>
</html>