<!DOCTYPE html>
<html lang="en">
<head>
 <title>Developthon - Yuva Entrepreneurship Program</title>
 <?php
 require_once 'essentials/meta.php';
 ?>
 <meta name="linkage" content="https://www.deshpandestartups.org/developthon"/>
 <meta property="og:site_name" content="Deshpande Startups"/>
 <meta property="og:type" content="website">
 <meta property="og:url" content="https://www.deshpandestartups.org/developthon">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/developthon-bg.png">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/developthon-img.png">
 <meta property="og:description" content="An initiative of “Yuva Entrepreneurship Program” of Deshpande Startups present “Developthon”, a National level Hackathon, 24 / 36-hour event witnessing a gathering of curious and like-minded Hackers, held at India’s Largest Incubation Center – Deshpande Startups, next to Hubballi Airport."/>
 <meta name="author" content="Deshpande Startups"/>
 <meta name="description" content="An initiative of “Yuva Entrepreneurship Program” of Deshpande Startups present “Developthon”, a National level Hackathon, 24 / 36-hour event witnessing a gathering of curious and like-minded Hackers, held at India’s Largest Incubation Center – Deshpande Startups, next to Hubballi Airport."/>
 <!-- <meta name="keywords" content=""/> -->
 <meta property="og:title" content="Developthon - Yuva Entrepreneurship Program">
 <link rel="canonical" href="https://www.deshpandestartups.org/developthon">
 <?php
         // $title = 'Deshpande Startups';
 require_once 'essentials/bundle.php';
 ?>
 <style type="text/css">
  /*@media only screen and (max-width: 360px) {
   #clockdiv{ 
    text-align: center;
    font-size: 35px !important;
    margin-top: 15px;
  }
}*/

/*#clockdiv{
 color: #000;
 display: inline-block;
 font-weight: 500;
 text-align: center;
 font-size: 88px;
}

#clockdiv > div{
  display: inline-block; 
}
#clockdiv div > span{
  padding-bottom: 0px;
  border-radius: 100%; 
  text-align: center;
  width: 130px;
  border: 5px solid #eb7c26;
  background: #ffffff; 
  display: inline-block;
} 
.smalltext{ 
 padding-top: 5px;
 font-size: 16px;
 color: #000;
 font-weight: bold;
 text-align: center;
}*/
</style>

</head>
<body>
 <?php
 require_once 'essentials/title_bar.php';
 require_once 'essentials/menus.php';
 ?>

 <img class="carousel-inner img-fluid" src="img/makers/developthon-bg.png" width="1349" height="198" alt="Deshpande Startups, events developthon">
 <nav aria-label="breadcrumb">
  <ol class="breadcrumb justify-content-end">
    <li class="breadcrumb-item"><a href="./">Home</a></li>
    <li class="breadcrumb-item"><a href="yuva-entrepreneurship">Yuva Entrepreneurship</a></li>
    <li class="breadcrumb-item active" aria-current="page">Developthon</li>
  </ol>
</nav>

<!-- <div class="container">
 <div class="row text-center">
  <div class="col-md-12">
    <div id="clockdiv"> 
      <div> 
        <span class="days" id="day"></span> 
        <div class="smalltext">Days</div> 
      </div> 
      <div> 
        <span class="hours" id="hour"></span> 
        <div class="smalltext">Hours</div> 
      </div> 
      <div> 
        <span class="minutes" id="minute"></span> 
        <div class="smalltext">Minutes</div> 
      </div> 
      <div> 
        <span class="seconds" id="second"></span> 
        <div class="smalltext">Seconds</div> 
      </div> 
    </div> 
  </div> 
</div> 
</div> 
<br> -->

<!-- <p id="demo"></p> -->


<div class="container">
 <!-- <br> -->
 <div class="center wow fadeInDown text-center">
  <h2 class="text-yellow text-center">DEVELOPTHON</h2>
  <div class="divider b-y text-yellow content-middle"></div>
  <h4 class="text-yellow">A National Level Hackathon</h4>
</div><br>
<div class="row">
  <div class="col-md-12">
   <!-- <div class="col-md-10"> -->
     <p class="text-justify wow slideInLeft">An initiative of <b>“Yuva Entrepreneurship Program”</b> of Deshpande Startups present <b>“Developthon”</b>, a National level Hackathon, 24/36-hour event witnessing a gathering of curious and like-minded Hackers, held at India’s Largest Incubation Center - Deshpande Startups, next to Hubballi Airport. <br><br><b>“Developthon”</b> is an exciting opportunity for all <b>Enthusiastic students</b> to meet at one single platform, and to bridge the gap between technology and innovation to inspire passionate individuals to build <b>Social Impact solutions</b> and witness propelling India’s true growth story.</p>
   </div>
   <!-- <div class="col-md-2">
    <div class="row pl-4">
      <a href="#" class="btn btn-rotate" target="_blank">Apply Now</a>
   </div>
 </div> -->
</div>
<p>Coders and Developers from 3<sup>rd</sup> and 4<sup>th</sup> year engineering can participate in a team of minimum 2 members and a maximum of 4 members and stand a chance to win exciting awards.</p>
<!-- <p>3rd and 4th Year Computer Science and Information Science students can participate and stand a chance to win exciting awards.</p> -->
</div>
<br>
<div class="container">
 <div class="center wow fadeInDown">
  <h2 class="text-yellow text-center">PAST <span class="text-muted">PARTNERS</span></h2>
  <div class="divider b-y text-yellow content-middle"></div>
</div><br>
<img src="img/makers/past-partners.png" width="1349" height="100" alt="makers lab, past partners" class="img img-fluid">
</div>
<br>
<br>

<div class="featured-bg-container">
 <div class="row valign-wrapper">
  <div class="col-md-8">
   <h4 class="text-yellow"><b>8 Reasons why you should participate/key highlights of the event:</b></h4>
   <ul class="text-justify wow slideInLeft">
    <li>Get to be a part of vibrant community of developers and build your network</li>
    <li>Internship opportunities at Deshpande Startups</li>
    <li>Opportunity to code under the guidance of professional coders/developers</li>
    <li>Hands-on laboratory experience</li>
    <li>Unique chance to meet and network with exciting startups and team with high-enthu developers and explore further opportunities</li>
    <li>Win awards and recognition among developers</li>
    <li>Inspire the younger generation by sharing your practical knowledge and nurture them in developing their skills, we are certain that giving back is as joyous as learning</li>
    <li>Add real value to your resume with certification from Deshpande Startups</li>
  </ul>
</div>
<div class="col-md-4">
 <img src="img/makers/developthon-img.png" width="398" height="270" alt="makers lab, developthon event" class="img img-fluid img-thumbnail wow zoomIn">
</div>
</div>
</div>


<script> 

 var deadline = new Date("Feb 01, 2020 10:00:00").getTime(); 

 var x = setInterval(function() { 

  var now = new Date().getTime(); 
  var t = deadline - now; 
  var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
  var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
  var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
  var seconds = Math.floor((t % (1000 * 60)) / 1000); 

// document.getElementById("demo").innerHTML = days + "d : " + hours + "h : "
//   + minutes + "m : " + seconds + "s ";
document.getElementById("day").innerHTML =days; 
document.getElementById("hour").innerHTML =hours; 
document.getElementById("minute").innerHTML = minutes;  
document.getElementById("second").innerHTML =seconds;  
if (t < 0) { 
  clearInterval(x); 
  document.getElementById("demo").innerHTML = "TIME UP"; 
  document.getElementById("day").innerHTML ='0'; 
  document.getElementById("hour").innerHTML ='0'; 
  document.getElementById("minute").innerHTML ='0' ;  
  document.getElementById("second").innerHTML = '0'; } 
}, 1000); 
</script> 

<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>