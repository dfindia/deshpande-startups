<!DOCTYPE html>
<html lang="en">
<head>
	<title>Events | Pitch your Idea to Scale</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/pitch-your-idea"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/pitch-your-idea">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/pitch-idea-bg.jpg">
	<meta property="og:description" content="Deshpande Startups Presents an opportunity to all the startups to Pitch your Idea & stand a chance to win Incubation Support."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Deshpande Startups Presents an opportunity to all the startups to Pitch your Idea & stand a chance to win Incubation Support."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Pitch your Idea to Scale">
	<link rel="canonical" href="https://www.deshpandestartups.org/pitch-your-idea">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/pitch-idea-bg.jpg" width="1349" height="198" alt="Deshpande Startups, events Pitch your Idea to Scale">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Pitch your Idea to Scale</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">PITCH YOUR </span>IDEA TO SCALE</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/pitch-idea.jpg" width="474" height="237" alt="Deshpande startups, events, Pitch your Idea to Scale">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center text-truncate">Pitch your Idea to Scale</h5>
									<p><b>Date : </b>August 11<sup>th</sup> 2018</p>
									<!-- <p><b>Time : </b>10:00 AM</p> -->
									<p><b>Last date to register : </b>August 7<sup>th</sup> 2018</p>
									<p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br>
										E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
									</p>
								</div>
								<div class="card-footer">
									<p class="text-yellow">The registrations has been closed.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-5 text-yellow"><b>Event Description:</b></p>
						<p class="text-justify wow slideInRight">Deshpande Startups presents an opportunity to all the startups to pitch your Idea & stand a chance to win Incubation Support.</p>
						<p class="text-justify wow slideInRight">We at Deshpande Startups <b>(A not-for-profit, section 8 entity which is also a Central Government recognized TBI of India)</b> support mission driven entrepreneurs to scale in their venture. Entrepreneurs call it as a living laboratory to test their business ideas, get it validated, build successful ventures and scale to the greater heights.</p>
						<p class="text-justify wow slideInRight"><b>Note:</b> After submitting the Form, Please send your business deck with below given format at <a href="mailto:seir&#064;dfmail&#046;org">seir&#064;dfmail&#046;org</a></p>
						<br>
						<p><b class="text-yellow">Business Deck format:</b></p>
						<ul class="wow slideInRight">
							<li>Introduction of the company & founding team - 1 slide</li>
							<li>Problem statement - 1 slide</li>
							<li>Your business deck - 1 to 3 slide</li>
							<li>Uniqueness about your ideas - 1 to 2 slide</li>
							<li>Business competition/existing solution providers - 1 slide</li>
							<li>Market size/opportunity - 1 to 2 slide</li>
							<li>Revenue model</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>