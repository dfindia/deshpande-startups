<!DOCTYPE html>
<html lang="en">
<head>
	<title>Webinar - Agri NEXT: Future of Innovation & Entrepreneurship in Agriculture</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/webinar"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/webinar">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/webinar3.jpg">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/webinar3-bg.jpg">
	<meta property="og:description" content="India is an agrarian economy and the majority of the individuals are engaged in this sector for the sustenance of better livelihoods. It renders a significant contribution to the growth and development of the country’s economy."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="India is an agrarian economy and the majority of the individuals are engaged in this sector for the sustenance of better livelihoods. It renders a significant contribution to the growth and development of the country’s economy."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Webinar - Agri NEXT: Future of Innovation & Entrepreneurship in Agriculture">
	<link rel="canonical" href="https://www.deshpandestartups.org/webinar">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		small {
			font-size: 70%!important;
			font-weight: 340!important;
		}
		#feedback a {
			display: block;
			background: #E66425;
			height: 45px;
			padding-top: 5px;
			width: 191px;
			text-align: center;
			color: #fff;
			font-family: Arial, sans-serif;
			font-size: 20px;
			font-weight: bold;
			text-decoration: none;
		}
		@media only screen and (max-width: 360px){
			#feedback a {
				display: block;
				height: 40px;
				padding-top: 5px;
				width: 155px;
				text-align: center;
			}
		}

		#feedback {
			height: 0px;
			width: 0px;
			/*width: 85px;*/
			position: fixed;
			/*right: 0;*/
			top: 50%;
			z-index: 998;
			transform: rotate(-90deg);
			-webkit-transform: rotate(-90deg);
			-moz-transform: rotate(-90deg);
			-o-transform: rotate(-90deg);
		}

		#telegram a {
			display: block;
			background: #0088cc;
			height: 45px;
			width: 45px;
			text-align: center;
			color: #fff;
			font-size: 30px;
		}

		@media only screen and (max-width: 360px){
			#telegram a {
				display: block;
				height: 45px;
				/*padding-top: 5px;*/
				width: 40px;
				text-align: center;
				color: #fff;
			}
		}

		#telegram {
			height: 0px;
			width: 0px;
			/* width: 85px; */
			position: fixed;
			/* right: 0; */
			top: 52%;
			z-index: 998;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/webinar3-bg.jpg" width="1349" height="198" alt="Deshpande Startups, events, Webinar - Healthcare NEXT: Opportunities in Bharat Healthcare">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Webinar</li>
		</ol>
	</nav>

	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center text-uppercase wow slideInDown"><span class="text-muted">Agri Next: Future of Innovation</span><br> & Entrepreneurship in Agriculture</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<p class="text-yellow"><b>Event Description:</b></p>
				<p class="text-justify wow slideInRight">India is an agrarian economy and the majority of the individuals are engaged in this sector for the sustenance of better livelihoods. It renders a significant contribution to the growth and development of the country’s economy. It is directly and indirectly linked with the other sectors of the economy and has the major objective of promoting overall growth and development of the economy.  The economic contribution of agriculture towards the Gross Domestic Product (GDP) of the country is declining with the economic growth that is taking place within the country. Agriculture with its allied sectors is the largest livelihood provider within the country.</p>

				<p class="text-justify wow slideInRight">India is expected to achieve the ambitious goal of doubling farm income by 2022. The agriculture sector in India is expected to generate better momentum in the next few years due to increased investments in agricultural infrastructure such as irrigation facilities, warehousing, and cold storage.</p>

				<p class="text-justify wow slideInRight">Our next webinar will host veterans from Industry and Government bodies in the Agriculture Sector. The webinar will hold discussions and focus on the future of Agriculture in the Bharat market and what it takes to build an Agritech enterprise in the Bharat ecosystem presenting some of the challenges which will be opportunities for entrepreneurs with interest in the sector.</p>
				<!-- <br> -->

				<div class="row text-center justify-content-md-center">
					<div class="text-center pt-2">
						<a href="https://youtu.be/IwwdPPBM7Po" class="btn btn-warning px-5" target="_blank">Click here for video</a>
					</div>
				</div>

			</div>
		</div>
	</div>
	<br>


	<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
		<h2 class=" text-yellow text-center text-uppercase wow slideInDown"><span class="text-muted">OUR </span> SPEAKERS</h2>
		<div class="divider b-y text-yellow content-middle"></div>
	</div>
	<div class="featured-bg-container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<div class="row text-center justify-content-md-center">
				<div class="col-md-2">
					<a href="https://www.linkedin.com/in/vineet-rai-536160/" rel="nofollow" target="_blank"><img src="img/speakers/vineet-rai.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Vineet Rai, Founder & Chairman, Aavishkaar Group, Speaker"></a>
					<h5 class="text-center text-yellow mb-0"><b>Shri. Vineet Rai</b></h5>
					<small>Founder & Chairman,<br> Aavishkaar Group</small>
				</div>
				<div class="col-md-2">
					<a href="https://www.linkedin.com/in/shardul-sheth-aa1aa659/" rel="nofollow" target="_blank"><img src="img/speakers/shardul-sheth.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s" alt="Shardul Sheth, Founder & CEO, AgroStar, Speaker"></a>
					<h5 class="text-center text-yellow mb-0"><b>Shri. Shardul Sheth</b></h5>
					<small>Founder & CEO,<br> Agrostar</small>
				</div>
				<div class="col-md-2">
					<a href="https://www.linkedin.com/in/suryakumar-p-v-s-03353052/" rel="nofollow" target="_blank"><img src="img/speakers/pvs-suryakumar.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" alt="Shri PVS Surya Kumar, Deputy Managing Director, NABARD, Speaker"></a>
					<h5 class="text-center text-yellow mb-0"><b>Shri. PVS Surya Kumar</b></h5>
					<small>Deputy Managing Director, NABARD</small>
				</div>
				<div class="col-md-2">
					<a href="https://www.linkedin.com/in/mohammad-innus-khan-a4801732/" rel="nofollow" target="_blank"><img src="img/speakers/innus-khan.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.7s" alt="Md. Innus Khan, Director of Agriculture Initiatives - Deshpande Foundation, Speaker"></a>
					<h5 class="text-yellow text-center mb-0"><b>Shri. Md. Innus Khan</b></h5>
					<small>Director of Agriculture Initiatives,<br> Deshpande Foundation</small>
				</div>
				<div class="col-md-2">
					<a href="https://www.linkedin.com/in/cmpatil/" rel="nofollow" target="_blank"><img src="img/team/c-m-patil.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.9s" alt="C. M. Patil, CEO - Deshpande Startups, Speaker"></a>
					<h5 class="text-yellow text-center mb-0"><b>Shri. C. M. Patil</b></h5>
					<small>CEO,<br> Deshpande Startups</small>
				</div>
			</div>
		</div>

	</div>
	<br/>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h4 class="text-center wow slideInRight"><b>Our Third  webinar on “AGRI NEXT: Future of Innovation & Entrepreneurship in Agriculture” is scheduled on 18<sup>th</sup> July 2020 from 5:00pm to 6:00pm</b></h4>
				<br>
				<p class="text-yellow"><b>Our upcoming webinar primarily focuses on the following:</b></p>

				<p class="text-justify wow slideInRight">COVID-19 has firmly established the need for active action and the establishment of a robust, collaborative, scalable, and agile Agritech infrastructure. Apart from this the ever-growing need for Agriculture also presents opportunities for startups and companies to make real accessible, affordable and quality Agri-Tech Solutions.</p>

				<p class="text-justify wow slideInRight">The emergence and growth of new and divergent Agritech startups with the adoption of advanced technology can bring tremendous growth in the sector. With Agritech-led transformation, agriculture outcomes can improve radically, building a world where food insecurity doesn’t exist and farmers can improve their livelihoods in a big way.</p>
				<ul class="wow slideInRight">
					<li>Opportunities and challenges in the Bharat Agriculture sector</li>
					<li>Role of innovation & its impact on the Bharat Agri market</li>
					<li>Upcoming trends in the Agritech sector</li>
					<li>What does it take to build an Agritech enterprise in the Bharat market?</li>
					<li>How can agri-tech startups evolve and emerge out of the COVID-19 pandemic?</li>
				</ul>
			</div>
		</div>
	</div>
	
	<!-- <br> -->
	<br>
	<br>
			<!-- <div id="feedback">
				<a href="webinar" target="_blank">Register now</a>
			</div> -->
			<div id="telegram">
				<a href="https://t.me/joinchat/AAAAAEWlzrvTlsP5RdP8qw" target="_blank" title="Join Chat Now"><i class="fa fa-telegram" aria-hidden="true"></i></a>
			</div>

			

			<?php
			require_once 'essentials/footer.php';
			require_once 'essentials/copyright.php';
			require_once 'essentials/js.php';
			?>
		</body>
		</html>