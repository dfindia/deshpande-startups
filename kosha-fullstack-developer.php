<!DOCTYPE html>
<html lang="en">
<head>
	<title>Full Stack Developer - Kosha Designs</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/kosha-fullstack-developer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/kosha-fullstack-developer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/kosha-big.png">
	<meta property="og:description" content="We are looking for Full Stack Developer. Job Position: Full Stack Developer, Experience: Minimum 4 years, Qualification: Formal education in computer science or related fields from a reputed institute."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Full Stack Developer. Job Position: Full Stack Developer, Experience: Minimum 4 years, Qualification: Formal education in computer science or related fields from a reputed institute."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Full Stack Developer, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/kosha-fullstack-developer">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Full Stack</span> Developer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Full Stack Developer<br>
					<strong>Startup:</strong> Kosha Designs<br>
					<strong>Qualification:</strong> Formal education in Computer Science or related fields from a reputed institute<br>
					<strong>Experience:</strong> Minimum 4 years of experience required<br>
					<strong>Start Date:</strong> Immediate<br>
					<strong>Job Location:</strong> Hubballi<br>
				</p>
			</div>
			<div class="col-md-6">
				<a href="https://www.koshadesigns.com/" target="_blank" rel="nofollow"><img src="img/career/kosha-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, Kosha Designs"></a>
			</div>
		</div>
		<!-- <br> -->

		<p class="text-justify pt-1">KOSHA Design is a social impact startup, has developed a technology platform to authenticate and market handmade products. KOSHA is founded by a team with deep experience in Textiles and Technology sector and Business Management domain. KOSHA is incubated by Deshpande Startups, Hubli and an awardee of Elevate-Startup Karnataka in 2020.</p>

		<p class="text-justify pt-1">We are looking for a strong Tech Lead with a minimum of 4 years of hands-on experience to be part of our startup team. This will be challenging work to build technology platform based on IOT, Mobile, AI-ML and Cloud technologies.</p>

		<div class="row pt-2">
			<div class="col-md-6">
				<h3 class="text-yellow">Skills and Expertise:</h3>
				<ul>
					<li>Java and Android SDK, experience, preferably with camera, accelerometer, GPS APIs</li>
					<li>Cloud development–with Python/ Node.js, AWS, Web Development</li>
					<li>Tools and best practices for design, development and deployment of Android and web applications (IDEs, Testing tools, GITHUB etc)</li>
					<li>Knowledge of AI/ML is desirable</li>
					<li>Agile methodology for development</li>
					<li>Team player – work with stakeholders from diverse background</li>
				</ul>
			</div>
			<div class="col-md-6">
				<h3 class="text-yellow pt-3">Roles and Responsibilities:</h3>
				<ul>
					<li>Design and develop Android mobile and cloud application components</li>
					<li>Design and develop cloud-based components, integrate with mobile apps and other interfaces, incorporate the security and other non-functional requirements</li>
					<li>Develop apps with optimal and maintainable code</li>
					<li>Support the application in pilot or in production with quick turnaround for issues</li>
					<li>Guide the rest of the team to achieve the team goals</li>
				</ul>
			</div>
		</div>
	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to <br> E:<a href="mailto: ramki&#046;k&#064;koshadesigns&#046;com"> ramki&#046;k&#064;koshadesigns&#046;com</a></b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>