<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sales Representative - Shopgro</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/shopgro-sales-representative"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/shopgro-sales-representative">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/shopgro-big.png">
	<meta property="og:description" content="We are looking for Sales Representative. Job Position: Sales Representative, Experience:  2 - 3 years, Experience: 2+ years of marketing and sales closure experience. Preferably should have knowledge of digital marketing."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Sales Representative. Job Position: Sales Representative, Experience:  2 - 3 years, Experience: 2+ years of marketing and sales closure experience. Preferably should have knowledge of digital marketing."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Sales Representative, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/shopgro-sales-representative">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Sales</span> Representative</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Sales Representative<br>
					<strong>Startup:</strong> Shopgro<br>
					<strong>Qualification:</strong> BBA / MBA<br>
					<strong>Experience:</strong> 2+ years of marketing and sales closure experience. Preferably should have knowledge of digital marketing<br>
					<strong>Job Location:</strong> Hubballi<br>
				</p>
			</div>
			<div class="col-md-6">
				<a href="https://www.shopgro.in/" target="_blank" rel="nofollow"><img src="img/career/shopgro-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, Shopgro" /></a>
			</div>
		</div>

		<p class="text-justify">As a Sales Representative at Shopgro, you use inbound selling strategies to find new business and help them grow using Shopgro software. Work with the Business Development team to research prospects and create outreach strategies. You run online demos of the Shopgro software and successfully sell the Shopgro value proposition. Your target clients will largely consist of -- but will not be limited to -- small and mid-sized businesses.</p>

		<h3 class="text-yellow">Skills and Expertise:</h3>
		<ul>
			<li>2 - 3 years of Closing Sales experience</li>
			<li>Fluency in  English</li>
			<li>Unmatched consultative selling and closing skills</li>
			<li>Accurate forecasting and pipeline management</li>
			<li>Track record of being a high performer (e.g. over quota, President's Club)</li>
			<li>A sharp focus on your goals and a strong approach for achieving them</li>
		</ul>
		<!-- <br> -->
		<h3 class="text-yellow pt-3">Roles and Responsibilities:</h3>
		<ul>
			<li>Quickly identify the hyperlocal marketing need of our prospective customers  for their business</li>
			<li>Consistently close new business at or above quota level</li>
			<li>Nurture relationships with highly qualified opportunities at small and mid-sized companies</li>
			<li>Build relationships with prospects and internal stakeholders to grow new business</li>
			<li>Work collaboratively with Shopgro's marketing and technology departments to evolve our sales strategy when new features and products are introduced</li>
			<li>Help shape the future of Shopgro's mission with your perspectives, ideas, and skills</li>
		</ul>
	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to <a href="mailto: uday&#064;shopgro&#046;in"> uday&#064;shopgro&#046;in</a> with subject as job title you are applying.</b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>