<!DOCTYPE html>
<html lang="en">
<head>
	<title>Market Research Analyst - Shopgro</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/shopgro-market-research"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/shopgro-market-research">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/shopgro-big.png">
	<meta property="og:description" content="We are looking for Market Research Analyst. Job Position: Market Research Analyst, Experience:  0 - 2 years, Qualification: MBA (Preferably with good knowledge of digital marketing)."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Market Research Analyst. Job Position: Market Research Analyst, Experience:  0 - 2 years, Qualification: MBA (Preferably with good knowledge of digital marketing)."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Market Research Analyst, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/shopgro-market-research">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Market Research</span> Analyst</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Market Research Analyst<br>
					<strong>Startup:</strong> Shopgro<br>
					<strong>Qualification:</strong> MBA (Preferably with good knowledge of digital marketing)<br>
					<strong>Experience:</strong> 0 - 2 years of experience required<br>
					<strong>Job Location:</strong> Hubballi<br>
				</p>
			</div>
			<div class="col-md-6">
				<a href="https://www.shopgro.in/" target="_blank" rel="nofollow"><img src="img/career/shopgro-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, Shopgro" /></a>
			</div>
		</div>

		<p class="text-justify">Entry-level research position at shopgro. The role is to conduct research in various categories and build a quality collection of interesting brands and SMB's. categories that we focus on include Personal care, Home and Kitchen appliance, etc.</p>
		<p class="text-justify">The ideal candidate will be innovative, team-oriented, an effective communicator, have a desire to participate in change and appreciate a dynamic environment with rapidly changing priorities. We are seeking someone with a demonstrated history of successful market research & lead generation. An understanding of, and passion for, technology is highly desired.
		</p>

		<h3 class="text-yellow">Basic Qualifications:</h3>
		<ul>
			<li>An understanding of and passion for e-commerce</li>
			<li>Demonstrated analytical skills</li>
			<li>Strong bias for action and ability to prioritize</li>
			<li>Aptitude for the organization, flexibility and producing results in a fast-paced environment</li>
			<li>Technical ability in using HTML and Excel, plus the ability to learn in-house tools quickly</li>
			<li>Strong communication skills: experience in coordinating teams and communicating to management</li>
			<li>Demonstrated highest level of integrity, intellectual honesty, and strong work ethic</li>
		</ul>
		<!-- <br> -->
		<h3 class="text-yellow pt-3">Skills and Expertise:</h3>
		<ul>
			<li>Organized and focused on delivering tasks within defined deadlines</li>
			<li>Strive for Excellence. Perform at the highest levels of accuracy and efficiency</li>
			<li>Strong commitment to improve things every day. Seek Continuous Feedback</li>
		</ul>
		<!-- <br> -->
		<h3 class="text-yellow pt-3">Roles and Responsibilities:</h3>
		<ul>
			<li>Conduct research in the assigned categories</li>
			<li>Actively seek feedback on the research done in daily feedback meetings with Manager</li>
		</ul>
	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to <a href="mailto: uday&#064;shopgro&#046;in">uday&#064;shopgro&#046;in</a> with subject as job title you are applying.</b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>