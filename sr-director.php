<!DOCTYPE html>
<html lang="en">
<head>
   <title>Sr. Director - Foundation for Deshpande Startups Initiative</title>
   <?php
   require_once 'essentials/meta.php';
   ?>
   <meta name="linkage" content="https://www.deshpandestartups.org/sr-director"/>
   <meta property="og:site_name" content="Deshpande Startups"/>
   <meta property="og:type" content="website">
   <meta property="og:url" content="https://www.deshpandestartups.org/sr-director">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/team-image.png">
   <meta property="og:description" content="We seek a leader who will realise this vision in an entrepreneurial and innovative way. The main performance expectations for this role are divided into responsibilities for FSSI overall and those specific to individual units/initiatives under FSSI."/>
   <meta name="author" content="Deshpande Startups"/>
   <meta name="description" content="We seek a leader who will realise this vision in an entrepreneurial and innovative way. The main performance expectations for this role are divided into responsibilities for FSSI overall and those specific to individual units/initiatives under FSSI."/>
   <!-- <meta name="keywords" content=""/> -->
   <meta property="og:title" content="Deshpande Startups Career Opportunities - Sr. Director">
   <link rel="canonical" href="https://www.deshpandestartups.org/sr-director">
   <?php
         // $title = 'Deshpande Startups';
   require_once 'essentials/bundle.php';
   ?>
   <style type="text/css">
   .cal{
      font-family: calibri;
   }
   .btn-link:hover {
     color: #636567;
     /*color: #868e96;*/
     text-decoration: underline;
     background-color: transparent;
     border-color: transparent;
  }
  /*p{text-align:justify;}*/
</style>
</head>
<body>
   <?php
   require_once 'essentials/title_bar.php';
   require_once 'essentials/menus.php';
   ?>
   <!-- <img class="carousel-inner img-fluid" src="img/bg-home/team-image.png" width="1349" height="395" alt="Deshpande Startups deshpande foundation">
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb justify-content-end">
         <li class="breadcrumb-item"><a href="./">Home</a></li>
         <li class="breadcrumb-item"><a href="career">Career</a></li>
         <li class="breadcrumb-item active" aria-current="page">Sr. Director</li>
      </ol>
   </nav> -->
   <div class="container cal" id="contact-info">
      <br>
      <!-- <p>Current  Openings: Chief Executive Officer</p> -->
      <div class="center  wow fadeInDown">
         <h2 class="text-yellow text-center">Deshpande Startups<br> <span class="text-muted">Sr. Director - Job Description</span> </h2>
         <div class="divider b-y text-yellow content-middle"></div>
      </div><br>
      <div class="pull-right"><a href="career-opportunities" class="btn btn-warning btn-md " target="_blank">Apply Now</a></div>
      <h2  class="text-yellow cal">Our Vision for Deshpande Startups</h2>
      <!-- <p>Our vision is to enable Entrepreneurship for All members of the society.</p> -->
      <p class="text-justify">Our vision is Entrepreneurship for All.  We envision an environment where anyone can realise their entrepreneurial aspiration, with free and full access to the startup ecosystem, regardless of one’s socio-cultural, geographic, education or financial background or domain or venture domain. To make this vision a reality, we also imagine the emergence of a startup ecosystem that would meet the realities of real India.<br><br> We see a wide spectrum of startups emerging in India to meet the high heterogeneous structure of the Indian economy and society. Venture-funded technology startups, whilst important, is just one such form and even there, the model still needs to evolve for India. We see the vibrancy of such entrepreneurship at scale drive the future of livelihoods, economic growth, global identity and social infrastructure development for India.</p>
      <p class="text-justify">The purpose of Deshpande Startups is to catalyze such an environment.  The world has 7 billion people.  About the top 2 billion are now served well by what we now know as the startup ecosystem in big metros in India and abroad.  The economic activity for the bottom 5 billion is picking up and that growing economy is underserved by startups.  There are startups coming up in big metros to serve this population in India.  However, they are ill suited because they are far removed from the customer base.  The audacious goal for the incubation centre in <b>HUBBALLI</b> is to create the <b>BEST ECOSYSTEM for STARTUPS</b> to serve the bottom 5 billion people in the world and the associated businesses.</p>
      <p class="text-justify">India has 1.2 billion people.  About 200 million of them live in big metros and about 1 billion lives in villages and semi-urban areas. Hubballi is a semi-urban area surrounded by villages.  The incubator in Hubballi uniquely offers access to customer base, motivated cost-effective talent and compassionate mentors to help the startups. If startups in Hubballi can serve the villages and semi-urban areas in India and they can then expand their businesses to expand to global markets to serve the 5 billion people.</p>

      <h2 class="text-yellow">Job Description – Key Responsibilities:</h2>
      <!-- <br> -->
      <p class="text-justify">We seek a leader who will realise this vision in an entrepreneurial and innovative way. The main performance expectations for this role are divided into responsibilities for FSSI overall and those specific to individual units/initiatives under FSSI. The primary service unit is FSSI’s incubation operation. Other initiatives being developed are an Electronics Systems Design and Manufacturing (‘ESDM’) Lab and a Fund for investing into FSSI incubated startups.</p>
      1. Overall FSSI related responsibilities<br>
      2. FSSI Incubator Related Responsibilities<br>
      <br>
      <h4 class="text-yellow">Overall FSSI related responsibilities</h4>
      <div class="col-md-12">
         <div class="row cal">
            <ul class="text-justify">
               <li>Understand the entrepreneurship characteristics and needs of the region and design a fit-for context strategy and execution plan to realise the vision. Refine and realign based on learning.</li>
               <li>Design, launch and refine innovative and outcome-driven programs for entrepreneur and startup development, across relevant stages of the startup lifecycle and for specific market segments.</li>
               <li>Build a talented and highly motivated team with a good mix of context appreciation, entrepreneurship development talent, leadership skills and execution expertise.  Develop a wider pool of expertise that can be tapped into as needed.</li>
               <li>Integrate FSSI into startup ecosystems that already exist so that our entrepreneurs are well resourced and we attract more startups whose products/solutions would impact billion lives.</li>
               <li>As part of such ecosystem connect, develop strong relationships with key ecosystem players, including investors of all kinds.</li>
               <li>Develop the Deshpande Startups brand as the first point of recall for entrepreneurs in North Karnataka or entrepreneurs seeking to do business in markets similar to those in North Karnataka.</li>
               <li>Contributes in developing a self-sustaining funding model for FSSI complementing defined contributions from the Deshpande Foundation. Such a model would include innovative revenue streams and grants, investments and other forms of external capital.</li>
               <li>Leverage technology, platforms and data to drive more value for our entrepreneurs and to scale effectively.</li>
               <li>Institute a suitable systems of metrics for disciplined execution, work with startups in connecting them to right set of mentors.</li>
               <li>FSSI is a board-managed organisation. The Sr. Director would work with CEO on relevant strategic and governance matters.</li>
               <li>Leverage the full platform of the Deshpande Foundation and contribute as relevant to other arms of the Foundation.</li>
            </ul>
         </div>
      </div>

      <h4 class="text-yellow pt-2">FSSI Incubator Related Responsibilities</h4>
      <div class="col-md-12">
         <div class="row cal">
            <p class="text-justify">The Sr. Director is directly responsible for the operations of the FSSI Incubator. The primary form of this unit is a 100K sq ft incubator / entrepreneurship development centre in Hubballi. The mission is to make this centre and its virtual/physical extensions and online channels, as the hub for entrepreneurship in North Karnataka.</p>
            <!-- <br> -->
            <p><b>Incubator-specific leadership responsibilities are:</b></p>
            <ul class="text-justify">
               <li>Develop and execute suitable marketing and sales strategies to expand the base of entrepreneurs in the region and to attract a large numbers of startups to Deshpande Startups.</li>
               <li>In addition to the program-design responsibilities mentioned earlier, institute an outcome-driven delivery model. Through this model, ensure that every FSSI incubated entrepreneur/startup has the right mix of resources to have the best shot at success. Whilst we cannot guarantee success, our goal is to ensure that no entrepreneur fails because of lack of access to resources – knowledge/expertise, capital, markets or physical.</li>
               <li>Develop alternative programs at the Hubballi centre such as co-working spaces, hosting labs that connect enterprises to startups who are looking to explore tier 2 city nurtured ecosystem.</li>
               <li>Develop a network of committed and relevant mentors, backed by a suitable alignment model, covering soft and technical aspects of startup development. Further, develop a network of service providers covering different aspects of venture execution, general and domain specific.</li>
               <li>Access to various B2C and B2B urban and rural markets (“sandbox markets”) is one of FSSI’s value propositions. Organising and scaling such access is an important responsibility. Another complementary proposition is helping North Karnataka originated startups access developed markets in India and potentially overseas. Such market access development is also part of FSSI leadership responsibilities.</li>
               <li>Manage the operations of the Hubballi centre.</li>
            </ul>
         </div>
      </div>
   </div>
   <br>
   <div class="container cal text-center">
      <p class="">Needless to add, as FSSI grows and diversifies, the nature of responsibilities will change too. The above set of responsibilities is what is envisaged for the first two-three years of this role.</p>
      <p class="">Passionate leader who is envisioning to promote entrepreneurship can share their resume to<br> E: <a href="mailto:shweta&#046;hr&#064;dfmail&#046;org" target="_blank" rel="noopener noreferrer">shweta&#046;hr&#064;dfmail&#046;org</a> <br>or<br> </p>
      <a href="career-opportunities" class="btn btn-warning btn-md " target="_blank">Apply Now</a>
      <p class=""><b>Job Location:</b> Hubballi, Karnataka.</p>
   </div>
   <?php
   require_once 'essentials/footer.php';
   require_once 'essentials/copyright.php';
   require_once 'essentials/js.php';
   ?>
</body>
</html>