<!DOCTYPE html>
<html lang="en">
<head>
	<title>Apply now for Internship | Yuva Entrepreneurship</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/internship-form"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/internship-form">
	 <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/internship-bg.png">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/internship-img.png">
	<meta property="og:description" content="An initiative of Yuva Entrepreneurship program is hosting 15 internship, A platform to Ideate, Design and Build your Product. Internship is a building stone to develop a community of engineers (students, graduates, professionals and others) to collaborate on building an innovative solutions for challenging problems."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="An initiative of Yuva Entrepreneurship program is hosting 15 internship, A platform to Ideate, Design and Build your Product. Internship is a building stone to develop a community of engineers (students, graduates, professionals and others) to collaborate on building an innovative solutions for challenging problems."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Apply now for an Internship">
	<link rel="canonical" href="https://www.deshpandestartups.org/internship-form">
	<?php
			// $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<br>
	<div class="container text-center">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">APPLY FOR</span> INTERNSHIP</h2>
		<div class="divider b-y text-yellow content-middle"></div>
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-lg-2">
				<iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
				</iframe>
				<form role="form" action="https://docs.google.com/forms/d/e/1FAIpQLSdyFzaj8Go5czfuXa9kGeDbq6yGsKS-UJuFnQ6Ui_oQyRsnaA/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">

					<div class="row w3-card p-3">
						<div class="col-md-12 pad">
							<div class="row">
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
									<label for="input1"><b>Name<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.1813290156" class="box2 form-control" maxlength="50" pattern="^(?![ .]+$)[a-zA-Z .]*$" placeholder="Mention your name" title="Mention your name" required="required">
								</div>
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
									<label for="input3"><b>Mobile number<span class="text-yellow">*</span></b></label>
									<input type="phone" name="entry.2001787411" class="box2 form-control" pattern="[6789][0-9]{9}" oninvalid="this.setCustomValidity('Number should be 10 digits starts with 6, 7, 8 or 9')" oninput="setCustomValidity('')" placeholder="Mention your mobile number" maxlength="10" minlength="10" title="Your mobile number" required="required">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
									<label for="input2"><b>Email-Id<span class="text-yellow">*</span></b></label>
									<input type="email" name="entry.1965281038" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
								</div>
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input4"><b>College/Institute<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.458264654" class="box2 form-control" placeholder="Name of College/Institute" required="required">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input5"><b>City<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.561536541" pattern="^(?![ .]+$)[a-zA-Z .]*$" class="box2 form-control" placeholder="Your location" required="required">
								</div>
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input5"><b>State<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.897538757" pattern="^(?![ .]+$)[a-zA-Z .]*$" class="box2 form-control" placeholder="Your State" required="required">
								</div>
							</div>
						</div>
						<div class="form-group col-md-12 m-0">
							<div class="row">
								<div class="form-group col-md-6">
									<label for="input45"><b>Academic Qualification<span class="text-yellow">*</span></b></label>
									<select class="form-control" name="entry.1160186455">
										<option value="-">Select...</option>
										<option value="B.E">B.E</option>
									</select>
								</div>

								<div class="form-group col-md-6">
									<label for="input45"><b>Stream/ Branch<span class="text-yellow">*</span></b></label>
									<select class="form-control" name="entry.960671923">
										<option value="CS" selected>CS</option>
										<option value="IS">IS</option>
										<option value="E&C">E&C</option>
										<option value="E&E">E&E</option>
										<option value="Mechanical">Mechanical</option>
										<option value="Civil">Civil</option>
										<option value="Architecture">Architecture</option>
										<option value="Bio Technology">Bio Technology</option>
										<option value="Chemical">Chemical</option>
										<option value="Aeronautical">Aeronautical</option>
										<option value="Biochemical">Biochemical</option>
										<option value="Other">Other</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group col-md-12">
							<label for="input45"><b>Year<span class="text-yellow">*</span></b></label>
							<select class="form-control" name="entry.217310837">
								<option value="-">Select...</option>
								<option value="3rd Year">3<sup>rd</sup> Year</option>
								<option value="4th Year">4<sup>th</sup> Year</option>
							</select>
						</div>


						<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input50"><b>Select the domain you want to learn during internship<span class="text-yellow">*</span></b></label>
						</div>
						<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<div class="row">
								<div class="col-md-6 form-group">
									<label for="iot"><input type="radio" name="entry.1137024689" id="one" value="IoT & Embedded" required="required"> IoT & Embedded</label>
								</div>
								<div class="col-md-6 form-group">
									<label for="mech"><input type="radio" name="entry.1137024689" id="two" value="Mechanical"> Mechanical</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 form-group">
									<label for="android"><input type="radio" name="entry.1137024689" id="four" value="Android & Web Development"> Android & Web development</label>
								</div>
							</div>
						</div>

						<div class="form-group col-lg-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.4s">
							<label for="input4"><b>Mention your technical skills<span class="text-yellow">*</span></b></label>
							<textarea name="entry.1631592613" class="box2 form-control" rows="4" minlength="5" maxlength="742" title="Your answer" placeholder="Ex: Programming/Coding languages, CAD designing etc" required="required"></textarea>
						</div>

						<div class="form-group col-lg-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.4s">
							<label for="input4"><b>Mention any technical projects you have worked on</b></label>
							<textarea name="entry.233162335" class="box2 form-control" rows="4" minlength="5" maxlength="742" title="Your answer" placeholder="Your answer"></textarea>
						</div>

						<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input6"><b>How did you come to know about internship?<span class="text-yellow">*</span></b></label><br>
							<div class="row">
								<div class="col-md-6">
									<label for="Personal"><input type="radio" name="entry.289225758" value="Personal Reference" required="required"> Personal reference</label>
								</div>
								<div class="col-md-6">
									<label for="Newsletters"><input type="radio" name="entry.289225758" value="Email News Letter"> Email news letter</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label for="facebook"><input type="radio" name="entry.289225758" value="Facebook"> Facebook</label>
								</div>
								<div class="col-md-6">
									<label for="whatsapp"><input type="radio" name="entry.289225758" value="Whatsapp"> Whatsapp</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label for="SocialMedia"><input type="radio" name="entry.289225758" value="Other Social Media"> Other social media</label>
								</div>
								<div class="col-md-6">
									<label for="PrintMedia"><input type="radio" name="entry.289225758" value="Print Media"> Print media</label>
								</div>
							</div>
						</div>
						<div class="form-group col-lg-12">
									<!-- <span class="text-yellow"><b>*</b></span>
										<div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div><br> -->
										<!-- <div class="form-group">
											<label for="agreement"><input type="checkbox" name="entry.133072460" value="I agree to make payment Rs. 4K for 15 days of internship" required="required"> I agree to make payment of Rs. 4000/- for 15 days of internship at Yuva Entrepreneurship Program.<span class="text-yellow"><b>*</b></span></label>
										</div> -->
										<span class="text-yellow"><h6><b>*</b> Fields are mandatory</h6></span>
										<input type="submit" class="btn custom-btn2 btn-warning" id="ss-submit" name="submit" value="Submit">
									</div>
								</div>

							</form>
						</div>		
					</div>
				</div>
				<!-- </div> -->
				<br>
				<br>
				<!-- </div> -->

				<script src='https://www.google.com/recaptcha/api.js'></script>
				<?php
				require_once 'essentials/footer.php';
				require_once 'essentials/copyright.php';
				require_once 'essentials/js.php';
				?>

				<!-- <script defer> 
					function ShowHideDiv() {
						var other = document.getElementById("other");
						var dvtext = document.getElementById("dvtext");
						dvtext.style.display = other.checked ? "block" : "none";
					}
				</script> -->
				<!-- <script type="text/javascript">
					$(function() {
						$('[name="entry.2073268077"]').on('click', function (e) {
							var val = $(this).val();
							if (val == "student") {
								$('.txbx1').show('fade');
								$('.txbx2').show('fade');
							}else {
								$('.txbx1').show('fade');
								$('.txbx2').hide();
							};
						});
					});
				</script> -->
				<script>
					window.onload = function() {
						var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
						recaptcha.required = true;
						recaptcha.oninvalid = function(e) {
	            // do something
	            alert("Please complete the captcha");
	        }
	    }
	</script>
</body>
</html>