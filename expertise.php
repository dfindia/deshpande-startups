<?php
$msg = '';
if (array_key_exists('pic', $_FILES)) {
	try {
		require_once(dirname(__FILE__) ."/include3/Framework/autoload.php");
		$Load = new Framework\Helper\Load();
		// $content = $Load->view('include3/contents', $_POST, TRUE);
		$content = $Load->view('include3/template', $_POST, TRUE);
		// echo $content;
		// die();
		$mail = new Framework\PHPMailer\PHPMailer(true);
		// $mail->SMTPAuth = true;
		$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		// $mail->SMTPSecure = "ssl";
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		// $mail->IsSMTP();

		// $mail->Username = "myemail@gmail.com";
		// $mail->Password = "**********";
		// $mail->Port = "465";

		// $mail->setFrom('umme.sandbox@dfmail.org', 'Sandbox Stratups Resume', FALSE);
		$mail->setFrom('gopal.bgk@dfmail.org', 'Deshpande Stratups Mentors', FALSE);

		// $mail->addAddress('umme.sandbox@dfmail.org', 'Sandbox Info');
		$mail->addAddress('gopal.bgk@dfmail.org', 'Deshpande Info');

		$mail->addReplyTo('no-reply@dfmail.org', 'No Reply');

	    // $mail->addCC('');
		$mail->addBCC('sagar.sa@dfmail.org');
		$mail->addBCC('umme.sandbox@dfmail.org');
		// $mail->addBCC('umme.sandbox@dfmail.org');

	    // Attachments
		// $mail->addAttachment($tmpName, $filename);


	    // Content
		$mail->isHTML(true);
		$mail->Subject = 'Deshpande Startups Mentors Page Online Submission';


		// Read an HTML message body from an external file, convert referenced images to embedded,
		// convert HTML into a basic plain-text alternative body
		$mail->msgHTML($content);
		// Replace the plain text body with one created manually
		$mail->AltBody = 'This is a plain-text message body';
		// Attach an image file
		if(is_uploaded_file($_FILES["pic"]["tmp_name"])){
			$pic_formats = array("jpg", "png");
			$pic_extension = pathinfo($_FILES["pic"]["name"], PATHINFO_EXTENSION);
			$cv_extension = pathinfo($_FILES["cv"]["name"], PATHINFO_EXTENSION);
			if(in_array(strtolower($pic_extension),$pic_formats)){
				$tempFile = $_FILES["pic"]["tmp_name"];
				$tempCV = $_FILES["cv"]["tmp_name"];
				$fileName = basename($_FILES["pic"]["name"], ".".$pic_extension);
				$cvName = basename($_FILES["cv"]["name"], ".".$cv_extension);
				// $fileName = $_FILES["pic"]["name"];
				$picDir = "mentors-pic/";
				$cvDir = "mentors-cv/";
				// $unique = md5( rand(0,1000) );
				// $fileTime =date("Ymdhis").$unique;
				$fileTime =date("Ymdhis");
				// $pic_name = "DFHRpic".$fileTime.".".$pic_extension;
				$pic_name = $fileName.'-'.$fileTime.".".$pic_extension;
				$cv_name = $cvName.'-'.$fileTime.".".$cv_extension;
				if(move_uploaded_file($tempFile, $picDir . basename($pic_name))){
					move_uploaded_file($tempCV, $cvDir . basename($cv_name));
					$mail->addAttachment($picDir . basename($pic_name));
					$mail->addAttachment($cvDir . basename($cv_name));
					if (!$mail->send()) {
						$data['response'] = false;
						$data['message'] = "Mailer Error: " . $mail->ErrorInfo;
					} else {
						$data['response'] = true;
						$data['message'] = "Your application has been submitted!<br>We'll get back to you soon";

						if( $curl = curl_init() ){
							extract($_POST);
							$filepath = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]".dirname($_SERVER['PHP_SELF']).'/'. $picDir . basename($pic_name);
							$cvpath = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]".dirname($_SERVER['PHP_SELF']).'/'. $cvDir . basename($cv_name);
							$params = array(
								'entry.1524348853' => $mentorname,
								'entry.1425841380' => $emailaddress,
								'entry.1067617385' => $mobilenumber,
								'entry.610863166' => $gender,
								'entry.1369284925' => $mcity,
								'entry.1693780564' => $companyname,
								'entry.1669126534' => $proftitle,
								'entry.653857999' => $expindustry,
								'entry.993668688' => $functionalexp,
								'entry.1749831183' => $industryexp,
								'entry.1185683383' => $corestrength,
								'entry.890805773' => $mentorbio,
								'entry.789402055' => $hourweek,
								'entry.1373786215' => $websitelink,
								'entry.1023452121' => $linkedin,
								'entry.1460988406' => $filepath,
								'entry.17503168' => $cvpath,
							);

							$url = 'https://docs.google.com/forms/d/e/1FAIpQLSfXSew1H7XfyAa_CoA_aGUYA8ggWON5h3P2oT_5_yQLEL2irQ/formResponse';

							$ch = curl_init();
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
							$result = curl_exec($ch);
							$error  = curl_error($ch);
						}
					}
				} else {
					$data['response'] = false;
					$data['message'] = 'Not uploaded.' ;
				}
			} else {
				$data['response'] = false;
				$data['message'] = 'Not allowed format, you can upload with these formats - pdf, doc, docx, jpg, png.' ;
			}
		} else {
			$data['response'] = false;
			$data['message'] = 'File is Not uploaded, attach your file and try again.' ;
		}
		// send the message, check for errors
		// $data['response'] = true;
		// $data['message'] = 'Here\'s your data.' ;
	} catch (Framework\Exceptions\FrameworkSDKException $e) {
		$data['response'] = false;
		$data['message'] = 'FrameworkSDK Error : '.$e->getMessage();
	} catch (PDOException $e) {
		$data['response'] = false;
		$data['message'] = 'PDO Error :'.$e->getMessage();
	} catch (Exception $e) {
		$data['response'] = false;
		$data['message'] = 'Php Error : '.$e;
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Be a mentor at Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/expertise"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/expertise">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/be-a-mentor.png">
	<!-- <meta property="og:description" content=""/> -->
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Deshpande Startups firmly believes in co-creating a strong entrepreneurial ecosystem by leveraging thought leaders through a selected mentor pool. We look for experts with considerable experience and are willing to guide and mentor startups to become part of the mentoring program."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Be a mentor at Deshpande Startups">
	<link rel="canonical" href="https://www.deshpandestartups.org/expertise">

	<?php
	require_once 'essentials/bundle.php';
	?>

	<style type="text/css">
		.bgcolorogr{background-color:rgba(0,0,0,.05)}
		.mandatory{color:red;}
		#g-recaptcha-response {
			display: block !important;
			position: absolute;
			margin: -78px 0 0 0 !important;
			width: 302px !important;
			height: 76px !important;
			z-index: -999999;
			opacity: 0;
		}
	</style>
</head>
<body onload="myFunction()" id="page-top">
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/bg-home/be-a-mentor.png" width="1349" height="400" alt="Deshpande Startups, Incubation Support">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Mentor at Deshpande Startups</li>
		</ol>
	</nav>
	<div class="container text-center">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">WOULD YOU LIKE</span> TO BE A MENTOR</h2>
		<div class="divider b-y text-yellow content-middle"></div>
	</div>
	<div class="container pt-3">
		<div class="col-md-12">
			<p class="text-justify wow zoomIn">Deshpande Startups firmly believes in co-creating a strong entrepreneurial ecosystem by leveraging thought leaders through a selected mentor pool. We look for experts with considerable experience and are willing to guide and mentor startups to become part of the mentoring program. Experts can contribute by mentoring startups, being a jury member at events, provide training and workshops for startups, speakers at events or by providing network as required by the startups.</p>
			<!-- <br> -->
			<div class="row">
				<div class="col-md-6">
					<h4 class="text-yellow"><b>Commitments from mentors </b></h4>
					<ul class="text-justify wow zoomIn">
						<li>Give 4 hours per week per startups being mentored </li>
						<li>Willingness to offer voluntary services for startups for 1 year period</li>
						<li>1 visit per quarter to the Deshpande Startups </li>
					</ul>
				</div>
				<!-- <br> -->
				<div class="col-md-6">
					<h4 class="text-yellow"><b>Who can be a mentor?</b></h4>
					<ul class="text-justify wow zoomIn">
						<li>Successful entrepreneurs</li>
						<li>Industry experts</li>
						<li>Investors</li>
						<li>Professionals with considerable management experiences</li>
						<li>Business leaders</li>
						<li>Domain experts</li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<br>
	<!-- Section Contact ================================================== -->
	<section id="contact" class="no-padding">
		<div class="container">
			<div class="center wow fadeInDown">
				<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">APPLY AS</span> MENTOR</h2>
				<div class="divider b-y text-yellow content-middle"></div>
				<!-- <br> -->
			</div>
		</div>

		<?php if (empty($msg)) { ?>
			<div class="container">
				<?php
				if (isset($data)) {
					if (!$data['response']) {
						?>
						<div class="alert alert-warning alert-dismissible text-center" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>Warning!</strong> <?php echo $data['message']?>
						</div>
						<?php
					} else {
						?>
						<div class="alert alert-success alert-dismissible text-center" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>Success!</strong> <?php echo $data['message']?>
						</div>
						<?php
					}
				}
				?>
				<div class="col-md-12">
					<div class="row justify-content-md-center">
						<!-- <div class="col-md-6"> -->
							<div class="col-lg-12 p-3 w3-card">
								<form action='<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>' enctype="multipart/form-data" id="ss-form" role="form" method="POST">
									<div class="row">
										<div class="col-md-6 pad">
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
												<label for="mentorname"><b>Full name<span class="mandatory">*</span></b></label>
												<input type="text" name="mentorname" id="mentorname" class="box2 form-control" maxlength="50" pattern="[A-Za-z\s]{1,50}" title="Mention your name" placeholder="Mention your name" required="required">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
												<label for="mobilenumber"><b>Mobile number<span class="mandatory">*</span></b></label>
												<input type="phone" name="mobilenumber" id="mobilenumber" class="box2 form-control" pattern="\d*" minlength="10" placeholder="Mention your mobile number" maxlength="10" title="Your mobile number" required="required">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
												<label for="emailaddress"><b>Email-id<span class="mandatory">*</span></b></label>
												<input type="email" name="emailaddress" id="emailaddress" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="gender"><b>Gender<span class="mandatory">*</span></b></label><br>
												<select class="form-control" name="gender" id="gender">
													<option value="Male">Male</option>
													<option value="Female">Female</option>
													<option value="Other">Other</option>
												</select>
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="mcity"><b>City<span class="mandatory">*</span></b></label>
												<input type="text" name="mcity" id="mcity" class="box2 form-control" placeholder="Your city" required="required">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="companyname"><b>Company name</b></label>
												<input type="text" name="companyname" id="companyname" class="box2 form-control" placeholder="Company name">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="proftitle"><b>Professional title<span class="mandatory">*</span></b></label>
												<input type="text" name="proftitle" id="proftitle" class="box2 form-control" title="Mention your professional title" placeholder="Mention your professional title" required="required">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="expindustry"><b>Expertise industry<span class="mandatory">*</span></b></label>
												<input type="text" name="expindustry" id="expindustry" class="box2 form-control" placeholder="ex: E-Commerce, Agri tech" required="required">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="functionalexp"><b>Functional expertise<span class="mandatory">*</span></b></label>
												<input type="text" name="functionalexp" id="functionalexp" class="box2 form-control" placeholder="ex: Business strategy, Product Development" required="required">
											</div>

										</div>

										<div class="col-md-6 pad">
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="industryexp"><b>Industry experience in years<span class="mandatory">*</span></b></label>
												<input type="text" name="industryexp" id="industryexp" class="box2 form-control" placeholder="Your industry experience" required="required">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="corestrength"><b>Core strengths<span class="mandatory">*</span></b></label>
												<input type="text" name="corestrength" id="corestrength" class="box2 form-control" placeholder="Your core strengths" required="required">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="mentorbio"><b>Bio<span class="mandatory">*</span></b></label>
												<textarea type="text" name="mentorbio" id="mentorbio" rows="5" minlength="10" maxlength="742" class="box2 form-control" placeholder="Paste your short bio" required="required"></textarea>
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="hourweek"><b>Committed no. of hours per week for mentoring<span class="mandatory">*</span></b></label>
												<input type="number" name="hourweek" id="hourweek" class="box2 form-control" placeholder="Committed no. of hours per week for mentoring" required="required">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="websitelink"><b>Website link</b></label>
												<input type="text" name="websitelink" id="websitelink" class="box2 form-control" placeholder="Website link">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="linkedin"><b>LinkedIn?<span class="mandatory">*</span></b></label>
												<input type="text" name="linkedin" id="linkedin" class="box2 form-control" placeholder="Your LinkedIn account" required="required">
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="upload"><b>Upload your photo<span class="mandatory">*</span></b></label>
												<input type="file" id="pic" name="pic" required="required"><br>
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<label for="upload"><b>Upload your CV<span class="mandatory">*</span></b></label>
												<input type="file" id="cv" name="cv" required="required"><br>
												<p><b><span class="mandatory">*</span></b> All fields are mandatory.</p>
											</div>
											<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
												<!-- <div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div> -->
												<input type="submit" name="submit" class="btn btn-warning" value="Submit">
											</div>
										</div>

									</div>
								</form>
								<!-- </div> -->
							</div>
							<br>
							<!-- </div> -->
						</div>
					</div>
				</div>
			<?php } else {
				echo $msg;
			} ?>
		</section>

		<div class="clearfix">
		</div><br>
		<!-- Section Footer ================================================== -->
		<?php
		require_once 'essentials/footer.php';
		require_once 'essentials/copyright.php';
		require_once 'essentials/js.php';
		?>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<script>
			window.onload = function() {
				var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
				recaptcha.required = true;
				recaptcha.oninvalid = function(e) {
	 // do something
	 alert("Please complete the captcha");
	}
}
</script>
</body>
</html>