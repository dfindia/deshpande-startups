<!DOCTYPE html>
<html lang="en">
<head>
   <title>Be A Part Of Our Ecosystem</title>
   <?php
   require_once 'essentials/meta.php';
   ?>
   <meta name="linkage" content="https://www.deshpandestartups.org/incubation-support-form"/>
   <meta property="og:site_name" content="Deshpande Startups"/>
   <meta property="og:type" content="website">
   <meta property="og:url" content="https://www.deshpandestartups.org/incubation-support-form">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/incubation/incubation-support-bg.png">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/incubation/incubation-services.jpg">
   <meta property="og:description" content="We invite Entrepreneurs to test, pilot and validate their ideas and build successful ventures."/>
   <meta name="author" content="Deshpande Startups"/>
   <meta name="description" content="We invite Entrepreneurs to test, pilot and validate their ideas and build successful ventures."/>
   <!-- <meta name="keywords" content=""/> -->
   <meta property="og:title" content="Be A Part Of Our Ecosystem">
   <!-- <link rel="canonical" href="https://www.deshpandestartups.org/incubation-support-form"> -->
   <?php
         // $title = 'Deshpande Startups';
   require_once 'essentials/bundle.php';
   ?>
</head>
<body>
   <?php
   require_once 'essentials/title_bar.php';
   require_once 'essentials/menus.php';
   ?>
   <br>
   <div class="container text-center">
      <h2 class=" text-yellow text-center text-uppercase Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">Be A Part Of Our</span><br> Ecosystem</h2>
      <div class="divider b-y text-yellow content-middle"></div>
      <!-- <h4 class="text-yellow">The registrations has been closed. We will get back to you soon.</h4> -->
   </div>
   <br>
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
            </iframe>
            <form role="form" action="https://docs.google.com/forms/d/e/1FAIpQLSepA3l3RIXt3qFnmQSXKyPss1KKNLXFrKpVrbwXfU8Fb4JMdg/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">
               <!-- <fieldset disabled> -->
                  <div class="row">
                     <div class="col-md-6 pad">
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
                           <label for="input1"><b>Entrepreneur name<span class="text-yellow">*</span></b></label>
                           <input type="text" name="entry.228668796" class="box2 form-control" maxlength="50" pattern="[A-Za-z\s]{1,50}" title="Mention your name" placeholder="Mention your name" required="required">
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
                           <label for="input3"><b>Mobile number<span class="text-yellow">*</span></b></label>
                           <input type="phone" name="entry.1452688954" class="box2 form-control" pattern="\d*" minlength="10" placeholder="Mention your mobile number" maxlength="10" title="Your mobile number" required="required">
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
                           <label for="input2"><b>Email address<span class="text-yellow">*</span></b></label>
                           <input type="email" name="entry.1358103933" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input5"><b>City<span class="text-yellow">*</span></b></label>
                           <input type="text" name="entry.1986024006" class="box2 form-control" placeholder="Your location" required="required">
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input4"><b>Startup name<span class="text-yellow">*</span></b></label>
                           <input type="text" name="entry.1462877651" class="box2 form-control" placeholder="Startup name" required="required">
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input99"><b>Sector<span class="text-yellow">*</span></b></label>
                           <input type="text" name="entry.1066385" class="box2 form-control" placeholder="Ex: Agri-tech, Med-tech, Edu-tech etc.." required="required">
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input77"><b>Brief about your startup idea<span class="text-yellow">*</span></b></label>
                           <textarea name="entry.939423999" class="box2 form-control" rows="5" minlength="5" maxlength="742" title="Your answer" placeholder="Describe your idea" required="required"></textarea>
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input77"><b>Number of co-founders</b></label><br>
                           <div class="row">
                              <div class="col-md-2">
                                 <label for="one"><input type="radio" name="entry.1181620906" value="1" checked="checked"> 1</label>
                              </div>
                              <div class="col-md-2">
                                 <label for="two"><input type="radio" name="entry.1181620906" value="2"> 2</label>
                              </div>
                              <div class="col-md-2">
                                 <label for="three"><input type="radio" name="entry.1181620906" value="3"> 3</label>
                              </div>
                              <div class="col-md-2">
                                 <label for="four"><input type="radio" name="entry.1181620906" value="4"> 4</label>
                              </div>
                              <div class="col-md-2">
                                 <label for="five"><input type="radio" name="entry.1181620906" value="5"> 5</label>
                              </div>
                           </div>
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input77"><b>Website</b></label>
                           <input type="url" name="entry.1369279994" class="box2 form-control" placeholder="Your website link">
                        </div>
                     </div>
                     <div class="col-md-6 pad">
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input63"><b>Are you?<span class="text-yellow">*</span></b></label><br>
                           <div class="row">
                              <div class="col-md-6">
                                 <label for="Startup Founder"><input type="radio" name="entry.2071782150" value="Startup Founder" required="required"> Startup Founder</label>
                              </div>
                              <div class="col-md-6">
                                 <label for="Aspiring entrepreneur"><input type="radio" name="entry.2071782150" value="Aspiring entrepreneur"> Aspiring entrepreneur</label>
                              </div>
                           </div>
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input7"><b>What are your expectation from Deshpande Startups?<span class="text-yellow">*</span></b></label><br>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="market"><input type="checkbox" name="entry.1492260201" value="Market in a box (access to market/early customers)" checked="checked"> Market in a box (access to market/early customers)</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="network"><input type="checkbox" name="entry.1492260201" value="Access to Mentors network (Business and Technology)"> Access to mentors network (business and technology)</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="makersLab"><input type="checkbox" name="entry.1492260201" value="Access to Makers Lab (to Build prototypes)"> Access to makers lab (to build prototypes)</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="funds"><input type="checkbox" name="entry.1492260201" value="Access to funds & investors"> Access to funds & investors</label>
                              </div>
                           </div>
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input70"><b>Which of the following best describes your progress?<span class="text-yellow">*</span></b></label><br>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="ideaStage"><input type="radio" name="entry.223580404" value="Concept stage/Idea stage" required="required"> Concept stage/Idea stage</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="product"><input type="radio" name="entry.223580404" value="Product Ready : We have commercially pilot-tested product"> Product ready : We have commercially pilot-tested product</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="revenue"><input type="radio" name="entry.223580404" value="Revenue Positive : Early transaction"> Revenue positive : Early transaction</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="growth"><input type="radio" name="entry.223580404" value="Revenue Positive : Growth Stage"> Revenue positive : Growth stage</label>
                              </div>
                           </div>
                        </div>
                           <!-- <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                              <label for="input77"><b>Please provide a brief background of the founding team (200 words)<span class="text-yellow">*</span></b></label>
                              <textarea name="entry.600217048" class="box2 form-control" rows="8" minlength="5" maxlength="742" title="Your answer" placeholder="Please include educational background and last 2 companies and designations" required="required"></textarea>
                           </div> -->
                           <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                              <label for="input6"><b>Are you incorporated?<span class="text-yellow">*</span></b></label><br>
                              <div class="row">
                                 <div class="col-md-6">
                                    <label for="yes"><input type="radio" name="entry.1643509216" value="Yes" required="required"> Yes</label>
                                 </div>
                                 <div class="col-md-6">
                                    <label for="no"><input type="radio" name="entry.1643509216" value="No"> No</label>
                                 </div>
                              </div>
                              <div class="row txbx1" style="display: none">
                                 <div class="form-group col-md-12">
                                    <label for="input4"><b>Select the date you incorporated</b></label>
                                    <input type="date" name="entry.1878909991" class="box2 form-control" placeholder="Date you incorporated">
                                 </div>
                              </div>
                           </div>

                           <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                              <!-- <label for="input87"><b>Are you willing to relocate to Hubballi - Karnataka?<span class="text-yellow">*</span></b></label><br> -->
                              <label for="input87"><b>Does your product/solution address non-metro market?<span class="text-yellow">*</span></b></label><br>
                              <div class="row">
                                 <div class="col-md-4">
                                    <label for="yes"><input type="radio" name="entry.1954998272" value="Yes" required="required"> Yes</label>
                                 </div>
                                 <div class="col-md-4">
                                    <label for="no"><input type="radio" name="entry.1954998272" value="No"> No</label>
                                 </div>
                                 <div class="col-md-4">
                                    <label for="Maybe"><input type="radio" name="entry.1954998272" value="May be"> May be</label>
                                 </div>
                              </div>
                           </div>

                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input6"><b>Where did you hear about us?<span class="text-yellow">*</span></b></label><br>
                           <div class="row">
                              <div class="col-md-6">
                                 <label for="whatsapp"><input type="radio" name="entry.1030535089" value="Whatsapp" required="required"> Whatsapp</label>
                              </div>
                              <div class="col-md-6">
                                 <label for="Newsletters"><input type="radio" name="entry.1030535089" value="Email News Letter"> Email news letter</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <label for="SocialMedia"><input type="radio" name="entry.1030535089" value="Social Media (FB, LinkedIN, Twitter)"> Social media</label>
                              </div>
                              <div class="col-md-6">
                                 <label for="PrintMedia"><input type="radio" name="entry.1030535089" value="Print Media"> Print media</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <label for="Personal"><input type="radio" name="entry.1030535089" value="Personal Reference"> Personal reference</label>
                              </div>
                              </div>

                              <div class="row txbx2" style="display: none">
                                 <div class="form-group col-md-12">
                                    <input type="text" name="entry.1159744617" class="box2 form-control" placeholder="Referred by?">
                                 </div>
                              </div>
                           </div>

                           <div class="form-group col-lg-12">
                              <!-- <span class="text-yellow"><b>*</b></span>
                                 <div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div> -->
                                 <span class="text-yellow">
                                    <h6><b>*</b> Fields are mandatory</h6>
                                 </span>
                                 <input type="submit" class="btn btn-warning" id="ss-submit" name="submit" value="Submit">
                              </div>
                           </div>
                        </div>
                        <!-- </fieldset> -->
                     </form>
                  </div>
               </div>
            </div>
            <br>
            <script src='https://www.google.com/recaptcha/api.js'></script>
            <?php
            require_once 'essentials/footer.php';
            require_once 'essentials/copyright.php';
            require_once 'essentials/js.php';
            ?>
            <script type="text/javascript">
               $(function() {
                  $('[name="entry.1643509216"]').on('click', function (e) {
                     var val = $(this).val();
                     if (val == "Yes") {
                        $('.txbx1').show('fade');
                     }else {
                        $('.txbx1').hide();
                     };
                  });
               });
            </script>
            <script type="text/javascript">
               $(function() {
                  $('[name="entry.1030535089"]').on('click', function (e) {
                     var val = $(this).val();
                     if (val == "Personal Reference") {
                        $('.txbx2').show('fade');
                     }else {
                        $('.txbx2').hide();
                     };
                  });
               });
            </script>
            <script>
               window.onload = function() {
                var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
                recaptcha.required = true;
                recaptcha.oninvalid = function(e) {
         // do something
         alert("Please complete the captcha");
      }
   }
</script>

         </body>
         </html>