<!DOCTYPE html>
<html lang="en">
<head>
   <title>Funding | Deshpande Startups</title>
   <?php
   require_once 'essentials/meta.php';
   ?>
   <meta name="linkage" content="https://www.deshpandestartups.org/funding"/>
   <meta property="og:site_name" content="Deshpande Startups"/>
   <meta property="og:type" content="website">
   <meta property="og:url" content="https://www.deshpandestartups.org/funding">
  <!--  <meta property="og:image" content="https://www.deshpandestartups.org/img/funded-startups/microchip-payments.jpg"> -->
   <meta property="og:description" content="We provides funding support to the eligible Innovators/startups through National Initiative for Developing and Harnessing Innovations (NIDHI)."/>
   <meta name="author" content="Deshpande Startups"/>
   <meta name="description" content="We provides funding support to the eligible Innovators/startups through National Initiative for Developing and Harnessing Innovations (NIDHI)."/>
   <!-- <meta name="keywords" content=""/> -->
   <meta property="og:title" content="Funding - Deshpande Startups">
   <link rel="canonical" href="https://www.deshpandestartups.org/funding">
   <?php
         // $title = 'Deshpande Startups';
   require_once 'essentials/bundle.php';
   ?>
   <style type="text/css">
     .cal{
      font-family: calibri;
   }
</style>
</head>
<body>
   <?php
   require_once 'essentials/title_bar.php';
   require_once 'essentials/menus.php';
   ?>

   <div class="container">
      <br>
      <div class="center wow fadeInDown">
         <h2 class="text-yellow text-center">FUNDING</h2>
         <div class="divider b-y text-yellow content-middle"></div>
      </div><br>
      <!-- <h4  class="text-yellow cal">NIDHI-Seed Support System (NIDHI-SSS)</h4> -->
      <p class="text-justify wow slideInLeft">Are you an Innovator/Startup entrepreneur? looking for funds to convert your Idea to prototype or seed fund to scale your business.</p>
      <p class="text-justify wow slideInLeft">
      Deshpande startups help innovators/entrepreneurs to raise funds and does in house funding for different stage startups.</p>

      <p class="text-justify wow slideInLeft">We provides funding support to the eligible Innovators/startups through National Initiative for Developing and Harnessing Innovations (NIDHI), it is an umbrella programme conceived and developed by the Innovation & Entrepreneurship division, Department of Science & Technology, Government of India, for nurturing ideas and innovations (knowledge-based and technology-driven) into successful startups. The programme would work in line with the national priorities and goals and its focus would be to build an innovation-driven entrepreneurial ecosystem with an objective of socio-economic development through wealth and job creation. NIDHI aims to nurture start-ups through scouting, supporting and scaling of innovations.
      </div>


   <!-- <div class="container">
      <br>
      <div class="center wow fadeInDown">
         <h2 class="text-yellow text-center">FUNDED<span class="text-muted"> STARTUPS</span> </h2>
         <div class="divider b-y text-yellow content-middle"></div>
      </div>
     
   </div>
--> 

<div class="featured-bg-container cal">
  <div class="col-md-12">
   <div class="row justify-content-md-center">
    <div class="col-md-4">
     <div class="text-center wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
      <h5>NIDHI-Seed Support System (NIDHI-SSS)</h5>
      <a href="dst-nidhi-sss" class="btn btn-md btn-warning px-5" target="_blank"> Know more</a>
   </div>
</div>
<div class="col-md-4">
  <div class="text-center wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
   <h5>NIDHI PRAYAS</h5>
   <a href="nidhi-prayas" class="btn btn-md btn-warning px-5" target="_blank"> Know more</a>
</div>
</div>
</div>
</div>
</div>

  <!-- <br>
    <br> -->

    <?php
    require_once 'essentials/footer.php';
    require_once 'essentials/copyright.php';
    require_once 'essentials/js.php';
    ?>
 </body>
 </html>