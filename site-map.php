<!doctype html>
<html lang="en">
<head>
	<title>Sitemap - Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/site-map"/>
	<meta name="author" content="Deshpande Startups"/>
	<meta property="og:title" content="Deshpande Startups">
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/site-map">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/deshpande-bg-new.jpg">
	<meta property="og:description" content="An eco-system of Resources, Connections, Knowledge & Talent to support mission driven entrepreneurs to scale in their venture."/>
	<meta name="description" content="An eco-system of Resources, Connections, Knowledge & Talent to support mission driven entrepreneurs to scale in their venture."/>
	<!-- <meta name="keywords" content="Business, business ideas, How to make a business, Incubation center, Tejas networks, female entrepreneurs, How to be an entrepreneur, How to become an entrepreneur, The sandbox"/> -->
	<link rel="canonical" href="https://www.deshpandestartups.org/site-map">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	.center {
		text-align: center;
		padding-bottom: 5px;
	}
	.table {
		width: 100%;
		margin-bottom: 20px;
		/*background-color: transparent;*/
		/*background-color: #dff0d8;*/
	}
	 td a {
		text-decoration: none !important;
		font-size: 16px;
	}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<div class="container-fluid">
		<br>
		<div class="center">
			<h2 class=" text-yellow"><i class="fa fa-map fa-4lg"></i> SITEMAP</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="pt-3">
			<table  class="table table-responsive-md table-responsive-sm table-striped table-hover center" width="200" border="0">
				<tr class="success center">
					<td><a href="index" target="_blank"><i class="fa fa-home"></i> Home</a></td>
					<!-- <td><a href="about-us"><i class="fa fa-ils"></i> About us</a></td> -->
					<!-- <td><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-university" aria-hidden="true"></i> About us</a> </td> -->
					<td><a href="#"><i class="fa fa-university" aria-hidden="true"></i> About us</a> </td>
					<td><a href="#"><i class="fa fa-tasks" aria-hidden="true"></i> Programs</a></td>
					<td><a href="esdm-cluster" target="_blank"><i class="fa fa-microchip"></i> ESDM Cluster</a></td>
					<td><a href="makers-lab" target="_blank"><i class="fa fa-cogs"></i> Makers Lab</a></td>
					<td><a href="#"><i class="fa fa-film" aria-hidden="true"></i> Media</a> </td>
					<td><a href="events" target="_blank"><i class="fa fa-calendar"></i> Events</a></td>
					<td><a href="#" target="_blank"><i class="fa fa-user"></i> Career</a></td>
					<td><a href="contact-us" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i> Contact Us</a></td>
				</tr>
				<tr class="center">
					<td></td>
					<td><i class="fa fa-long-arrow-down"></i></td>
					<td class="center"><i class="fa fa-long-arrow-down"></i></td>
					<td></td>
					<td></td>
					<td><i class="fa fa-long-arrow-down"></i></td>
					<td></td>
					<td><i class="fa fa-long-arrow-down"></i></td>
					<td></td>
				</tr>
				<tr class="success  center">
					<td></td>
					<td><a href="about-us" target="_blank"><i class="fa fa-ils"></i></i> Who We Are</a></td>
					<td><a href="incubation-support" target="_blank"><i class="fa fa-universal-access" aria-hidden="true"></i> Incubation</a></td>
					<td></td>
					<td></td>
					<td><a href="news" target="_blank"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Media Coverage</a></td>
					<td></td>
					<td><a href="career" target="_blank"><i class="fa fa-user"></i> At Deshpande Startups</a></td>
					<td></td>
				</tr>
				<tr class="center">
					<td></td>
					<td><i class="fa fa-long-arrow-down"></i></td>
					<td><i class="fa fa-long-arrow-down"></i></td>
					<td></td>
					<td></td>
					<td><i class="fa fa-long-arrow-down"></i></td>
					<td></td>
					<td><i class="fa fa-long-arrow-down"></i></td>
					<td></td>
				</tr>
				<tr class="success center">
					<td></td>
					<td><a href="mentors" target="_blank"><i class="fa fa-users"></i> Mentors</a></td>
					<td><a href="edge" target="_blank"><i class="fa fa-users"></i> EDGE</a></td>
					<td></td>
					<td></td>
					<td><a href="https://justaskdesh.com/" rel="nofollow" target="_blank"><i class="fa fa-info-circle" aria-hidden="true"></i> Insights from Desh</a></td>
					<td></td>
					<td><a href="career-startup" target="_blank"><i class="fa fa-user"></i> At Incubated Startups</a></td>
					<td></td>
				</tr>
				<tr class="center">
					<td></td>
					<td></td>
					<!-- <td><i class="fa fa-long-arrow-down"></i></td> -->
					<td><i class="fa fa-long-arrow-down"></i></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr class="success center">
					<td></td>
					<td></td>
					<!-- <td><a href="#" target="_blank"><i class="fa fa-snowflake-o" aria-hidden="true"></i> Startup-Stories</a></td> -->
					<td><a href="yuva-entrepreneurship" target="_blank"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Yuva Entrepreneurship</a></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr class="center">
					<td></td>
					<td></td>
					<td><i class="fa fa-long-arrow-down"></i></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr class="success center">
					<td></td>
					<td></td>
					<td><a href="funding" target="_blank"><i class="fa fa-money" aria-hidden="true"></i> Funding</a></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr class="center">
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<!-- <tr class="success center">
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr> -->
			</table>
			<br>
		</div>
		<br>
	</div>
	<!-- <div class="clearfix"></div> -->
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>