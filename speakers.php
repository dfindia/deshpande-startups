<!DOCTYPE html>
<html lang="en">
<head>
   <title>Speakers | Deshpande Startups - Incubation speakers</title>
   <?php
   require_once 'essentials/meta.php';
   ?>
   <meta name="linkage" content="https://www.deshpandestartups.org/speakers"/>
   <meta property="og:site_name" content="Deshpande Startups"/>
   <meta property="og:type" content="website">
   <meta property="og:url" content="https://www.deshpandestartups.org/speakers">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/mentors/gururaj.png">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/inauguration/amitabh-kant.png">
   <meta property="og:description" content="Speakers will enrich your ideas, train you the current trends, navigate the right approach, help you in strategizing your business."/>
   <meta name="author" content="Deshpande Startups"/>
   <meta name="description" content="Speakers will enrich your ideas, train you the current trends, navigate the right approach, help you in strategizing your business."/>
      <!-- <meta name="keywords" content="Sushil vachani, Hemang dave, Raju reddy, Phanindra sama, Ravi Narayan, Kattayil rajinish menon, Anand talwai, Ram ramnathan, Vivek pawar CEO deshpande foundation india, Sasisekar krish Founde and CEO nanoPix, Siva ramamoorthy, Anup akkihal, Founder, NEXT IN - Growth Partners to Entrepreneurs, Raj Melville executive directory
         deshpande foundation, Jayashri murali"/> -->
         <meta property="og:title" content="Deshpande startups speakers">
         <link rel="canonical" href="https://www.deshpandestartups.org/speakers">
         <?php
         // $title = 'Deshpande Startups';
         require_once 'essentials/bundle.php';
         ?>
      </head>
      <body>
         <?php
         require_once 'essentials/title_bar.php';
         require_once 'essentials/menus.php';
         ?>
         <img class="carousel-inner img-fluid" src="img/mentors/deshpande-bg-mentors.jpg" width="1349" height="198" alt="Deshpande Startups, mentors">
         <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end">
               <li class="breadcrumb-item"><a href="index">Home</a></li>
               <li class="breadcrumb-item"><a href="inauguration">Inauguration</a></li>
               <li class="breadcrumb-item active" aria-current="page">Speakers</li>
            </ol>
         </nav>
         <div class="container">
            <div class="center wow fadeInDown">
               <h2 class="text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">GUEST</span> SPEAKERS</h2>
               <div class="divider b-y text-yellow content-middle"></div>
               <br>
               <!-- <p>Mentors will enrich your ideas, train you the current trends, navigate the right approach, help you in strategizing your business and also monitor your journey.</p> -->
            </div>
         </div>
         <!-- <br> -->
         <div class="container">
            <div class="row text-center">
               <!---first row-->
               <!--  <div class="col-md-1"></div> -->
               <div class="col-md-2">
                  <img src="img/speakers/k-j-george.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Mr. K J George, Honorable IT BT minister, Govt. of Karnataka">
                  <p class="text-center text-yellow"><b>K J George</b>
                     <br>
                     <small class="text-muted">Honorable IT BT minister,<br> Govt. of Karnataka</small>
                  </p>
               </div>
               <div class="col-md-2">
                  <img src="img/speakers/gaurav-gupta.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Gaurav Gupta, Principal Secretary, Department of IT BT, Govt. of Karnataka">
                  <p class="text-center text-yellow"><b>Gaurav Gupta</b>
                     <br>
                     <small class="text-muted">Principal Secretary, Department of IT BT, Govt. of Karnataka</small>
                  </p>
               </div>
               <div class="col-md-2">
                  <img src="img/speakers/phani.jpg" width="160px" height="560px" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Phanindra Sama, Co-Founder Redbus & Chief Innovation Officer, Telangana">
                  <p  class="text-yellow text-center"><b>Phanindra Sama</b>
                     <br>
                     <small class="text-muted">Co-Founder Redbus & Chief Innovation Officer, Telangana</small>
                  </p>
               </div>
               <div class="col-md-2">
                  <img src="img/speakers/manish-anand.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Manish Anand, Founder, Provenio Capital & Utilis Capital">
                  <p class="text-center text-yellow"><b>Manish Anand</b>
                     <br>
                     <small class="text-muted">Founder, Provenio Capital & Utilis Capital</small>
                  </p>
               </div>
               <div class="col-md-2">
                  <img src="img/speakers/kv-anand.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Anand K V, Ex. Senior Vice President, Flipkart">
                  <p class="text-center text-yellow"><b>Anand K V</b>
                     <br>
                     <small class="text-muted">Ex. Senior Vice President, Flipkart</small>
                  </p>
               </div>
               <div class="col-md-2">
                  <img src="img/speakers/ravi-narayan.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Ravi Narayana, Mentor Microsoft">
                  <p  class="text-yellow text-center"><b>Ravi Narayana</b>
                     <br>
                     <small class="text-muted">Mentor, Microsoft</small>
                  </p>
               </div>
            </div>
            <!--first row-->
            <br/>
            <!---second row-->
            <div class="row text-center">
               <div class="col-md-2">
                  <img src="img/speakers/raj-belgaumkar.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Raj Belgaumkar, Chairman, KLS IMER">
                  <p class="text-center text-yellow"><b>Raj Belgaumkar</b>
                     <br>
                     <small class="text-muted">Chairman, KLS IMER</small>
                  </p>
               </div>
               <div class="col-md-2">
                  <img src="img/speakers/anand-sankeshwar.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Anand sankeshwar, CEO, VRL">
                  <p class="text-center text-yellow"><b>Anand sankeshwar</b>
                     <br>
                     <small class="text-muted">CEO, VRL</small>
                  </p>
               </div>
               <div class="col-md-2">
                  <img src="img/speakers/santosh.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Santosh Hurlikopi, Investor Astrac Ventures">
                  <p class="text-center text-yellow"><b>Santosh Hurlikopi</b>
                     <br>
                     <small class="text-muted">Investor, Astrac Ventures</small>
                  </p>
               </div>
               <div class="col-md-2">
                  <img src="img/speakers/rajiv-upadyay.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Rajiv Updahyay, Utilis Capital">
                  <p class="text-center text-yellow"><b>Rajiv Updahyay</b>
                     <br>
                     <small class="text-muted">Utilis Capital</small>
                  </p>
               </div>
               <div class="col-md-2">
                  <img src="img/speakers/seshu.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Dr. Seshu, Director IIT Dharwad">
                  <p class="text-center text-yellow"><b>Dr. Seshu</b>
                     <br>
                     <small class="text-muted">Director IIT, Dharwad</small>
                  </p>
               </div>
               <div class="col-md-2">
                  <img src="img/speakers/rajeev-prakash.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Rajeev Prakash, Director, Deshpande Startups">
                  <p class="text-center text-yellow"><b>Rajeev Prakash</b>
                     <br>
                     <small class="text-muted">Director, Deshpande Startups</small>
                  </p>
               </div>
            <!-- <div class="col-md-2">
               <img src="img/speakers/arvind-melligeri.jpg" width="160px" height="560px" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Arvind Meligeri, MD, AEQUES">
               <p  class="text-yellow text-center"><b>Arvind Meligeri</b>
               <br>
               <small class="text-muted">MD, AEQUES</small>
               </p>
            </div> -->
         </div>
         <!--second row-->
         <br>
         <div class="row text-center">
            <!---third row-->
            <!--  <div class="col-md-1"></div> -->
            <div class="col-md-2">
               <img src="img/speakers/ravi-linganuri.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Ravi Linganuri, Early Stage Investor & Advisory">
               <p class="text-center text-yellow"><b>Ravi Linganuri</b>
                  <br>
                  <small class="text-muted">Early Stage Investor & Advisory</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/bala-girisaballa.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Bala Girisbala, President, Techstars">
               <p class="text-center text-yellow"><b>Bala Girisbala</b>
                  <br>
                  <small class="text-muted">President, Techstars</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/chetan-anand.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Chetan Anand, Director, PWC">
               <p class="text-center text-yellow"><b>Chetan Anand</b>
                  <br>
                  <small class="text-muted">Director, PWC</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/sandeep-agarwal.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Sandeep Agarwal, VP, Happiest Minds">
               <p class="text-center text-yellow"><b>Sandeep Agarwal</b>
                  <br>
                  <small class="text-muted">VP, Happiest Minds</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/rk-patil.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="R K Patil, CEO, Sankya lab">
               <p class="text-center text-yellow"><b>R K Patil</b>
                  <br>
                  <small class="text-muted">CEO, Sankya lab</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/jayant.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Jayant Hummarwadi, MD, Akshoka IRON Works">
               <p class="text-center text-yellow"><b>Jayant Hummarwadi</b>
                  <br>
                  <small class="text-muted">MD, Akshoka IRON Works</small>
               </p>
            </div>
            <!-- <div class="col-md-2">
               <img src="img/speakers/praveen-roy.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Praveen Roy, Scientist, DST, Govt. of India">
               <p class="text-center text-yellow"><b>Praveen Roy</b>
               <br>
               <small class="text-muted">Scientist, DST, Govt. of India</small>
               </p>
            </div> -->
         </div>
         <!--third row-->
         <!-- <br/> -->
         <div class="row text-center">
            <!---second row-->
            <div class="col-md-2">
               <img src="img/speakers/lr-bhat.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="L R Bhat, Executive Director, VRL">
               <p class="text-center text-yellow"><b>L R Bhat</b>
                  <br>
                  <small class="text-muted">Executive Director, VRL</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/chirant-patil.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Chirant Patil, Investor Mentor">
               <p class="text-center text-yellow"><b>Chirant Patil</b>
                  <br>
                  <small class="text-muted">Investor Mentor</small>
               </p>
               <!-- <p class="mb-10">Co-founder, redBus.in </p> -->
            </div>
            <div class="col-md-2">
               <img src="img/speakers/manish-chowdary.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Manish Choudray, Tally Solutions">
               <p class="text-center text-yellow"><b>Manish Choudray</b>
                  <br>
                  <small class="text-muted">Tally Solutions</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/manoj-kumar.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Manoj Kumar, Trustee, TATA Trust">
               <p class="text-center text-yellow"><b>Manoj Kumar</b>
                  <br>
                  <small class="text-muted">Trustee, TATA Trust</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/muthu-singaram.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Muthu Singaram, CEO, IIT Madras HTIC Incubator">
               <p class="text-center text-yellow"><b>Muthu Singaram</b>
                  <br>
                  <small class="text-muted">CEO, IIT Madras HTIC Incubator</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/vijay-kumar.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Vijay Kumar, CEO, Mygate">
               <p class="text-center text-yellow"><b>Vijay Kumar</b>
                  <br>
                  <small class="text-muted">CEO, Mygate</small>
               </p>
            </div>
         </div>
         <!--second row-->
         <br/>
         <!---third row-->
         <div class="row text-center">
            <div class="col-md-2">
               <img src="img/speakers/prasanna.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Prassanna, Dean, IIT Dharwad">
               <p class="text-center text-yellow"><b>Prassanna</b>
                  <br>
                  <small class="text-muted">Dean, IIT Dharwad</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/rajeev-brigade.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Rajeev Nair, Head Operations, brigade REAP">
               <p class="text-center text-yellow"><b>Rajeev Nair</b>
                  <br>
                  <small class="text-muted">Head Operations, brigade REAP</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/harkesh-mittal.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Harikesh Mittal, Advisor, Ministry of Science & Technology">
               <p class="text-center text-yellow"><b>Harikesh Mittal</b>
                  <br>
                  <small class="text-muted">Advisor, Ministry of Science & Technology</small>
               </p>
               <!-- <p class="mb-10">Global Director, Microsoft Accelerator</p> -->
            </div>
            <div class="col-md-2">
               <img src="img/speakers/jitender-chadda.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Jitendra Chaddah, Vice Chairman & Senior Director, IESA & SLN Technologies Pvt. Ltd">
               <p class="text-center text-yellow"><b>Jitendra Chaddah</b>
                  <br>
                  <small class="text-muted">Vice Chairman & Sr. Director, IESA & SLN Technologies Pvt. Ltd</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/girendra-kasmalkar.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Gireendra Kasmalkar, Founder Director, Ideas to Impact Innovations Pvt. Ltd">
               <p class="text-center text-yellow"><b>Gireendra Kasmalkar</b>
                  <br>
                  <small class="text-muted">Founder Director, Ideas to Impact Innovations Pvt. Ltd</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/anil-muniswami.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Anil Kumar Muniswamy, Chairman & MD, IESA & SLN Technologies Pvt. Ltd.">
               <p  class="text-yellow text-center"><b>Anil Kumar Muniswamy</b>
                  <br>
                  <small class="text-muted">Chairman & MD, IESA & SLN Technologies Pvt. Ltd.</small>
               </p>
            </div>
         </div>
         <!--third row-->
         <!---fourth row-->
         <div class="row text-center">
            <div class="col-md-2">
               <img src="img/speakers/shyam-vasudevrao.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Shyam Vasudevarao, Founder, Director Forus Health Pvt. Ltd.">
               <p class="text-center text-yellow"><b>Shyam Vasudevarao</b>
                  <br>
                  <small class="text-muted">Founder, Director Forus Health Pvt. Ltd.</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/raj-kumar.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Raj Kumar Srivastav, Advisor, IT BT Minister">
               <p class="text-center text-yellow"><b>Raj Kumar Srivastav</b>
                  <br>
                  <small class="text-muted">Advisor, IT BT Minister</small>
               </p>
            </div>
            <div class="col-md-2">
               <img src="img/speakers/kartik-padmanabhan.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Karthik Padmananbhan, Relationship Manager, Google">
               <p class="text-center text-yellow"><b>Karthik Padmananbhan</b>
                  <br>
                  <small class="text-muted">Relationship Manager, Google</small>
               </p>
            </div>
            <!-- <div class="col-md-2">
               <img src="img/mentors/sasi-shekar.jpg" width="160" height="165" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Santosh Hurlikopi, Astrac Ventures">
               <p class="text-center text-yellow"><b>Santosh Hurlikopi</b>
               <br>
               <small class="text-muted">Astrac Ventures</small>
               </p>
            </div> -->
         </div>
         <br/>
         <!--fourth row-->
      </div>
      <?php
      require_once 'essentials/footer.php';
      require_once 'essentials/copyright.php';
      require_once 'essentials/js.php';
      ?>
   </body>
   </html>