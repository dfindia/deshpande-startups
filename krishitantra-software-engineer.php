<!DOCTYPE html>
<html lang="en">
<head>
	<title>Software Engineer (System Integration) - Krishitantra</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/krishitantra-software-engineer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/krishitantra-software-engineer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/krishitantra-big.png">
	<meta property="og:description" content="We are looking for Software Engineer. Job Position: Software Engineer, Experience:  Freshers, Qualification: B.E. in Electrical and Electronics/ Electronics and Communication."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Software Engineer. Job Position: Software Engineer, Experience:  Freshers, Qualification: B.E. in Electrical and Electronics/ Electronics and Communication."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Software Engineer, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/krishitantra-software-engineer">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Software</span> Engineer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class=""><strong>Job Position:</strong> Software Engineer (System Integration)<br>
					<strong>Startup:</strong> Krishitantra (Klonec Automation Systems Pvt. Ltd.)<br>
					<strong>Qualification:</strong> B.E. in Electrical and Electronics/Electronics and Communication<br>
					<strong>Experience:</strong> Freshers<br>
					<strong>Skills:</strong> System/Firmware engineer with code integration capabilities and troubleshoot our rapidly expanding infrastructure<br>
					<strong>Job Location:</strong> Udupi<br>
				</p>
			</div>
			<div class="col-md-6">
				<img src="img/career/krishitantra-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, Krishitantra"/>
			</div>
		</div>
		<!-- <br> -->

		<div class="row pt-2">
			<div class="col-md-12">
				<h3 class="text-yellow">Job Description:</h3>
				<ul>
					<li>Developing web application in python framework (djungo web framework)</li>
					<li>Linux internals knowledge for setting up the system (shell scripting, shell commands)</li>
					<li>Knowledge about the internals of raspberry pi or equivalent embedded board</li>
					<li>Integrating raspberry pi or equivalent board (eg: up board from intel) with microcontroller</li>
					<li>Interfacing local web application with the cloud via rest api</li>
					<li>Parallel programming (working with multiple process, multiple thread, synchronization techniques etc)</li>
					<li>Knowledge in basic image processing technique, rest api and work experience in aws/gcp cloud is an added advantage</li>
				</ul>
			</div>
		</div>

		<!-- <br> -->
	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to<br> E:<a href="mailto: sandeep&#046;kondaji&#064;krishitantra&#046;com"> sandeep&#046;kondaji&#064;krishitantra&#046;com</a></b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>