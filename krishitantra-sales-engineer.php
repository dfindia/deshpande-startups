<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sales Engineer - Krishitantra</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/krishitantra-sales-engineer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/krishitantra-sales-engineer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/krishitantra-big.png">
	<meta property="og:description" content="We are looking for Sales Engineer. Job Position: Sales Engineer, Experience:  Freshers, Qualification: Graduate from any Agri discipline."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Sales Engineer. Job Position: Sales Engineer, Experience:  Freshers, Qualification: Graduate from any Agri discipline."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Sales Engineer, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/krishitantra-sales-engineer">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Sales</span> Engineer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Sales Engineer<br>
					<strong>Startup:</strong> Krishitantra (Klonec Automation Systems Pvt. Ltd.)<br>
					<strong>Qualification:</strong> Graduate from any Agri discipline<br>
					<strong>Experience:</strong> Freshers<br>
					<strong>Job Location:</strong> Hubballi<br>
				</p>
			</div>
			<div class="col-md-6">
				<img src="img/career/krishitantra-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, Krishitantra"/>
			</div>
		</div>
		<!-- <br> -->

		<div class="row pt-2">
			<div class="col-md-12">
				<h3 class="text-yellow">Job Description:</h3>
				<ul>
					<li>Prepare and deliver technical presentations explaining products or services to customers and prospective customers</li>
					<li>Confer with customers and engineers to assess equipment needs and to determine system requirements</li>
					<li>Collaborate with sales teams to understand customer requirements and provide sales support</li>
					<li>Secure and renew orders and arrange delivery</li>
					<li>Plan and modify products to meet customer needs</li>
					<li>Help clients solve problems with installed equipment</li>
					<li>Recommend improved materials or machinery to customers, showing how changes will lower costs or increase production</li>
					<li>Interface between Agri universities and technical team</li>
				</ul>
			</div>
		</div>

		<!-- <br> -->
	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to<br> E:<a href="mailto: sandeep&#046;kondaji&#064;krishitantra&#046;com"> sandeep&#046;kondaji&#064;krishitantra&#046;com</a></b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>