<!DOCTYPE html>
<html lang="en">
<head>
	<title>Incubation Support - Are you looking for Incubation support?</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/incubation-support"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/incubation-support">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/incubation/incubation-support-bg.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/incubation/incubation-services.jpg">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/incubation/incubation-impact.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/incubation/journey.png">
	<meta property="og:description" content="We invite Entrepreneurs to test, pilot and validate their ideas and build successful ventures. Startups can leverage an ecosystem of resources, world-class infrastructure, mentorship from experts, networking, knowledge from industry leaders and peers, talent, and fund."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We invite Entrepreneurs to test, pilot and validate their ideas and build successful ventures. Startups can leverage an ecosystem of resources, world-class infrastructure, mentorship from experts, networking, knowledge from industry leaders and peers, talent, and fund."/>
	<!-- <meta name="keywords" content="Networking, knowledge, talent, finance and mentors, Market validation platform, Makers Lab &amp; ESDM Cluster,Industry connects, validate Business Solutions or Productsm, Mentors connects, In-House seed funding, Access to VCs, Our journey and impact"/> -->
	<meta property="og:title" content="Are you looking for Incubation support">
	<link rel="canonical" href="https://www.deshpandestartups.org/incubation-support">

	<style type="text/css">
	.vl {
		border-right: 1px solid #ec9520;
		/* border-right: 3px solid #df2d2c; */
		height: auto;
		/* height: 265px; */
	}
      #feedback a {
               display: block;
               background: #E66425;
               height: 40px;
               padding-top: 5px;
               width: 155px;
               text-align: center;
               color: #fff;
               font-family: Arial, sans-serif;
               /*font-size: 17px;*/
               font-weight: bold;
               text-decoration: none;
            }

            #feedback {
               height: 0px;
               width: 0px;
               /*width: 85px;*/
               position: fixed;
               /*right: 0;*/
               top: 50%;
               z-index: 998;
               transform: rotate(-90deg);
               -webkit-transform: rotate(-90deg);
               -moz-transform: rotate(-90deg);
               -o-transform: rotate(-90deg);
            }
</style>
<?php
require_once 'essentials/bundle.php';
?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>

	<img class="carousel-inner img-fluid" src="img/incubation/incubation-support-bg.png" width="1349" height="400" alt="Deshpande Startups, Incubation Support">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Incubation Support</li>
		</ol>
	</nav>
	
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col-md-12">
				<i class="fa fa-quote-left fa-4x pb-2"></i>
				<h3 class="text-yellow wow zoomIn"><b>Deshpande Startups offers startups a strategic gateway to Bharat – rural and<br> semi-urban India which accounts for 70% of India’s population.</b></h3>
				<h3 class="wow zoomIn">Our incubation is driven by our vision and commitment to co-create solutions<br> and bring global solutions for local problems.</h3>
				<div class="divider b-y pt-2 text-yellow content-middle"></div>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-md-6 youtube-wrapper">
						<div class="embed-responsive youtube mb-3" data-embed="oIPbKS6lqW8">
							<div class="play-button"></div>
						</div>
						<div class="text-center pt-0 mt-0">
							<strong>About Deshpande Startups | Global Incubator and Accelerator</strong>
						</div>
					</div>
					<div class="col-md-6 pt-5">
						<p class="wow zoomIn text-justify pt-3">
							We invite Entrepreneurs to test, pilot and validate their ideas and build successful ventures.
						</p>
						<br>
						<p class="wow zoomIn text-justify">
							Startups can leverage an ecosystem of <b>resources, world-class infrastructure, mentorship from experts, networking, knowledge from industry leaders and peers, talent, and fund.</b>
						</p><br>
						<div class="text-center">
							<a href="https://justaskdesh.com/" class="btn btn-warning" target="_blank">Insights from Desh</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<h2 class=" text-yellow text-center wow slideInDown pt-2" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted"> BENEFITS</span> OF INCUBATION</h2>
	<div class="divider b-y text-yellow content-middle"></div>
	<img src="img/incubation/incubation-benefits.png" class="img-fluid" width="100%" height="512" alt="Deshpande startups, Benefits of Incubation">
	<!-- <img src="img/incubation/incubation-services.jpg" class="img-fluid" width="100%" height="395" alt="Deshpande startups journey"> -->
	<br>

	<h2 class="pt-4 text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted"> HOW</span> TO APPLY</h2>
	<div class="divider b-y text-yellow content-middle"></div>
	<br>
	<!-- <a href="incubation-support-form" target="_blank"><img src="img/incubation/how-to-apply.png" class="img img-fluid blog-img" width="100%" height="192" alt="Apply for Incubation, Deshpande Startups"></a>
		<br> -->

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<a href="incubation-support-form" target="_blank"><img src="img/incubation/apply.png" class="img img-fluid blog-img" width="338" height="192" alt="Apply for Incubation, Deshpande Startups"></a>
			</div>
			<div class="col-md-3">
				<img src="img/incubation/pitch.png" class="img img-fluid blog-img" width="338" height="192" alt="Pitch, Deshpande Startups">
			</div>
			<div class="col-md-3">
				<!-- <img src="img/incubation/incubation-assessment.png" class="img img-fluid blog-img" width="338" height="192" alt="Assessment, Deshpande Startups"> -->
				<img src="img/incubation/assessment.png" class="img img-fluid blog-img" width="338" height="192" alt="Assessment, Deshpande Startups">
			</div>
			<div class="col-md-3">
				<img src="img/incubation/get-incubated.png" class="img img-fluid blog-img" width="338" height="192" alt="Get Incubated, Deshpande Startups">
			</div>
		</div>
	</div>

		<div class="container">
			<!-- <br> -->
			<br>
			<div class="row justify-content-md-center">
				<div class="col-lg-10 col-md-10">
					<p class="wow zoomIn text-center"><b>
					If you are a startup with innovative solutions for rural and semi-urban India then Deshpande Startups could be your place to begin or build. Apply now to be part of our incubation program.</b>
				</p>
			</div>
		</div>
		<!-- <br> -->
		<div class="text-center">
			<a href="incubation-support-form" class="btn btn-warning" target="_blank">Apply here if you are looking for support</a>
		</div>
		<!-- </div> -->
	</div>
	<br>

	<!-- <h2 class=" text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">OUR</span> IMPACT</h2>
	<div class="divider b-y text-yellow content-middle"></div>
	<img src="img/incubation/incubation-impact.png" class="img-fluid img" width="100%" height="400" alt="Deshpande startups impact">
	<br> -->
	<!-- <br> -->
	<h2 class=" text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">OUR</span> JOURNEY</h2>
	<div class="divider b-y text-yellow content-middle"></div>
	<!-- <div class="wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"> -->
		<img src="img/incubation/journey.png" class="img-fluid img" width="100%" height="400" alt="Deshpande startups journey">
		<!-- </div> -->
		<br>
		<br>

		<h2 class=" text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">OUR INCUBATED</span> STARTUPS</h2>
		<div class="divider b-y text-yellow content-middle"></div>
		<div class="container pt-3">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6 vl">
									<p>[Startup Bharat] No internet, no problem: this Hubli-based startup is enabling digital payments over voice network</p>
									<h6><a class="text-yellow" href="img/pdf/08march2019-yourstory.pdf" target="_blank">YOURSTORY</a></h6>
								</div>
								<div class="col-md-6 vl">
									<p>[Startup Bharat] Made in Hubli: a portable device that makes hearing tests 80 percent cheaper</p>
									<h6><a class="text-yellow" href="img/pdf/22march2019-yourstory.pdf" target="_blank">YOURSTORY</a></h6>
								</div>

							</div>
							<br>
							<div class="row">
								<div class="col-md-6 vl">
									<p>This doctor built a portable machine to save newborn babies from dying of jaundice across rural India</p>
									<h6><a class="text-yellow" href="img/pdf/23jan2019-yourstory.pdf" target="_blank">YOURSTORY</a></h6>
								</div>
								<div class="col-md-6 vl">
									<p>Freshboxx: For organic vegetables and fruits</p>
									<h6><a class="text-yellow" href="img/pdf/13march2019-prajavani.pdf" target="_blank">PRAJAVANI</a></h6>
								</div>

							</div>
							<br>
							<div class="row">
								<div class="col-md-6 vl">
									<p class="pt-3">A machine for pickle business</p>
									<h6><a class="text-yellow" href="img/pdf/10april2019-prajavani.pdf" target="_blank">PRAJAVANI</a></h6>
								</div>
								<div class="col-md-6 vl">
									<p>[Startup Bharat] How healthcare startups in Tier II and III cities are solving the problem of access in their hometowns</p>
									<h6><a class="text-yellow" href="img/pdf/05june2019-yourstory.pdf" target="_blank">YOURSTORY</a></h6>
								</div>

							</div>
						</div>
						<div class="col-md-6">
							<img src="img/incubation/incubated-startup-logos.png" width="570" height="460" class="img img-fluid" alt="Deshpande Startup, Incubated Startups Logo">
							<div class="row justify-content-md-center">
								<a href="index#visitors-gallery" class="btn btn-warning m-1" target="_blank">Eminent Personalities<br> at Deshpande Startups</a>
								<a href="mentors" class="btn btn-warning m-1" target="_blank">Mentors<br> at Deshpande Startups</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<br>
		<!-- <hr class="featurette-divider-sm"> -->
	<!-- <div class="container pt-2">
		<div class="row">
			<div class="col-md-12">
				<div class="row justify-content-md-center">
							<a href="index#visitors-gallery" class="btn btn-warning m-1" target="_blank">Eminent Personalities<br> at Deshpande</a>
							<a href="mentors" class="btn btn-warning m-1" target="_blank">Mentors at<br> Deshpande</a>
				</div>
			</div>
		</div>
	</div>
	<br> -->

	<div class="featured-bg-container">
		<!-- row 1 -->
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-md-4 youtube-wrapper">
						<div class="embed-responsive youtube mb-3" data-embed="xsa-k8aEGBs">
							<div class="play-button"></div>
						</div>
						<div class="text-center mt-0">
							<strong>Deshpande Startups Hubballi</strong>
						</div>
					</div>
					<div class="col-md-4 youtube-wrapper">
						<div class="embed-responsive youtube mb-3" data-embed="wfJjC85IMEw">
							<div class="play-button"></div>
						</div>
						<div class="text-center mt-0">
							<strong>Ramanan Ramanathan <br>Speaks at Startup Dialogue 2019</strong>
						</div>
					</div>
					<div class="col-md-4 youtube-wrapper">
						<div class="embed-responsive youtube mb-3" data-embed="dv0BsAEVjho">
							<div class="play-button"></div>
						</div>
						<div class="text-center mt-0">
							<strong>T. N. Hari <br>Speaks at Startup Dialogue 2019</strong>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- row 1 -->
	</div>
            <div id="feedback">
                     <a href="incubation-support-form" target="_blank">Apply Now</a>
                  </div>
	<!-- <br> -->

	<!-- <hr class="featurette-divider-sm"> -->

	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>