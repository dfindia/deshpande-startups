<!DOCTYPE html>
<html lang="en">
<head>
	<title>Illuminate idea to execution | Events, Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/illuminate"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/illuminate">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/illuminate.jpg">
	<meta property="og:description" content="Sandbox Illuminate - Idea to Execution is a Initiative of Deshpande Startups. It is a platform for Startup Enthusiasts to nurture their ideas."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Sandbox Illuminate - Idea to Execution is a Initiative of Deshpande Startups. It is a platform for Startup Enthusiasts to nurture their ideas."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Deshpande Startups Illuminate - idea to execution">
	<link rel="canonical" href="https://www.deshpandestartups.org/illuminate">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/sandbox-bg-illuminate.jpg" width="1349" height="198" alt="Deshpande Startups, events illuminate">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Sandbox Illuminate</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">SANDBOX ILLUMINATE</span><br> IDEA TO EXECUTION</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/illuminate.jpg" width="474" height="237" alt="Sandbox startups nanopix logo">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center">Sandbox Illuminate - Idea To Execution</h5>
									<p><b>Date : </b>May 10<sup>th</sup> 2018</p>
									<!-- <p><b>Last Date to Register : </b>May 05<sup>th</sup> 2018</p> -->
									<p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br>
										E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
									</p>
								</div>
								<div class="card-footer">
									<p class="text-yellow">The registrations has been closed.</p>
									<!-- <a href="https://docs.google.com/forms/d/e/1FAIpQLSeKnlPzsiZr-cK-gxH82MXeIHkIDZstE5WaetPKmlOaZ0Sv_Q/viewform" class="btn btn-primary" target="_blank">Apply Now</a> -->
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-5 text-yellow"><b>Event Description:</b></p>
						<p class="text-justify wow slideInRight">"Sandbox Illuminate - Idea to Execution" is a Initiative of Deshpande Startups. It is a platform for Startup Enthusiasts to nurture their ideas.<br><br> Sandbox Illuminate invites the Entrepreneurs across India to come up with their ideas, present their business Plan & Win Incubation Support from Deshpande Startups.</p>
						<p class="text-justify pt-3 wow slideInRight">We at Deshpande startups (a not-for-profit, section 8 entity which is also a Central Government recognized TBI of India) support mission driven entrepreneurs to scale in their venture.<br><br> Entrepreneurs call it as a living laboratory to test their business ideas, get it validated, build successful ventures and scale to the greater heights.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<br>

	<br>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>