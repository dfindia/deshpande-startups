<!DOCTYPE html>
<html lang="en">
<head>
 <title>Makers Lab Membership</title>
 <?php
 require_once 'essentials/meta.php';
 ?>
 <meta name="linkage" content="https://www.deshpandestartups.org/membership"/>
 <meta property="og:site_name" content="Deshpande Startups"/>
 <meta property="og:type" content="website">
 <meta property="og:url" content="https://www.deshpandestartups.org/membership">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/deshpande-startups.jpg">
 <meta property="og:description" content="Membership in Makers Lab offers a direct connection to many of our services, programs and technological help for your start-ups. As a member, you will be looped into the access of 3D Design-Scan-Print, IOT and Electronics, Mechanical, designing software and have access to technical innovation, cutting-edge technology, networking opportunities, and exclusive member benefits."/>
 <meta name="author" content="Deshpande Startups"/>
 <meta name="description" content="Membership in Makers Lab offers a direct connection to many of our services, programs and technological help for your start-ups. As a member, you will be looped into the access of 3D Design-Scan-Print, IOT and Electronics, Mechanical, designing software and have access to technical innovation, cutting-edge technology, networking opportunities, and exclusive member benefits."/>
 <!-- <meta name="keywords" content=""/> -->
 <meta property="og:title" content="Makers Lab Membership">
 <link rel="canonical" href="https://www.deshpandestartups.org/membership">
 <?php
         // $title = 'Deshpande Startups';
 require_once 'essentials/bundle.php';
 ?>
 <style type="text/css">
 p a {
   text-decoration: none !important;
 }
 
.parallax {
 background-image: url("img/makers/deshpande.jpg");
 min-height: 300px; 
 background-attachment: fixed;
 background-position: center;
 background-repeat: no-repeat;
 background-size: cover;
}
.pt-6 {
  padding-top: 4rem!important;
}
</style>

</head>
<body>
 <?php
 require_once 'essentials/title_bar.php';
 require_once 'essentials/menus.php';
 ?>

 <!-- <img class="carousel-inner img-fluid" src="img/makers/internship-bg.jpg" width="1349" height="198" alt="Deshpande Startups, events IDEATHON">
 <nav aria-label="breadcrumb">
  <ol class="breadcrumb justify-content-end">
   <li class="breadcrumb-item"><a href="./">Home</a></li>
   <li class="breadcrumb-item"><a href="makers-lab">Makers Lab</a></li>
   <li class="breadcrumb-item active" aria-current="page">Membership</li>
 </ol>
</nav> -->

<div class="parallax inverse-text" data-parallax-img="img/makers/deshpande.jpg" data-parallax-img-width="1920" data-parallax-img-height="1078">
  <br>
  <div class="center wow fadeInDown pt-6">
   <h2 class="text-yellow text-center"><span class="text-white">MAKERS LAB</span> MEMBERSHIP</h2>
   <div class="divider b-y text-yellow content-middle"></div>
 </div>
</div>
<br>

<div class="container">
  <!-- <br> -->
  <div class="center wow fadeInDown">
  <!--  <h2 class="text-yellow text-center">MAKERS LAB <span class="text-muted">MEMBERSHIP</span> </h2>
   <div class="divider b-y text-yellow content-middle"></div> -->
   <h3 class="text-yellow text-center">WELCOME TO OUR COMMUNITY OF CREATIVE THINKERS AND YOUNG ENTREPRENEURS!</h3>
 </div><br>
 <!-- <div class="pull-right"><a href="membership-form" class="btn btn-warning btn-md" target="_blank">Apply Now</a></div><br> -->
 <!-- <h4  class="text-yellow cal">About membership:</h4> -->
 <div class="row">
  <div class="col-md-10">
   <p class="text-justify wow slideInLeft">Membership in Makers Lab offers a <b>direct connection to many of our services, programs and technological help</b> for your startups. As a member, you will be looped into the access of <b>3D Design-Scan-Print, IoT and electronics, mechanical, designing software</b> and have access to <b>technical innovation, cutting-edge technology, networking opportunities, and exclusive member benefits</b>.</p>
 </div>
 <div class="col-md-2">
  <!-- <div class="row justify-content-md-center"> -->
    <div class="row pl-4">
      <a href="membership-form" class="btn btn-rotate" target="_blank">Apply Now</a>
    </div>
  </div>
</div>
<br>

<h4 class="text-yellow">Membership subscription:</h4>
<p class="text-justify wow slideInLeft">Makers Lab offers their members access to different memberships that will help you stay current in the community.</p>
<ul class="text-justify wow slideInLeft">
 <li><b>Exclusively for students:</b> 1k - 6 months (only one student can access this membership)</li>
 <li><b>Startups/Professionals:</b> 10k - 1 year (two members can access to this membership)</li>
</ul>
<br>
</div>


<div class="featured-bg-container">
  <div class="col-md-12">
   <div class="row">
    <div class="col-md-6">
     <h4 class="text-yellow">Points to be noted:</h4>
     <ul class="text-justify wow slideInLeft">
      <li>Collect the Makers Lab membership card after your subscription.</li>
      <li>Make sure you have your subscription card to get access to the Lab.</li>
      <li>Subscribe again for the access post-expiry.</li>
      <li>Record the usages with us, in order to maintain history online.</li>
      <li>Payment must be done before you use any assets.</li>
      <li>Get your own raw materials, 3D printer Raw Materials available if required and will be charged for it.</li>
      <li>Assets consumed by the members will be paid by them based on their usages.</li>
    </ul>
  </div>
  <div class="col-md-6">
   <h4 class="text-yellow">Membership Benefits:</h4>
   <ul class="text-justify wow slideInLeft">
    <li>Makers Lab delivers access to the industry's most essential technical information, networking opportunities, career development programs, and many other exclusive benefits to its members.</li>
    <li>Access to sections, | 3D Printers and Scanners | IoT & Electronics | Mechanical Machineries | Fabrication | Carpentry Power Tools | Machinery Tools | Designing Software.</li>
    <li>Expertise technical support and guidance from the team to operate machines.</li>
  </ul>
</div>
</div>
</div>
</div>
<br>

 <div class="container">
   <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
    <h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">FREQUENTLY</span> ASKED QUESTIONS</h2>
    <div class="divider b-y text-yellow content-middle"></div>
  </div>
  <br>
  <div id="accordion">
    <div class="card cal card-hover-shadow">
     <div class="card-header" id="headingNine">
      <h5 class="mb-0">
       <button class="btn btn-link" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
        <h4 class="text-yellow"> When does my membership expire?</h4>
      </button>
    </h5>
  </div>
  <div id="collapseNine" class="collapse show" aria-labelledby="headingNine" data-parent="#accordion">
    <div class="card-body">
     <div class="wpb_text_column wpb_content_element ">
      <div class="wpb_wrapper cal">
       <ul class="text-justify">
        <li>For <b>Startups/Professionals</b>, 12 months from the end of the month in which your membership was processed.<br><b class="text-yellow">Example:</b> If your membership is processed on 01-01-2019, then your membership expires on 31-12-2019.</li><br>
        <li>For <b>Students</b>, 6 months from the end of the month in which your membership was processed.<br><b class="text-yellow">Example:</b> If your membership is processes on 01-02-2019, then your membership expires on 31-07-2019</li>
      </ul>
    </div>
  </div>
</div>
</div>
</div>
<!-- <div class="card cal card-hover-shadow">
 <div class="card-header" id="headingOne">
  <h5 class="mb-0">
   <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" ">
    <h4 class="text-yellow">Can i transfer my membership to someone else?</h4>
  </button>
</h5>
</div>
<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
 <div class="card-body">
  <div class="wpb_wrapper cal">
   <p class="text-justify"> 
   </p>
 </div>
</div>
</div>
</div> -->
<!-- <div class="card cal card-hover-shadow">
 <div class="card-header" id="headingTwo">
  <h5 class="mb-0">
   <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
    <h4 class="text-yellow"></h4>
  </button>
</h5>
</div>
<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
  <div class="card-body">
   <div class="wpb_wrapper cal">
    <p class="text-justify"></p>
  </div>
</div>
</div>
</div> -->
</div>
<br>
</div>
<!-- </div> -->
<!-- <div class="featured-bg-container">
   <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
    <h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">CONCERNED </span>IN CHARGE</h2>
    <div class="divider b-y text-yellow content-middle"></div>
  </div>
  <br>
    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
  <div class="row justify-content-md-center">
        <div class="col-md-4 wow slideInLeft">
         <h4>Shashidhar</h4>
         <p><i class="fa fa-id-card text-yellow" aria-hidden="true"></i>&nbsp;&nbsp; 3D Printing</p>
         <p><i class="fa fa-phone text-yellow"></i>&nbsp;&nbsp;<a class="text-white" href="tel:+91-953-540-4005"> +91-953-540-4005</a></p>
         <p><i class="fa fa-envelope text-yellow"></i>&nbsp;&nbsp; <a class="text-white" href="mailto:shashidharb&#046;sandbox&#064;dfmail&#046;org">shashidharb&#046;sandbox&#064;dfmail&#046;org</a></p>
       </div>

       <div class="col-md-4 wow slideInLeft">
         <h4>Harish</h4>
         <p><i class="fa fa-id-card text-yellow" aria-hidden="true"></i>&nbsp;&nbsp; Mechanical</p>
         <p><i class="fa fa-phone text-yellow"></i>&nbsp;&nbsp;<a class="text-white" href="tel:+91-855-336-2858"> +91-855-336-2858</a></p>
         <p><i class="fa fa-envelope text-yellow"></i>&nbsp;&nbsp; <a class="text-white" href="mailto:harish&#046;sandbox&#064;dfmail&#046;org">harish&#046;sandbox&#064;dfmail&#046;org </a></p>
       </div>

       <div class="col-md-4 wow slideInLeft">
         <h4>Shirish</h4>
         <p><i class="fa fa-id-card text-yellow" aria-hidden="true"></i>&nbsp;&nbsp; IoT & Electronics</p>
         <p><i class="fa fa-phone text-yellow"></i>&nbsp;&nbsp;<a class="text-white" href="tel:+91-809-505-1642"> +91-809-505-1642</a></p>
         <p><i class="fa fa-envelope text-yellow"></i>&nbsp;&nbsp; <a class="text-white" href="mailto:shirish&#046;sandbox&#064;dfmail&#046;org">shirish&#046;sandbox&#064;dfmail&#046;org</a></p>
       </div>
     </div>
     </div>
     </div> -->
       <!-- <br> -->
       <!-- <br> -->

       <?php
       require_once 'essentials/footer.php';
       require_once 'essentials/copyright.php';
       require_once 'essentials/js.php';
       ?>
     </body>
     </html>