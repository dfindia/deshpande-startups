<!DOCTYPE html>
<html lang="en">
<head>
	<title>Makers Lab - Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/makers-lab"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/makers-lab">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/makerslab-bg.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/makers/makers-lab-facilities.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/makers/makers-lab-benefits.png">
	<meta property="og:description" content="Makers Lab, a workspace for experimentation, fabrication and prototyping, as well as provides co-working and incubation space in makers lab"/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Makers Lab, a workspace for experimentation, fabrication and prototyping, as well as provides co-working and incubation space in makers lab"/>
	<!-- <meta name="keywords" content="Makers Lab, Lab, Exposure to technology, engineering innovation"/> -->
	<meta property="og:title" content="Deshpande Startups Makers Lab">
	<link rel="canonical" href="https://www.deshpandestartups.org/makers-lab">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		.featured-bg-container {
			color: #eee;
			background: linear-gradient(45deg, rgba(35, 36, 37, 0.59) 25%, rgba(40, 50, 74, 0.3) 25%, rgba(60, 61, 62, 0.35) 90%, rgba(45, 52, 66, 0.7) 90%), linear-gradient(-45deg, rgba(236, 240, 247, 0.28) 30%, rgba(29, 29, 39, 0.41) 30%, rgba(58, 58, 88, 0.26) 60%, rgba(33, 40, 84, 0.03) 60%, rgba(57, 57, 58, 0.64) 60%);
			background-color: #48494a;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<!-- <img data-u="image" src="img/makers/makerslab-bg2.png" width="1349" height="512" class="img img-fluid carousel-inner" alt="Deshpande startups, welcome to makers lab"> -->
	<img data-u="image" src="img/bg-home/makerslab-bg.png" width="1349" height="512" class="img img-fluid carousel-inner" alt="Deshpande startups, welcome to makers lab">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Makers Lab</li>
		</ol>
	</nav>

	<div class="container pb-5">
		<h2 class="text-yellow text-center"><strong>"Transforming Innovations &amp; Building Makers Community"</strong></h2>
	</div>
	<!-- <div class="container-fluid"> -->
		<div class="featured-bg-container">
			<div class="row featurette valign-wrapper">
				<div class="col-md-8 col-sm-6 col-lg-8 col-xs-12 text-center p-4">
					<h2 class=" wow slideInLeft text-yellow">ABOUT MAKERS LAB</h2>
					<div class="divider b-w content-middle"><span class="fs-11">...</span></div>
					<div class=" ">
						<p class="text-justify wow slideInLeft">
							<b>Makers Lab</b>, a workspace for practical exposure to <b>technology, experimentation, fabrication, and prototyping</b>. The space is intended to support product startups, foster innovative and efficient engineering projects; and nurture a community of makers within the Deshpande Startup Entrepreneurial ecosystem.
						</p>
						<p class="text-justify wow slideInLeft">Makers Lab is spread over <b>12,000 (Sq.ft)</b> square feet in Hubballi, being a first facility of its kind in North Karnataka.</p>
						<p class="text-justify wow slideInLeft"> Makers Lab aim to imbibe and inculcate the Entrepreneurial vision and Core values of Deshpande Startups.
						</p>
						<p class="text-justify wow slideInLeft">
							<b>Makers Lab is fully loaded</b> with 3D Printers & Scanners, IoT & Electronics, Mechanical section with CNC Lathe, VMC & Laser cutting/ Engraving, Conventional Machines, Carpentry, Power Tools, Fabrication & Painting.
						</p>
					</div>
				</div>
				<div class="col-md-4 pt-2">
					<img src="img/makers/makerslab-logo.png" alt="Deshpande startups, makers lab logo" width="380" height="327" class="img img-fluid">
				</div>
			</div>
			<!-- <br> -->
		</div>

		<img class="img-fluid img-responsive carousel-inner" src="img/makers/makers-lab-facilities.png" width="1349" height="512" alt="Deshpande Startups, makers lab facilities">
		<!-- <img src="img/makers/makers-lab-benefits.png" class="img-fluid img-responsive carousel-inner" width="1349" height="512" alt="Deshpande startups, makers lab benefits"> -->
	<!-- <div class="parallax inverse-text" data-parallax-img="img/makers/deshpande2.jpg" data-parallax-img-width="1920" data-parallax-img-height="1078">
		<br>
		<div id="ml-activities">
			<div class="container">
				<div class="row text-center">
					<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
						<div class="row justify-content-md-center">
							<div class="col-md-3 col-sm-6 col-lg-3 col-xs-12">
								<img src="img/makers/membership.png" width="255" height="195" class="blog-img img-fluid img pb-1" alt="Membership, Makers Lab, Activities" width="255" height="195">
								<div class="p-2">
									<a href="membership" class="btn p-1 btn-warning" target="_blank">Know More</a>
									<a href="membership-form" class="btn p-1 btn-warning" target="_blank">Apply Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<br>
	</div> -->

	<div class="featured-bg-container">
		<div class="row valign-wrapper">
			<div class="col-md-4 col-lg-4 text-center">
				<!-- <img src="img/makers/makerslab-objective.png" width="388" height="269" alt="makers lab, objectives" class="img img-fluid img-thumbnail"> -->
				<img src="img/makers/membership.png" width="255" height="195" class="blog-img img-fluid img pb-1" alt="Membership, Makers Lab, Activities" width="255" height="195">
				<div class="p-2">
					<a href="membership" class="btn p-1 btn-warning" target="_blank">Know More</a>
					<a href="membership-form" class="btn p-1 btn-warning" target="_blank">Apply Now</a>
				</div>
			</div>
			<div class="col-md-8 col-sm-6 col-lg-8 col-xs-12 p-4">
				<div class="text-center">
					<h2 class="wow slideInRight text-yellow"> OBJECTIVE OF THE MAKERS LAB</h2>
				</div>
				<div class="divider b-w content-middle text-yellow"><span class="fs-11">...</span></div>
				<ul class="wow slideInRight">
					<li>To promote and facilitate ideation product startups in experimenting, designing and building prototypes</li>
					<li>To serve as a resource centre providing professional guidance and technical support</li>
					<li>To build prototypes much faster & reduce the product development cycle</li>
					<li>To support student's ideas to prototype / PoC</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- <br> -->
	<!-- <img class="img-fluid img-responsive carousel-inner" src="img/makers/makers-lab-facilities.png" width="1349" height="512" alt="Deshpande Startups, makers lab facilities"> -->
	<!-- <br> -->


	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>