<!DOCTYPE html>
<html lang="en">
<head>
	<title>Apply for Yuva Entrepreneurship program</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/yuvaentrepreneur-form"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/yuvaentrepreneur-form">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/makers/yuvaentrepreneurship-bg.png">
	<meta property="og:description" content="Yuva Entrepreneurship is a unique program, where student communities are trained to build their Entrepreneurial mindset by creating their ideas into product."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Yuva Entrepreneurship is a unique program, where student communities are trained to build their Entrepreneurial mindset by creating their ideas into product."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Apply for Yuva Entrepreneurship program">
	<link rel="canonical" href="https://www.deshpandestartups.org/yuvaentrepreneur-form">
	<?php
			// $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	.disp{
		display:none;
	}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<br>
	<div class="container text-center">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">APPLY NOW FOR</span><br> YUVA ENTREPRENEURSHIP PROGRAM</h2>
		<div class="divider b-y text-yellow content-middle"></div>
		<!-- <h6>The registration has been closed</h6> -->
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-12 w3-card p-3">
				<iframe name="hidden_iframe" id="hidden_iframe" class="disp" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
				</iframe>
				<form role="form" action="https://docs.google.com/forms/d/e/1FAIpQLScoGWeW1AriPklQfNoL74_Zdu5rL2uo60VcD8SnP1oiJqZ5Ww/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">
					<!-- <fieldset disabled> -->
						<div class="row">
							<div class="col-md-6 pad">
								<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
									<label for="input1"><b>Name<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.963234631" class="box2 form-control" maxlength="50" pattern="[A-Za-z\s]{1,50}" title="Mention your name" placeholder="Mention your name" required="required">
								</div>
								<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
									<label for="input3"><b>Mobile number<span class="text-yellow">*</span></b></label>
									<input type="phone" name="entry.1749294617" class="box2 form-control" pattern="\d*" minlength="10" placeholder="Mention your mobile number" maxlength="10" title="Your mobile number" required="required">
								</div>
								<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
									<label for="input2"><b>Email-id<span class="text-yellow">*</span></b></label>
									<input type="email" name="entry.344233608" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
								</div>
								<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input5"><b>City<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.1835241721" class="box2 form-control" placeholder="Your location" required="required">
								</div>

								<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input7"><b>Are You?<span class="text-yellow">*</span></b></label><br>
								</div>
								<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<div class="row">
										<div class="form-group col-md-4">
											<label for="student"><input type="radio" name="entry.1036776747" value="student" required="required"> Student</label>
										</div>
										<div class="form-group col-md-4">
											<label for="Graduate"><input type="radio" name="entry.1036776747" value="Graduate"> Graduate</label>
										</div>
										<div class="form-group col-md-4">
											<label for="Professional"><input type="radio" name="entry.1036776747" value="professional"> Professional</label>
										</div>
									</div>

									<div class="row txbx1" style="display: none">
										<div class="form-group col-md-12 m-0">
											<div class="row">
												<div class="form-group col-md-6">
													<label for="input45"><b>Academic Qualification<span class="text-yellow">*</span></b></label>
													<select class="form-control" name="entry.1470326552">
														<option value="Diploma">Diploma</option>
														<option value="B.E">B.E</option>
														<option value="M.Tech">M.Tech</option>
														<option value="BBA">BBA</option>
														<option value="MBA">MBA</option>
														<option value="BCA">BCA</option>
														<option value="MCA">MCA</option>
														<option value="B.Com">B.Com</option>
														<option value="M.Com">M.Com</option>
														<option value="B.Sc">B.Sc</option>
														<option value="M.Sc">M.Sc</option>
														<option value="ITI">ITI</option>
													</select>
												</div>

												<div class="form-group col-md-6">
													<label for="input45"><b>Stream/ Branch<span class="text-yellow">*</span></b></label>
													<select class="form-control" name="entry.2091174969">
														<option value="CS" selected>CS</option>
														<option value="IS">IS</option>
														<option value="E&C">E&C</option>
														<option value="E&E">E&E</option>
														<option value="Mechanical">Mechanical</option>
														<option value="Civil">Civil</option>
														<option value="Architecture">Architecture</option>
														<option value="Bio Technology">Bio Technology</option>
														<option value="Chemical">Chemical</option>
														<option value="Aeronautical">Aeronautical</option>
														<option value="Biochemical">Biochemical</option>
														<option value="Other">Other</option>
													</select>
												</div>
											</div>
										</div>
									</div>

									<div class="row txbx2" style="display: none">
										<div class="form-group col-md-12">
											<div class="row">
												<div class="form-group col-md-12">
													<label for="input45"><b>Year<span class="text-yellow">*</span></b></label>
													<select class="form-control" name="entry.2132213656">
														<option value="-">Select...</option>
														<option value="1st Year">1<sup>st</sup> Year</option>
														<option value="2nd Year">2<sup>nd</sup> Year</option>
														<option value="3rd Year">3<sup>rd</sup> Year</option>
														<option value="4th Year">4<sup>th</sup> Year</option>
													</select>
												</div>
											</div>
										</div>
									</div>

									<div class="row txbx3" style="display: none">
										<div class="form-group col-md-12">
											<div class="row">
												<div class="form-group col-md-12">
													<label for="input45"><b>Professional In?<span class="text-yellow">*</span></b></label>
													<input type="text" name="entry.1657405517" class="box2 form-control" title="You are professional in?" placeholder="You are professional in?">
												</div>
											</div>
										</div>
									</div>
<!-- <select class="form-control" name="">
								<option value=""></option>
								<option value=""></option>
								<option value=""></option>
								<option value=""></option>
							</select> -->
						</div>


						<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input77"><b>Team members<span class="text-yellow">*</span></b></label>
							<input type="number" name="entry.1760751567" class="box2 form-control" placeholder="Number of team members" required="required">
						</div>

						<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input7"><b>What do you expect form Yuva Entrepreneurship Program?<span class="text-yellow">*</span></b></label><br>
							<div class="row">
								<div class="col-md-12">
									<label for="understand"><input type="checkbox" name="entry.1142260864" value="Understand the nuance of entrepreneurship" checked="checked"> Understand the nuance of entrepreneurship</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label for="validation"><input type="checkbox" name="entry.1142260864" value="Validation for your Startup Idea (Technically along with market validation)"> Validation for your startup idea (Technically along with market validation)</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label for="makersLab"><input type="checkbox" name="entry.1142260864" value="Access to Makers lab to Build Prototype"> Access to makers lab to build prototype</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label for="mentor"><input type="checkbox" name="entry.1142260864" value="Industry & Mentor Connect"> Industry & mentor connect</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label for="coworking"><input type="checkbox" name="entry.1142260864" value="Co-working space"> Co-working space</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 pad">
						<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input77"><b>Problem statement<span class="text-yellow">*</span></b></label>
							<input type="text" name="entry.1083505895" class="box2 form-control" placeholder="Describe the problem you are trying to solve in 50 words or less" required="required">
						</div>
						<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input77"><b>Brief about the idea<span class="text-yellow">*</span></b></label>
							<textarea type="text" name="entry.860091389" class="box2 form-control" rows="2" minlength="10" maxlength="500" placeholder="Describe your idea/product in 50 words or less" required="required"></textarea>
						</div>
						<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input16"><b>Problem Addressing Sector<span class="text-yellow">*</span></b></label>
						</div>
						<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<div class="row">
								<div class="col-md-6 form-group">
									<label for="smartcity"><input type="radio" name="entry.610667957" value="Smart City" required="required" checked="checked"> Smart City</label>
								</div>
								<div class="col-md-6 form-group">
									<label for="agriculture"><input type="radio" name="entry.610667957" value="Agriculture"> Agriculture</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 form-group">
									<label for="renewable"><input type="radio" name="entry.610667957" value="renewable energy"> Renewable Energy</label>
								</div>
								<div class="col-md-6 form-group">
									<label for="healthcare"><input type="radio" name="entry.610667957" value="Healthcare"> Healthcare</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 form-group">
									<label for="edutech"><input type="radio" name="entry.610667957" value="Edutech"> Edutech</label>
								</div>
								<div class="col-md-6 form-group">
									<label for="electronics"><input type="radio" name="entry.610667957" value="Electronis & IoT"> Electronics & IoT</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 form-group">
									<label for="WaterManagement"><input type="radio" name="entry.610667957" value="Water Management"> Water Management</label>
								</div>
								<div class="col-md-6 form-group">
									<label for="waste management"><input type="radio" name="entry.610667957" value="waste management"> Waste Management</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 form-group">
									<label for="Safety & Security"><input type="radio" name="entry.610667957" value="Safety & Security"> Safety & Security</label>
								</div>
								<div class="col-md-6 form-group">
									<label for="Software development"><input type="radio" name="entry.610667957" value="Software development"> Software Development</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 form-group">
									<label for="Automation"><input type="radio" name="entry.610667957" value="Automation"> Automation</label>
								</div>
								<div class="col-md-6 form-group">
									<label for="Transportation"><input type="radio" name="entry.610667957" value="Transportation"> Transportation</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label for="otherrr"><input type="radio" name="entry.610667957" value="Other"> Other</label>
								</div>
							</div>
						</div>
						<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input18"><b>How did you come to know about this program?<span class="text-yellow">*</span></b></label><br>
							<div class="row">
								<div class="col-md-6">
									<label for="Personal"><input type="radio" name="entry.1395211462" value="Personal Reference" required="required" checked="checked"> Personal Reference</label>
								</div>
								<div class="col-md-6">
									<label for="Newsletters"><input type="radio" name="entry.1395211462" value="Email News Letter"> Email News Letter</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label for="CollegeNotice"><input type="radio" name="entry.1395211462" value="College Notice"> College Notice</label>
								</div>
								<div class="col-md-6">
									<label for="whatsapp"><input type="radio" name="entry.1395211462" value="Whatsapp"> Whatsapp</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label for="PrintMedia"><input type="radio" name="entry.1395211462" value="Print Media"> Print Media</label>
								</div>
								<div class="col-md-6">
									<label for="SocialMedia"><input type="radio" name="entry.1395211462" value="Other Social Media"> Other Social Media</label>
								</div>
							</div>
						</div>
						<div class="form-group col-lg-12">
									  <!--  <span class="text-yellow"><b>*</b></span>
									  	<div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div> -->
									  	<span class="text-yellow">
									  		<h6><b>*</b> Fields are mandatory</h6>
									  	</span>
									  	<input type="submit" class="btn btn-warning" id="ss-submit" name="submit" value="Submit">
									  </div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<br>
				<script src='https://www.google.com/recaptcha/api.js'></script>
				<?php
				require_once 'essentials/footer.php';
				require_once 'essentials/copyright.php';
				require_once 'essentials/js.php';
				?>
				<script type="text/javascript">
					$(function() {
						$('[name="entry.1036776747"]').on('click', function (e) {
							var val = $(this).val();
							if (val == "student") {
								$('.txbx1').show('fade');
								$('.txbx2').show('fade');
								$('.txbx3').hide();
							}else if (val == 'Graduate') {
								$('.txbx1').show('fade');
								$('.txbx2').hide();
								$('.txbx3').hide();
							}else {
								$('.txbx1').show('fade');
								$('.txbx2').hide();
								$('.txbx3').show('fade');
							};
						});
					});
				</script>
				<script>
					window.onload = function() {
						var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
						recaptcha.required = true;
						recaptcha.oninvalid = function(e) {
			// do something
			alert("Please complete the captcha");
		}
	}
</script>
</body>
</html>