<!DOCTYPE html>
<html lang="en">
<head>
	<title>Events | Sandbox Uplift</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/uplift"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/uplift">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/uplift.jpg">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/uplift-bg.jpg">
	<meta property="og:description" content="We are hosting a platform for all the mission driven entrepreneurs across India to pitch their business idea to access support from Deshpande Startups in scaling their business ventures."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are hosting a platform for all the mission driven entrepreneurs across India to pitch their business idea to access support from Deshpande Startups in scaling their business ventures."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Sandbox Uplift">
	<!-- <link rel="canonical" href="https://www.deshpandestartups.org/uplift"> -->

	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	.gray{background-color: #403b3b !important;}
	.white{color:#fff;}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/uplift-bg.jpg" width="1349" height="198" alt="Deshpande Startups, events Sandbox Uplift">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Sandbox Uplift</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">SANDBOX</span> UPLIFT</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/uplift.jpg" width="474" height="237" alt="Deshpande startups, events, Sandbox Uplift">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center">Sandbox Uplift</h5>
									<p><b>Date : </b>February 28<sup>th</sup> 2019</p>
									<!-- <p><b>Last date to register : </b>May 05<sup>th</sup> 2018</p> -->
									<p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br>
										E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
									</p>
								</div>
								<div class="card-footer">
									<p class="text-yellow">The registrations has been closed.</p>
									<!-- <a href="uplift-form" class="btn btn-primary px-2" target="_blank">Apply Now</a> -->
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-5 text-yellow"><b>Event description:</b></p>
						<p class="text-justify wow slideInRight">We are hosting a platform for all the mission driven entrepreneurs across India to pitch their business idea to access support from Deshpande Startups in scaling their business ventures. As you may have been already able to witness and experience the ecosystem at Deshpande Startups during our flagship conference <b>Startup Dialogue.</b> We would like to invite you to participate in this contest to stand a chance to join our ecosystem.</p>
						<p class="text-yellow"><b>Event dates and deadlines:</b></p>
						<p class="text-justify wow slideInRight"><i class="fa fa-arrow-right text-primary" aria-hidden="true"></i> Event Date : <b>28<sup>th</sup> February 2019</b><br>
							<i class="fa fa-arrow-right text-primary" aria-hidden="true"></i> Last date to apply : <b>26<sup>th</sup> February 2019</b>
						</p>
						<br>
						<p class="text-justify wow slideInRight"><b class="text-yellow">Note :</b><br> Once the form is submitted, please send your business deck format at <a href="mailto:seir&#064;dfmail&#046;org">seir&#064;dfmail&#046;org</a> (Confirmation mail will contain the business deck format)</p>
						<p class="text-justify wow slideInRight"><b class="text-yellow">*</b> Applications will be selected on first come first serve basis.</p>
						<br>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>