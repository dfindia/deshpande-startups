<!DOCTYPE html>
<html lang="en">
<head>
	<title>3D design Contest | Events, Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php'
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/3d-contest"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/3d-contest">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/3d-contest-flyer.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/3d-contest.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/3d-contest-bg.png">
	<meta property="og:description" content="An initiative of Yuva Entrepreneurship Program of Deshpande Startups presents 3D Design Contest 3D Design Contest is an exciting opportunity for all Enthusiastic students to participate virtually and design solutions for socially impactful problem statements"/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="An initiative of Yuva Entrepreneurship Program of Deshpande Startups presents 3D Design Contest 3D Design Contest is an exciting opportunity for all Enthusiastic students to participate virtually and design solutions for socially impactful problem statements"/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="3D design Contest">
	<link rel="canonical" href="https://www.deshpandestartups.org/3d-contest">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/3d-contest-bg.png" width="1349" height="198" alt="Deshpande Startups, events, 3D design Contest">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">3D design Contest</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center text-uppercase wow slideInDown"><span class="text-muted">3D DESIGN</span> CONTEST</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/3d-contest.png" width="474" height="237" alt="Deshpande startups, 3D design Contest">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center text-truncate">3D design Contest</h5>
									<p><b>Date : </b>May 10<sup>th</sup> 2020</p>
									<!-- <p class="text-yellow">The registrations has been closed.</p> -->
									<p><b>Last date to apply : </b>May 07<sup>th</sup> 2020</p>
									<p><b>Model submission : </b>May 15<sup>th</sup> 2020</p>
									<p><b>Timeline for completing model : </b>05 days</p>
									<p><b>Registration amount : </b>Rs.100/- per team</p>
									<p><b>Venue :</b> Virtual
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
										E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a>
									</p>
									<!-- <br> -->
								</div>
								<div class="card-footer">
									<!-- <a href="#" class="btn p-1 btn-primary" target="_blank">Apply Now</a> -->
									<p class="text-yellow">The registrations has been closed.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-5 text-yellow"><b>Event Description:</b></p>
						<p class="text-justify wow slideInRight"> An initiative of <b>“Yuva Entrepreneurship Program”</b> of Deshpande Startups presents "3D Design Contest". It is an exciting opportunity for all Enthusiastic students to participate virtually and design solutions for socially impactful problem statements and witness propelling India’s true growth story. CAD designers from any year can participate in this 5 days contest.</p>
						
						<p><b class="text-yellow">Reasons why you should participate:</b></p>
						<ul class="wow slideInRight">
							<li>Get to be a part of vibrant community of 3D CAD Designers and build your network</li>
							<li>Internship opportunities at Deshpande Startups</li>
							<li>Unique chance to Meet and Network with exciting start-ups and team with high-enthuse CAD Designers</li>
							<li>Win awards and recognition among CAD modelers</li>
							<li>Inspire the younger generation by sharing your practical knowledge and nurture them in developing their skills, we are certain that giving back is as joyous as learning</li>
							<li>Add real value to your resume with certification from Deshpande Startups</li>
						</ul>
						<p><b class="text-yellow">Winner will get access to:</b></p>
						<ul class="wow slideInRight">
							<li>3 months free membership to access Makers Lab</li>
							<li>Goodies</li>
							<li>3 printed prototypes</li>
							<li>Participants certificates</li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="container">
		<!-- <h3>Frequently Asked Questions</h3> -->
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">Frequently</span> Asked Questions</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div id="accordion">
			<div class="card cal card-hover-shadow">
				<div class="card-header" id="headingOne">
					<h5 class="mb-0">
						<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							<h4 class="text-yellow">Who can participate?</h4>
						</button>
					</h5>
				</div>
				<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">
						<div class="wpb_wrapper cal">
							<p class="text-justify">
								Students who knows CAD design software
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="card cal card-hover-shadow">
				<div class="card-header" id="headingTwo">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							<h4 class="text-yellow">Is it maximum of 4 people or strictly a team of 4?</h4>
						</button>
					</h5>
				</div>
				<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
					<div class="card-body">
						<!-- <br> -->
						<div class="wpb_wrapper cal">
							<p class="text-justify">
							Maximum 4</p>
						</div>
					</div>
				</div>
			</div>
			<div class="card cal card-hover-shadow">
				<div class="card-header" id="headingThree">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							<h4 class="text-yellow"> Are cross-college teams allowed?</h4>
						</button>
					</h5>
				</div>
				<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
					<div class="card-body">
						<div class="wpb_text_column wpb_content_element ">
							<div class="wpb_wrapper cal">
								<p class="text-justify">
									Yes
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card cal card-hover-shadow">
				<div class="card-header" id="headingFour">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							<h4 class="text-yellow"> Are UG-PG collaboration teams allowed?</h4>
						</button>
					</h5>
				</div>
				<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
					<div class="card-body">
						<div class="wpb_text_column wpb_content_element ">
							<div class="wpb_wrapper cal">
								<p class="text-justify">
									Yes
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card cal card-hover-shadow">
				<div class="card-header" id="headingFive">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
							<h4 class="text-yellow"> What analysis can we come up with for the models?</h4>
						</button>
					</h5>
				</div>
				<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
					<div class="card-body">
						<div class="wpb_text_column wpb_content_element ">
							<div class="wpb_wrapper cal">
								<p class="text-justify">
									Preferably begin with Static Structural analysis and show that it has an optimum factor of safety, then go ahead with Dynamic Analysis. In certain cases, Thermal (Static/Transient) can also be done.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card cal card-hover-shadow">
				<div class="card-header" id="headingSix">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
							<h4 class="text-yellow"> What if we don't perform any Analysis tests using some software as we aren't knowing any?</h4>
						</button>
					</h5>
				</div>
				<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
					<div class="card-body">
						<div class="wpb_text_column wpb_content_element ">
							<div class="wpb_wrapper cal">
								<p class="text-justify">
									It's not at all a problem as it's only going to give you bonus points and isn't necessary, just be aware of the critical conditions in which your model might fail and you should be able to convincingly answer the judge the same. Also 'basic analysis' can be carried out 'manually, without any software' and the calculations can be shown in the presentation.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card cal card-hover-shadow">
				<div class="card-header" id="headingSeven">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
							<h4 class="text-yellow"> Any particular file formats necessary?</h4>
						</button>
					</h5>
				</div>
				<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
					<div class="card-body">
						<div class="wpb_text_column wpb_content_element ">
							<div class="wpb_wrapper cal">
								<p class="text-justify">
									Yes, get your
								</p>
								<ul class="text-justify">
									<li>Model in IGES/STEP format preferably </li>
									<li>Drawings in .dwg or converted to .pdf</li>
									<li>For the analysis carried out using software, get pictures and copy those into your PowerPoint file (.ppt); manual calculations can be neatly done, scanned and attached to the .ppt file too</li>
									<li>Other photos if any u can get them as .jpg if you aren't uploading them in your .ppt file</li>
									<li>Animation videos can be .mp4/.avi/.3gp and must preferably be linked to your .ppt</li>
									<li>In case of files apart from .ppt a folder must be submitted well before time of your presentation through a pen drive to the organizers on the event day</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>