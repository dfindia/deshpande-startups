<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sasisekar Krish - CEO & Co-founder - nanoPix</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/sasisekar"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/sasisekar">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/edge/sasisekar.png">
	<meta property="og:description" content="An alumnus of renowned Indian technological institutes such as the IISc and IIT, Sasisekar Krish is the CEO and co-founder of nanoPix. The man behind its patented technologies such as imageIn and nanoSorter — that is making waves in the food-processing industry — is an expert on VLSI and digital image processing."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="An alumnus of renowned Indian technological institutes such as the IISc and IIT, Sasisekar Krish is the CEO and co-founder of nanoPix. The man behind its patented technologies such as imageIn and nanoSorter — that is making waves in the food-processing industry — is an expert on VLSI and digital image processing."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Makers Lab Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Sasisekar Krish - CEO & Co-founder - nanoPix">
	<link rel="canonical" href="https://www.deshpandestartups.org/sasisekar">
	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	/*p{text-align:justify;}*/
	.cal{
		font-family: calibri;
	}
	.modal-title {
		text-align: left;
		margin: 0;
		line-height: 1;
	}
	.follow-us{
		margin:20px 0 0;
	}
	.follow-us li{ 
		display:inline-block; 
		width:auto; 
		margin:0 5px;
	}
	.follow-us li .fa{ 
		font-size:25px; 
		/*color:#767676;*/
		color: #e7572a;
	}
	.follow-us li .fa:hover{ 
		color:#025a8e;
	}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>

	<div class="container cal pt-4">
		<div class="row wow fadeInLeft" data-wow-duration="1s" data-wow-offset="50">
			<div class="col-md-12 team-main">
				<div>
					<div class="modal-header">
						<!-- <a type="button" class="close" data-dismiss="modal" aria-label="Close" href="#"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> back </a> -->
						<h4 class="modal-title modal-title-cust text-yellow" id="myModalLabel">SASISEKAR KRISH<br><small class="text-muted">CEO & Co-founder - nanoPix</small></h4>
						<ul class="follow-us clearfix">
							<li><a href="https://twitter.com/ksasisekar" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="https://www.linkedin.com/in/ksasisekar/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="modal-body">
						<p class="text-justify">
							<img src="img/edge/sasisekar.png" width="165" height="160" class="img img-fluid mr-2 rounded-circle" alt="Sasisekar Krish - CEO & Co-founder - nanoPix, EDGE Mentor" align="left">
							An alumnus of renowned Indian technological institutes such as the IISc and IIT, Sasisekar Krish is the CEO and co-founder of nanoPix. The man behind its patented technologies such as imageIn and nanoSorter - that is making waves in the food processing industry - is an expert on VLSI and digital image processing.<br><br>
							He is the moving force when it comes to product conceptualization, development or prototyping. Sasisekar is no accidental entrepreneur: he worked at leading IT companies, such as Wipro and PMC Sierra, before the entrepreneur bug bit him.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>