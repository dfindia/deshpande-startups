<!DOCTYPE html>
<html lang="en">
<head>
 <title>Deshpande Startups - India's Largest Incubation Center</title>
 <?php
 require_once 'essentials/meta.php';
 ?>
 <meta name="linkage" content="https://www.deshpandestartups.org"/>
 <meta name="author" content="Deshpande Startups"/>
 <meta name="twitter:card" content="summary">
 <meta name="twitter:site" content="@DFstartups">
 <meta name="twitter:title" content="https://www.deshpandestartups.org">
 <meta name="twitter:description" content="An eco-system of Resources, Connections, Knowledge & Talent to support mission driven entrepreneurs to scale in their venture.">
 <meta name="twitter:image" content="https://www.deshpandestartups.org/img/bg-home/mass-deshpande-startups.jpg"/>
 <meta name="twitter:image" content="https://www.deshpandestartups.org/img/bg-home/deshpande-bg-new.png"/>
 <meta name="twitter:image:alt" content="Deshpande Startups Entrepreneurship">
 <meta property="og:title" content="Deshpande Startups">
 <meta property="og:site_name" content="Deshpande Startups"/>
 <meta property="og:type" content="website">
 <meta property="og:url" content="https://www.deshpandestartups.org">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/mass-deshpande-startups.jpg">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/desh-deshpande.png">
 <meta property="og:description" content="An eco-system of Resources, Connections, Knowledge & Talent to support mission driven entrepreneurs to scale in their venture."/>
 <meta name="description" content="An eco-system of Resources, Connections, Knowledge & Talent to support mission driven entrepreneurs to scale in their venture."/>
 <meta name="keywords" content="Business, business ideas, How to make a business, Incubation center, Tejas networks, female entrepreneurs, How to be an entrepreneur, How to become an entrepreneur, The sandbox"/>
 <link rel="canonical" href="https://www.deshpandestartups.org/">

 <?php
         // $title = 'Deshpande Startups';
 require_once 'essentials/bundle.php';
 ?>
 
 <style type="text/css">
  /*.modal-popup button {
   position: absolute;
   left: 98% !important;
   top: -8% !important;
   color: #ffffff !important;
 }
 .close {
   float: right;
   font-size: 2.0rem !important;
   font-weight: 700;
   line-height: 1;
   color: #000000;
   text-shadow: 0 1px 0 #ffffff !important;
   opacity: unset !important; 
   }*/

   .gray {
    /* background-color: #f7f7f5!important; */
    background-color: #f1f4fa!important;
    /*background-color: #fbfbfb!important;*/
  }

 /*.parallax {
  background-image: url("img/makers/deshpande.jpg");
  min-height: 300px; 
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  }*/

</style>
</head>
<body>
 <?php
 require_once 'essentials/title_bar.php';
 require_once 'essentials/menus.php';
 ?>
 <!-- Generator: Jssor Slider Maker -->
 <div id="jssor_1" class="slide1">
  <!-- Loading Screen -->
  <div data-u="loading" class="jssorl-009-spin load">
   <img class="spin1" src="img/spin.svg" alt="spin"/>
 </div>
 <div data-u="slides" class="slide2">
  <div data-p="170.00">
    <img data-u="image" src="img/bg-home/mass-deshpande-startups.jpg" class="img-responsive" alt="MASS, A gateway to bharat, Deshpande Startups">
  </div>
  <!-- <div data-p="170.00">
    <img data-u="image" src="img/bg-home/deshpande-bg-new.png" class="img-responsive" alt="India's largest incubation center Deshpande startups building">
  </div> -->
  <div data-p="170.00">
    <img data-u="image" src="img/bg-home/desh-deshpande.png" class="img-responsive" alt="Dr. Gururaj Desh Deshpande, Founder, Deshpande Foundation">
  </div>
</div>
<!-- Bullet Navigator -->
<div data-u="navigator" class="jssorb051 bull" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
 <div data-u="prototype" class="i bulletnav">
  <svg viewbox="0 0 16000 16000" class="pos">
   <circle class="b" cx="8000" cy="8000" r="5800"></circle>
 </svg>
</div>
</div>
</div>

<!-- <div class="container mt-50">
  <div class="row">
  <div class="col-md-12">
  <h1 class="text-center">Click on the feedback button</h1>
  </div>
  </div>
</div> -->


<div class="container-fluid">
 <div class="row text-center">
  <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 p-3">
    <i class="fa fa-quote-left text-yellow fa-4x pb-2"></i>
    <div class="divider b-y w-75 text-yellow content-middle"></div>
    <h1 class="wow fadeInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">"Enable Entrepreneurs to Impact Billions of Lives in Semi-urban<br> and Rural Communities in India"</h1>
    <h2 class="text-yellow wow fadeInDown pb-4" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">India’s largest platform to penetrate into the 70% (TIER-II & TIER-III)<br> untapped India's market.</h2>
    <div class="divider b-y w-75 text-yellow content-middle"></div>
  </div>
</div>
</div>
<!-- ========================== Marketing messaging and featurettes ======================== -->
<div class="container pb-4">

  <div class="row">
   <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 wow fadeInDown">
    <p class="text-center font-weight-bold"> An eco-system of resources, connections, knowledge &amp; talent to support mission driven entrepreneurs to scale in their venture. Entrepreneurs call it as a living laboratory to test their business ideas, get it validated, build successful ventures and scale to the greater heights. </p>
  </div>
</div>
</div>


<h2 class=" text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">ECOSYSTEM</span> OFFERINGS</h2>
<div class="divider b-y text-yellow content-middle"></div>
<img src="img/bg-home/ecosystem-offerings.png" class="img-fluid img" width="100%" height="395" alt="Deshpande startups, Offerings">
<!-- <br>
  <br> -->

  <div class="featured-bg-container">
    <div class="col-md-12">
      <div class="row justify-content-md-center">
        <div class="col-md-4">
          <div class="text-center wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
            <h5>Apply now for incubation program</h5>
            <a href="incubation-support" class="btn btn-md btn-warning px-5" target="_blank"> Apply Now</a>
          </div>
        </div>
       <!--  <div class="col-md-4">
          <div class="text-center wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
            <h5>Apply now for funding</h5>
            <a href="dst-nidhi-sss" class="btn btn-md btn-warning px-5" target="_blank"> Apply Now</a>
          </div>
        </div> -->
        <div class="col-md-4">
          <div class="text-center wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
            <h5>Apply to be a mentor</h5>
            <a href="expertise" class="btn btn-md btn-warning px-5" target="_blank"> Apply Now</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- <br> -->
  <img src="img/incubation/incubation-impact.png" class="img-fluid img" width="100%" height="400" alt="Deshpande startups impact">
  <!-- <br> -->

  <div class="container-fluid gray">
    <div class="row pt-4 justify-content-md-center">
     <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
      <h2 class="text-yellow text-center p-1 wow fadeInDown">&nbsp; TESTIMONIALS</h2>
      <div class="divider b-y text-yellow content-middle"></div>
      <div class="carousel slide wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" data-ride="carousel" id="quote-carousel">
       <!-- Carousel Slides / Quotes -->
       <div class="carousel-inner">
        <!-- Quote 1 -->
        <div class="carousel-item active">
         <blockquote>
          <div class="row">
           <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center">
            <p class="testimonial">Sandbox came in when we were at the cocoon stage, our idea was strong but with limited understanding about building a startup. Sandbox brought in an understanding of building a startup. Their periodic tracking systems, the enriched guidance on “how to make a business” & how to connect the dots between "Concepts to Product” pushed us to reach our goals. </p>
            <br>
            <h3 class="featurette-heading-sm text-yellow">SHEKAR</h3>
            <h3 class="featurette-heading-sm">Widemobility - Founder</h3>
          </div>
        </div>
      </blockquote>
    </div>
    <!-- Quote 2 -->
    <div class="carousel-item ">
     <blockquote>
      <div class="row">
       <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center">
        <p class="testimonial">The sandbox startup has continuously provided mentorship, space to grow, training to hone skills and capabilities, a confident environment for the team and the discipline that is needed succeed. </p>
        <br>
        <h3 class="featurette-heading-sm text-yellow">SASISEKAR KRISH</h3>
        <h3 class="featurette-heading-sm">nanoPix - Founder</h3>
      </div>
    </div>
  </blockquote>
</div>
<!-- Quote 3 -->
<div class="carousel-item">
 <blockquote>
  <div class="row">
   <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center">
    <p class="testimonial">LinkEZStartup journey starts with a Great Idea and Founders need to focus fully on the Technical and Marketing aspects to be Successful. Sandbox has really been this complete Ecosystem that has Supported and Nurtured LinkEZ to grow into what it is today.
    </p>
    <br>
    <h3 class="featurette-heading-sm text-yellow">DIVYESH SHAH</h3>
    <h3 class="featurette-heading-sm">LinkEZ - Founder</h3>
  </div>
</div>
</blockquote>
</div>
<!-- Quote 4 -->
<div class="carousel-item">
 <blockquote>
  <div class="row">
   <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center">
    <p class="testimonial">Team Sandbox Startups has been very supportive from ideation stage to Product Execution and Infrastructure here is well organized to execute projects at any scale. </p>
    <br>
    <h3 class="featurette-heading-sm text-yellow">FARHAN</h3>
    <h3 class="featurette-heading-sm">Microchip Payments - Founder</h3>
  </div>
</div>
</blockquote>
</div>
</div>
<!-- Bottom Carousel Indicators -->
<div class="row justify-content-md-center">
  <ol class="carousel-indicators">
    <li data-target="#quote-carousel" data-slide-to="0" class="active">
     <img class="img-fluid" width="128" height="128" src="img/testimonials/shekar.png" data-src="img/testimonials/small/shekar.jpg" alt="Deshpande startups, Shekar Founder Widemobility Pvt. Ltd">
   </li>
   <li data-target="#quote-carousel" data-slide-to="1">
     <img class="img-fluid" width="128" height="128" src="img/testimonials/sasi-sekar.jpg" data-src="img/testimonials/small/sasi-sekar.jpg" alt="Deshpande startups, sasisekar krish Nanopix - Founder">
   </li>
   <li data-target="#quote-carousel" data-slide-to="2"><img class="img-fluid" width="128" height="128" src="img/testimonials/divyesh.jpg" data-src="img/testimonials/small/divyesh.jpg" alt="Deshpande startups, divesh shah LinkeZ - Founder">
   </li>
   <li data-target="#quote-carousel" data-slide-to="3"><img class="img-fluid" width="128" height="128" src="img/testimonials/farhan.jpg" data-src="img/testimonials/small/farhan.jpg" alt="Deshpande startups, Farhan Founder Picrochip Payments Pvt. Ltd">
   </li>
 </ol>
</div>
</div>
</div>
<!-- <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 p-0 text-center">
  <h2 class="text-yellow text-center wow fadeInDown"><span class="text-muted">IMPACT OF</span> INCUBATION CENTER</h2>
  <div class="divider b-y text-yellow content-middle"></div>
  <div class="wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
   <img src="img/deshpande-startups-impact.png" width="666" height="428" class="img-fluid" alt="Deshpande startups impact">
 </div>
</div> -->
</div>
</div>
<br>


<!-- narrating opportunity -->
<!-- <hr class="featurette-divider-sm"> -->

<div id="visitors-gallery">
  <div class="container-fluid text-center">
    <h2 class="text-yellow text-center wow fadeInDown"><span class="text-muted">VISITORS</span> GALLERY</h2>
    <div class="divider b-y text-yellow content-middle"></div>
    <div class="row blog">
      <div class="col-md-12">
        <div id="blogCarousel" class="carousel slide" data-ride="carousel">

          <ol class="carousel-indicators pt-4">
            <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#blogCarousel" data-slide-to="1"></li>
            <li data-target="#blogCarousel" data-slide-to="2"></li>
            <li data-target="#blogCarousel" data-slide-to="3"></li>
            <li data-target="#blogCarousel" data-slide-to="4"></li>
          </ol>

          <!-- Carousel items -->
          <div class="carousel-inner">

            <div class="carousel-item active">
              <div class="row">
                <div class="col-md-4">
                  <!-- <a href="#"> -->
                    <img src="img/influencers/amitabh-kant.jpg" class="blog-img img-fluid img img-thumbnail" alt="Dr. Amitabh Kant, CEO, NITI Aayog" width="410" height="307">
                    <p class="text-yellow mb-0">Dr. Amitabh Kant</p>
                    <p class="pt-0 mb-0 des-name">CEO, <br>NITI Aayog</p>
                    <a href="https://photos.google.com/share/AF1QipPXJ0kVWs-lT-s6ZUMZqNeA9ybhe5Dcaw9QENZ5QRFk7vKx9qD8Ml9ZCmFM3K29eA?key=VFRBLTRFaU10b1BYVUVwMXIyaVZkT25SNVhHc1Fn" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                    <!-- </a> -->
                  </div>
                  <div class="col-md-4">
                    <!-- <a href="#"> -->
                      <img src="img/influencers/narayana-murthy.jpg" class="blog-img img-fluid img img-thumbnail" alt="N. R. Narayana Murthy, Founder & Former Chairman, Infosys" width="410" height="307">
                      <p class="mb-0 text-yellow">N. R. Narayana Murthy</p>
                      <p class="pt-0 mb-0 des-name">Founder & Former Chairman, <br>Infosys</p>
                      <a href="https://photos.google.com/share/AF1QipNU-HXMTcuELPpU8a1UBk2DoUyktwLi_yrOqwyA9OEz9xNCJuxL6M_LwxgyGw8vAA?key=UC15czkybWZUMG5lXzJmWXdoY2pBZHJqa3Y2bW53" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                      <!-- </a> -->
                    </div>
                    <div class="col-md-4">
                      <!-- <a href="#"> -->
                        <img src="img/influencers/nabard-visit.png" class="blog-img img-fluid img img-thumbnail" alt="NABARD, Hemant Songadkar, Chief General Manager, Off-farm Development & Rajesh Ranjan, CFA, CAIA CEO, Nab Ventures of NABARD" width="410" height="307">
                        <p class="text-yellow mb-0">National Bank for Agriculture and Rural Development</p>
                        <p class="pt-0 mb-0 des-name">Hemant Songadkar, Chief General Manager, Off-farm Development & Rajesh Ranjan, CFA, CAIA CEO, Nab Ventures of NABARD</p>
                        <a href="https://photos.google.com/share/AF1QipOXWcfd-m4lwfkQCeI1bMCveI0Lmt1frOZ3-duehqdsO3YmkgnWhmTd9KejabUjHQ?key=bDhBLXdNb0syaHRzb1ZOV0dPSEFYYkpQTkg2LXdR" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                        <!-- </a> -->
                      </div>
                    </div>
                    <!--.row-->
                  </div>
                  <!--.item-->
                  <div class="carousel-item">
                    <div class="row">
                      <div class="col-md-4">
                        <!-- <a href="#"> -->
                          <img src="img/influencers/sudha-murthy.jpg" class="blog-img img-fluid img img-thumbnail" alt="Sudha Murthy, Chairperson, Infosys" width="410" height="307">
                          <p class="text-yellow mb-0">Sudha Murthy</p>
                          <p class="pt-0 mb-0 des-name">‎Chairperson, <br>Infosys</p>
                          <a href="https://photos.google.com/share/AF1QipMJoakF-xJ3UHWluaM2XSumEfqd_7p1vRdu8R6_haKhTPCqSJmZRSUUfAUEy3jxRw?key=Z21YXzVUYndDNWtVX2Z3WGVvZXBrMEJ2dDF1WVBB" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                          <!-- </a> -->
                        </div>
                        <div class="col-md-4">
                          <!-- <a href="#"> -->
                            <img src="img/influencers/ramanan-ramnathan.jpg" class="blog-img img-fluid img img-thumbnail" alt="Ramanan Ramanathan, Mission Director, Atal Innovation Mission" width="410" height="307">
                            <p class="text-yellow mb-0">Ramanan Ramanathan</p>
                            <p class="pt-0 mb-0 des-name">Mission Director, <br>Atal Innovation Mission</p>
                            <a href="https://photos.app.goo.gl/TjegWCSUZQvVg7t99" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                            <!-- </a> -->
                          </div>
                          <div class="col-md-4">
                            <!-- <a href="#"> -->
                              <img src="img/influencers/g-m-rao.png" class="blog-img img-fluid img img-thumbnail" alt="G M Rao, Founder & chairman of GMR group" width="410" height="307">
                              <p class="text-yellow mb-0">G M Rao</p>
                              <p class="pt-0 mb-0 des-name">Founder & chairman,<br> GMR group</p>
                              <a href="https://photos.app.goo.gl/P1WAKF1hKPXC746y8" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                              <!-- </a> -->
                            </div>
                          </div>
                        </div>
                        <!--.item-->

                        <div class="carousel-item">
                          <div class="row">
                            <div class="col-md-4">
                              <img src="img/influencers/jagadish-shettar.png" class="blog-img img-fluid img img-thumbnail" alt="Jagadish Shettar, Former Chief minister of Karnataka" width="410" height="307">
                              <p class="text-yellow mb-0">Jagadish Shettar</p>
                              <p class="pt-0 mb-0 des-name">Former Chief Minister,<br> Karnataka</p>
                              <a href="https://photos.google.com/share/AF1QipMzEz-EDzEYfMbuGym9e9PaojVWedDkiGb79FcSwLay0KwpuJJopqUhR-e-JJCJWA?key=Q3E3RzYxbnZLUDdxN250dVJXSTlITmpPNHhPOHB3" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                            </div>
                            <div class="col-md-4">
                              <img src="img/influencers/k-j-george.jpg" class="blog-img img-fluid img img-thumbnail" alt="K J George, Honorable IT BT minister, Govt. of Karnataka" width="410" height="307">
                              <p class="text-yellow mb-0">K J George</p>
                              <p class="pt-0 mb-0 des-name">Honorable IT BT Minister, <br>Govt. of Karnataka</p>
                              <a href="https://photos.google.com/share/AF1QipMit2BDIS1Pjkjwh-XZTItytEneR0Bjs3d-BzjBem-fcDbIn2WKI8rYh9ke-vF_dA?key=ejhmTkJWaXBKaWUxanpHOEVEQ29yUDdHVmtfclJR" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                            </div>
                            <div class="col-md-4">
                              <!-- <a href="#"> -->
                                <img src="img/influencers/dominic.jpg" class="blog-img img-fluid img img-thumbnail" alt="Dominic McAllister, British Deputy High Commissioner, Bengaluru" width="410" height="307">
                                <p class="text-yellow mb-0">Dominic McAllister</p>
                                <p class="pt-0 mb-0 des-name">British Deputy High Commissioner, <br>Bengaluru</p>
                                <!-- <a href="https://photos.app.goo.gl/TjegWCSUZQvVg7t99" class="btn btn-sm btn-warning" target="_blank">Click for more</a> -->
                                <!-- </a> -->
                              </div>
                            </div>
                          </div>
                          <!--.item-->
                          <div class="carousel-item">
                            <div class="row">
                              <div class="col-md-4">
                                <img src="img/influencers/krishna.jpg" class="blog-img img-fluid img img-thumbnail" alt="Krishna Byre Gowda, Minister for Rural Development, Law and Parliamentary Affairs" width="410" height="307">
                                <p class="text-yellow mb-0">Krishna Byre Gowda</p>
                                <p class="pt-0 mb-0 des-name">Minister for Rural Development, <br> Law and Parliamentary Affairs</p>
                                <a href="https://photos.app.goo.gl/VgMmzjZ8FXxgsCUw7" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                              </div>
                              <div class="col-md-4">
                                <img src="img/influencers/t-n-hari.jpg" class="blog-img img-fluid img img-thumbnail" alt="T. N. Hari, Head – Human Resources, BigBasket" width="410" height="307">
                                <p class="text-yellow mb-0">T. N. Hari</p>
                                <p class="pt-0 mb-0 des-name">Head of Human Resources, <br>BigBasket</p>
                                <a href="https://photos.app.goo.gl/LP9VV1Fqokf7GZMh6" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                              </div>
                              <div class="col-md-4">
                                <!-- <a href="#"> -->
                                  <img src="img/influencers/neeraj-kakkar.jpg" class="blog-img img-fluid img img-thumbnail" alt="Neeraj Kakkar, CEO, Hector Beverages Pvt. Ltd." width="410" height="307">
                                  <p class="text-yellow mb-0">Neeraj Kakkar</p>
                                  <p class="pt-0 mb-0 des-name">CEO,<br> Hector Beverages Pvt. Ltd.</p>
                                  <a href="https://photos.app.goo.gl/eM1Lsxtx14EDQ7yU9" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                                  <!-- </a> -->
                                </div>
                              </div>
                            </div>
                            <!--.item-->
                            <div class="carousel-item">
                              <div class="row">
                                <div class="col-md-4">
                                  <!-- <a href="#"> -->
                                    <img src="img/influencers/shradha-sharma.jpg" class="blog-img img-fluid img img-thumbnail" alt="Shradha Sharma, Founder & CEO, YourStory Media Pvt. Ltd." width="410" height="307">
                                    <p class="text-yellow mb-0">Shradha Sharma</p>
                                    <p class="pt-0 mb-0 des-name">Founder & CEO, <br>YourStory Media Pvt. Ltd.</p>
                                    <a href="https://photos.app.goo.gl/TjegWCSUZQvVg7t99" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                                    <!-- </a> -->
                                  </div>
                                  <div class="col-md-4">
                                    <!-- <a href="#"> -->
                                      <img src="img/influencers/deepa-cholan.jpg" class="blog-img img-fluid img img-thumbnail" alt="Deepa Cholan, Deputy Commissioner, Dharwad" width="410" height="307">
                                      <p class="text-yellow mb-0">Deepa Cholan</p>
                                      <p class="pt-0 mb-0 des-name">Deputy Commissioner, <br>Dharwad</p>
                                      <a href="https://photos.app.goo.gl/QRbkGvfMycoqaHnL9" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                                      <!-- </a> -->
                                    </div>
                                    <div class="col-md-4">
                                      <img src="img/influencers/anita-gupta.jpg" class="blog-img img-fluid img img-thumbnail" alt="Dr. Anita Gupta, Adviser/Scientist G, at DST, Govt. of India" width="410" height="307">
                                      <p class="text-yellow mb-0">Dr. Anita Gupta</p>
                                      <p class="pt-0 mb-0 des-name">Adviser/Scientist G, <br>at DST, Govt. of India</p>
                                      <a href="https://photos.app.goo.gl/z7oX6V8z6wqgyJ66A" class="btn btn-sm btn-warning" target="_blank">Click for more</a>
                                    </div>
                                  </div>
                                </div>

                              </div>
                              <!--.carousel-inner-->
                            </div>
                            <!--.Carousel-->
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                    <br>
                    <!-- <hr class="featurette-divider-sm"> -->

                    <!-- webinar video -->

                    <div class="container-fluid bg-grey1 pt-3 pb-3">
                     <h2 class=" text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"> WEBINARS</h2>
                     <div class="divider b-y text-yellow content-middle"></div>
                     <!-- row 1 -->
                     <div class="row">
                       <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="row">
                         <div class="col-md-4 youtube-wrapper">
                           <div class="embed-responsive youtube mb-3" data-embed="bSkSneSozQw">
                            <div class="play-button"></div>
                          </div>
                          <div class="text-center mt-0">
                            <strong>Envisioning Bharat Opportunities</strong>
                          </div>
                        </div>
                        <div class="col-md-4 youtube-wrapper">
                         <div class="embed-responsive youtube mb-3" data-embed="AOB6gSLDirY">
                          <div class="play-button"></div>
                        </div>
                        <div class="text-center mt-0">
                          <strong>Healthcare NEXT: Opportunities in Bharat Healthcare</strong>
                        </div>
                      </div>
                      <div class="col-md-4 youtube-wrapper">
                       <div class="embed-responsive youtube mb-3" data-embed="IwwdPPBM7Po">
                        <div class="play-button"></div>
                      </div>
                      <div class="text-center mt-0">
                        <strong>AgriNEXT: Future of Innovation & Entrepreneurship in Agriculture</strong>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- row 1 -->
            </div>
            <br>
            <!-- mentors video -->
            <h2 class=" text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">GLIMPSE OF PAST</span> STARTUP DIALOGUE</h2>
            <div class="divider b-y text-yellow content-middle"></div>
            <div class="container-fluid">
              <!-- row 1 -->
              <div class="row">
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="row">
                 <div class="col-md-4 youtube-wrapper">
                   <div class="embed-responsive youtube mb-3" data-embed="p-HYZGaU1_M">
                    <div class="play-button"></div>
                  </div>
                  <div class="text-center mt-0">
                    <strong>Padma Bhushan Shri Ajai Chowdhry <br> Speaks at Startup Dialogue 2020</strong>
                  </div>
                </div>
                <div class="col-md-4 youtube-wrapper">
                 <div class="embed-responsive youtube mb-3" data-embed="Wp_DncBxtDg">
                  <div class="play-button"></div>
                </div>
                <div class="text-center mt-0">
                  <strong>Padma Shree T. V. Mohandas Pai <br>Speaks at Startup Dialogue 2020</strong>
                </div>
              </div>
              <div class="col-md-4 youtube-wrapper">
               <div class="embed-responsive youtube mb-3" data-embed="jOLmjLTucjg">
                <div class="play-button"></div>
              </div>
              <div class="text-center mt-0">
                <strong>Srikar Reddy <br>Speaks at Startup Dialogue 2020</strong>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- row 1 -->
    </div>
    <!-- <br> -->
    <!-- <hr class="featurette-divider-sm"> -->

    <hr class="featurette-divider-sm">
    <h2 class="text-yellow text-center wow fadeInDown"><span class="text-muted">OUR</span> PARTNERS</h2>
    <div class="divider b-y text-yellow content-middle"></div>
    <div class="container">
     <!-- partners -->
     <div class="row text-center justify-content-md-center">
       <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
         <a href="https://www.deshpandefoundationindia.org/" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" src="img/partners/df-logo.jpg" width="150" height="150" alt="Deshpande startups partners Deshpande Foundation logo"></a>
       </div>
       <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
         <a href="http://www.dst.gov.in/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" src="img/partners/dstlogo-1.jpg" width="150" height="150" alt="Deshpande startups partners department of science and technology logo"></a>
       </div>
       <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
         <a href="http://k-tech.org/" rel="nofollow"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" src="img/partners/ktech.png" width="150" height="150" alt="Deshpande startups partners ktech logo"></a>
       </div>
       <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
         <a href="http://www.iesaonline.org/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" src="img/partners/iesa.png" width="150" height="150" alt="Deshpande startups partners iesa logo"></a>
       </div>
       <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
         <a href="http://klsimer.edu/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s" src="img/partners/imer-8.jpg" width="150" height="150" alt="Deshpande startups partners imer logo"></a>
       </div>
       <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
        <a href="https://www.zoho.com/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="1.1s" src="img/partners/zoho-12.jpg" width="150" height="150" alt="Deshpande startups partners zoho logo"></a>
      </div>
    </div>
    <br>
    <div class="row text-center justify-content-md-center">
      <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
        <a href="https://www.mathworks.com/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" src="img/partners/mathworks.jpg" width="150" height="150" alt="Deshpande startups partners mathworks logo"></a>
      </div>
      <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
        <a href="http://tiecon.tiehubli.org/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" src="img/partners/tie-hubli.png" width="150" height="150" alt="Deshpande startups partners tie hubli logo"></a>
      </div>
      <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
        <a href="http://deshpande.mit.edu/" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" src="img/partners/dc-10.jpg" width="150" height="150" alt="Deshpande startups partners deshpande center logo"></a>
      </div>
      <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
        <a href="http://www.tejasnetworks.com/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" src="img/partners/tejas-3.jpg" width="150" height="150" alt="Deshpande startups partners tejas networks logo"></a>
      </div><div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
        <a href="https://www.omnivore.vc/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s" src="img/partners/omnivore.png" width="150" height="150" alt="Deshpande startups partners Omnivore logo"></a>
      </div>
      <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
       <a href="https://eforall.org/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="1.1s" src="img/partners/eall-13.jpg" width="150" height="150" alt="Deshpande startups partners E for all logo"></a>
     </div>
   </div>
   <br>
   <div class="row text-center justify-content-md-center">
     <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
       <a href="http://vtu.ac.in/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" src="img/partners/vtu-11.jpg" width="150" height="150" alt="Deshpande startups partners vtu logo"></a>
     </div>
     <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
       <img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" src="img/partners/pdc-6.jpg" width="150" height="150" alt="Deshpande startups partners pdc logo">
     </div>
     <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
       <a href="http://unituscapital.com/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" src="img/partners/unitus-7.jpg" width="150" height="150" alt="Deshpande startups partners unitus logo"></a>
     </div>
     <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
       <a href="http://www.queensu.ca/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" src="img/partners/queens-14.jpg" width="150" height="150" alt="Deshpande startups partners queen's university logo"></a>
     </div>
     <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
      <a href="https://www.solidworks.com/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s" src="img/partners/solidworks.png" width="150" height="150" alt="Deshpande startups partners Solid Works logo"></a>
    </div>
  </div>
  <br>
         <!-- <div class="row text-center justify-content-md-center">
      </div>
      <br> -->
      <br>



      <h2 class="text-yellow text-center wow fadeInDown"><span class="text-muted">ACADEMIC</span> PARTNERS</h2>
      <div class="divider b-y text-yellow content-middle"></div>
      <!-- partners -->
      <div class="row text-center justify-content-md-center">
       <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
        <a href="https://iiitdwd.ac.in/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" src="img/partners/iiit-dharwad.png" width="150" height="150" alt="Deshpande startups partners IIIT Dharwad logo"></a>
      </div>
      <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
        <a href="https://www.iitdh.ac.in/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" src="img/partners/iit-dharwad.png" width="150" height="150" alt="Deshpande startups partners IIT Dharwad logo"></a>
      </div>
      <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
        <a href="http://www.uasd.edu/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" src="img/partners/uas-dharwad.png" width="150" height="150" alt="Deshpande startups partners UAS Dharwad logo"></a>
      </div>
      <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
        <a href="https://vdrit.org/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" src="img/partners/kle-vdit.png" width="150" height="150" alt="Deshpande startups partners KLE VDIT logo"></a>
      </div>
      <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6">
        <a href="https://sdmcet.ac.in/" rel="nofollow" target="_blank"><img class="img-fluid wow zoomIn img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s" src="img/partners/sdm-dharwad.png" width="150" height="150" alt="Deshpande startups partners SDM Dharwad logo"></a>
      </div>
    </div>
    <br>
    <br>
  </div>

    <!-- <div class="modal fade" id="overlay">
     <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <div class="modal-body text-center modal-popup">
        <a href="webinar" target="_blank"><img src="img/events/webinar3-popup.jpg" alt="Webinar, Agri NEXT: Future of Innovation & Entrepreneurship in Agriculture, Deshpande startups" class="img-fluid"></a>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
    </div>
  </div>
</div> -->



<script src="js/jssor.slider-27.1.0.min.js"></script>
<script>
 jssor_1_slider_init = function() {

  var jssor_1_SlideshowTransitions = [
  {$Duration:800,$Opacity:2}
  ];

  var jssor_1_options = {
   $AutoPlay: 1,
   $SlideshowOptions: {
    $Class: $JssorSlideshowRunner$,
    $Transitions: jssor_1_SlideshowTransitions,
    $TransitionsOrder: 1
  },
  $ArrowNavigatorOptions: {
    $Class: $JssorArrowNavigator$
  },
  $BulletNavigatorOptions: {
    $Class: $JssorBulletNavigator$
  }
};

var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

/*#region responsive code begin*/

var MAX_WIDTH = 1920;

function ScaleSlider() {
 var containerElement = jssor_1_slider.$Elmt.parentNode;
 var containerWidth = containerElement.clientWidth;

 if (containerWidth) {

  var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

  jssor_1_slider.$ScaleWidth(expectedWidth);
}
else {
  window.setTimeout(ScaleSlider, 30);
}
}

ScaleSlider();

$Jssor$.$AddEvent(window, "load", ScaleSlider);
$Jssor$.$AddEvent(window, "resize", ScaleSlider);
$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
/*#endregion responsive code end*/
};
</script>
<script>jssor_1_slider_init();</script>
<script>
// optional
$('#blogCarousel').carousel({
 interval: 5000
});
</script>


<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>

</body>
</html>