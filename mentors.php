<!DOCTYPE html>
<html lang="en">
<head>
	<title>Mentors | Deshpande Startups Incubation mentors</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/mentors"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/mentors">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/mentors/deshpande-bg-mentors.jpg">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/mentors/gururaj.png">
	<meta property="og:description" content="Mentors will enrich your ideas, train you the current trends, navigate the right approach, help you in strategizing your business."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Mentors will enrich your ideas, train you the current trends, navigate the right approach, help you in strategizing your business."/>
	  <!-- <meta name="keywords" content="Sushil vachani, Hemang dave, Raju reddy, Phanindra sama, Ravi Narayan, Kattayil rajinish menon, Anand talwai, ramanathan narayan, Vivek pawar CEO deshpande foundation india, Sasisekar krish Founde and CEO nanoPix, Siva ramamoorthy, Anup akkihal, Founder, NEXT IN - Growth Partners to Entrepreneurs, Raj Melville executive directory
	  	deshpande foundation, Jayashri murali"/> -->
	  	<meta property="og:title" content="Deshpande startups Incubation mentors">
	  	<!-- <link rel="canonical" href="https://www.deshpandestartups.org/mentors"> -->

	  	<?php
		 // $title = 'Deshpande Startups';
	  	require_once 'essentials/bundle.php';
	  	?>
	  </head>
	  <body>
	  	<?php
	  	require_once 'essentials/title_bar.php';
	  	require_once 'essentials/menus.php';
	  	?>

	  	<img class="carousel-inner img-fluid" src="img/mentors/deshpande-bg-mentors.jpg" width="1349" height="198" alt="Deshpande Startups, mentors">
	  	<nav aria-label="breadcrumb">
	  		<ol class="breadcrumb justify-content-end">
	  			<li class="breadcrumb-item"><a href="./">Home</a></li>
	  			<li class="breadcrumb-item active" aria-current="page">Mentors</li>
	  		</ol>
	  	</nav>
	  	
	  	<div class="container">
	  		<div class="center wow fadeInDown">
	  			<h2 class="text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">OUR</span> MENTORS</h2>
	  			<div class="divider b-y text-yellow content-middle"></div>
	  			<br>
	  			<p class="text-center"><b>Mentors will enrich your ideas, train you the current trends, navigate the right approach, help you in strategizing your business and also monitor your journey.</b></p>
	  		</div>
	  	</div>
	  	<br>
	  	<div class="container">
	  		<div class="row text-center justify-content-md-center">
	  			<div class="col-md-2">
	  				<a href="https://www.linkedin.com/in/deshdeshpande" target="_blank"><img src="img/mentors/gururaj.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Dr. Gururaj Desh Deshpande, Founder, Mentor"></a>
	  				<p  class="text-yellow text-center"><b>Dr. Gururaj "Desh" Deshpande</b></p>
	  				<!-- <p>Founder, Deshpande Foundation</p> -->
	  			</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/jv-ramamurthy-6b4bab63/" target="_blank"><img src="img/mentors/jv-ramamurthy.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s" alt="JV Ramamurthy, Mentor"></a>
	  					<p class="text-center text-yellow"><b>JV Ramamurthy</b></p>
	  					<!-- <p class="mb-10">Director, Grantmaking & Partnerships Deshpande Foundation India </p> -->
	  				</div>
	  			<div class="col-md-2">
	  				<a href="https://www.linkedin.com/in/sushil-vachani-4b145a86" target="_blank"><img src="img/mentors/sushil-vachani.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" alt="Dr. Sushil Vachani, Mentor"></a>
	  				<p class="text-center text-yellow"><b>Dr. Sushil Vachani</b></p>

	  			</div>
	  			<div class="col-md-2">
	  				<a href="https://www.linkedin.com/in/phanisama" target="_blank"><img src="img/mentors/phanindra-sama.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.7s" alt="Phanindra Sama, Mentor"></a>
	  				<p class="text-center text-yellow"><b>Phanindra Sama</b></p>
	  				<!-- <p class="mb-10">Co-founder, redBus.in </p> -->
	  			</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/mj-mahesh-jadhav/" target="_blank"><img src="img/mentors/mahesh-jadhav.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.9s" alt="Mahesh Jadhav, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Mahesh Jadhav</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/anilkumar-muniswamy-17a9a3/" target="_blank"><img src="img/mentors/anil-muniswamy.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="1.1s" alt="Anil Kumar Muniswamy, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Anil Kumar Muniswamy</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  			</div>
	  			<br/>

	  			<div class="row text-center justify-content-md-center">
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/jinesh-shah-b810813/" target="_blank"><img src="img/mentors/jinesh-shah.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Jinesh Shah, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Jinesh Shah</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/sharadasatrasala/" target="_blank"><img src="img/mentors/sharada-satrasala.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s" alt="Sharada Satrasala, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Sharada Satrasala</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/rajivprakashnextin/" target="_blank"><img src="img/mentors/rajeev-prakash.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" alt="Rajiv Prakash, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Rajiv Prakash</b></p>
	  					<!-- <p class="mb-10">Director, Grantmaking & Partnerships Deshpande Foundation India </p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/samirkmr/" target="_blank"><img src="img/mentors/samir-kumar.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.7s" alt="Samir Kumar, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Samir Kumar</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/anand-kv-8288842/" target="_blank"><img src="img/mentors/anand-kv.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.9s" alt="Anand K V, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Anand K V</b></p>
	  					<!-- <p class="mb-10">Director, Grantmaking & Partnerships Deshpande Foundation India </p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/aravindchinchure/" target="_blank"><img src="img/mentors/aravind-chinchure.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="1.1s" alt="Dr. Aravind Chinchure, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Dr. Aravind Chinchure</b></p>
	  					<!-- <p class="mb-10">Director, Grantmaking & Partnerships Deshpande Foundation India </p> -->
	  				</div>
	  			</div>
	  			<br/>

	  			<div class="row text-center justify-content-md-center">
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/vivekgpawar" target="_blank"><img src="img/mentors/vivek-pawar.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Vivek Pawar, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Vivek Pawar</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  			<div class="col-md-2">
	  				<a href="https://www.linkedin.com/in/ramanathann" target="_blank"><img src="img/mentors/ramnathan.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s" alt="
	  					Ramanathan Narayan, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Ramanathan Narayan</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/poyni-bhatt-972b932/" target="_blank"><img src="img/mentors/poyni-bhatt.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" alt="Poyni Bhatt, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Poyni Bhatt</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/raj-hirwani-6b46926/" target="_blank"><img src="img/mentors/raj-hirwani.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.7s" alt="Raj Hirwani, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Raj Hirwani</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/vivekpunekar/" target="_blank"><img src="img/mentors/vivek-punekar.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.9s" alt="Vivek Punekar, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Vivek Punekar</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/rajendra-belgaumkar-83b13a38/" target="_blank"><img src="img/mentors/rajendra-belgaumkar.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="1.1s" alt="Rajendra Belgaumkar, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Rajendra Belgaumkar</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  			</div>
	  			<br>

	  			<div class="row text-center justify-content-md-center">
	  			<div class="col-md-2">
	  				<a href="https://www.linkedin.com/in/rajmelville" target="_blank"><img src="img/mentors/raj-melville.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Raj Melville, Mentor"></a>
	  				<p class="text-center text-yellow"><b>Raj Melville</b></p>
	  				<!-- <p class="mb-10">Chair Prof. of Innovation, Symbiosis University</p> -->
	  			</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/shyamvasudevaraoprofile/" target="_blank"><img src="img/mentors/shyam-vasudevarao.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s" alt="Shyam Vasudevarao, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Shyam Vasudevarao</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/ram-subramanian-002961179/" target="_blank"><img src="img/mentors/ram-subramanian.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" alt="Ram Subramanian, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Ram Subramanian</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/shreyanshsinghal/" target="_blank"><img src="img/edge/shreyansh.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.7s" alt="Shreyansh Singhal, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Shreyansh Singhal</b></p>
	  					<!-- <p class="mb-10">Director, Grantmaking & Partnerships Deshpande Foundation India </p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/mohanprototyze/" target="_blank"><img src="img/mentors/mohan-krishnan.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.9s" alt="Mohan Krishnan, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Mohan Krishnan</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="#" target="_blank"><img src="img/mentors/medha-pawar.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="1.1s" alt="Medha Pawar, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Medha Pawar</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  			</div>
	  			<br>

	  			<div class="row text-center justify-content-md-center">
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/menondp/" target="_blank"><img src="img/mentors/deepak-menon.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Deepak Menon, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Deepak Menon</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/rajeshranjan/" target="_blank"><img src="img/mentors/rajesh-ranjan.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s" alt="Rajesh Ranjan, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Rajesh Ranjan</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="#" target="_blank"><img src="img/mentors/srikrishna-joshi.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" alt="Srikrishna Joshi, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Srikrishna Joshi</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/vivek-nayak-9777024b/" target="_blank"><img src="img/mentors/vivek-nayak.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.7s" data-wow-offset="50" data-wow-delay="0.5s" alt="Vivek Nayak, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Vivek Nayak</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="#" target="_blank"><img src="img/mentors/venkatesh-patil.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.9s" alt="V. K. Patil, Mentor"></a>
	  					<p class="text-center text-yellow"><b>V. K. Patil</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/murugankathirvelu-3777209/" target="_blank"><img src="img/mentors/murugan-kathirvel.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="1.1s" alt="Murugan Kathirvel, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Murugan Kathirvel</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  			</div>
	  			<br>

	  			<div class="row text-center justify-content-md-center">
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/dominic-blakely-295260b/" target="_blank"><img src="img/mentors/dominic-blakely.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Dominic Blakely, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Dominic Blakely</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/prasadgpatil/" target="_blank"><img src="img/mentors/prasad-patil.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s" alt="Prasad Patil, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Prasad Patil</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/anandkadakol/" target="_blank"><img src="img/mentors/anand-kadakol.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" alt="Anand Kadakol, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Anand Kadakol</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/sudhir-dodamani-4762a17/" target="_blank"><img src="img/mentors/sudhir-dodamani.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.7s" alt="Sudhir Dodamani, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Sudhir Dodamani</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/d-a-dodamani-96785a15/" target="_blank"><img src="img/mentors/d-a-dodamani.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.9s" alt="D. A. Dodamani, Mentor"></a>
	  					<p class="text-center text-yellow"><b>D. A. Dodamani</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/ravindra-patwardhan/" target="_blank"><img src="img/mentors/ravindra-patwardhan.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="1.1s" alt="Ravindra Patwardhan, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Ravindra Patwardhan</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  			</div>
	  			<br>

	  			<div class="row text-center justify-content-md-center">
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/jigneshu/" target="_blank"><img src="img/mentors/jignesh-upadhyay.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Jignesh Upadhyay, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Jignesh Upadhyay</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/cmpatil/" target="_blank"><img src="img/team/c-m-patil.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s" alt="C. M. Patil, Mentor"></a>
	  					<p class="text-center text-yellow"><b>C. M. Patil</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/ravish-bhimani-22152369/" target="_blank"><img src="img/mentors/ravish-bhimani.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" alt="Ravish Bhimani, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Ravish Bhimani</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/rakesh-gupta-7b512b9/" target="_blank"><img src="img/mentors/rakesh-gupta.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.7s" alt="Rakesh Gupta, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Rakesh Gupta</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/gautam-khot-162294/" target="_blank"><img src="img/mentors/gautam-khot.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.9s" alt="Gautam Khot, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Gautam Khot</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/himanshuperiwal/" target="_blank"><img src="img/mentors/himanshu-periwal.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="1.1s" alt="Himanshu Periwal, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Himanshu Periwal</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  			</div>
	  			<br>

	  			<div class="row text-center justify-content-md-center">
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/ksasisekar" target="_blank"><img src="img/mentors/sasi-shekar.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Sasisekar Krish, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Sasisekar Krish</b></p>
	  					<!-- <p class="mb-10">Chair Prof. of Innovation, Symbiosis University</p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/p-n-nayak-95138ab/" target="_blank"><img src="img/mentors/p-n-nayak.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s" alt="P N Nayak, Mentor"></a>
	  					<p class="text-center text-yellow"><b>P N Nayak</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/bgk/" target="_blank"><img src="img/team/gopalkrishnan.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" alt="B Gopala Krishnan, Mentor"></a>
	  					<p class="text-center text-yellow"><b>B Gopala Krishnan</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/amit-ray-ops-strategy/" target="_blank"><img src="img/mentors/amit-ray.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.7s" alt="Amit Ray, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Amit Ray</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  				<div class="col-md-2">
	  					<a href="https://www.linkedin.com/in/mohammad-innus-khan-a4801732/" target="_blank"><img src="img/mentors/innus-khan.png" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.9s" alt="Innus Khan, Mentor"></a>
	  					<p class="text-center text-yellow"><b>Innus Khan</b></p>
	  					<!-- <p class="mb-10"></p> -->
	  				</div>
	  			</div>
	  			<br>
	  			<br>

	  		</div>
	  		<?php
	  		require_once 'essentials/footer.php';
	  		require_once 'essentials/copyright.php';
	  		require_once 'essentials/js.php';
	  		?>
	  	</body>
	  	</html>