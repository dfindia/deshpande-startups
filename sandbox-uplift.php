<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sandbox Uplift | Events, Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/sandbox-uplift"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/sandbox-uplift">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/sandbox-uplift.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/sandbox-uplift-bg.png">
	<meta property="og:description" content="Sandbox Uplift is an Initiative of Deshpande Startups. It is a platform for Startup Enthusiasts to nurture their ideas. Sandbox Uplift invites the Entrepreneurs across India to come up with their ideas, present their business Plan & Win Incubation Support from Deshpande Startups."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Sandbox Uplift is an Initiative of Deshpande Startups. It is a platform for Startup Enthusiasts to nurture their ideas. Sandbox Uplift invites the Entrepreneurs across India to come up with their ideas, present their business Plan & Win Incubation Support from Deshpande Startups."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Sandbox Uplift">
	<link rel="canonical" href="https://www.deshpandestartups.org/sandbox-uplift">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/sandbox-uplift-bg.png" width="1349" height="198" alt="Deshpande Startups, events, Sandbox Uplift">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Sandbox Uplift</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center text-uppercase wow slideInDown"><span class="text-muted">SANDBOX </span>Uplift</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/sandbox-uplift.png" width="474" height="237" alt="Deshpande startups, Sandbox Uplift ">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center text-truncate">Sandbox Uplift</h5>
									<!-- <p><b>Date : </b>June 13<sup>th</sup> 2020</p> -->
									<!-- <p class="text-yellow">The registrations has been closed.</p> -->
									<p><b>Last date to apply : </b>June 13<sup>th</sup> 2020</p>
									<p><b>Venue :</b> Virtual
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										<!-- M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br> -->
										M:<a href="tel:+91-951-331-5287"> +91-951-331-5287</a><br>
										E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
									</p>
									<br>
								</div>
								<div class="card-footer">
									<!-- <a href="#" class="btn p-1 btn-primary" target="_blank">Apply Now</a> -->
									<p class="text-yellow">The registrations has been closed.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-5 text-yellow mb-1"><b>Event Description:</b></p>
						<p class="text-justify wow slideInRight">"Sandbox Uplift" is an initiative by Deshpande Startups. It is a platform for Startup founders and aspiring entrepreneurs to present their ideas. Sandbox Uplift invites all entrepreneurs across India to present their business plan and have the chance to be part of our unique ecosystem.</p>
						<p class="text-yellow mb-1"><b>How does it work:</b></p>
						<ul class="wow slideInRight">
							<li><b>Step 1</b> - <!-- <a href="#" class="text-yellow" target="_blank"> -->Register<!-- </a> --> for the event</li>
							<li><b>Step 2</b> - After submitting the application, Please send your business deck to <a class="text-yellow" href="mailto:seir&#064;dfmail&#046;org">seir&#064;dfmail&#046;org</a></li>
							<li><b>Step 3</b> - Applications will be evaluated and shortlisted by a committee of experts</li>
							<!-- <li><b>Step 3</b> - The committee of experts will shortlist startups</li> -->
							<!-- <li><b>Step 4</b> - Shortlisted startups will be given the opportunity to pitch their business plan in front of our live panel</li> -->
							<li><b>Step 4</b> - Shortlisted startups will be given the opportunity to pitch their business plan virtually in front of our panel</li>
						</ul>
						<p class="text-yellow mb-1"><b>Business deck format:</b></p>
						<ul class="wow slideInRight">
							<li>About the company - 1 slide</li>
							<li>Team - 1 slide</li>
							<li>Problem statement - 1 slide</li>
							<li>Solution & USP - 1 slide</li>
							<li>Business model (Value proposition, Target market, Customers segments, Revenue model) - 1 slide</li>
							<li>Progress so far - 1 slide</li>
							<li>Market size & competition - 1 slide</li>
							<li>Why Deshpande Startups? & the ask - 1 slide</li>
							<li>Contact details of founders - 1 slide</li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>

	<br>

	<br>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>