<!doctype html>
<html lang="en">
<head>
	<title>SOP for ESDM SMT Process Input| ESDM </title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/esdm-cluster"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/sop-for-esdm-smt-process-input">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/deshpande-startups-esdm.jpg">
	<meta property="og:description" content="Deshpande ESDM Cluster is an initiative of Deshpande Startups along with Govt of Karnataka &amp; IESA with a vision to promote the ESDM industry in the North Karnataka"/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Deshpande ESDM Cluster is an initiative of Deshpande Startups along with Govt of Karnataka &amp; IESA with a vision to promote the ESDM industry in the North Karnataka"/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Deshpande ESDM Cluster">
	<link rel="canonical" href="https://www.deshpandestartups.org/esdm-cluster">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="/">Home</a></li>
			
			<li class="breadcrumb-item active" aria-current="page">SOP For ESDM SMT Process Input</li>
		</ol>
	</nav>

	<div class="container">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">SOP For ESDM SMT Process Input</h2>
      <div class="divider b-y text-yellow content-middle"></div>
		<div class="row">
			<div class="col-md-12 pl-5">
				<iframe src="pdf/sop-for-esdm-smt-process-input.pdf" width="100%" height="1000px" frameborder="0"></iframe>
			</div>
		</div>
	</div>
	<br>

	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once dirname(__FILE__).'/essentials/js.php';
	?>
</body>
</html>