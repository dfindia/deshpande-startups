<!DOCTYPE html>
<html lang="en">
<head>
	<title>EDGE - Idea to POC in 12-weeks</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/edge"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/edge">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/edge-bg.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/edge-theme.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/startups-launched.png">
	<meta property="og:description" content="With the objectives to nurture the entrepreneurial mindset and support first time entrepreneurs through their entrepreneurial journey, EDGE aims to aid in decreasing the failure rate of startups within India."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content= "With the objectives to nurture the entrepreneurial mindset and support first time entrepreneurs through their entrepreneurial journey, EDGE aims to aid in decreasing the failure rate of startups within India."/>
	<!-- <meta name="keywords" content="incubation, esdm cluster, pcb prototyping, Testing &amp; certification, arduino, raspberry pi boards, sandbox startups"/> -->
	<meta property="og:title" content="Deshpande Startups, EDGE Program">
	<link rel="canonical" href="https://www.deshpandestartups.org/edge">
	<link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">

	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		.white{color:#fff;}
		.request a{
			text-decoration: none;
		}
		.parallax {
			background-image: url("img/makers/deshpande.jpg");
			min-height: 300px; 
			background-attachment: fixed;
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
		}
		/* EDGE mentors */
		ul{
			margin:0;
			padding:0;
			list-style:none;
			line-height: 32px
		}
		.lh{
			line-height: 45px;
		}
		/* carousel indicators */
		@media (max-width: 768px){
			.carousel-indicators li{
				width: 14px !important;
				}}

				/* journey */
				section {
					padding: 60px 0;
				}

				#feedback a {
					display: block;
					background: #E66425;
					height: 40px;
					padding-top: 5px;
					width: 155px;
					text-align: center;
					color: #fff;
					font-family: Arial, sans-serif;
					/*font-size: 17px;*/
					font-weight: bold;
					text-decoration: none;
				}

				#feedback {
					height: 0px;
					width: 0px;
					/*width: 85px;*/
					position: fixed;
					/*right: 0;*/
					top: 50%;
					z-index: 998;
					transform: rotate(-90deg);
					-webkit-transform: rotate(-90deg);
					-moz-transform: rotate(-90deg);
					-o-transform: rotate(-90deg);
				}
			</style>
		</head>
		<body>
			<?php
			require_once 'essentials/title_bar.php';
			require_once 'essentials/menus.php';
			?>
			<!-- Generator: Jssor Slider Maker -->
			<div id="jssor_1" class="slide1">
				<!-- Loading Screen -->
				<div data-u="loading" class="jssorl-009-spin load">
					<img class="spin1" src="img/spin.svg" alt="spin" />
				</div>
				<div data-u="slides" class="slide2">
					<div data-p="170.00">
						<img data-u="image" src="img/bg-home/edge-bg.png" class="img-fluid" alt="Deshpande Startups, EDGE Program">
					</div>
					<div data-p="170.00">
						<a href="edge-form" target="_blank"><img data-u="image" src="img/bg-home/edge-theme.png" class="img-fluid" alt="EDGE theme"></a>
					</div>
					<div data-p="170.00">
						<img data-u="image" src="img/bg-home/startups-launched.png" class="img-fluid" alt="Deshpande Startups, EDGE Program, startups launched">
					</div>
					<div data-p="170.00">
						<img data-u="image" src="img/edge/impact-of-edge.png" class="img-fluid" alt="Deshpande Startups, EDGE Program, Impact of EDGE Program">
					</div>
				</div>
				<!-- Bullet Navigator -->
				<div data-u="navigator" class="jssorb051 bull" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
					<div data-u="prototype" class="i bulletnav">
						<svg viewbox="0 0 16000 16000" class="pos">
							<circle class="b" cx="8000" cy="8000" r="5800"></circle>
						</svg>
					</div>
				</div>
			</div>

			<nav aria-label="breadcrumb">
				<ol class="breadcrumb justify-content-end">
					<li class="breadcrumb-item"><a href="./">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page"> EDGE</li>
				</ol>
			</nav>

				<!-- <h2 class="text-yellow text-center wow fadeInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">ABOUT </span> EDGE</h2>
					<div class="divider b-y text-yellow content-middle"></div> -->
					<div class="container text-center p-3">
						<div class="row">
							<div class="col-md-12">
								<i class="fa fa-quote-left fa-4x pb-2"></i>
								<p class="wow zoomIn">
									<b>To help early-stage startups to create sustainable venture by prioritizing customer discovery, validating the existence of problem and thereby understanding nuances of entrepreneurship.</b>
								</p>
								<div class="divider b-y pb-4 w-75 text-yellow content-middle"></div>
								<h4 class="wow zoomIn"><b class="text-yellow"> EDGE</b> provides first-generation entrepreneurs with the opportunity to test, validate and continuously improve on their business ideas. This 12-week intensive program is centered on customer discovery where entrepreneurs are expected to interact with their customers,<br> gain insights, and keep pivoting and iterating their ideas.</h4>
								<div class="divider b-y pt-4 w-75 text-yellow content-middle"></div>
							</div>
						</div>
					<!-- <div class="text-center">
						<a href="edge-form" class="btn btn-warning" target="_blank">Apply Now</a>
					</div> -->
				</div>

				<div class="container">
					<div class="text-center">
						<h2 class="text-yellow  wow fadeInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">EDGE </span>JOURNEY</h2>
						<div class="divider b-y text-yellow content-middle"></div>
					</div>
				</div>

				<section id="team" class="container pb-4">
					<!-- <div class="container"> -->
						<div class="row">
							<!-- Team member -->
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="image-flip" ontouchstart="this.classList.toggle('hover');">
									<div class="mainflip">
										<div class="frontside">
											<div class="card">
												<div class="card-body text-center">
													<img class=" img-fluid" src="img/edge/1-4weeks.png" alt="card image">
												</div>
											</div>
										</div>
										<div class="backside">
											<div class="card">
												<div class="card-body">
													<h3 class="text-yellow text-center">1 - 4 Weeks</h3>
													<ul class="font-weight-bold">
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Problem understanding & intensity of the problem</li>
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Market Analysis - Know your industry & business players</li>
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Customer Analysis - Know your customer</li>
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Design Thinking - Empathy</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- ./Team member -->
							<!-- Team member -->
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="image-flip" ontouchstart="this.classList.toggle('hover');">
									<div class="mainflip">
										<div class="frontside">
											<div class="card">
												<div class="card-body text-center">
													<img class=" img-fluid" src="img/edge/5-8weeks.png" alt="card image">
												</div>
											</div>
										</div>
										<div class="backside">
											<div class="card">
												<div class="card-body">
													<h3 class="text-yellow text-center">5 - 8 Weeks</h3>
													<ul class="font-weight-bold">
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Customer discovery phase</li>
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> 50+ Potential customer interviews</li>
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Customer needs, pains & gains mapping</li>
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Early adaptor identification</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- ./Team member -->
							<!-- Team member -->
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="image-flip" ontouchstart="this.classList.toggle('hover');">
									<div class="mainflip">
										<div class="frontside">
											<div class="card">
												<div class="card-body text-center">
													<img class=" img-fluid" src="img/edge/9-12weeks.png" alt="card image">
												</div>
											</div>
										</div>
										<div class="backside">
											<div class="card">
												<div class="card-body">
													<h3 class="text-yellow text-center">9 - 12 Weeks</h3>
													<ul class="font-weight-bold lh">
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Product/ Solution Design</li>
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Value Proposition Design</li>
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Business Model Design</li>
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Business Plan Development</li>
														<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Investor Pitching</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- ./Team member -->

						</div>
						<!-- </div> -->
					</section>
					<!-- Team -->
					<!-- <div class="container"> -->
						<div class="container p-0">
							<div class="get-started text-center" style="visibility: visible;">
								<h2>Apply Now For  EDGE</h2>
								<div class="request">
									<h4><a href="edge-form" target="_blank"><b> Click here to register now</b></a></h4>
								</div>
							</div>
						</div>
						<!-- </div> -->
						<br>

						<h2 class="text-yellow text-center wow fadeInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">WHAT DO</span> WE OFFER</h2>
						<div class="divider b-y text-yellow content-middle"></div>
						<!-- <br> -->
						<div class="container">
							<br>
							<div class="row text-center">
								<div class="col-md-3">
									<img src="img/edge/mentorship-edge.png" class="img img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" width="200" height="200" alt=" EDGE, Mentorship with successful entrepreneurs">
									<p class="font-weight-bold">Mentorship from successful entrepreneurs/ industry experts</p>
								</div>
								<div class="col-md-3">
									<img src="img/edge/pitching-edge.png" class="img img-fluid wow zoomIn rounded-circle img-hover-shadow"  data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" width="200" height="200" alt=" EDGE, Pitching opportunities">
									<p class="font-weight-bold">Pitching opportunities to early stage investors/ Angel VC's</p>
								</div>
								<div class="col-md-3">
									<img src="img/edge/tools-frameworks.png" class="img img-fluid wow zoomIn rounded-circle img-hover-shadow"  data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" width="200" height="200" alt=" EDGE, Access to internationally accepted tools & frameworks">
									<p class="font-weight-bold">Access to internationally accepted tools & frameworks</p>
								</div>
								<div class="col-md-3">
									<img src="img/edge/co-working-edge.png" class="img img-fluid wow zoomIn rounded-circle img-hover-shadow"  data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" width="200" height="200" alt=" EDGE, Access to co-working space">
									<p class="font-weight-bold">Access to co-working space<br> & labs</p>
								</div>
							</div>
							<!-- <br> -->
							<div class="row justify-content-md-center text-center">
								<div class="col-md-3">
									<img src="img/edge/workshop-edge.png" class="img img-fluid wow zoomIn rounded-circle img-hover-shadow"  data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" width="200" height="200" alt=" EDGE, Workshops by industry experts">
									<p class="font-weight-bold">Workshops by industry experts</p>
								</div>
								<div class="col-md-3">
									<a href="incubation-support" target="_blank"><img src="img/edge/incubation-edge.png" class="img img-fluid wow zoomIn rounded-circle img-hover-shadow"  data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" width="200" height="200" alt=" EDGE, Entry to incubation program"></a>
									<p class="font-weight-bold">Entry to incubation program</p>
								</div>
							</div>
							<br>
						</div>
						<br>

						<h2 class="text-yellow wow slideInDown text-center" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">TESTIMONIALS</h2>
						<div class="divider b-y text-yellow content-middle"></div>
						<br>
						<div class="parallax inverse-text" data-parallax-img="img/makers/deshpande.jpg" data-parallax-img-width="1920" data-parallax-img-height="1078">
							<!-- <br> -->
							<div id="carousel">
								<div class="container text-white pt-5 pb-5">
									<div class="row justify-content-md-center">
										<div class="col-md-8">
											<div class="quote text-white"><i class="fa fa-quote-left fa-4x"></i></div>
											<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
												<!-- Carousel indicators -->
												<ol class="carousel-indicators">
													<li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>
													<li data-target="#fade-quote-carousel" data-slide-to="1"></li>
													<li data-target="#fade-quote-carousel" data-slide-to="2"></li>
													<li data-target="#fade-quote-carousel" data-slide-to="3"></li>
												</ol>
												<!-- Carousel items -->
												<div class="carousel-inner">
													<div class="item carousel-item active">
														<div class="profile-circle">
															<img src="img/edge/testimonial/ajay.png" width="100" height="100" class="img img-fluid" alt="ajay kabadi, Founder, Docketrun">
														</div>
														<p class="text-center">AJAY KABADI <br><span class="text-yellow">Founder, Docketrun</span></p>
														<blockquote>
															<p> EDGE is a great platform where every session was touch down approach where most of the use cases were relatable to our problems. Tools & Frameworks like Business Model Canvas & Design Thinking helped validate the idea through Market Survey.</p>
														</blockquote>
													</div>
													<div class="item carousel-item">
														<div class="profile-circle">
															<img src="img/edge/testimonial/priya.png" width="100" height="100" class="img img-fluid" alt="priya tolanavar, Founder, Innate Diapers">
														</div>
														<p class="text-center">PRIYA TOLANAVAR <br><span class="text-yellow">Founder, Innate Diapers</span></p>
														<blockquote>
															<p>It was a great experience. I learnt a lot from all the sessions, mentors and peers. Great networking platform also. I have a much clearer plan for my business now and have a lot of people I can discuss it with. Hoping to learn even more. A must attend if you are a start up.</p>
														</blockquote>
													</div>
													<div class="item carousel-item">
														<div class="profile-circle">
															<img src="img/edge/testimonial/aaqib.png" width="100" height="100" class="img img-fluid" alt="aaqib, Co-founder, DWAIL Private Limited">
														</div>
														<p class="text-center">AAQIB <br><span class="text-yellow">Co-founder, DWAIL Private Limited</span></p>
														<blockquote>
															<p>Excellent program for startups who are seriously pursuing scaling up. The program instructors have a proven track record and know what it takes to take the leap. Great opportunities to showcase your product and team to eminent personalities from industry and ecosystem.</p>
														</blockquote>
													</div>
													<div class="item carousel-item">
														<div class="profile-circle">
															<img src="img/edge/testimonial/vikas-j.png" width="100" height="100" class="img img-fluid" alt="vikas anand jamkhandi, Founder, Micro Electronic Controls">
														</div>
														<p class="text-center">VIKAS ANAND JAMKHANDI <br><span class="text-yellow">Founder, Micro Electronic Controls</span></p>
														<blockquote>
															<p>This program made me explore many different and unexplored business possibilities and approaches. Thank you Sandbox!</p>
														</blockquote>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /.container --> 
						</div>
						<br>

						<h2 class="text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">OUR</span> MENTORS
						</h2>
						<div class="divider b-y text-yellow content-middle"></div>
						<!-- <br> -->
						<!-- row1 -->
						<div class="container pt-2">
							<ul class="row justify-content-md-center">
								<li class="col-md-6 col-lg-3 col-sm-6">
									<div class="cnt-block">
										<img src="img/mentors/vivek-pawar.jpg" width="165" height="160" class="img rounded-circle img-fluid wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" alt="Vivek Pawar, CEO-Deshpande Foundation">
										<h3>Vivek Pawar</h3>
										<p>Chief Executive Officer,<br> Deshpande Foundation</p>
										<!-- <ul class="follow-us clearfix">
											<li><a href="#" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
										</ul> -->
										<a href="vivek-pawar" target="_blank" class="btn btn-outline-secondary">Know More</a>

									</div>
								</li>
								<li class="col-md-6 col-lg-3 col-sm-6">
									<div class="cnt-block">
										<img src="img/team/c-m-patil.jpg" width="165" height="160" class="img rounded-circle img-fluid wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" alt="C. M. Patil, CEO-Deshpande Startups">
										<h3>C. M. Patil</h3>
										<p>Chief Executive Officer,<br> Deshpande Startups</p>
										<a href="cmpatil" target="_blank" class="btn btn-outline-secondary">Know More</a>

									</div>
								</li>
								<li class="col-md-6 col-lg-3 col-sm-6">
									<div class="cnt-block">
										<img src="img/edge/shreyansh.png" width="165" height="160" class="img rounded-circle img-fluid wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" alt="Shreyansh, Venture Capital-Ankur Capital, EDGE Mentor">
										<h3>Shreyansh Singhal</h3>
										<p>Venture Capital,<br> Ankur Capital</p>
										<a href="shreyansh" target="_blank" class="btn btn-outline-secondary">Know More</a>
									</div>
								</li>
								<li class="col-md-6 col-lg-3 col-sm-6">
									<div class="cnt-block">
										<img src="img/edge/kuppulakshmi.png" width="165" height="160" class="img rounded-circle img-fluid wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" alt="Kuppulakshmi, product Evangelist-zoho, EDGE Mentor">
										<h3>Kuppulakshmi</h3>
										<p>Product Evangelist, <br>Zoho</p>
										<a href="kuppulakshmi" target="_blank" class="btn btn-outline-secondary">Know More</a>
									</div>
								</li>
								<li class="col-md-6 col-lg-3 col-sm-6">
									<div class="cnt-block">
										<img src="img/edge/divyeshshah.png" width="165" height="160" class="img rounded-circle img-fluid wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" alt="Divyesh Shah, Founder & CEO, LinkEZ Technologies">
										<h3>Divyesh Shah</h3>
										<p>Founder & CEO,<br> LinkEZ Technologies</p>
										<a href="divyesh-shah" target="_blank" class="btn btn-outline-secondary">Know More</a>
									</div>
								</li>
								<li class="col-md-6 col-lg-3 col-sm-6">
									<div class="cnt-block">
										<img src="img/edge/sasisekar.png" width="165" height="160" class="img rounded-circle img-fluid wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" alt="Sasisekar Krish, CEO & Co-founder, nanoPix">
										<h3>Sasisekar Krish</h3>
										<p>CEO & Co-founder,<br> nanoPix</p>
										<a href="sasisekar" target="_blank" class="btn btn-outline-secondary">Know More</a>
									</div>
								</li>
							</ul>
						</div>
						<!-- row1 -->
						<br>


						<div class="container">
							<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2 pb-3">
								<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">EDGE</span> GALLERY</h2>
								<div class="divider b-y text-yellow content-middle"></div>
							</div>
							<!-- <br> -->
							<div id="myBtnContainer">
								<button class="btn p-1 active btn-lg btn-warning" onclick="filterSelection('all')"> Show All</button>
								<button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('workshops')"> Workshops</button>
								<button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('sessions')"> Knowledge Sessions</button>
								<button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('mentors')"> Mentors Interaction</button>
								<button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('coworking')"> Co-working Space</button>
								<button class="btn p-1 btn-lg btn-warning" onclick="filterSelection('graduation')"> EDGE Graduation</button>
							</div>
							<br>

							<!-- EDGE Gallery Grid -->
							<div class="tz-gallery">
								<div class="row">
									<div class="col-md-3 gal workshops">
										<a class="lightbox" href="img/edge/gallery/big/workshop-1.png">
											<img src="img/edge/gallery/small/workshop-1.png" width="259" height="173" alt="EDGE, Workshops" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal workshops">
										<a class="lightbox" href="img/edge/gallery/big/workshop-2.png">
											<img src="img/edge/gallery/small/workshop-2.png" width="259" height="173" alt="EDGE, Workshops" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal workshops">
										<a class="lightbox" href="img/edge/gallery/big/workshop-3.png">
											<img src="img/edge/gallery/small/workshop-3.png" width="259" height="173" alt="EDGE, Workshops" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal workshops">
										<a class="lightbox" href="img/edge/gallery/big/workshop-4.png">
											<img src="img/edge/gallery/small/workshop-4.png" width="259" height="173" alt="EDGE, Workshops" class="img img-fluid img-thumbnail">
										</a>
									</div>

									<div class="col-md-3 gal sessions">
										<a class="lightbox" href="img/edge/gallery/big/knowledge-sharing-1.png">
											<img src="img/edge/gallery/small/knowledge-sharing-1.png" width="259" height="173" alt="EDGE, Knowledge sharing sessions" class="img img-fluid img-thumbnail">
										</a>																				
									</div>
									<div class="col-md-3 gal sessions">
										<a class="lightbox" href="img/edge/gallery/big/knowledge-sharing-2.png">
											<img src="img/edge/gallery/small/knowledge-sharing-2.png" width="259" height="173" alt="EDGE, Knowledge sharing sessions" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal sessions">
										<a class="lightbox" href="img/edge/gallery/big/knowledge-sharing-3.png">
											<img src="img/edge/gallery/small/knowledge-sharing-3.png" width="259" height="173" alt="EDGE, Knowledge sharing sessions" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal sessions">
										<a class="lightbox" href="img/edge/gallery/big/knowledge-sharing-4.png">
											<img src="img/edge/gallery/small/knowledge-sharing-4.png" width="259" height="173" alt="EDGE, Knowledge sharing sessions" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal mentors">
										<a class="lightbox" href="img/edge/gallery/big/girish-visit.jpg">
											<img src="img/edge/gallery/small/girish-visit.jpg" width="259" height="173" alt="EDGE, Mentors interaction" class="img img-fluid img-thumbnail">
										</a>																				
									</div>
									<div class="col-md-3 gal mentors">
										<a class="lightbox" href="img/edge/gallery/big/girish-visit1.jpg">
											<img src="img/edge/gallery/small/girish-visit1.jpg" width="259" height="173" alt="EDGE, Mentors interaction" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal mentors">
										<a class="lightbox" href="img/edge/gallery/big/sudha-murthy-visit2.jpg">
											<img src="img/edge/gallery/small/sudha-murthy-visit2.jpg" width="259" height="173" alt="EDGE, Mentors interaction" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal mentors">
										<a class="lightbox" href="img/edge/gallery/big/sudha-murthy-visit1.jpg">
											<img src="img/edge/gallery/small/sudha-murthy-visit1.jpg" width="259" height="173" alt="EDGE, Mentors interaction" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal coworking">
										<a class="lightbox" href="img/edge/gallery/big/edge-room.jpg">
											<img src="img/edge/gallery/small/edge-room.jpg" width="259" height="173" alt="Startups EDGE room" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal coworking">
										<a class="lightbox" href="img/edge/gallery/big/amphi-theater.jpg">
											<img src="img/edge/gallery/small/amphi-theater.jpg" width="259" height="173" alt="Startups Amphi Theater" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal coworking">
										<a class="lightbox" href="img/edge/gallery/big/coworking-space.jpg">
											<img src="img/edge/gallery/small/coworking-space.jpg" width="259" height="173" alt="Startups co working space" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal coworking">
										<a class="lightbox" href="img/edge/gallery/big/infrastructure1.jpg">
											<img src="img/edge/gallery/small/infrastructure1.jpg" width="259" height="173" alt="Startups" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal graduation">
										<a class="lightbox" href="img/edge/gallery/big/edge-graduation-1.png">
											<img src="img/edge/gallery/small/edge-graduation-1.png" width="259" height="173" alt="EDGE, Batch 1 Graduation" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal graduation">
										<a class="lightbox" href="img/edge/gallery/big/edge-graduation-2.png">
											<img src="img/edge/gallery/small/edge-graduation-2.png" width="259" height="173" alt="EDGE, Batch 2 Graduation" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal graduation">
										<a class="lightbox" href="img/edge/gallery/big/edge-graduation-3.png">
											<img src="img/edge/gallery/small/edge-graduation-3.png" width="259" height="173" alt="EDGE, Batch 3 Graduation" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<div class="col-md-3 gal graduation">
										<a class="lightbox" href="img/edge/gallery/big/edge-graduation-4.png">
											<img src="img/edge/gallery/small/edge-graduation-4.png" width="259" height="173" alt="EDGE, Batch 4 Graduation" class="img img-fluid img-thumbnail">
										</a>
									</div>
									<!-- END GRID -->
								</div>
							</div>
							<!-- END MAIN -->
							<br>
						</div>

						<div id="feedback">
							<a href="edge-form" target="_blank">Apply Now</a>
						</div>

						<script src="js/jssor.slider-27.1.0.min.js"></script>
						<script>
							jssor_1_slider_init = function() {

								var jssor_1_SlideshowTransitions = [
								{$Duration:800,$Opacity:2}
								];

								var jssor_1_options = {
									$AutoPlay: 1,
									$SlideshowOptions: {
										$Class: $JssorSlideshowRunner$,
										$Transitions: jssor_1_SlideshowTransitions,
										$TransitionsOrder: 1
									},
									$ArrowNavigatorOptions: {
										$Class: $JssorArrowNavigator$
									},
									$BulletNavigatorOptions: {
										$Class: $JssorBulletNavigator$
									}
								};

								var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

								/*#region responsive code begin*/

								var MAX_WIDTH = 1920;

								function ScaleSlider() {
									var containerElement = jssor_1_slider.$Elmt.parentNode;
									var containerWidth = containerElement.clientWidth;

									if (containerWidth) {

										var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

										jssor_1_slider.$ScaleWidth(expectedWidth);
									}
									else {
										window.setTimeout(ScaleSlider, 30);
									}
								}

								ScaleSlider();

								$Jssor$.$AddEvent(window, "load", ScaleSlider);
								$Jssor$.$AddEvent(window, "resize", ScaleSlider);
								$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
								/*#endregion responsive code end*/
							};
						</script>
						<script>jssor_1_slider_init();</script>

						<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
						<script>
							baguetteBox.run('.tz-gallery');
						</script>
						<script>
							filterSelection("all")
							function filterSelection(c) {
								var x, i;
								x = document.getElementsByClassName("gal");
								if (c == "all") c = "";
								for (i = 0; i < x.length; i++) {
									w3RemoveClass(x[i], "show");
									if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
								}
							}

							function w3AddClass(element, name) {
								var i, arr1, arr2;
								arr1 = element.className.split(" ");
								arr2 = name.split(" ");
								for (i = 0; i < arr2.length; i++) {
									if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
								}
							}

							function w3RemoveClass(element, name) {
								var i, arr1, arr2;
								arr1 = element.className.split(" ");
								arr2 = name.split(" ");
								for (i = 0; i < arr2.length; i++) {
									while (arr1.indexOf(arr2[i]) > -1) {
										arr1.splice(arr1.indexOf(arr2[i]), 1);     
									}
								}
								element.className = arr1.join(" ");
							}


// Add active class to the current button (highlight it)
var btnContainer = document.getElementById("myBtnContainer");
var btns = btnContainer.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
	btns[i].addEventListener("click", function(){
		var current = document.getElementsByClassName("active");
		current[0].className = current[0].className.replace(" active", "");
		this.className += " active";
	});
}
</script>

<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>