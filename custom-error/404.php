<!doctype html>
<html lang="en">
<head>
	<title>404 Deshpande Startups</title>
	<?php
	require_once '../essentials/meta.php';
	?>
      <meta name="linkage" content="https://www.deshpandestartups.org"/>
      <meta name="author" content="Deshpande Startups"/>
      <meta name="description" content="An eco-system of Resources, Connections, Knowledge & Talent to support mission driven entrepreneurs to scale in their venture."/>
      <!-- <meta name="keywords" content="Business, business ideas, How to make a business, Incubation center, Tejas networks, female entrepreneurs, How to be an entrepreneur, How to become an entrepreneur"/> -->
	<?php
	require_once '../essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once '../essentials/title_bar.php';
	require_once '../essentials/menus.php';
	?>
	<div class="container full-height">
		<div class="col-md-12 mi">
			<br/><br/><br/>
			<h5 class="title-w3">Page not found</h5>
			<div class="divider b-y text-yellow content-middle"></div>
			<br/>
			<p class="text-center">Unfortunately the page you are looking may be broken or removed,<br>
				if you think the page should be here, please contact the Administrator.
			</p>
			<br>
		</div>
	</div>
	<?php
	require_once '../essentials/footer.php';
	require_once '../essentials/copyright.php';
	require_once dirname(__FILE__).'/../essentials/js.php';
	?>
</body>
</html>