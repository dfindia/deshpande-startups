<!DOCTYPE html>
<html lang="en">
<head>
   <title>CEO - Foundation for Deshpande Startups Initiative</title>
   <?php
   require_once 'essentials/meta.php';
   ?>
   <meta name="linkage" content="https://www.deshpandestartups.org/fssi-ceo"/>
   <meta property="og:site_name" content="Deshpande Startups"/>
   <meta property="og:type" content="website">
   <meta property="og:url" content="https://www.deshpandestartups.org/fssi-ceo">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/team-image.png">
   <meta property="og:description" content="We are looking for a CEO who will be located in Hubballi, Karnataka, to join us and work with other impact units of the Deshpande Foundation to improve the lives of those living in semiurban and rural population."/>
   <meta name="author" content="Deshpande Startups"/>
   <meta name="description" content="We are looking for a CEO who will be located in Hubballi, Karnataka, to join us and work with other impact units of the Deshpande Foundation to improve the lives of those living in semiurban and rural population."/>
   <!-- <meta name="keywords" content=""/> -->
   <meta property="og:title" content="Deshpande Startups Career Opportunities - CEO">
   <link rel="canonical" href="https://www.deshpandestartups.org/fssi-ceo">
   <?php
         // $title = 'Deshpande Startups';
   require_once 'essentials/bundle.php';
   ?>
   <style type="text/css">
      .cal{
         font-family: calibri;
      }
      .btn-link:hover {
        color: #636567;
        /*color: #868e96;*/
        text-decoration: underline;
        background-color: transparent;
        border-color: transparent;
     }
     .marquee{
      background-color: #1717179e;
      /*background-color: #E66425;*/
      color: #fff;
   }
   marquee a{
      color: #fff;
      font-weight: bold;
   }
   /*p{text-align:justify;}*/
</style>
</head>
<body>
   <?php
   require_once 'essentials/title_bar.php';
   require_once 'essentials/menus.php';
   ?>
   

   <div class="pb-0 mb-0">
      <marquee width="100%" direction="left"  class="pt-2 pb-2 marquee">
         <p class="pb-0 mb-0" onmouseover="this.parentNode.stop()" 
         onmouseout="this.parentNode.start()">Please send your application to Email: <a href="mailto:ApplyCeoStartups&#064;gmail&#046;com" rel="noopener noreferrer">ApplyCeoStartups&#064;gmail&#046;com</a></p>
      </marquee>
   </div>

   <div class="container cal" id="contact-info">
      <!-- <br> -->
      <!-- <p>Current  Openings: Chief Executive Officer</p> -->
      <div class="center  wow fadeInDown pt-2">
         <h2 class="text-yellow text-center">Deshpande Startups<br> <span class="text-muted">CEO - Job Description</span> </h2>
         <div class="divider b-y text-yellow content-middle"></div>
      </div>
      <br>
      <!-- <div class="pull-right"><a href="career-opportunities" class="btn btn-warning btn-md " target="_blank">Apply Now</a></div> -->
      <h2  class="text-yellow cal pb-0 mb-0">Our Vision for Deshpande Startups</h2>

      <div class="col-md-12">
         <div class="row">
            <div class="col-md-6 pb-0 embed-responsive embed-responsive-16by9">
               <video width="100%" controls autoplay>
                 <source src="img/video/desh-deshpande-ceo-jd.mp4" type="video/mp4">
                    Your browser does not support HTML video.
                 </video>
              </div>
              <div class="col-md-6">
               <p class="text-justify">Our vision is to enable Entrepreneurship for All members of society.  We envision an environment where anyone can realize one’s entrepreneurial aspiration, with free and full access to our start-up ecosystem, regardless of one’s socio-cultural, geographic, educational or financial background or venture domain; one that will meet the realities of real India.</p>

               <p class="text-justify">We see a wide spectrum of start-ups emerging in India to meet the heterogeneous structure of the Indian economy and society. Venture-funded technology start-ups, whilst important, are just one such form and even there, the model still needs to evolve for India. We see the vibrancy of such entrepreneurship at scale drive the future of livelihoods, economic growth, global identity and social infrastructure development for India.</p>
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-md-12">
            <p class="text-justify pt-4">India has around 1.4 billion people. About 400 million of them live in cities and over 1 billion live in rural and semi-urban areas. The market place for the 1 billion has come to be known as BHARAT. For companies trying to address the needs of BHARAT, they need to be located close to their customer base. DESHPANDE STARTUPS, our incubator located in Hubballi, Karnataka, is an ideal location for such startups.</p>

            <p class="text-justify">Hubballi is part of India’s Smart City initiative that has significantly built up its infrastructure. The city can boast of all the benefits and amenities of larger metros while providing the ambiance and environment of smaller cities. From Hubballi one can quickly access rural India in the villages that surround the metro area. The incubator at Hubballi is the largest in India with 100,000 Sq. Ft. of space and state-of-the art facilities. The incubator offers access to rural and semi-urban customer base, motivated cost-effective talent and compassionate mentors who can nurture start-ups. The startups can draw on the expertise and support of 700 professionals of the Deshpande Foundation’s Hubballi sandbox who are actively engaged in programs for skill development, agriculture, livelihoods and entrepreneurship. With this deep understanding of the customer base and easy access to them, startups can refine their business models and grow their companies. The programs at the incubator also make entrepreneurship an accessible option for aspiring, context-aware founders from smaller cities such as Hubballi.</p>

            <p class="text-justify">The vision of Deshpande Startups is to be a home for the best startups in the country that cater to Bharat’s 1 billion people and to change the lives of the semi-urban and rural population in India.</p>

            <h2 class="text-yellow">Job Description - Key Responsibilities:</h2>
            <!-- <br> -->
            <p class="text-justify mb-0 font-weight-bold">The current CEO has done a good job of laying a solid foundation over the last four years</p>
            <ul>
               <li>Supported over 210 startups under various initiatives</li>
               <li>Created well-developed programs for both startups and scaleups</li>
               <li>Secured Government funds both for operations and funding the startups</li>
               <li>Established a reputation with thought leaders in the incubator space across India</li>
               <li>Accelerated implementation of virtual programs in response to the Covid-19 crisis</li>
               <li>Built a cohesive team of 25 members</li>
            </ul>
            <!-- <br> -->
            <p class="text-justify mb-0">We are looking for a leader who can build on this foundation by taking the organization to the next level and increasing its global recognition.<br>We are looking for a CEO who will be located in Hubballi, Karnataka, to join us and work with other impact units of the Deshpande Foundation to improve the lives of those living in semiurban and rural population.<br><b> The initiatives include:</b></p>

            <p class="text-justify mb-0"><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Continue to build on the virtual programs so that we can extend our reach throughout India for:</p>
            <ul>
               <li>The BEST entrepreneurs</li>
               <li>The BEST mentors</li>
               <li>The MOST appropriate capital for the ventures</li>
               <li>The BEST practices to nurture startups</li>
               <li>The BEST talent in the country to run the incubator</li>
            </ul>
            <p class="text-justify"><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Make Deshpande Startups the home for the top companies in the country serving the semi-urban and rural population.</p>

            <p class="text-justify">In addition to the overall success of the incubator, the CEO is responsible for leading the development and execution of long-term strategies.<br></p>

            <p class="text-yellow font-weight-bold mb-0"> The duties of the CEO include:</p>
            <ul class="">
               <li>Communicating, on behalf of the incubator, with entrepreneurs, government entities, investors, industry and the public</li>
               <li>Positioning and representing Deshpande Startups on the national stage</li>
               <li>Maintaining strategic relationships with key governmental and industry leaders</li>
               <li>Leading the development of short and long term strategy</li>
               <li>Reviewing, updating and implementing the vision and mission</li>
               <li>Hiring and managing the team</li>
               <li>Raising funds for operations and funds for the startups</li>
               <li>Maintaining awareness of the landscape in the country for entrepreneurship</li>
               <li>Owning the mindshare of the thought leaders in the startup space in the country</li>
               <li>Running helpful programs for entrepreneurs so that the best entrepreneurs seek and compete to get into the incubator</li>
               <li>Support the development of an ecosystem for Bharat startups by seeding entrepreneurial culture in the region, influencing the development of alternative funding models for such startups, etc.</li>
               <li>Setting strategic goals and making sure they are measurable and describable</li>
               <li>Leveraging sister organizations within the Deshpande Foundation</li>
            </ul>

            <!-- <br> -->
            <p class="font-weight-bold mb-0 text-yellow">Key Characteristics of the CEO would include:</p>
            <ul class="">
               <li>A motivational leader</li>
               <li>Passionate about building solutions for BHARAT</li>
               <li>Mission driven and committed to making change happen</li>
               <li>A strategic thinker with a track record of turning plans into successful implementations</li>
               <li>An articulate communicator comfortable on a national stage</li>
               <li>Able to build and maintain strategic relationships</li>
               <li>Team oriented willing to work across internal and external organizations</li>
            </ul>

            <h2 class="text-yellow">Education and Experience:</h2>
            <p class="text-justify">Bachelor's or Master’s Degree<br> Senior leadership experience having nurtured entrepreneurs or gone through entrepreneurial experience.</p>
         </div>
      </div>
   </div>
   <!-- <br> -->
   <div class="container cal text-center">
      <div class="row">
       <div class="col-md-12">
         <p class="mb-0">If you are a visionary leader who wants to change the world and interested in pursuing this opportunity, please send your application to<br> Email: <a href="mailto:ApplyCeoStartups&#064;gmail&#046;com" target="_blank" rel="noopener noreferrer">ApplyCeoStartups&#064;gmail&#046;com</a> </p>
      <!-- <div class="pt-2">
      <a href="career-opportunities" class="btn btn-warning btn-md" target="_blank">Apply Now</a>
   </div> -->
   <!-- <br> -->
   <!-- <p class="pt-3">For more details <a href="https://www.linkedin.com/posts/deshdeshpande_startups-entrepreneurship-activity-6709215521683881984-s44R/" target="_blank" rel="nofollow"> click here</p> -->
      <p class=""><b>Job Location:</b> Hubballi, Karnataka.</p>
   </div>
</div>
</div>
<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>