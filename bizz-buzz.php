<!DOCTYPE html>
<html lang="en">
<head>
	<title>Events | Bizz-Buzz</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/bizz-buzz"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/bizz-buzz">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/bizz-buzz.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/bizz-buzz-bg.png">
	<meta property="og:description" content="Deshpande Startups Presents an opportunity to all the startups to Pitch your Idea & stand a chance to win Incubation Support."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Deshpande Startups Presents an opportunity to all the startups to Pitch your Idea & stand a chance to win Incubation Support."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="BIZZ-BUZZ, Deshpande Startups Event">
	<link rel="canonical" href="https://www.deshpandestartups.org/bizz-buzz">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	

</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/bizz-buzz-bg.png" width="1349" height="198" alt="Deshpande Startups, events, BIZZ-BUZZ">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Bizz Buzz</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">BIZZ-</span>BUZZ</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/bizz-buzz.png" width="474" height="237" alt="Deshpande startups, events, BIZZ-BUZZ">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center text-truncate">BIZZ-BUZZ</h5>
									<p><b>Date : </b>September 14<sup>th</sup> 2019</p>
									<!-- <p><b>Time : </b>10:00 AM</p> -->
									<p><b>Last date to register : </b>September 10<sup>th</sup> 2019</p>
									<p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br>
										E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
									</p>
								</div>
								<div class="card-footer">
									<!-- <a href="#" class="btn btn-warning" target="_blank">Apply Now</a> -->
									<p class="text-yellow">The registrations has been closed.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-5 text-yellow"><b>Event description:</b></p>
						<p class="text-justify wow slideInRight">BIZZ-BUZZ is an outreach program of Deshpande Startups which  provides a platform for external startups to showcase their idea to an audience comprising of angel investors, mentors, technology experts & access the support system of Deshpande Startups.</p>
						<p><b class="text-yellow">How does it work:</b></p>
						<ul class="wow slideInRight text-justify">
							<li><b>Step 1</b> - Register now</li>
							<li><b>Step 2</b> - All applications shall be evaluated by a committee of experts</li>
							<li><b>Step 3</b> - The committee of experts will shortlist startups</li>
							<li><b>Step 4</b> - Shortlisted startups shall then be given an opportunity to pitch their ideas in front of panel physically in our ecosystem</li>
						</ul>
						<p><b class="text-yellow">Business deck format:</b></p>
						<ul class="wow slideInRight">
							<li>Introduction of the company & founding team - 1 slide</li>
							<li>Problem statement - 1 slide</li>
							<li>Your business deck - 1 to 3 slide</li>
							<li>Uniqueness about your ideas - 1 to 2 slide</li>
							<li>Business competition/existing solution providers - 1 slide</li>
							<li>Market size/opportunity - 1 to 2 slide</li>
							<li>Revenue model</li>
						</ul>
					</div>
				</div>

				<p class="text-yellow font-weight-bold">What does BIZZ-BUZZ  has to offer to entrepreneurs:</p>
				<ul class="wow slideInRight text-justify">
					<li>Opportunity to pitch your business plan to panel of experts, entrepreneurs, industry veteran & angel investor</li>
					<li>Opportunity to get access to personalized support to grow your startup through Deshpande startups incubation program</li>
					<li>Access to our state-of-the-art Makers Lab and ESDM Cluster to build your products/solutions</li>
					<li>Network, connect and receive inputs from industry leading mentors, industry experts, investors</li>
					<li>Access to in-house seed-funding & VC connect for elegible entrepreneurs</li>
				</ul>
				<p class="text-justify wow slideInRight"><h4 class="text-danger font-weight-bold">Note:</h4> After submitting the Form, Please send your business deck to <a href="mailto:seir&#064;dfmail&#046;org">seir&#064;dfmail&#046;org</a></p>

			</div>
		</div>
	</div>
	<br>

<br>
<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>