<!DOCTYPE html>
<html lang="en">
<head>
	<title>Co-working - Deshpande startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/co-working"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/co-working">
	<!-- <meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/contact-us.jpg"> -->
	<meta property="og:description" content=""/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content=""/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Co-working San  Startups">
	<link rel="canonical" href="https://www.deshpandestartups.org/co-working">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<div class="container full-height">
		<div class="col-md-12 mi">
			<br/><br/><br/>
			<h5 class="title-w3">This page is under development.</h5>
			<div class="divider b-y text-yellow content-middle"></div>
			<br/>
			<!-- <p class="text-center">This page is under development.</p> -->
			<br>
		</div>
	</div>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once dirname(__FILE__).'/essentials/js.php';
	?>
</body>
</html>