<?php
namespace DotsUnited\BundleFu\Filter;

class CallbackFilter implements FilterInterface
{
    /**
     * @var mixed
     */
    protected $callback;

    /**
     * @var mixed
     */
    protected $callbackFile;

    /**
     * Constructor.
     *
     * @param mixed $callback
     * @param mixed $callbackFile
     */
    public function __construct($callback = null, $callbackFile = null)
    {
        $this->callback     = $callback;
        $this->callbackFile = $callbackFile;
    }

    /**
     * {@inheritDoc}
     */
    public function filter($content)
    {
        if (null === $this->callback) {
            return $content;
        }

        return call_user_func($this->callback, $content);
    }

    /**
     * {@inheritDoc}
     */
    public function filterFile($content, $file, \SplFileInfo $fileInfo, $bundleUrl, $bundlePath)
    {
        if (null === $this->callbackFile) {
            return $content;
        }

        return call_user_func($this->callbackFile, $content, $file, $fileInfo, $bundleUrl, $bundlePath);
    }
}
