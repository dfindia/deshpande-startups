<?php
namespace Framework\Helper;

use Framework\Exceptions\FrameworkSDKException;

// define('VIEWPATH', realpath(dirname(__FILE__).'../../../../../').'/');
// define('VIEWPATH', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..').'/');
define('VIEWPATH', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..').'/');

/**
*
*/
class Load
{


	/**
	* List of paths to load views from
	*
	* @var	array
	*/
	protected $_view_paths =	array(VIEWPATH => TRUE);

	protected $_cached_vars =	array();


	/**
	* View Loader
	*
	* Loads "view" files.
	*
	* @param	string	$view	View name
	* @param	array	$vars	An associative array of data
	*				to be extracted for use in the view
	* @param	bool	$return	Whether to return the view output
	*				or leave it to the Output class
	* @return	object|string
	*/
	public function view($view, $vars = array(), $return = FALSE)
	{
		return $this->_load(array('_view' => $view, '_vars' => $this->_object_to_array($vars), '_return' => $return));
	}


	public function _load($_data)
	{
			// print_r($_view);
		// Set the default data variables
		foreach (array('_view', '_vars', '_path', '_return') as $_val)
		{
			$$_val = isset($_data[$_val]) ? $_data[$_val] : FALSE;
		}
		$file_exists = FALSE;
		// Set the path to the requested file
		if (is_string($_path) && $_path !== '')
		{
			$_x = explode('/', $_path);
			$_file = end($_x);
		}
		else
		{
			$_ext = pathinfo($_view, PATHINFO_EXTENSION);
			$_file = ($_ext === '') ? $_view.'.php' : $_view;
			foreach ($this->_view_paths as $_view_file => $cascade)
			{
				if (file_exists($_view_file.$_file))
				{
					$_path = $_view_file.$_file;
					$file_exists = TRUE;
					break;
				}
				if ( ! $cascade)
				{
					break;
				}
			}
		}
		if ( ! $file_exists && ! file_exists($_path))
		{
			throw new FrameworkSDKException('Unable to load the requested file: '.$_file);
		}
		// This allows anything loaded using $this->load (views, files, etc.)
		// to become accessible from within the Controller and Model functions.
		/*$_CI =& get_instance();
		foreach (get_object_vars($_CI) as $_key => $_var)
		{
			if ( ! isset($this->$_key))
			{
				$this->$_key =& $_CI->$_key;
			}
		}*/
		/*
		 * Extract and cache variables
		 *
		 * You can either set variables using the dedicated $this->load->vars()
		 * function or via the second parameter of this function. We'll merge
		 * the two types and cache them so that views that are embedded within
		 * other views can have access to these variables.
		 */
		if (is_array($_vars))
		{
			$this->_cached_vars = array_merge($this->_cached_vars, $_vars);
		}
		extract($this->_cached_vars);
		/*
		 * Buffer the output
		 *
		 * We buffer the output for two reasons:
		 * 1. Speed. You get a significant speed boost.
		 * 2. So that the final rendered template can be post-processed by
		 *	the output class. Why do we need post processing? For one thing,
		 *	in order to show the elapsed page load time. Unless we can
		 *	intercept the content right before it's sent to the browser and
		 *	then stop the timer it won't be accurate.
		 */
		ob_start();
		// If the PHP installation does not support short tags we'll
		// do a little string replacement, changing the short tags
		// to standard PHP echo statements.
		if ( ! version_compare(phpversion(), '5.3.10', '<') && ! ini_get('short_open_tag') && FALSE)
		{
			echo eval('?>'.preg_replace('/;*\s*\?>/', '; ?>', str_replace('<?=', '<?php echo ', file_get_contents($_path))));
		}
		else
		{
			include($_path); // include() vs include_once() allows for multiple views with the same name
		}
		// log_message('info', 'File loaded: '.$_path);
		// Return the file data if requested
		if ($_return === TRUE)
		{
			$buffer = ob_get_contents();
			@ob_end_clean();
			return $buffer;
		}
		/*
		 * Flush the buffer... or buff the flusher?
		 *
		 * In order to permit views to be nested within
		 * other views, we need to flush the content back out whenever
		 * we are beyond the first level of output buffering so that
		 * it can be seen and included properly by the first included
		 * template and any subsequent ones. Oy!
		 */
		if (ob_get_level() > 1)
		{
			ob_end_flush();
		}
		else
		{
			// $_CI->output->append_output(ob_get_contents());
			@ob_end_clean();
		}
		return $this;
	}

	/**
	* CI Object to Array translator
	*
	* Takes an object as input and converts the class variables to
	* an associative array with key/value pairs.
	*
	* @param	object	$object	Object data to translate
	* @return	array
	*/
	protected function _object_to_array($object)
	{
		return is_object($object) ? get_object_vars($object) : $object;
	}

}