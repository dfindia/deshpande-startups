<?php
namespace Framework\Exceptions;

/**
 * Class FrameworkSDKException
 *
 * @package Framework
 */
class FrameworkSDKException extends \Exception
{
}
