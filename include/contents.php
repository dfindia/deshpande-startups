<!DOCTYPE html>
<html>
<head>
	<title>
		Deshpande Startups
	</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
	#outlook a {
		padding: 0;
	}
	.ReadMsgBody {
		width: 100%;
	}
	.ExternalClass {
		width: 100%;
	}
	.ExternalClass * {
		line-height: 100%;
	}
	body {
		margin: 0;
		padding: 0;
		-webkit-text-size-adjust: 100%;
		-ms-text-size-adjust: 100%;
	}
	table,
	td {
		border-collapse: collapse;
		mso-table-lspace: 0pt;
		mso-table-rspace: 0pt;
	}
	img {
		border: 0;
		height: auto;
		line-height: 100%;
		outline: none;
		text-decoration: none;
		-ms-interpolation-mode: bicubic;
	}
	p {
		display: block;
		margin: 13px 0;
	}
</style>
<style type="text/css">
@media only screen and (min-width:480px) {
	.mj-column-per-100 {
		width: 100% !important;
	}
	.mj-column-per-45 {
		width: 45% !important;
	}
}
</style>
<style type="text/css">
</style>
</head>
<body>
	<div>
		<div style="Margin:0px auto;max-width:600px;">
			<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
				<tbody>
					<tr>
						<td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
							<div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
								<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
									<tr>
										<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
												<tbody>
													<tr>
														<td style="width:550px;">
															<a href="https://recast.ai?ref=newsletter" target="_blank">
																<img
																height="auto" src="https://cdn.recast.ai/newsletter/city-01.png" style="border:0;display:block;outline:none;text-decoration:none;width:100%;" width="550"
																/>
															</a>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div style="Margin:0px auto;max-width:600px;">
			<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
				<tbody>
					<tr>
						<td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
							<div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
								<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
									<tr>
										<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<div style="font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#616161;">
												You have an email from deshpandestartups.org career online form!
											</div>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div style="Margin:0px auto;max-width:600px;">
			<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
				<tbody>
					<tr>
						<td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
							<div class="mj-column-per-45 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
								<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
									<tr>
										<td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
											<div style="font-family:Roboto, Helvetica, sans-serif;font-size:18px;font-weight:500;line-height:24px;text-align:center;color:#616161;">
												Name: <?php echo $firstname.' '.$lastname; ?>
											</div>
										</td>
									</tr>
									<tr>
										<td style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="border-top:solid 2px #616161;font-size:1;margin:0px auto;width:100%;">
											</p>
										</td>
									</tr>
									<tr>
										<td style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="border-top:solid 2px #616161;font-size:1;margin:0px auto;width:45%;">
											</p>
										</td>
									</tr>
									<tr>
										<td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
											<div style="font-family:Roboto, Helvetica, sans-serif;font-size:18px;font-weight:500;line-height:24px;text-align:center;color:#616161;">
												Email: <?php echo $emailaddress; ?>
											</div>
										</td>
									</tr>
									<tr>
										<td style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="border-top:solid 2px #616161;font-size:1;margin:0px auto;width:100%;">
											</p>
										</td>
									</tr>
									<tr>
										<td style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="border-top:solid 2px #616161;font-size:1;margin:0px auto;width:45%;">
											</p>
										</td>
									</tr>
									<tr>
										<td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
											<div style="font-family:Roboto, Helvetica, sans-serif;font-size:18px;font-weight:500;line-height:24px;text-align:center;color:#616161;">
												Phone Number: <?php echo $mobilenumber; ?>
											</div>
										</td>
									</tr>
									<tr>
										<td style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="border-top:solid 2px #616161;font-size:1;margin:0px auto;width:100%;">
											</p>
										</td>
									</tr>
									<tr>
										<td style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="border-top:solid 2px #616161;font-size:1;margin:0px auto;width:45%;">
											</p>
										</td>
									</tr>
									<tr>
										<td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
											<div style="font-family:Roboto, Helvetica, sans-serif;font-size:18px;font-weight:500;line-height:24px;text-align:center;color:#616161;">
												Is Interested In: <?php echo $select; ?>
											</div>
										</td>
									</tr>
									<tr>
										<td style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="border-top:solid 2px #616161;font-size:1;margin:0px auto;width:100%;">
											</p>
										</td>
									</tr>
									<tr>
										<td style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="border-top:solid 2px #616161;font-size:1;margin:0px auto;width:45%;">
											</p>
										</td>
									</tr>
									<tr>
										<td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
											<div style="font-family:Roboto, Helvetica, sans-serif;font-size:18px;font-weight:500;line-height:24px;text-align:center;color:#616161;">
												Here's what he/she comment with: <?php echo $comments; ?>
											</div>
										</td>
									</tr>
									<tr>
										<td style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="border-top:solid 2px #616161;font-size:1;margin:0px auto;width:100%;">
											</p>
										</td>
									</tr>
									<tr>
										<td style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="border-top:solid 2px #616161;font-size:1;margin:0px auto;width:45%;">
											</p>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div style="Margin:0px auto;max-width:600px;">
			<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
				<tbody>
					<tr>
						<td style="direction:ltr;font-size:0px;padding:0px;padding-top:30px;text-align:center;vertical-align:top;">
							<div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
								<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
									<tr>
										<td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<div style="font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:left;color:#616161;">
												<p>Hi There!</p>
												<p> Don't panic. This is a automatically generated email!. Link to the form is <?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?></p>
												<p>This is regarding the career opportunities online form, the details above and resume attached will help you through!</p>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div style="Margin:0px auto;max-width:600px;">
			<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
				<tbody>
					<tr>
						<td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
							<div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
								<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
									<tr>
										<td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<table align="left" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
												<tbody>
													<tr>
														<td style="width:150px;">
															<a href="https://www.deshpandestartups.org/" target="_blank">
																<img
																height="auto" src="https://www.deshpandestartups.org/img/deshpande-startup-logo.png" style="border:0;display:block;outline:none;text-decoration:none;width:100%;" width="150"
																/>
															</a>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>