<!DOCTYPE html>
<html lang="en">
<head>
	<title>Webinar - Decoding bharat market | Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/past-webinar"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/past-webinar">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/webinar.jpg">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/webinar-bg.jpg">
	<meta property="og:description" content="The world is reeling from the COVID-19 pandemic. Indian Startups are finding it difficult with innumerable challenges battered in all directions but more critically important for our future than ever."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="The world is reeling from the COVID-19 pandemic. Indian Startups are finding it difficult with innumerable challenges battered in all directions but more critically important for our future than ever."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Webinar - Decoding bharat market">
	<link rel="canonical" href="https://www.deshpandestartups.org/past-webinar">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">

	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/webinar-bg.jpg" width="1349" height="198" alt="Deshpande Startups, events, Webinar - Decoding bharat market">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Webinar</li>
		</ol>
	</nav>



	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center text-uppercase wow slideInDown"><span class="text-muted">DECODING BHARAT MARKET</span> WEBINAR SERIES</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>

		<div class="row">
			<div class="col-md-12 px-5">
				<p class="text-yellow"><b>Event Description:</b></p>
				<p class="text-justify wow slideInRight">The world is reeling from the COVID-19 pandemic. Indian startups are finding it difficult with innumerable challenges battered in all directions most of them are repurposing to be meaningful to the market situation. It's also true entrepreneurs are the only breed who have abilities to convert challenges into meaningful opportunities and build sustainable ventures.</p>
				<p class="text-justify wow slideInRight">As an ecosystem that has been enabling startups we felt the need to turn this challenge and bring to the forefront the opportunities that lie in Bharat. Our webinar series "Decoding Bharat Market" is designed to bring together leading industry veterans who are unleashing the opportunities lying in the Bharat ecosystem which is largely untapped.</p>
				<p class="wow slideInRight">We invite you to join us for inspiring insights, learning and to explore the opportunities in Bharat market.</p>
				<!-- <br> -->

				<h4 class="text-yellow text-center pt-2 pb-2"><b>First webinar on “Envisioning Bharat Opportunities”</b></h4>
				<p class="text-justify wow slideInRight">The webinar will have discussions on how to convert Bharat market challenges into opportunities and what it takes to build an enterprise in Bharat ecosystem.</p>
			</div>
		</div>
	</div>


	<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
		<h2 class=" text-yellow text-center text-uppercase wow slideInDown"><span class="text-muted">OUR </span> SPEAKERS</h2>
		<div class="divider b-y text-yellow content-middle"></div>
	</div>
	<div class="featured-bg-container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<div class="row text-center justify-content-md-center">
				<div class="col-md-2">
					<a href="https://www.linkedin.com/in/ajaichowdhry/" target="_blank"><img src="img/speakers/ajai-choudhry.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" alt="Shri. Ajai Chowdhry, Speaker"></a>
					<p class="text-center text-yellow mb-0"><b>Shri. Ajai Chowdhry</b></p>
					<small>Founder-HCL, Co-founder-Indian ANGEL Network</small>
				</div>
				<div class="col-md-2">
					<a href="https://www.linkedin.com/in/mohandaspai/" target="_blank"><img src="img/speakers/mohandas-pai.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s" alt="Shri. T. V. Mohandas Pai, Speaker"></a>
					<p class="text-center text-yellow mb-0"><b>Shri. T. V. Mohandas Pai</b></p>
					<small>Chairman, Manipal Global</small>
				</div>
				<div class="col-md-2">
					<a href="https://www.linkedin.com/in/deshdeshpande/" target="_blank"><img src="img/speakers/desh-deshpande.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.5s" alt="Dr. Gururaj Desh Deshpande, Founder, Speaker"></a>
					<p class="text-yellow text-center mb-0"><b>Dr. Gururaj "Desh" Deshpande</b></p>
					<small>Founder, Deshpande Foundation</small>
				</div>
				<div class="col-md-2">
					<a href="https://www.linkedin.com/in/harsh-bhanwala-85731716/" target="_blank"><img src="img/speakers/harsh-bhanawala.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.7s" alt="Shri. Harsh Kumar Bhanwala, Speaker"></a>
					<p class="text-center text-yellow mb-0"><b>Shri. Harsh Kumar Bhanwala</b></p>
					<small>Chairman, NABARD</small>
				</div>
				<div class="col-md-2">
					<a href="https://www.linkedin.com/in/dr-anita-gupta-a04b609/" target="_blank"><img src="img/speakers/anita-gupta.jpg" width="165" height="160" class="img-fluid rounded-circle wow zoomIn" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.9s" alt="Dr. Anita Gupta, Speaker"></a>
					<p class="text-center text-yellow mb-0"><b>Dr. Anita Gupta</b></p>
					<small>Head of Innovation & Entrepreneurship, DST, Govt. of India</small>
				</div>
			</div>
		</div>

	</div>
	<br/>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h4 class="text-center wow slideInRight"><b>Our first webinar on “Envisioning Bharat Opportunities” is scheduled on 31<sup>st</sup> May 2020 from 5:00pm to 6:00pm</b></h4>
				<br>
				<div class="row featurette valign-wrapper">
					<div class="col-md-6">
						<p class="text-yellow"><b>The webinars will focus on the following aspects:</b></p>
						<ul class="wow slideInRight">
							<li>How to look at challenges as opportunity?</li>
							<li>Mindset shift necessary for an entrepreneur to build enterprise</li>
							<li>Opportunities available for entrepreneurs in Bharat market</li>
							<li>What does it take to build an enterprise?</li>
						</ul>
					</div>
					<div class="col-md-6 youtube-wrapper">
						<div class="embed-responsive youtube mb-3" data-embed="bSkSneSozQw">
							<div class="play-button"></div>
						</div>
					</div>
				</div>
				<div class="text-center pt-2">
					<a href="https://youtu.be/bSkSneSozQw" class="btn btn-warning px-5" target="_blank">Click here for video</a>
				</div>
				<!-- <br> -->
						<!-- <div class="text-center pt-3">
							<h4 class="text-yellow pb-2">Apply now for the webinar</h4>
							<a href="#" class="btn btn-warning px-5" target="_blank">Register now</a>
						</div> -->
					</div>
				</div>
			</div>
			<br>

			<!-- <br> -->
			<br>
			<br>

			<?php
			require_once 'essentials/footer.php';
			require_once 'essentials/copyright.php';
			require_once 'essentials/js.php';
			?>
		</body>
		</html>