<!DOCTYPE html>
<html lang="en">
<head>
   <title>Apply now for NIDHI PRAYAS</title>
   <?php
   require_once 'essentials/meta.php';
   ?>
   <meta name="linkage" content="https://www.deshpandestartups.org/nidhi-prayas-form"/>
   <meta property="og:site_name" content="Deshpande Startups"/>
   <meta property="og:type" content="website">
   <meta property="og:url" content="https://www.deshpandestartups.org/nidhi-prayas-form">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/funded-startups/nidhi-prayas-benefits.jpg">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/funded-startups/nidhi-focus-area.jpg">
   <meta property="og:description" content="A NIDHI-PRomotion and Acceleration of Young and Aspiring technology entrepreneur (NIDHI-PRAYAS) - Support from Idea to Prototype, is the scheme under NIDHI, aimed at addressing the gap in the very early stage of an idea/proof of concept funding"/>
   <meta name="author" content="Deshpande Startups"/>
   <meta name="description" content="A NIDHI-PRomotion and Acceleration of Young and Aspiring technology entrepreneur (NIDHI-PRAYAS) - Support from Idea to Prototype, is the scheme under NIDHI, aimed at addressing the gap in the very early stage of an idea/proof of concept funding"/>
   <!-- <meta name="keywords" content=""/> -->
   <meta property="og:title" content="Apply now for NIDHI Prayas">
   <link rel="canonical" href="https://www.deshpandestartups.org/nidhi-prayas-form">
   <?php
         // $title = 'Deshpande Startups';
   require_once 'essentials/bundle.php';
   ?>
   <style type="text/css">
      .disp{
         display:none;
      }
   </style>
</head>
<body>
   <?php
   require_once 'essentials/title_bar.php';
   require_once 'essentials/menus.php';
   ?>
   <br>
   <br>
   <div class="container pt-5">
         <div class="row justify-content-md-center">
            <br>
            <div class="col-md-10">
               <h4 class="text-center">Registration for the NIDHI PRAYAS 1.0 has been closed because we have reached the maximum number of registrations for the event.<br><br> Thank you for your interest!</h4>
               
            </div>
         </div>
      </div>
      <br>
      <br>
      <br>
      <br>
      <br>
         <script src='https://www.google.com/recaptcha/api.js'></script>
         <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once 'essentials/js.php';
         ?>

</body>
</html>