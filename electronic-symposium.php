<!DOCTYPE html>
<html lang="en">
<head>
	<title>Electronics Symposium | Events, Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/electronic-symposium"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/electronic-symposium">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/electronic-symposium.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/electronic-symposium-bg.png">
	<meta property="og:description" content="An initiative of ESDM Cluster of Deshpande Startups is hosting “Electronic Symposium” in association with SMET Pvt. Ltd. A Platform for Electronic Startups, Companies, R&D Centers, Students and Academicians."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="An initiative of ESDM Cluster of Deshpande Startups is hosting “Electronic Symposium” in association with SMET Pvt. Ltd. A Platform for Electronic Startups, Companies, R&D Centers, Students and Academicians."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Electronics Symposium">
	<link rel="canonical" href="https://www.deshpandestartups.org/electronic-symposium">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/electronic-symposium-bg.png" width="1349" height="198" alt="Deshpande Startups, events, Electronic Symposium">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Electronics Symposium</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">ELECTRONICS </span>SYMPOSIUM</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/electronic-symposium.png" width="474" height="237" alt="Deshpande startups, ESDM, Electronic Symposium">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center text-truncate">Electronics Symposium</h5>
									<p><b>Date : </b>June 21<sup>st</sup> 2019</p>
									<p><b>Time : </b>09:00 AM</p>
									<p><b>Last date to register : </b>June 18<sup>th</sup> 2019</p>
									<!-- <p class="text-yellow">The registrations has been closed.</p> -->
									<p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										<p class="pt-2 mb-0"><b>Mr. Veeranagouda</b></p>
										M:<a href="tel:+91-951-331-5793"> +91-951-331-5793</a><br>
										E:<a href="mailto:veeru&#046;sandbox&#064;dfmail&#046;org"> veeru&#046;sandbox&#064;dfmail&#046;org</a>
										<p class="pt-2 mb-0"><b>Mr. T.T Patil</b></p>
										M:<a href="tel:+91-988-676-4582"> +91-988-676-4582</a><br>
										E:<a href="mailto:patil&#064;mysmindia&#046;com"> patil&#064;mysmindia&#046;com</a>
									</p>
									<!-- <br> -->
								</div>
								<div class="card-footer">
									<!-- <a href="#" class="btn btn-primary p-1" target="_blank">Apply Now</a> -->
									<p class="text-yellow">The registrations has been closed.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-5 text-yellow"><b>Event Description:</b></p>

						<p class="text-justify wow slideInRight">An initiative of ESDM Cluster of Deshpande Startups is hosting <b>“Electronic Symposium”</b> in association with SMET Pvt. Ltd. <br><br>A Platform for Electronic Startups, Companies, R&D Centers, Students and Academicians, an opportunity to acquire knowledge on electronic design solutions, manufacturing system level products, one stop solutions and learn the available components in the industry, additionally build your domain network and share your knowledge among like minded people.</p>

						<p class="text-yellow"><b>Key Highlights of the Symposium:</b></p>
						<ul class="text-justify wow slideInRight">
							<li>Vibrant knowledge on electronic development and modules</li>
							<li>Get connected with electronic domain experts and industry</li>
							<li>Meet like minded people, share your knowledge and ideas</li>
							<li>Experience the Deshpande Startups ecosystem</li>
							<li>Opportunity to get incubated</li>
						</ul>
						<p class="text-yellow"><b>Topics Included:</b></p>
						<ul class="text-justify wow slideInRight">
							<li><b>Quectel</b> - Wireless modules GSM, LTE, Android, GPS, IRNSS and GNSS</li>
							<li><b>Renesas</b> - Renesas synergy, RL78, Rx, RZ families, and Renesas analog & power modules</li>
							<li><b>Nordic</b> - Wireless solutions, BLE mesh, NBIOT, and other wireless protocols</li>
							<li><b>Tianma</b> - Monochrome LCD’s, Segment LCD, Dot Matrix LCD and TFT</li>
							<li><b>Cosel</b> - Power supplies, MGS and TUHS series</li>
							<li><b>Mouser</b> - Active and Passive electronic components distributor for 750 industry-leading manufacturers</li>
							<li><b>SMET Solutions</b> - Renesas based synergy, RZ & RL 78 solutions, wireless based solutions – Nordic, Quectel, Power Electronic Solutions</li>
						</ul>
					</div>
				</div><br>
						<p class="text-yellow"><b>Who Can Apply/Participate:</b></p>
						<ul class="text-justify wow slideInRight">
							<li>Electronics Startups/Companies/R&D Centers/Students/Academicians</li>
						</ul>
				<p class="text-yellow pt-2"><b>About SM Electronic Technologies Pvt. Ltd.:</b></p>
				<p class="text-justify wow slideInRight">SM Electronic Technologies Pvt. Ltd., established in <b>1990</b>, is a <b>leading Indian supply chain & design chain company</b> in the field of Electronic Components and is being managed by a group of professionals having both business and technical expertise with a very structured operation for foreign & local currency business. SMET is <b>one-stop sourcing and design solution provider</b> for electronic products. We source and stock wide range of electronic components, <b>RFID reader & TAGS, Connectors, WI-FI, Zigbee & Bluetooth Modules, Active Components, Passive Components and Graphic, TFT LCD Displays</b>.</p>
			</div>
		</div>
	</div>

	<br>
	<br>
	<!-- <br>
	<br> -->
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>