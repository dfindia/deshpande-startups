<!DOCTYPE html>
<html lang="en">
<head>
	<title>Media Coverage - Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/news"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/news">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/deshpande-bg-news.jpg">
	<meta property="og:description" content="Tech Incubator Sandbox Startups Provide DST-NIDHI Funding to Three Hubballi Based Companies."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Tech Incubator Sandbox Startups Provide DST-NIDHI Funding to Three Hubballi Based Companies."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Deshpande Startups Media Coverage">
	<link rel="canonical" href="https://www.deshpandestartups.org/news">
	<?php
         // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	.bg-light1{
		background-color: #fd7e141a;
	}

	.list-group 
	{
		padding:10px;
	}

	.list-group-item 
	{
		background-color:#FD7E14;
		color:#fff;
	}

	a
	{
		color:#fff;
		font-size:16px;
	}
	a:hover {
  		color: black;
	}	
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="../img/events/deshpande-bg-news.png" width="1349" height="198" alt="Deshpande Startups, media coverage">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">News</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">MEDIA</span> COVERAGE</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
	</div>
	<br>
	<section>
        <div class="container-fluid">
		
         	<div class="row">
			 <div class="col-sm-2" >
			 	<div class="list-group" style="border:black 1px solid;">
               <br/>
             	<h3 class="main-header-new">Archive</h3>
               <br/>
              		<div class="list-group-item ">
					<a href="./" >2020-2019</a>
                </div>
				
				<div class="list-group-item ">
						<a href="2019" >2019-2018</a>
                </div>
				<div class="list-group-item ">
                    	 <a href="2018" > 2018-2017</a>
                </div>   
            </div>
		</div>
		<div class="col-sm-10 ">
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
									<span class="post-month">28</span>
									<span class="post-month">January</span>
									<span class="post-date">2017</span>
									<a href="img/pdf/28jan2017-theeconomictimes.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
									<!-- <a href="https://economictimes.indiatimes.com/small-biz/startups/hubballi-to-get-countrys-largest-startup-incubator/articleshow/56829858.cms?from=mdr" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
									<div class="pt-3">
										<p>Hubballi to get country's largest startup incubator</p><br><br>
										<!-- <small class="text-yellow"><b>Published by</b></small> -->
										<h6 class="text-yellow">THE ECONOMIC TIMES - ETRISE</h6>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 card-hover-shadow">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
									<span class="post-month">28</span>
									<span class="post-month">January</span>
									<span class="post-date">2017</span>
									<a href="img/pdf/28jan2017-khaleejmag.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
									<!-- <a href="http://www.khaleejmag.com/entrepreneurship/india-launch-largest-startup-incubation-center-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
									<div class="pt-3">
										<p>India to launch its Largest Startup Incubation Center in Hubballi</p><br><br>
										<!-- <small class="text-yellow"><b>Published by</b></small> -->
										<h6 class="text-yellow">Khaleej Mag</h6>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 bg-light1 card-hover-shadow">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
									<span class="post-month">28</span>
									<span class="post-month">January</span>
									<span class="post-date">2017</span>
									<a href="img/pdf/28jan2017-thetimesofindia.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
									<!-- <a href="https://timesofindia.indiatimes.com/business/india-business/hubballi-to-host-indias-biggest-startup-incubation/articleshow/56822425.cms?intenttarget=no&utm_source=newsletter&utm_medium=email&utm_campaign=Top_Headlines" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
									<div class="pt-3">
										<p>Hubballi to host India's biggest startup incubation</p><br><br>
										<!-- <small class="text-yellow"><b>Published by</b></small> -->
										<h6 class="text-yellow">THE TIMES OF INDIA</h6>
									</div>
								</div>
							</div>
						</div>
			</div>
				
					<div class="row">
						<div class="col-md-4 card-hover-shadow">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
									<span class="post-month">29</span>
									<span class="post-month">January</span>
									<span class="post-date">2017</span>
									<a href="img/pdf/29jan2017-newskart.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
									<!-- <a href="http://www.newskart.com/gururaj-deshpande-settingup-indias-biggest-incubator-startups-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
									<div class="pt-3">
										<p>Gururaj Deshpande Setting Up India’s Biggest Incubator For Startups In Hubballi</p><br>
										<!-- <small class="text-yellow"><b>Published by</b></small> -->
										<h6 class="text-yellow">NEWSKART</h6>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 bg-light1 card-hover-shadow">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
									<span class="post-month">25</span>
									<span class="post-month">November</span>
									<span class="post-date">2017</span>
									<a href="img/pdf/25nov2017-thehindu.pdf" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a>
									<!-- <a href="http://www.thehindu.com/news/national/karnataka/hubballi-set-to-house-countrys-biggest-startup-incubation-centre/article17105461.ece" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a> -->
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
									<div class="pt-3">
										<p>Hubballi set to house country’s biggest startup incubation centre</p><br><br>
										<!-- <small class="text-yellow"><b>Published by</b></small> -->
										<h6 class="text-yellow">THE HINDU</h6>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
	    </div>
	</div>
	</section>
	<br>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>