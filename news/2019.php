<!DOCTYPE html>
<html lang="en">
<head>
	<title>Media Coverage - Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/news"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/news">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/deshpande-bg-news.jpg">
	<meta property="og:description" content="Tech Incubator Sandbox Startups Provide DST-NIDHI Funding to Three Hubballi Based Companies."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Tech Incubator Sandbox Startups Provide DST-NIDHI Funding to Three Hubballi Based Companies."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Deshpande Startups Media Coverage">
	<link rel="canonical" href="https://www.deshpandestartups.org/news">
	<?php
         // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	.bg-light1{
		background-color: #fd7e141a;
	}

	.list-group 
	{
		padding:10px;
	}

	.list-group-item 
	{
		background-color:#FD7E14;
		color:#fff;
	}

	a
	{
		color:#fff;
		font-size:16px;
	}
	a:hover {
  		color: black;
	}	
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="../img/events/deshpande-bg-news.png" width="1349" height="198" alt="Deshpande Startups, media coverage">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">News</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">MEDIA</span> COVERAGE</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
	</div>
	<br>
	<section>
        <div class="container-fluid">
		
         	<div class="row">
			 <div class="col-sm-2" >
			 <div class="list-group" style="border:black 1px solid;">
               <br/>
			   <h3 class="main-header-new">Archive</h3>
               <br/>
              		<div class="list-group-item ">
					<a href="./" >2020-2019</a>
                </div>
				
				<div class="list-group-item ">
						<a href="2019" >2019-2018</a>
                </div>
				<div class="list-group-item ">
                    	 <a href="2018" > 2018-2017</a>
                </div> 
            </div>
		</div>
	<!-- <div class="container-fluid"> -->
		<div class="col-md-10">
			<div class="row">
				
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">21</span>
							<span class="post-month">February</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/21feb2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/02/startupbharat-hubli-sandbox-startups" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] From portable machines that can save newborns to AI startups, these Sandbox Hubli startups are solving real-world problems</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">20</span>
							<span class="post-month">February</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/20feb2019-cnbctv18.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Narayana Murthy lists key things small-town Hubli needs to become startup powerhouse</p><br><br>
								<h6 class="text-yellow">CNBC TV19</h6>
							</div>
						</div>
					</div>
				</div>
			    <div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">15</span>
							<span class="post-month">February</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/15feb2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/02/startupbharat-nr-narayana-murthy-sandbox-hubli-h1j95o6hk4" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>StartupBharat: NR Narayana Murthy reveals what it would take for Hubli startups to be No. 1 in India at Sandbox Hubli’s Startup Dialogue 2019 event</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">04</span>
							<span class="post-month">February</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/04feb2019-udayavani.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.readwhere.com/read/2011151/Hubli-Edition/04-Feb-2019#page/2/2" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>"Failure is not the end of entrepreneurship journey" <br>- Mr. NR Narayana Murthy </p><br>
								<h6 class="text-yellow">UDAYAVANI</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">29</span>
							<span class="post-month">January</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/29jan2019-thenewindianexpress.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://epaper.newindianexpress.com/2001930/The-New-Indian-Express-Hubbali/29-JAN-2019#page/4/2" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Judicial Magistrates, Public Prosecutors sensitised on animal laws</p><br><br>
								<h6 class="text-yellow">THE NEW INDIAN EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">23</span>
							<span class="post-month">January</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/23jan2019-yourstory.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/01/save-babies-jaundice-portable-machine/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>This doctor built a portable machine to save newborn babies from dying of jaundice across rural India</p><br>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">23</span>
							<span class="post-month">January</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/23jan2019-yourstory.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/01/save-babies-jaundice-portable-machine/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>This doctor built a portable machine to save newborn babies from dying of jaundice across rural India</p><br>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">11</span>
							<span class="post-month">December</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/startup-india.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Amit Sharma from Startup India have shared the details of initiatives of Startup India to support small, micro & medium entreprises</p>
								<h6 class="text-yellow">PRAJAVANI</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">29</span>
							<span class="post-month">October</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/the-new-indian-express-hubballi-291018.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://epaper.newindianexpress.com/1874287/The-New-Indian-Express-Hubbali/29-OCT-2018#page/5/1" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>ESDM facility could help Hubballi become next electronic hub of K'taka</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE NEW INDIAN EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
					<div class="col-md-4 card-hover-shadow">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
								<span class="post-month">28</span>
								<span class="post-month">October</span>
								<span class="post-date">2018</span>
								<a href="img/pdf/UV-hubballi-281018.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a><!-- <a href="http://epaper.udayavani.com/home.php?edition=Hubli&date=2018-10-28&pageno=2&pid=UVANI_HUB#Article/UVANI_HUB_20181028_2_3/363px" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
								<div class="pt-3">
									<p>Hubballi will be the electronic center in future : Hegde</p><br><br>
									<!-- <small class="text-yellow"><b>Published by</b></small> -->
									<h6 class="text-yellow">UDAYAVANI</h6>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 bg-light1 card-hover-shadow">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
								<span class="post-month">28</span>
								<span class="post-month">October</span>
								<span class="post-date">2018</span>
								<a href="img/pdf/28oct2018-sayuktakarnataka.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
								<!-- <a href="http://www.samyukthakarnataka.com/1872900/Samyukta-Karnataka-Hubballi-%E0%B2%B8%E0%B2%82%E0%B2%AF%E0%B3%81%E0%B2%95%E0%B3%8D%E0%B2%A4-%E0%B2%95%E0%B2%B0%E0%B3%8D%E0%B2%A8%E0%B2%BE%E0%B2%9F%E0%B2%95-%E0%B2%B9%E0%B3%81%E0%B2%AC%E0%B3%8D%E0%B2%AC%E0%B2%B3%E0%B3%8D%E0%B2%B3%E0%B2%BF-10-06-2017/28-10-2018#page/5/2" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
								<div class="pt-3">
									<p>If there is fear, life is not successful</p><br><br><br>
									<!-- <small class="text-yellow"><b>Published by</b></small> -->
									<h6 class="text-yellow">SAMYUKTA KARNATAKA</h6>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 card-hover-shadow">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
								<span class="post-month">28</span>
								<span class="post-month">October</span>
								<span class="post-date">2018</span>
								<a href="img/pdf/kp-hubballi-281018.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
								<div class="pt-3">
									<p>The minister of state for skill development & entrepreneurship, Anant Kumar Hegde said: The state's next electronic station is Hubballi.</p>
									<!-- <small class="text-yellow"><b>Published by</b></small> -->
									<h6 class="text-yellow">KANNADA PRABHA</h6>
								</div>
							</div>
						</div>
					</div>			
			</div>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
								<span class="post-month">24</span>
								<span class="post-month">September</span>
								<span class="post-date">2018</span>
								<a href="img/pdf/24sep2018-thenewindianexpress.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
								<!-- <a href="http://epaper.newindianexpress.com/1828432/The-New-Indian-Express-Hubbali/24-SEP-2018#issue/2/1" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Admin to extend all necessary support to budding entrepreneurs</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE NEW INDIAN EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
								<span class="post-month">25</span>
								<span class="post-month">July</span>
								<span class="post-date">2018</span>
								<a href="img/pdf/25july2018-timesofindia.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
								<!-- <a href="https://timesofindia.indiatimes.com/city/hubballi/startup-designs-portable-phototherapy-unit-to-tackle-neonatal-jaundice/articleshow/65124261.cms" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
								<div class="pt-3">
									<p>Startup designs portable device to tackle neonatal jaundice</p><br><br>
									<!-- <small class="text-yellow"><b>Published by</b></small> -->
									<h6 class="text-yellow">THE TIMES OF INDIA</h6>
								</div>
							</div>
						</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">24</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/24july2018-thenewenglandnews.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://indianewengland.com/2018/07/deshpande-foundation-launches-indias-largest-startup-incubation-in-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Deshpande Foundation Launches India’s Largest Startup Incubation in Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">INDIA NEW ENGLAND NEWS</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">20</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/20july2018-KNNknowledge.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://knnindia.co.in/news/newsdetails/sectors/indias-first-largest-startup-incubation-centre-launched-in-hubballi-city-to-promote-entrepreneurship" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>India’s first largest startup Incubation Centre launched in Hubballi city to promote entrepreneurship</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">KNN Knowledge & News Network</h6>
							</div>							    
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
								<span class="post-month">19</span>
								<span class="post-month">July</span>
								<span class="post-date">2018</span>
								<a href="img/pdf/19july2018-indianweb2.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
								<!-- <a href="https://www.indianweb2.com/2018/07/19/sandbox-startups-unveils-indias-largest-startup-incubation-centre-in-hubballi-city/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
								<div class="pt-3">
									<p>Sandbox Startups Unveils India’s Largest Startup Incubation Centre in Hubballi City</p><br>
									<!-- <small class="text-yellow"><b>Published by</b></small> -->
									<h6 class="text-yellow">indianWeb2</h6>
								</div>
							</div>
						</div>
					</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">19</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/19july2018-thehindu.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.thehindu.com/todays-paper/tp-national/tp-karnataka/niti-aayog-ceo-sees-need-for-overhauling-education/article24457227.ece" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>NITI Aayog CEO sees need for overhauling education</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE HINDU</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">18</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/18july2018-thehindu-BusinessLine.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.thehindubusinessline.com/news/niti-aayog-ceo-farms-health-education-problems-need-disruptive-innovations/article24454516.ece" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>NITI Aayog CEO: Farms, health, education problems need disruptive innovations</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE HINDU - BusinessLine</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">18</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/18july2018-thehindu.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.thehindu.com/news/national/karnataka/esdm-cluster-to-benefit-young-entrepreneurs/article24446740.ece" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>ESDM Cluster to benefit young entrepreneurs</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE HINDU</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">18</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/18july2018-yourstory.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2018/07/indias-largest-startup-incubation-centre-inaugurated-in-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>India’s largest startup incubation centre inaugurated in Hubballi</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">18</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/18july2018-thetimesofindia.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://timesofindia.indiatimes.com/city/hubballi/device-helps-local-stores-stave-off-e-commerce-sites/articleshow/65029927.cms?from=mdr" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Device helps local stores stave off e-commerce sites</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE TIMES OF INDIA</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">18</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/18july2018-thetimesofindia2.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://timesofindia.indiatimes.com/city/hubballi/ap-telangana-may-poach-our-investors/articleshow/65030005.cms" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>'AP, Telangana may poach our investors'</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE TIMES OF INDIA</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">17</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/17july2018-canindia.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.canindia.com/electronics-hardware-cluster-unveiled-in-karnataka-city/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Electronics hardware cluster unveiled in Karnataka city</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">CanIndia</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">17</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/17july2018-thenewindianexpress.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.newindianexpress.com/states/karnataka/2018/jul/17/startups-moving-from-crowded-metros-to-connected-economical-hubballi-1844466.html" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Startups moving from crowded metros to connected, economical Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE NEW INDIAN EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">12</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/12july2018-indiathishour.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://indiathishour.in/sandbox-startups-is-set-to-induct-indias-largest-incubation-centre-in-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Sandbox Startups Is Set To Induct  India’s Largest Incubation Centre In Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">INDIA THIS HOUR</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">12</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/12july2018-dailynewspost.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://dailynewspost.live/sandbox-startups-is-set-to-induct-indias-largest-incubation-centre-in-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Sandbox Startups Is Set To Induct  India’s Largest Incubation Centre In Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">DailyNewsPost</h6>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">11</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/11july2018-indianweb2.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.indianweb2.com/2018/07/11/indias-largest-startup-incubator-to-be-inaugurated-on-18-july-in-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>India’s Largest Startup Incubator To Be Inaugurated on 18 July in Hubballi</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">indianWeb2</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">11</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/11july2018-theweek.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.theweek.in/news/biz-tech/2018/07/11/india-largest-startup-incubator-inaugurated-hubballi-next-week.html" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>India's largest startup incubator to be inaugurated in Hubballi next week</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE WEEK</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">10</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/10july2018-bangalorenewsnetwork.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.bangalorenewsnetwork.com/m/news_detail.php?f_news_id=1156" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Minister K J George to inaugurate state-of-the-art Sandbox ESDM Cluster in Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">BNN - Bangalore News Network</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">10</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/10july2018-thehindu-businessline.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.thehindubusinessline.com/news/national/electronics-design-cluster-for-startups-to-be-opened-shortly-in-hubballi/article24380166.ece" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Electronics design cluster for startups to be opened shortly in Hubballi</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE HINDU - BusinessLine</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">10</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/10july2018-thenewindianexpress.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://epaper.newindianexpress.com/1729666/The-New-Indian-Express-Hubbali/10-JUL-2018#issue/4/1" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Hubballi gets electronic system design and manufacturing cluster</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE NEW INDIAN EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>	
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">09</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/09july2018-theeconomictimes.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://economictimes.indiatimes.com/small-biz/startups/newsbuzz/deshpande-foundation-to-launch-sandbox-startups-esdm-cluster-in-hubballi/articleshow/64920808.cms" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Deshpande Foundation to launch Sandbox Startups ESDM Cluster in Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE ECONOMIC TIMES - ETRISE</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">19</span>
							<span class="post-month">April</span>
							<span class="post-date">2018</span>
							<a href="https://m.siliconindia.com/news/business/Tech-Incubator-Sandbox-Startups-Provide-DSTNIDHI-Funding-to-Three-Hubli-Based-Companies-nid-204142-cid-3.html" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Tech Incubator Sandbox Startups Provide DST-NIDHI Funding to Three Hubli Based Companies</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">SiliconIndia</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">23</span>
							<span class="post-month">April</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/23april2018-socialstory.pdf" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2018/04/deshpande-foundation-sandbox-startup-nidhisss-funding/" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Technology incubator Sandbox Startups invests in three Hubballi-based companies</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
			







			</div>



	</div>
	</section>
	<br>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>