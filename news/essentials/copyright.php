<!-- FOOTER -->
<footer class="container-fluid bg-dark text-secondary">
	<hr class="featurette-divider-sm p-0 m-0 bg-yellow">
	<!-- <p class="float-right"><a href="#">Back to top</a></p> -->
	<div class="row">
		<div class="col-md-12 p-3 text-center wow animated slideInUp" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
			<!-- <p>© <?php //date_default_timezone_set("Asia/Kolkata");?><?php //echo date("Y"); ?> Deshpande Startups - All Rights Reserved | <a href="https://www.deshpandestartups.org/privacy" target="_blank" class="text-white">Privacy Policy</a> </p> -->
			<p>Copyrights <?php date_default_timezone_set("Asia/Kolkata");?><?php echo date("Y"); ?> All Rights Reserved. Powered by Deshpande Startups. </p>
			<small>We have moved to the new domain from <span class="text-yellow">www.sandboxstartups.org</span> to <a href="https://www.deshpandestartups.org/" target="_blank" class="text-yellow">www.deshpandestartups.org</a></small>
		</div>
	</div>
	
	<div class="social-icons" id="s-icons">
		<a href="https://chat.whatsapp.com/IiHHjr7TtrhBeY683Nghdp" target="_blank" class="btn btn-success btn-social img-hover-shadow" id="btn-share" data-toggle="tooltip" data-placement="left" title="Join Our Startup Community">
			<i class="fa fa-whatsapp fa-3x" aria-hidden="true"></i>
		</a>
	</div>
	<button onclick="topFunction()" id="myBtn-scroll" title="Go to top"><i class="fa fa-chevron-up"></i></button>

	</footer>
</main>