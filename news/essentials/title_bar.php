<div class="container-fluid masthead text-white">
	<div class="row wow fadeInDown">
		<div class="col-md-12 col-lg-6 p-0">
			<!-- Apply Now:
			<a href="../yuva-entrepreneurship" target="_blank" class="btn btn-warning btn-sm"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Yuva Entrepreneurship</a>
			<a href="../edge" target="_blank" class="btn btn-warning btn-sm"><i class="fa fa-users"></i> EDGE</a>
			<a href="../incubation-support-form" class="btn btn-warning btn-sm" target="_blank"><i class="fa fa-superpowers"></i> Incubation Support</a>
			<a href="../co-working-space-form" class="btn btn-warning btn-sm" target="_blank"><i class="fa fa-linode"></i> Co-working Space</a>
			<a href="../fssi-visit-form" target="_blank" class="btn btn-warning btn-sm"><i class="fa fa-location-arrow" aria-hidden="true"></i> Exposure Visit</a> -->
	</div>
	<div class="col-md-12 col-lg-6 text-right text-white d-lg-block p-0">
		<ul class="nav nav-pills justify-content-end text-white">
			<li class="nav-item"><a class="nav-link" href="https://www.facebook.com/DeshpandeStartups/" target="_blank"><i class="fa fa-facebook"></i></a></li>
			<li class="nav-item"><a class="nav-link px-0">|</a></li>
			<li class="nav-item"><a class="nav-link" href="https://www.youtube.com/channel/UCTqnxzdB8-2j_w83LZroqjA" target="_blank"><i class="fa fa-youtube"></i></a></li>
			<li class="nav-item"><a class="nav-link px-0">|</a></li>
			<li class="nav-item"><a class="nav-link" href="https://www.linkedin.com/company/deshpande-startups-incubator" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			<li class="nav-item"><a class="nav-link px-0">|</a></li>
			<li class="nav-item"><a class="nav-link" href="https://www.instagram.com/deshpande_startups/" target="_blank"><i class="fa fa-instagram"></i></a></li>
			<li class="nav-item"><a class="nav-link px-0">|</a></li>
			<li class="nav-item"><a class="nav-link" href="https://twitter.com/DFstartups" target="_blank"><i class="fa fa-twitter"></i></a></li>
			<!-- <li class="nav-item"><a class="nav-link px-0">|</a></li>
			<li class="nav-item"><a class="nav-link" href="https://chat.whatsapp.com/E0s0f8mHVkN9nZO4EXUj40" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li> -->
			<!-- <li class="nav-item"><a class="nav-link" href="https://chat.whatsapp.com/EysLh0OOtor5KRoeYxQ35Y" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li> -->
		</ul>
	</div>
</div>
</div>