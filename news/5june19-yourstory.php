<!doctype html>
<html lang="en">
<head>
	<title>Media Coverage | Yourstory</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/news/5june19-yourstory"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/news/5june19-yourstory">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/deshpande-bg-news.jpg">
	<meta property="og:description" content="[Startup Bharat] How healthcare startups in Tier II and III cities are solving the problem of access in their hometowns"/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="[Startup Bharat] How healthcare startups in Tier II and III cities are solving the problem of access in their hometowns"/>
	<!-- <meta name="keywords" content="nanoPix, Freshboxx, LinkEZ, nirnal, Adaptive, Artwaley, Happydesk, i-Tracker, Nexus3D, SMAT, Train-DE, Tier-II City Startups Competition – A Recap"/> -->
	<meta property="og:title" content="Deshpande Startups Media Coverage">
	<link rel="canonical" href="https://www.deshpandestartups.org/news/5june19-yourstory">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="../">Home</a></li>
			<li class="breadcrumb-item"><a href="./">Media Coverage</a></li>
			<li class="breadcrumb-item active" aria-current="page">Yourstory</li>
		</ol>
	</nav>

	<div class="container">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">Yourstory</h2>
      <div class="divider b-y text-yellow content-middle"></div>
		<div class="row">
			<div class="col-md-12 pl-5">
				<iframe src="https://docs.google.com/viewer?url=https://www.deshpandestartups.org/img/pdf/05june2019-yourstory.pdf&embedded=true" width="100%" height="800px" frameborder="0"></iframe>
			</div>
		</div>
	</div>
	<br>

	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once dirname(__FILE__).'/essentials/js.php';
	?>
</body>
</html>