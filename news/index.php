<!DOCTYPE html>
<html lang="en">
<head>
	<title>Media Coverage - Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/news"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/news">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/deshpande-bg-news.png">
	<meta property="og:description" content="Deshpande Startups Recognized in Elevate"/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Deshpande Startups Recognized in Elevate"/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Deshpande Startups Media Coverage">
	<link rel="canonical" href="https://www.deshpandestartups.org/news">
	<?php
         // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	.bg-light1{
		background-color: #fd7e141a;
	}
	.list-group{
		padding: 10px;
	}
	.list-group-item 
	{
		background-color:#FD7E14;
		color:#fff;
	}
	.list-group-item a
	{
		color:#fff;
		font-size:16px;
	}
	.list-group-item a:hover {
  		color: black;
	}	
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="../img/events/deshpande-bg-news.png" width="1349" height="198" alt="Deshpande Startups, media coverage">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="../">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Media Coverage</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">MEDIA</span> COVERAGE</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
	</div>
	<br>
	<section>
        <div class="container-fluid">
		
         	<div class="row">
			 <div class="col-sm-2" >
			 <div class="list-group" style="border:black 1px solid;">
               <br/>
			   <h3 class="main-header-new">Archive</h3>
               <br/>
              		<div class="list-group-item ">
					<a href="./" >2020-2019</a>
                </div>
				
				<div class="list-group-item ">
						<a href="2019" >2019-2018</a>
                </div>
				<div class="list-group-item ">
                    	 <a href="2018" > 2018-2017</a>
                </div>   
            </div>
		</div>
	<!-- <div class="container-fluid"> -->
		<div class="col-md-10">
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
								<span class="post-month">7</span>
								<span class="post-month">October</span>
								<span class="post-date">2020</span>
								<a href="7oct20-udayavani" target="_blank" class="btn btn-warning button4 lg-block">View</a>
								<!-- <a href="https://www.thehindubusinessline.com/companies/sandbox-startups-signs-mou-with-educational-institutes-to-identify-student-entrepreneurs/article29943239.ece#" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
								<div class="pt-3">
									<p>With the support of Deshpande Startups, thousands of start up ventures are about to get started.</p><br>
									<h6 class="text-yellow">Udayavani</h6>
								</div>
							</div>
						</div>	
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">05</span>
							<span class="post-month">August</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/05august2019-udayavani.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Sandbox Startups Recognized in Elevate</p><br><br>
								<h6 class="text-yellow">UDAYAVANI</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">15</span>
							<span class="post-month">July</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/15july2019-forbesindia.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.forbesindia.com/article/startup-hubs-special/beyond-bengaluru-north-karnatakas-startup-opportunities/54349/1" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Beyond Bengaluru: North Karnataka's startup opportunities</p><br><br>
								<h6 class="text-yellow">Forbes INDIA</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">05</span>
							<span class="post-month">June</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/05june2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/06/startup-bharat-healthcare-healthtech-tier-ii-iii-towns" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] How healthcare startups in Tier II and III cities are solving the problem of access in their hometowns</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">31</span>
							<span class="post-month">May</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/31may2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/05/startup-bharat-aasalabs-vyavasahaaya-agritech-open-innovation" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] With the likes of Bayer as clients, Mysuru’s Aasalabs is connecting agritech startups with corporates</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">29</span>
							<span class="post-month">May</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/29may2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/05/startup-bharat-prime-minister-narendra-modi-expectations" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] Here are the four things ‘Startup Bharat’ wants Prime Minister Narendra Modi to focus on</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
								<span class="post-month">27</span>
								<span class="post-month">May</span>
								<span class="post-date">2019</span>
								<a href="img/pdf/27may2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
								<!-- <a href="https://yourstory.com/2019/05/startup-bharat-beyond-metros-deep-tech-solutions-real-india-problems" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
								<div class="pt-3">
									<p>[Startup Bharat] Away from the metros, a new breed of deep-tech startups is solving ‘real India’ problems</p>
									<h6 class="text-yellow">YOURSTORY</h6>
								</div>
							</div>
						</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">17</span>
							<span class="post-month">May</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/17may2019-krishijagran.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://krishijagran.com/agriculture-world/inviting-digital-solutions-for-combating-fall-armyworm-in-india/" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Inviting Digital Solutions for Combating Fall Armyworm in India</p><br><br>
								<h6 class="text-yellow">Krishijagran.com</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4  bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">23</span>
							<span class="post-month">April</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/23april2019-inc42.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://inc42.com/features/india-govt-backed-startup-incubation-centres/" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>India’s State-Backed Incubators Are Proving That Government Support Can Lift The Startup Ecosystem</p><br>
								<h6 class="text-yellow">Inc42</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
								<span class="post-month">10</span>
								<span class="post-month">April</span>
								<span class="post-date">2019</span>
								<a href="img/pdf/10april2019-prajavani2.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
								<div class="pt-3">
									<p>Battery power replaces fossil fuel in newly invented paddy sowing machine</p><br>
									<h6 class="text-yellow">PRAJAVANI</h6>
								</div>
							</div>
						</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">10</span>
							<span class="post-month">April</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/10april2019-prajavani.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>A machine for pickle business</p><br><br><br>
								<h6 class="text-yellow">PRAJAVANI</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">09</span>
							<span class="post-month">April</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/09april2019-theindianexpress.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href=https://indianexpress.com/article/cities/mumbai/maharashtra-government-picks-13-start-ups-in-push-to-mumbai-fintech-hub-5478851/?fbclid=IwAR27hysWYWnJhPCcNoZzEFE3FPGdOCY8CjH1q0MQ9ZstosRZI_wkPLB7bbw" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Maharashtra government picks 13 start-ups in push to Mumbai fintech hub</p><br>
								<h6 class="text-yellow">The Indian EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
								<span class="post-month">22</span>
								<span class="post-month">March</span>
								<span class="post-date">2019</span>
								<a href="img/pdf/22march2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
								<!-- <a href="https://yourstory.com/2019/03/startup-bharat-nautilus-hubli-portable-hearing-loss-lpv72epgza?__sta=pbh.uosvdzxrhwzjsq.lvvn%7CUIY&__stm_medium=email&__stm_source=smartech" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
								<div class="pt-3">
									<p>[Startup Bharat] Made in Hubli: a portable device that makes hearing tests 80 percent cheaper</p><br>
									<h6 class="text-yellow">YOURSTORY</h6>
								</div>
							</div>
						</div>
					</div>
				
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">20</span>
							<span class="post-month">March</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/20march2019-prajavani.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Auto Startup for the smooth functioning of pumpset</p><br><br>
								<h6 class="text-yellow">PRAJAVANI</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">14</span>
							<span class="post-month">March</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/14march2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/03/startup-bharat-healthcare-startups-medtech-vfoy563gjz" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] With these startups, healthcare services are getting better in non-metros</p><br>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
								<span class="post-month">13</span>
								<span class="post-month">March</span>
								<span class="post-date">2019</span>
								<a href="img/pdf/13march2019-prajavani.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
								<div class="pt-3">
									<p>Freshboxx: For organic vegitables and fruits</p><br><br>
									<h6 class="text-yellow">PRAJAVANI</h6>
								</div>
							</div>
						</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">08</span>
							<span class="post-month">March</span>
							<span class="post-date">2019</span>
							<!-- <a href="https://yourstory.com/2019/03/microchip-payments-digital-transactions-voice-network-tnbm/amp" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
							<a href="img/pdf/08march2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] No internet, no problem: this Hubli-based startup is enabling digital payments over voice network</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		
	</section>
	<br>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>