<!doctype html>
<html lang="en">
<head>
	<title>Media Coverage | Vijayavani</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/news/12dec18-vijayavani"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/news/12dec18-vijayavani">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/deshpande-bg-news.jpg">
	<meta property="og:description" content="Amit Sharma from Startup India have shared the details of initiatives of Startup India to support small, micro & medium entreprises"/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Amit Sharma from Startup India have shared the details of initiatives of Startup India to support small, micro & medium entreprises"/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Deshpande Startups Media Coverage">
	<link rel="canonical" href="https://www.deshpandestartups.org/news/12dec18-vijayavani">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="../">Home</a></li>
			<li class="breadcrumb-item"><a href="./">Media Coverage</a></li>
			<li class="breadcrumb-item active" aria-current="page">Vijayavani</li>
		</ol>
	</nav>

	<div class="container">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">Vijayavani</h2>
      <div class="divider b-y text-yellow content-middle"></div>
		<div class="row">
			<div class="col-md-12 pl-5">
				<iframe src="https://docs.google.com/viewer?url=https://www.deshpandestartups.org/img/pdf/12dec2018-vijayavani.pdf&embedded=true" width="100%" height="800px" frameborder="0"></iframe>
			</div>
		</div>
	</div>
	<br>

	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once dirname(__FILE__).'/essentials/js.php';
	?>
</body>
</html>