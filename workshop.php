<!DOCTYPE html>
<html lang="en">
<head>
 <title>Yuva Entrepreneurship program | Workshops</title>
 <?php
 require_once 'essentials/meta.php';
 ?>
 <meta name="linkage" content="https://www.deshpandestartups.org/workshop"/>
 <meta property="og:site_name" content="Deshpande Startups"/>
 <meta property="og:type" content="website">
 <meta property="og:url" content="https://www.deshpandestartups.org/workshop">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/workshop-bg.png">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/3d-printers-expo.png">
 <meta property="og:description" content="Yuva Entrepreneurship program organizes workshops on the topics of IoT and Electronics, 3D Printing, Mechanical and Civil, Web and Android Development and Machine Learning. Placed in a tier-2 city Deshpande Startups provides world-class infrastructure for Entrepreneurs with creative environment to host a vibrant execution-based ecosystem."/>
 <meta name="author" content="Deshpande Startups"/>
 <meta name="description" content="Yuva Entrepreneurship program organizes workshops on the topics of IoT and Electronics, 3D Printing, Mechanical and Civil, Web and Android Development and Machine Learning. Placed in a tier-2 city Deshpande Startups provides world-class infrastructure for Entrepreneurs with creative environment to host a vibrant execution-based ecosystem."/>
 <!-- <meta name="keywords" content=""/> -->
 <meta property="og:title" content="Yuva Entrepreneurship program - Workshops">
 <link rel="canonical" href="https://www.deshpandestartups.org/workshop">
 <?php
 require_once 'essentials/bundle.php';
 ?>

 <style type="text/css">
 </style>

</head>
<body>
 <?php
 require_once 'essentials/title_bar.php';
 require_once 'essentials/menus.php';
 ?>

 <!-- <img class="carousel-inner img-fluid" src="img/events/3d-arduino-workshop-bg.jpg" width="1349" height="198" alt="Deshpande Startups, workshops"> -->
 <img class="carousel-inner img-fluid" src="img/makers/workshop-bg.png" width="1349" height="198" alt="Deshpande Startups, workshops">
 <nav aria-label="breadcrumb">
  <ol class="breadcrumb justify-content-end">
    <li class="breadcrumb-item"><a href="./">Home</a></li>
    <li class="breadcrumb-item"><a href="yuva-entrepreneurship">Yuva Entrepreneurship</a></li>
    <li class="breadcrumb-item active" aria-current="page">Workshops</li>
  </ol>
</nav> 

<div class="container">
  <!-- <br> -->
  <div class="center wow fadeInDown">
   <h2 class="text-yellow text-center">WORKSHOPS</h2>
   <div class="divider b-y text-yellow content-middle"></div>
 </div><br>
 <!-- <div class="pull-right"><a href="#" class="btn btn-warning btn-md" target="_blank">Apply Now</a></div><br> -->
 <!-- <h4  class="text-yellow">Workshop description:</h4> -->
 <div class="row">
   <!-- <div class="col-md-12"> -->
   <div class="col-md-10">
     <p class="text-justify wow slideInLeft">Yuva Entrepreneurship program organizes workshops on the topics of <b>IoT and Electronics, 3D Printing, Mechanical and Civil, Web and Android Development and Machine Learning</b>. Placed in a tier-2 city Deshpande Startups provides world-class infrastructure for Entrepreneurs with creative environment to host a vibrant execution-based ecosystem.</p>
   </div>
     <!-- <div class="row justify-content-md-center"> -->
   <div class="col-md-2">
      <div class="row pl-4">
       <a href="workshop-form" class="btn btn-rotate" target="_blank">Apply Now</a>
     </div>
   </div>
 </div>
</div>
<br>
<!-- <br> -->

<div id="3d-printing">
  <div class="featured-bg-container">
    <div class="row valign-wrapper">
     <div class="col-md-4">
      <img src="img/makers/3d-printers-expo.png" width="380" height="213" alt="Yuva Entrepreneurship program, workshop, 3d printing" class="img img-fluid img-thumbnail wow zoomIn">
    </div>
    <div class="col-md-8">
     <h4 class="text-yellow">3D Printing:</h4>
     <p class="text-justify wow slideInRight">
       3D Printing is an <b>Additive Manufacturing technique</b> that creates a physical object from a virtual 3D CAD model by depositing successive layers of material. They work like the traditional inkjet printers, but instead of ink, a 3D printer deposits desired material to manufacture an object from its digital format.
     </p>
     <p class="text-justify wow slideInRight">This workshop on 3D Printing helps the participants understand The Basics of 3D printing, understand the design, functioning and operation of basic 3D printer.</p>
     <p class="text-justify wow slideInRight">Students of any background with interest in 3D printing, preferably engineering students can attend this workshop.</p>
     <p class="text-justify wow slideInRight text-yellow font-weight-bold mb-0">Benefits:</p>
     <ul class="text-justify wow slideInRight">
       <li>Quench your knowledge in the latest 3D printing domain with a deeper understanding of various types of printers</li>
       <li>Get exposed to Design concepts, Design Softwares, etc.</li>
       <li>E-certificates for all participants</li>
     </ul>
   </div>
 </div>
</div>
<br>
</div>

<div id="iot-electronics">
  <div class="container-fluid px-5">
    <div class="row valign-wrapper">
      <div class="col-md-8">
        <h4 class="text-yellow">IoT and Electronics:</h4>
        <p class="text-justify wow slideInLeft">IoT is bringing more and more things into the digital fold every day, which will likely make the IoT technology a very popular and demanding industry in the near future.</p>
        <ul class="text-justify wow slideInLeft">
         <li><b class="text-yellow">Arduino for Beginners:</b> This workshop is mainly for the beginners introduces them to the amazing world of IoT and its fascinating applications. <br>Students will get exposed to basics of programming and few hands-on projects. Interested School/College students who has basic electronic knowledge can attend this workshop.</li>
         <li><b class="text-yellow">Wireless with Arduino:</b> Three days workshop specially designed for 3rd and final year engineering students. 
          Apart from gaining practical skills on the Internet of Things Technology, by attending this workshop participants will learn about the Advance Arduino Programming, In-depth Hardware, Interfacing with Arduino (GSM, GPS, BLE, Wi-Fi), Embedded C, etc.
        </li>
        <li><b class="text-yellow">Embedded Coding:</b> Three days’ workshop conducted for engineering students takes a holistic view of the embedded coding focusing on Basics of C, Coding AT Mega 328 using embedded C and a few hands-on-projects.</li>
        <li><b class="text-yellow">Explorer STM 32:</b> A one-day workshop conducted for Engineering students focuses on Basics of STM 32 and Arm.</li>
      </ul>
      <p class="text-justify wow slideInLeft text-yellow font-weight-bold mb-0">Benefits:</p>
      <ul class="text-justify wow slideInLeft">
       <li>Get hands in with Arduino and few other components virtually</li>
       <li>Get basic knowledge and enhance your skills on Arduino</li>
       <li>E-certificates for all participants</li>
     </ul>
   </div>
   <div class="col-md-4">
    <img src="img/makers/iot-electronics-img.png" width="398" height="270" alt="Yuva Entrepreneurship program, workshop, IoT and Electronics" class="img img-fluid img-thumbnail wow zoomIn">
  </div>
</div>
</div>
<br>
</div>

<div id="mech-civil">
  <div class="featured-bg-container">
    <div class="row valign-wrapper">
     <div class="col-md-4 pt-5">
      <img src="img/makers/mech-civil-img.png" width="398" height="270" alt="Yuva Entrepreneurship program, workshops, mechanical and civil" class="img img-fluid img-thumbnail wow zoomIn">
    </div>
    <div class="col-md-8">
     <h4 class="text-yellow">Mechanical and Civil:</h4>
     <p class="text-justify wow slideInRight">Mechanical Engineering is about machines and has a lot to do with technical competence, and an attention to detail, skills which can be developed by working on more and different kind of projects.</p>
     <ul class="text-justify wow slideInRight">
       <li><b class="text-yellow">The Carpentry Workshop:</b> Carpentry workshop is a hands-on, one-day event that covers the core skills of Carpentry, Carpentry tools, Bosch Power tools and Basics of CNC Rotar. Any Engineering students can attend this workshop.</li>
       <li><b class="text-yellow">Laser Cutting Workshop:</b> A laser cutter enables you to cut out a complex shape with maximum precision. This one-day workshop conducted for Engineering students focuses on the basics, hands-on experience for Laser Cutting and The Basics of Laser Engraving.</li>
       <li><b class="text-yellow">CNC Router:</b> A one-day workshop conducted for Engineering students focuses on Basics of CNC Router and a few hands-on projects.</li>
       <li><b class="text-yellow">CNC Machineries:</b> A one-day workshop provides participants with hands-on experience through instruction, demonstrations, and discussion. Conducted mainly for engineering students, this workshop focuses on The Basics of Programming, CNC Lathe and CNC Milling.</li>
     </ul>
   </div>
 </div>
</div>
<br>
</div>

<div id="web-android">
  <div class="container-fluid px-5">
    <div class="row valign-wrapper">
      <div class="col-md-8">
        <h4 class="text-yellow">Web and Android Development:</h4>
        <p class="text-justify wow slideInLeft">The Web and Android Application Development Workshop provides participants technical training on the concepts and programming methodologies needed to develop applications for mobile devices or websites.</p>
        <h4 class="text-yellow">Machine Learning:</h4>
        <p class="text-justify wow slideInLeft">Machine Learning is the study of getting computers act without being explicitly programmed. Participants attending this workshop can learn about tomorrow's technology.</p>
      </div>
      <div class="col-md-4">
        <img src="img/makers/artificial-intelligence.png" width="398" height="270" alt="Yuva Entrepreneurship program, Workshops, web, android and machine learning" class="img img-fluid img-thumbnail wow zoomIn">
      </div>
    </div>
  </div>
  <br>
</div>
<br>

<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>