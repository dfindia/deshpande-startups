<!DOCTYPE html>
<html lang="en">
<head>
	<title>Events | Alexathon</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/alexathon"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/alexathon">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/alexathon.jpg">
	<meta property="og:description" content="An initiative of Makers Lab of Deshpande Startups, hosting an ALEXATHON a workshop on Alexa for 8 hours and 16 Hours Hackathon the competition."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="An initiative of Makers Lab of Deshpande Startups, hosting an ALEXATHON a workshop on Alexa for 8 hours and 16 Hours Hackathon the competition."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="ALEXATHON Event">
	<link rel="canonical" href="https://www.deshpandestartups.org/alexathon">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/alexathon-bg.jpg" width="1349" height="198" alt="Deshpande Startups, events, ALEXATHON">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">ALEXATHON</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown">ALEXATHON</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/alexathon.jpg" width="474" height="237" alt="Deshpande startups, events, ALEXATHON">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center text-truncate">ALEXATHON</h5>
									<p><b>Date : </b>August 17<sup>th</sup> 2018</p>
									<!-- <p><b>Time : </b>10:00 AM</p> -->
									<!-- <p><b>Last date to register : </b>August<br> 10<sup>th</sup> 2018</p> -->
									<p><b>Registration fees:</b> Rs.500/- per person</p>
									<p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										M:<a href="tel:+91-968-665-4749"> +91-951-331-5791</a><br>
										E:<a href="mailto:makerslab&#064;dfmail&#046;org"> makerslab&#064;dfmail&#046;org</a>
									</p>
								</div>
								<div class="card-footer">
									<p class="text-yellow">The registrations has been closed.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-5 text-yellow"><b>Event description:</b></p>
						<p class="text-justify">Under the Makers Lab's initiatives of Deshpande Startups. we are hosting <b>"Alexathon"</b> its a workshop cum hands-on coding experiences, we are very fortunate to have Tech leads of Alexa, Amazon India team, who would have handle this workshop for 8 hours and followed by 16 Hours <b>Hackathon</b> on building voice-based modules/applications.</p>
						<!-- <br> -->
						<p class="text-justify">We cordially invite you to come and participate, also win exciting prizes worth up to 1 lakh for 3 teams. An event of 24 hours for the developers to solve the problems by coding.</p>
						<p class="text-yellow"><b>6 Reason why you should participate?</b></p>
						<ul class="wow slideInRight">
							<li>Know how to build Alexa voice modules</li>
							<li>Be a part of vibrant developers network and community</li>
							<li>Opportunity to get hired</li>
							<li>Internship opportunities at Deshpande Startups</li>
							<li>Code along with professional experts and get guidance</li>
							<li>Hands on lab</li>
						</ul>
						<p class="text-yellow"><b>Who can apply</b></p>
						<ul class="wow slideInRight">
							<li>3<sup>rd</sup> and 4<sup>th</sup> Year Computer Science and Information Science students and professional software developers</li>
							<li>Should know Java and Javascript language</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>