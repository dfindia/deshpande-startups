<!DOCTYPE html>
<html lang="en">
<head>
   <title>Apply now for EDGE Program</title>
   <?php
   require_once 'essentials/meta.php';
   ?>
   <meta name="linkage" content="https://www.deshpandestartups.org/edge-form"/>
   <meta property="og:site_name" content="Deshpande Startups"/>
   <meta property="og:type" content="website">
   <meta property="og:url" content="https://www.deshpandestartups.org/edge-form">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/deshpande-bg-new.jpg">
   <!-- <meta property="og:description" content=""/> -->
   <meta name="author" content="Deshpande Startups"/>
   <meta name="description" content="Deshpande Startups Hubballi- India’s largest platform for startups is glad to announce the EDGE Program- A 12 weeks program for aspiring Entrepreneurs."/>
   <!-- <meta name="keywords" content=""/> -->
   <meta property="og:title" content="EDGE - Deshpande Startups">
   <link rel="canonical" href="https://www.deshpandestartups.org/edge-form">
   <?php
         // $title = 'Deshpande Startups';
   require_once 'essentials/bundle.php';
   ?>
   <style type="text/css">
   .disp{
      display:none;
   }
</style>
</head>
<body>
   <?php
   require_once 'essentials/title_bar.php';
   require_once 'essentials/menus.php';
   ?>
   <br>
   <div class="container text-center">
      <h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">APPLY NOW FOR</span><br> EDGE PROGRAM</h2>
      <div class="divider b-y text-yellow content-middle"></div>
      <!-- <h6>The registration has been closed</h6> -->
   </div>
   <br>
   <div class="container">
      <div class="row">
         <div class="col-md-12 w3-card p-3">
            <iframe name="hidden_iframe" id="hidden_iframe" class="disp" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
            </iframe>
            <form role="form" action="https://docs.google.com/forms/d/e/1FAIpQLSfYULcK4avUWa36noA-pfPMCXYOvFdfIkNsWWlWysy9AWOycw/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">
               <!-- <fieldset disabled> -->
                  <div class="row">
                     <div class="col-md-6 pad">
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
                           <label for="input1"><b>Founder name<span class="text-yellow">*</span></b></label>
                           <input type="text" name="entry.1338549293" class="box2 form-control" maxlength="50" pattern="^(?![ .]+$)[a-zA-Z .]*$" title="Mention your name" placeholder="Mention your name" required="required">
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
                           <label for="input3"><b>Mobile number<span class="text-yellow">*</span></b></label>
                           <input type="phone" name="entry.1346207399" class="box2 form-control" pattern="\d*" minlength="10" placeholder="Mention your mobile number" maxlength="10" title="Your mobile number" required="required">
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
                           <label for="input2"><b>Email-id<span class="text-yellow">*</span></b></label>
                           <input type="email" name="entry.1362871276" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input5"><b>Your location<span class="text-yellow">*</span></b></label>
                           <input type="text" name="entry.1354601117" pattern="^(?![ .]+$)[a-zA-Z .]*$" class="box2 form-control" placeholder="Your location" required="required">
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input4"><b>Startup name<span class="text-yellow">*</span></b></label>
                           <input type="text" name="entry.682309183" class="box2 form-control" placeholder="Startup name" required="required">
                        </div>
                        <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                           <label for="input7"><b>What do you expect from EDGE program?<span class="text-yellow">*</span></b></label><br>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="validate"><input type="checkbox" name="entry.1044211128" value="Validate the startup idea" checked="checked"> Validate the startup idea</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="market"><input type="checkbox" name="entry.1044211128" value="Market & customer analysis"> Market & customer analysis</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="businessmodel"><input type="checkbox" name="entry.1044211128" value="Build a business model for your startup"> Build a business model for your startup</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="understand"><input type="checkbox" name="entry.1044211128" value="Understand the nuance of entrepreneurship"> Understand the nuance of entrepreneurship</label>
                              </div>
                           </div>
                           <!-- <div class="row">
                              <div class="col-md-12">
                                 <label for="othr"><input type="checkbox" name="entry.1044211128" value="Other"> Other</label>
                              </div>
                           </div> -->
                           <!-- <label for="otherr"> <input type="checkbox" id="otherr" name="entry.1044211128" value="otherr"> Other</label><br>
                              <div id="dvtext" style="display: none">
                              	<input type="text" name="entry.278813244" class="box2 form-control" placeholder="Mention your expectations">
                              </div>
                           </div> -->

                           <!-- <div class="row txbx" style="display: none">
                              <div class="form-group col-md-12">
                                 <div class="row">
                                    <div class="form-group col-md-12">
                                       <label for="input45"><b>Other Expectations?<span class="text-yellow">*</span></b></label>
                                       <input type="text" name="entry.278813244" class="box2 form-control" title="Mention your expectations" placeholder="Mention your expectations">
                                    </div>
                                 </div>
                              </div>
                           </div> -->
                        </div>
                        <div class="form-group col-md-12">
                                       <label for="input45"><b>Other Expectations?</b></label>
                                       <input type="text" name="entry.278813244" class="box2 form-control" title="Mention your expectations" placeholder="Mention your expectations">
                                    </div>
                        </div>
                        <div class="col-md-6 pad">
                           <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                              <label for="input77"><b>How many co-founders are there in your startup?<span class="text-yellow">*</span></b></label>
                              <input type="number" name="entry.1567080354" class="box2 form-control" placeholder="Number of co-founders in your startup" required="required">
                           </div>
                           <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                              <label for="input77"><b>Problem statement<span class="text-yellow">*</span></b></label>
                              <input type="text" name="entry.1671775440" class="box2 form-control" placeholder="Describe the problem you are trying to solve in 50 words or less" required="required">
                           </div>
                           <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                              <label for="input77"><b>Brief about solution<span class="text-yellow">*</span></b></label>
                              <textarea type="text" name="entry.1815448070" rows="3" minlength="10" maxlength="742" class="box2 form-control" placeholder="Describe your idea/product in 50 words or less" required="required"></textarea>
                           </div>
                           <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                              <label for="input70"><b>Are you working full time on your startup?<span class="text-yellow">*</span></b></label><br>
                              <div class="row">
                                 <div class="col-md-6">
                                    <label for="Yes"><input type="radio" name="entry.1519504722" value="Yes" checked="checked"> Yes</label>
                                 </div>
                                 <div class="col-md-6">
                                    <label for="No"><input type="radio" name="entry.1519504722" value="No"> No</label>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                              <label for="input70"><b>What drives you the most<span class="text-yellow">*</span></b></label><br>
                              <div class="row">
                                 <div class="col-md-6">
                                    <label for="Passion"><input type="radio" name="entry.265839477" value="Passion" checked="checked"> Passion</label>
                                 </div>
                                 <div class="col-md-6">
                                    <label for="Honesty"><input type="radio" name="entry.265839477" value="Honesty"> Honesty</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <label for="Making money"><input type="radio" name="entry.265839477" value="Money"> Money</label>
                                 </div>
                                 <div class="col-md-6">
                                    <label for="Buid"><input type="radio" name="entry.265839477" value="Build a startup"> Build a startup</label>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
                              <label for="input6"><b>How did you come to know about cohort?<span class="text-yellow">*</span></b></label><br>
                              <div class="row">
                                 <div class="col-md-6">
                                    <label for="Personal"><input type="radio" name="entry.1814048392" value="Personal Reference" checked="checked"> Personal Reference</label>
                                 </div>
                                 <div class="col-md-6">
                                    <label for="Newsletters"><input type="radio" name="entry.1814048392" value="Email News Letter"> Email News Letter</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <label for="SocialMedia"><input type="radio" name="entry.1814048392" value="Social Media"> Social Media</label>
                                 </div>
                                 <div class="col-md-6">
                                    <label for="PrintMedia"><input type="radio" name="entry.1814048392" value="Print Media"> Print Media</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <label for="F6S"><input type="radio" name="entry.1814048392" value="F6S"> F6S</label>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-lg-12">
                             <!--  <span class="text-yellow"><b>*</b></span>
                              <div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div> -->
                              <span class="text-yellow">
                                 <h6><b>*</b> Fields are mandatory</h6>
                              </span>
                              <input type="submit" class="btn btn-warning" id="ss-submit" name="submit" value="Submit">
                           </div>
                        </div>
                     </div>
                     <!-- </fieldset> -->
                  </form>
               </div>
            </div>
         </div>
         <br>
         <script src='https://www.google.com/recaptcha/api.js'></script>
         <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once 'essentials/js.php';
         ?>

         <!-- <script type="text/javascript">
               $(function() {
                  $('[name="entry.1044211128"]').on('click', function (e) {
                     var val = $(this).val();
                     if (val == "Other") {
                        $('.txbx').show('fade');
                     }else {
                        $('.txbx').hide();
                     }
                  });
               });
            </script> -->
         <!-- <script>
            $('input[name="entry.1044211128"][type="checkbox"]').on('click', function (e) {
         if (this.value == 'otherr') {
            $('#dvtext').fadeIn();
         } else {
            $('#dvtext').fadeOut();
         }
      });
   </script> -->
         <script>
            window.onload = function() {
              var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
              recaptcha.required = true;
              recaptcha.oninvalid = function(e) {
         // do something
         alert("Please complete the captcha");
      }
   }
</script>
</body>
</html>