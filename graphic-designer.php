<!DOCTYPE html>
<html lang="en">
<head>
	<title>Graphic Designer | Any Degree </title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/graphic-designer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/graphic-designer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/team-image.png">
	
	<meta property="og:description" content="Job Position: Graphic Designer, Experience:  2 - 3 years, Education: Any Degree"/>
	<meta name="author" content="Deshpande Startups"/>
	
	<meta name="description" content="Job Position: Graphic Designer, Experience:  2 - 3 years, Education: Any Degree"/>
	
	<meta property="og:title" content="Graphic Designer, Current openings at Deshpande startups">
	<link rel="canonical" href="https://www.deshpandestartups.org/graphic-designer">
	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Graphic</span> Designer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
			
		</div>

		<br>
		<div class="row pull-right">
			<div class="pull-right"><a href="career-opportunities" class="btn btn-warning btn-md" target="_blank">Apply Now</a></div>
		</div>
		<br><br>
		<div class="row">
            <div class="col-md-6">
				
					<p class="text-justify"><strong>Job Position:</strong> Graphic Designer<br>
						<strong>Organization:</strong> Deshpande Startups<br>
						<strong>Department Name:</strong> Media & Communications<br>
						<strong>Experience:</strong> 2-3 years of experience<br></p>
			</div>
			<div class="col-md-6">
			    <p>
				<!-- <div class="pull-right"><a href="career-opportunities" class="btn btn-warning btn-md" target="_blank">Apply Now</a></div> -->
					<strong>Education:</strong> Any Degree <br>
					<strong>Job Location:</strong> Hubballi<br>
					<strong>Tentative Date of Joining:</strong> Immediate
				</p>
			</div>
					
			
		</div>
            <div class="row">
                <div class="col-md-6">
		            <h3  class="text-yellow">Job Description:</h3>
		                <ul>
			                <li>Study design briefs and determine requirements</li>
			                <li>Conceptualize visuals based on requirements</li>
			                <li>Prepare rough drafts and present ideas</li>
			                <li>Develop illustrations, logos and other designs using software <br> or by hand</li>
			                <li>Use the appropriate colors and layouts for each graphic</li>
			                <li>Work with the creative director to produce the final design</li>
			                <li>Test graphics across various media</li>
                            <li>Amend designs after feedback</li>
                            <li>Ensure final graphics and layouts are visually appealing and <br> on-brand</li>
		                </ul>
                </div>
		<br>
                <div class="col-md-6">
		            <h3  class="text-yellow">Knowledge, Skills & Abilities:</h3>
		                <ul>
		                	<li>Proven graphic designing experience</li>
		                	<li>A strong portfolio of illustrations or other graphics</li>
		                	<li>Familiarity with design software and technologies<br> (Such as InDesign, Illustrator,Photoshop)</li>
		                	<li>A keen eye for aesthetics and details</li>
		                	<li>Excellent communication skills</li>
		                	<li>Ability to work methodically and meet deadlines</li>
		                	<li>Excellent multitasking skills</li>
		                </ul>
                </div>
        </div>
    </div>
<div class="pull-right pull-pad"><a href="career-opportunities" class="btn btn-warning btn-md" target="_blank">Apply Now</a></div>
	<br><br>
	<!-- <div class="container cal">
		<p class="text-center">For any further enquiries, please email E: <a href="mailto:hr.sandbox@dfmail.org">hr.sandbox@dfmail.org</a> or call us M: +91-889-297-0077</p>
	</div> -->
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>