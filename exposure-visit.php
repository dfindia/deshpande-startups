<!DOCTYPE html>
<html lang="en">
<head>
   <title>Exposure Visit to Deshpande Startups</title>
   <?php
   require_once 'essentials/meta.php';
   ?>
   <meta name="linkage" content="https://www.deshpandestartups.org/exposure-visit"/>
   <meta property="og:site_name" content="Deshpande Startups"/>
   <meta property="og:type" content="website">
   <meta property="og:url" content="https://www.deshpandestartups.org/exposure-visit">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/co-working-space.png">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/esdm-cluster.jpg">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/3d-printers-expo.png">
   <meta property="og:description" content="Any visitor coming for an exposure at Deshpande Startups is taken on a campus tour and introduced to various entrepreneurial Programs, Facilities and Services. Our facilities also include, Electronic and IoT Lab, Services/Prototypes and Mechanics support."/>
   <meta name="author" content="Deshpande Startups"/>
   <meta name="description" content="Any visitor coming for an exposure at Deshpande Startups is taken on a campus tour and introduced to various entrepreneurial Programs, Facilities and Services. Our facilities also include, Electronic and IoT Lab, Services/Prototypes and Mechanics support."/>
   <!-- <meta name="keywords" content=""/> -->
   <meta property="og:title" content="Exposure Visit to Deshpande Startups">
   <link rel="canonical" href="https://www.deshpandestartups.org/exposure-visit">
   <?php
   require_once 'essentials/bundle.php';
   ?>
   <style type="text/css">
   .parallax {
      background-image: url("img/makers/deshpande.jpg");
      min-height: 300px; 
      background-attachment: fixed;
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
   }
</style>

</head>
<body>
   <?php
   require_once 'essentials/title_bar.php';
   require_once 'essentials/menus.php';
   ?>

   <div class="parallax inverse-text" data-parallax-img="img/makers/deshpande.jpg" data-parallax-img-width="1920" data-parallax-img-height="1078">
      <br>
      <div class="center wow fadeInDown pt-5">
         <h2 class="text-yellow text-center"><span class="text-white">EXPOSURE VISIT TO</span><br> DESHPANDE STARTUPS</h2>
         <div class="divider b-y text-yellow content-middle"></div>
      </div>
   </div>
   <br>

   <div class="container">
     <div class="row">
      <div class="col-md-10 pt-4">
        <p class="text-justify wow slideInLeft">Deshpande startups is a <b>living laboratory of Resources, Connections, Knowledge & Talent</b> to support mission driven entrepreneurs to scale in their venture. Placed in a tier-2 city Deshpande Startups provides world-class infrastructure for Entrepreneurs with creative environment to host a vibrant execution-based ecosystem.</p>
     </div>
     <div class="col-md-2">
        <!-- <div class="row justify-content-md-center"> -->
         <div class="row pl-4">
          <a href="fssi-visit-form" class="btn btn-rotate" target="_blank">Apply Now</a>
       </div>
    </div>
 </div>
 <br>
</div>

<div class="featured-bg-container">
   <h4 class="text-yellow">Our 1 lakh square feet Incubation Space includes facilities such as:</h4>
   <br>
   <div class="row">
      <div class="col-md-4">
         <img src="img/makers/co-working-space.png" width="380" height="213" alt="Deshpande startups, exposure visit, co-working space" class="img img-fluid img-thumbnail">
         <p class="pt-3">Co-Working space with 300+ plug and play work stations encouraging peer to peer learning</p> 
      </div>
      <div class="col-md-4">
         <img src="img/makers/esdm-cluster.jpg" width="380" height="213" alt="Deshpande startups, exposure visit, ESDM cluster" class="img img-fluid img-thumbnail">
         <p class="pt-3">ESDM Lab to cater Electronics Manufacturing solutions equipped with professional operating team</p>
      </div>
      <div class="col-md-4">
         <img src="img/makers/3d-printers-expo.png" width="380" height="213" alt="Deshpande startups, exposure visit, Makers Lab" class="img img-fluid img-thumbnail">
         <p class="pt-3">Makers Lab with 3D printers, lathe machines, CNC machines is a workspace for experimentation, fabrication and prototyping</p>
      </div>
   </div>
</div>
<br>

<div class="container">
   <p class="text-justify wow slideInLeft">Any visitor coming for an exposure at Deshpande Startups is taken on a campus tour and introduced to various entrepreneurial <b>Programs, Facilities and Services</b>. Our facilities also include, <b>Electronic and IoT Lab, Services/Prototypes and Mechanics support</b>.</p>
   <p class="text-yellow"><b>Requirements for an exposure visit:</b></p>
   <ul class="text-justify wow slideInLeft">
      <li>Mindset of curious entrepreneurial catalysts</li>
      <li>Prior permission with regard to the allowance of you/your group to the Makers Lab</li>
      <li>A spark enough to start something new</li>
      <li>Students, Graduates, Businessmen, anyone can come for the exposure visit</li>
   </ul>
</div>
<br>
<br>

<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>