<!DOCTYPE html>
<html lang="en">
<head>
	<title>Senior Web Developer - IngeniousPix Creative Solutions</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/ingeniouspix-web-developer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/ingeniouspix-web-developer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/ingeniouspix-big.png">
	<meta property="og:description" content="We are looking for Senior Web Developer. Job Position: Senior Web Developer, Experience:  3 - 5 years, Qualification: Bachelor's degree in Computer Science or related technical field. BE/BCA/MCA."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Senior Web Developer. Job Position: Senior Web Developer, Experience:  3 - 5 years, Qualification: Bachelor's degree in Computer Science or related technical field. BE/BCA/MCA."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Senior Web Developer, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/ingeniouspix-web-developer">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Senior Web</span> Developer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Senior Web Developer<br>
					<strong>Startup:</strong> IngeniousPix Creative Studios<br>
					<strong>Qualification:</strong> Bachelor's degree in Computer Science or related technical field (BE/BCA/MCA)<br>
					<strong>Experience:</strong> 3 - 5 years of experience required<br>
					<strong>Job Location:</strong> Hubballi<br>
				</p>
			</div>
			<div class="col-md-6">
				<a href="http://ingeniouspix.com/" target="_blank" rel="nofollow"><img src="img/career/ingeniouspix-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, IngeniousPix" /></a>
			</div>
		</div>
		<!-- <br> -->

		<!-- <p class="text-justify pt-1"></p> -->

		<!-- <div class="row pt-2"> -->
			<!-- <div class="col-md-7"> -->
				<h3 class="text-yellow">Skills and Expertise:</h3>
				<ul>
					<li>Understanding of responsive design techniques and principles</li>
					<li>Extensive knowledge in HTML5, CSS3, JavaScript, JQuery, AJAX, PHP, MYSQL, MONGO, NODEJS, SASS</li>
					<li>Understanding of PHP frameworks like, CodeIgnitor, Laravel</li>
					<li>Experience with project management tools like GITHUB, BITBUCKET, JIRA, ASANA, TRELLO is desirable</li>
					<li>Hands on experience with CSS frameworks like Bootstrap and Material UI will be an added advantage</li>
					<li>Experience working in a Node JS environment will be a bonus point</li>
				</ul>
				<!-- </div> -->
				<!-- </div> -->

				<!-- <br> -->
				<h3 class="text-yellow pt-3">Roles and Responsibilities:</h3>
				<ul>
					<li>Candidate will be responsible for full stack development. Excessive in languages like PHP, NODE JS, ASP. MYSQL, MONGO</li>
					<li>Update and maintain existing projects and contribute to new projects as a team member</li>
					<li>Capability to design the architecture for the project code base</li>
					<li>Motivated ideology and a self learner with exceptional problem solving ability</li>
					<li>Lead the team, be a mentor to fellow colleagues</li>
					<li>High analytical skills, to pursue the requirement of any product and improvements</li>
					<li>A team worker, who should contribute to the team in terms of deliverables</li>
					<li>Should understand what Rapid Prototyping is and ability to work in a fast paced environment</li>
					<li>Should have a keen eye on perfection and Test Driven Development is preferable</li>
				</ul>
			</div>
			<br>

			<div class="container cal">
				<p class="text-center"><b>Interested candidates email Resumes to <br>E: <a href="mailto: hr&#064;ingeniouspix&#046;com"> hr&#064;ingeniouspix&#046;com</a></b></p>
			</div>
			<br>
			<?php
			require_once 'essentials/footer.php';
			require_once 'essentials/copyright.php';
			require_once 'essentials/js.php';
			?>
		</body>
		</html>