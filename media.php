<!DOCTYPE html>
<html lang="en">
<head>
	<title>Media | Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/media"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/media">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/media-kit/PNG/deshpande-startups.png">
	<meta property="og:description" content="Get the latest at Deshpande Startups at Media page"/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Get the latest at Deshpande Startups at Media page"/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Deshpande Startups Media">
	<link rel="canonical" href="https://www.deshpandestartups.org/media">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		.middle {
			display: flex;
			justify-content: center;
			align-items: center;
		}
		.mid {
			display: flex;
			justify-content: center;
		}
		.mediaKit {
			/*background: url(img/yellow.png) center center no-repeat;*/
			border-radius: 1rem;
			background-color: #eb7c26;
		}
		.col-equal > [class*='col-'] {
			display: flex;
			flex-direction: column;
		}
		.mediaKit a {
			display: block;
			color: white;
			margin-bottom: 1rem;
			text-decoration: none;
		}
		.mediaKit a:hover {
			opacity: 0.75;
		}
		.mediaKit a .link-text {
			width: 80%;
			font-size: 20px;
			border-bottom: 1px solid white;
		}
		.mediaKit a span {
			display: inline-block;
		}
		.front {
			width: 90%;
			height: 90%;
			margin: 10px;
			z-index: 5;
			opacity: 1;
			/* display: block; */
			position: absolute;
			top: 0;
			left: 0;
			-webkit-transition: opacity .5s ease;
		}
		.front:hover {
			opacity: 0;
			z-index: -1;
		}
		.panel-box {
			background: url(img/stripes-dark-10.png) repeat;
			margin: 5px;
			padding: 0;
			display: inline-block;
			clear: none;
			float: left;
			width: 100%;
			height: 100%;
			min-height: 200px;
			position: relative;
			text-align: center;
		}
		.back {
			width: 100%;
			height: 100%;
			/* margin: 5%; */
			/* padding: 10px; */
			color: white;
			background-color: #eb7c26;
			/*background-color: #F0AD4E;*/
			position: absolute;
			border-radius: 5%;
			top: 0;
			left: 0;
			z-index: 0;
			opacity: 0;
			-webkit-transition: opacity 1s ease;
		}
		.back:hover {
			z-index: 100;
			opacity: 1;
		}
		.link {
			position: relative;
			top: 43%;
			transform: translateY(-50%);
			color: white;
		}
		.link:hover {
			color: white;
		}

	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>

	<div class="container pt-3">
		<h2 class="text-yellow text-center">MEDIA</h2>
		<div class="divider b-y text-yellow content-middle"></div>
	</div>
	<!-- <br> -->
	<section class="pb-4">
		<div class="container pb-3">
			<div class="row middle">
				<div class="col-sm-12">
					<h3 class="p-4 text-center">Deshpande Startups India Identity</h3>
				</div>
			</div>
			<div class="row col-equal mid">
				<div class="col-sm-6 p-2 mediaKit mid">
					<div class="row">
						<div class="col-sm-6">
							<a href="img/media-kit/AI/deshpande-startups.ai" download>
								<span><i class="fa fa-cloud-download fa-2x"></i></span>
								<span class="link-text">AI</span>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="img/media-kit/AI/deshpande-startups-gray.ai" download>
								<span><i class="fa fa-cloud-download fa-2x"></i></span>
								<span class="link-text">AI Gray</span>
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<a href="img/media-kit/EPS/deshpande-startups.eps" download>
								<span><i class="fa fa-cloud-download fa-2x"></i></span>
								<span class="link-text">EPS</span>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="img/media-kit/EPS/deshpande-startups-gray.eps" download>
								<span><i class="fa fa-cloud-download fa-2x"></i></span>
								<span class="link-text">EPS Gray</span>
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<a href="img/media-kit/PDF/deshpande-startups.pdf" download>
								<span><i class="fa fa-cloud-download fa-2x"></i></span>
								<span class="link-text">PDF</span>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="img/media-kit/PDF/deshpande-startups-gray.pdf" download>
								<span><i class="fa fa-cloud-download fa-2x"></i></span>
								<span class="link-text">PDF Gray</span>
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<a href="img/media-kit/PNG/deshpande-startups.png" download>
								<span><i class="fa fa-cloud-download fa-2x"></i></span>
								<span class="link-text">PNG</span>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="img/media-kit/PNG/deshpande-startups-gray.png" download>
								<span><i class="fa fa-cloud-download fa-2x"></i></span>
								<span class="link-text">PNG Gray</span>
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<a href="img/media-kit/PSD/deshpande-startups.psd" download>
								<span><i class="fa fa-cloud-download fa-2x"></i></span>
								<span class="link-text">PSD</span>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="img/media-kit/PSD/deshpande-startups-gray.psd" download>
								<span><i class="fa fa-cloud-download fa-2x"></i></span>
								<span class="link-text">PSD Gray</span>
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-xs-12 mid">
					<div class="panel-box">
						<div class="back">
							<a class="link" href="img/media-kit/media-kit-deshpande-startups-logo.zip" download><i class="fa fa-cloud-download fa-2x"> Download</i></a>
						</div>
						<h5 class="front box-style middle">Zip File Containing Logo In All Formats</h5>
					</div>
				</div>
			</div>
			<!-- End Page Content-->
		</div>
	</section>


	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>