<!DOCTYPE html>
<html lang="en">
<head>
	<title>Contact Us - Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/contact-us"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/contact-us">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/contact-us.jpg">
	<meta property="og:description" content="Enthusiastic entrepreneurs who are looking for a platform to scale their ventures, make a mark at the global level, contribute their efforts in building the nation can reach us."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Entrepreneurs who are looking for a platform to scale their ventures, make a mark at the global level, contribute their efforts in building the nation can reach us."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Deshpande Startups Contact Us Page">
	<link rel="canonical" href="https://www.deshpandestartups.org/contact-us">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	.disp{
		display:none;
	}
	.txt-body{
		color: #5a5a5a;
	}
	p a {
		text-decoration: none !important;
	}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>

	<a href="incubation-support-form" target="_blank"><img class="carousel-inner img-fluid" src="img/bg-home/contact-us.jpg" width="1349" height="198" alt="Deshpande Startups, incubation centre"></a>
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Contact Us</li>
		</ol>
	</nav>

	<div class="container">
		<h2 class=" text-yellow text-center Pt-4 wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">CONTACT </span>US</h2>
		<div class="divider b-y text-yellow content-middle"></div>
		<p>Enthusiastic entrepreneurs who are looking for a platform to scale their ventures, make a mark at the global level, contribute their efforts in building the nation can reach us. </p>
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<iframe name="hidden_iframe" id="hidden_iframe" class="disp" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
				</iframe>
				<div class="p-3 w3-card">
					<form role="form" action="https://docs.google.com/forms/d/e/1FAIpQLSe0-klXfMvBWFtKoPx2JvVor0uu6zImMfkpQkpeW68pfxlA3w/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">
						<div class="form-row">
							<div class="form-group col-lg-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
								<label for="input1"><b>Name<span class="text-yellow">*</span></b></label>
								<input type="text" name="entry.411051948" class="box2 form-control" maxlength="25" placeholder="Mention your name" required="required">
							</div>
							<div class="form-group col-lg-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
								<label for="input2"><b>Email-id<span class="text-yellow">*</span></b></label>
								<input type="email" name="entry.458044753" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-lg-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
								<label for="input3"><b>Mobile number<span class="text-yellow">*</span></b></label>
								<input type="phone" name="entry.1343514587" class="box2 form-control" pattern="\d*" min="12" placeholder="Mention your mobile number" maxlength="12" title="Your 12 Digit No." required="required">
							</div>
							<div class="form-row form-group col-lg-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
								<label><b>I'd like to know about...<span class="text-yellow">*</span></b></label>
								<select class="form-control" name="entry.1108443848">
									<option value="Yuva Entrepreneurship" selected>Yuva Entrepreneurship</option>
									<option value="EDGE">EDGE</option>
									<option value="Incubation Support">Incubation Support</option>
									<option value="Co-working Space">Co-working Space</option>
									<option value="Makers Lab">Makers Lab</option>
									<option value="ESDM Cluster">ESDM Cluster</option>
									<option value="Internship/Career">Internship/Career at Startup</option>
									<option value="Other">Other</option>
								</select>
							</div>
						</div>
						<div class="form-group wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.4s">
							<label for="input4"><b>Message<span class="text-yellow">*</span></b></label>
							<textarea name="entry.107616130" class="box2 form-control" rows="8" min="10" maxlength="350" placeholder="Message (remember, short & sweet please)" title="Your Text Here" required="required"></textarea>
						</div>
						<div class="form-row">
							<div class="form-group col-lg-6">
							<!-- <b><span class="text-yellow">*</span></b>
								<div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div> -->
								<span class="text-yellow"><h6><b>*</b> Fields are mandatory</h6></span>
								<input type="submit" class="btn btn-warning" name="Submit" value="Submit">
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="col-md-4 details wow fadeInLeft">
				<div>
					<!-- <h3 class="featurette-heading-sm font-weight-normal text-yellow wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">Contact</span> Address</h3> -->
					<!-- <div class="divider b-y text-yellow w-50"></div> -->
					<p><i class="fa fa-map-marker"></i> &nbsp; Deshpande Startups,<br> Next to Airport, Opp to Gokul Village,<br> Gokul Road, Hubballi, Karnataka. </p>
					<!-- <p><i class="fa fa-globe"></i> &nbsp;<a href="https://www.deshpandestartups.org/">www.deshpandestartups.org</a></p> -->
				</div>

				<div class="pt-2">
					<h3 class="featurette-heading-sm font-weight-normal text-yellow wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">EDGE & </span>Incubation</h3>
					<div class="divider b-y text-yellow w-50"></div>
					<p><i class="fa fa-phone"></i><a class="txt-body" href="tel:+91-951-331-5287"> &nbsp;+91-951-331-5287</a></p>
					<!-- <p><i class="fa fa-phone"></i><a class="txt-body" href="tel:+91-968-665-4749"> &nbsp;+91-968-665-4749</a></p> -->
					<p><i class="fa fa-envelope text-white"></i><a class="txt-body" href="mailto:seir&#064;dfmail&#046;org"> &nbsp;seir&#064;dfmail&#046;org</a></p>
				</div>

				<div class="pt-3">
					<h3 class="featurette-heading-sm font-weight-normal text-yellow wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">Makers</span> Lab</h3>
					<div class="divider b-y text-yellow w-50"></div>
					<p><i class="fa fa-phone"></i><a class="txt-body" href="tel:+91-951-331-5791"> &nbsp;+91-951-331-5791</a></p>
					<p><i class="fa fa-envelope text-white"></i><a class="txt-body" href="mailto:makerslab&#064;dfmail&#046;org"> &nbsp;makerslab&#064;dfmail&#046;org</a></p>
				</div>

				<div class="pt-3">
					<h3 class="featurette-heading-sm font-weight-normal text-yellow wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">ESDM</span> Cluster</h3>
					<div class="divider b-y text-yellow w-50"></div>
					<p><i class="fa fa-phone"></i><a class="txt-body" href="tel:+91-951-331-5793"> &nbsp;+91-951-331-5793</a></p>
					<p><i class="fa fa-envelope text-white"></i><a class="txt-body" href="mailto:veeru&#046;sandbox&#064;dfmail&#046;org"> &nbsp;veeru&#046;sandbox&#064;dfmail&#046;org</a></p>
				</div>
			</div>
		</div>
	</div>

	<hr class="featurette-divider-sm">
	<h2 class=" text-yellow text-center Pt-3"><span class="text-muted">WE ARE </span>LOCATED</h2>
	<div class="divider b-y text-yellow content-middle"></div>
	
	<!-- sandbox startups map -->
	<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3561.618409517373!2d75.07641263306877!3d15.355151801849042!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sen!2sin!4v1554804189703!5m2!1sen!2sin" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe> -->
	<!-- sandbox startups map -->

	<!-- Deshpande startups map -->
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1650.195684243588!2d75.0780695973108!3d15.354586139391305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8bbb312ac6a9e0ac!2sDeshpande%20Startups!5e1!3m2!1sen!2sin!4v1572341057965!5m2!1sen!2sin" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen></iframe>
	<!-- Deshpande startups map -->

	<!--for google map with no iframe-->
<!-- <div id="map">
	<div id="googleMap" style="width:100%;height:400px;"></div>
</div> -->

<!--for google map with no iframe-->
<script>
	function myMap() {
		var mapProp= {
			center:new google.maps.LatLng(15.354457, 75.078536),
			zoom:17,
    // mapTypeId: google.maps.MapTypeId.SATELLITE
    mapTypeId: 'hybrid'
};
var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
</script>

<!--for google map with no iframe-->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgo1CbFrs45199-b9x8HNxHZMHrCn4EZQ&callback=myMap"></script> -->

<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
	window.onload = function() {
		var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
		recaptcha.required = true;
		recaptcha.oninvalid = function(e) {
    // do something
    alert("Please complete the captcha");
}
}
</script>

<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>