<!DOCTYPE html>
<html lang="en">
<head>
 <title>Virtual Hackathon - Makers Lab Events</title>
 <?php
 require_once 'essentials/meta.php';
 ?>
 <meta name="linkage" content="https://www.deshpandestartups.org/hackathon"/>
 <meta property="og:site_name" content="Deshpande Startups"/>
 <meta property="og:type" content="website">
 <meta property="og:url" content="https://www.deshpandestartups.org/hackathon">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/hackathon-bg.png">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/hackathon-img.png">
 <meta property="og:description" content="An initiative of “Yuva Entrepreneurship Program” of Deshpande Startups present “VIRTUAL HACKATHON”a 24-hour event witnessing a gathering of curious and like-minded Hackers, organized by India’s Largest Incubation Center – The Maker’s Lab"/>
 <meta name="author" content="Deshpande Startups"/>
 <meta name="description" content="An initiative of “Yuva Entrepreneurship Program” of Deshpande Startups present “VIRTUAL HACKATHON”, a 24-hour event witnessing a gathering of curious and like-minded Hackers, organized by India’s Largest Incubation Center – The Maker’s Lab"/>
 <!-- <meta name="keywords" content=""/> -->
 <meta property="og:title" content="Past Event: Virtual Hackathon - Makers Lab Events">
 <link rel="canonical" href="https://www.deshpandestartups.org/hackathon">
 <?php
         // $title = 'Deshpande Startups';
 require_once 'essentials/bundle.php';
 ?>
 
 <style type="text/css">
 </style>
</head>
<body>
 <?php
 require_once 'essentials/title_bar.php';
 require_once 'essentials/menus.php';
 ?>

 <img class="carousel-inner img-fluid" src="img/makers/hackathon-bg.png" width="1349" height="198" alt="Deshpande Startups, events IDEATHON">
 <nav aria-label="breadcrumb">
   <ol class="breadcrumb justify-content-end">
    <li class="breadcrumb-item"><a href="./">Home</a></li>
    <li class="breadcrumb-item"><a href="yuva-entrepreneurship">Yuva Entrepreneurship</a></li>
    <li class="breadcrumb-item active" aria-current="page">Virtual Hackathon</li>
  </ol>
</nav>

<div class="container">
 <!-- <br> -->
 <div class="center wow fadeInDown">
  <h2 class="text-yellow text-center">VIRTUAL HACKATHON</h2>
  <div class="divider b-y text-yellow content-middle"></div>
</div><br>
<div class="row">
 <div class="col-md-12">

    <p class="text-justify wow slideInLeft">An initiative of <b>“Yuva Entrepreneurship Program”</b> of Deshpande Startups present <b>“Virtual Hackathon”</b>, a 36-hour event witnessing a gathering of curious and like-minded Hackers, organized by India’s Largest Incubation Center - Deshpande Startups, next to Hubballi Airport.<br><br><b>“Virtual Hackathon”</b> is an exciting opportunity for all <b>programmers, coders, developers and hackers</b> to meet at one single platform, and <b>develop Social Impact solutions</b> and witness propelling India’s true growth story. 2<sup>nd</sup>, 3<sup>rd</sup> and 4<sup>th</sup> year Computer Science and Information Science students can participate and also stand a chance to win exciting prizes.</p>
  </div>
<!--  <div class="col-md-2">-->
<!--    <div class="row pl-4">-->
<!--      <a href="hackathon-form" class="btn btn-rotate" target="_blank">Apply Now</a>-->
<!--    </div>-->
<!--  </div>-->
</div>
</div>
<br>

<div class="featured-bg-container">
 <div class="row valign-wrapper">
  <div class="col-md-4">
    <img src="img/makers/hackathon-img.png" width="398" height="270" alt="Makers lab, hackathon event" class="img img-fluid img-thumbnail wow zoomIn">
  </div>
  <div class="col-md-8">
    <h4 class="text-yellow"><b>8 Reasons why you should participate/key highlights of the event:</b></h4>
    <ul class="text-justify wow slideInRight">
     <li>Get to be a part of vibrant community of developers and build your network</li>
     <li>Internship opportunities at Deshpande Startups</li>
     <li>Opportunity to code under the guidance of professional coders/developers</li>
     <li>Hands-on laboratory experience</li>
     <li>Unique chance to meet and network with exciting startups and team with high-enthu developers and explore further opportunities</li>
     <li>Win awards and recognition among developers</li>
     <li>Inspire the younger generation by sharing your practical knowledge and nurture them in developing their skills, we are certain that giving back is as joyous as learning</li>
     <li>Add real value to your resume with certification from Deshpande Startups</li>
   </ul>
 </div>
</div>
</div>

<div class="container pt-3">
  <!-- <h3>Frequently Asked Questions</h3> -->
  <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
   <h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">Frequently</span> Asked Questions</h2>
   <div class="divider b-y text-yellow content-middle"></div>
</div>
<br>
<div id="accordion">
   <div class="card cal card-hover-shadow">
      <div class="card-header" id="headingOne">
         <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
               <h4 class="text-yellow">What is the team size?</h4>
            </button>
         </h5>
      </div>
      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
         <div class="card-body">
            <div class="wpb_wrapper cal">
               <p class="text-justify">
                Each team can have maximum of 4 members &amp; a minimum of 2 members.
             </p>
          </div>
       </div>
    </div>
 </div>
 <div class="card cal card-hover-shadow">
   <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
         <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <h4 class="text-yellow">How much is the registration fee?</h4>
         </button>
      </h5>
   </div>
   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
         <!-- <br> -->
         <div class="wpb_wrapper cal">
            <p class="text-justify">
            We charge 200 INR per Team.</p>
         </div>
      </div>
   </div>
</div>
<div class="card cal card-hover-shadow">
   <div class="card-header" id="headingThree">
      <h5 class="mb-0">
         <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            <h4 class="text-yellow">What are the registration options?</h4>
         </button>
      </h5>
   </div>
   <!--<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
         <div class="wpb_text_column wpb_content_element ">
            <div class="wpb_wrapper cal">
               <p class="text-justify">
                <!--<a href="hackathon-form" target="_blank"> Click here</a> for registration.
             </p>
          </div>
       </div>
    </div>
 </div>
</div>-->
<div class="card cal card-hover-shadow">
   <div class="card-header" id="headingFour">
      <h5 class="mb-0">
         <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
            <h4 class="text-yellow"> What are the modes of payment?</h4>
         </button>
      </h5>
   </div>
   <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">
         <div class="wpb_text_column wpb_content_element ">
            <div class="wpb_wrapper cal">
               <p class="text-justify">
                Payments will be made online either through NEFT/UPI. Once the applicant receives a confirmation email for Deshpande Startups team only then the payment should be done on the details shared in the email. please share the transaction ID details to <a href="mailto:yuvaprogram&#064;dfmail&#046;org" target="_blank" rel="noopener noreferrer">yuvaprogram&#064;dfmail&#046;org</a> along with member details & purpose of payment once the payment is done.
             </p>
          </div>
       </div>
    </div>
 </div>
</div>
<div class="card cal card-hover-shadow">
   <div class="card-header" id="headingFive">
      <h5 class="mb-0">
         <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
            <h4 class="text-yellow"> What is the reporting time?</h4>
         </button>
      </h5>
   </div>
   <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
      <div class="card-body">
         <div class="wpb_text_column wpb_content_element ">
            <div class="wpb_wrapper cal">
               <p class="text-justify">
                It will be mentioned in the agenda for the Program. A detailed agenda will be shared with the teams who confirms there participation post-payment.
             </p>
          </div>
       </div>
    </div>
 </div>
</div>
<div class="card cal card-hover-shadow">
   <div class="card-header" id="headingSix">
      <h5 class="mb-0">
         <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
            <h4 class="text-yellow"> What is the duration of the Hackathon?</h4>
         </button>
      </h5>
   </div>
   <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
      <div class="card-body">
         <div class="wpb_text_column wpb_content_element ">
            <div class="wpb_wrapper cal">
               <p class="text-justify">
                The duration of the Hackathon is 36 hours.
             </p>
          </div>
       </div>
    </div>
 </div>
</div>
<div class="card cal card-hover-shadow">
   <div class="card-header" id="headingSeven">
      <h5 class="mb-0">
         <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
            <h4 class="text-yellow"> Where is the Hackathon conducted?</h4>
         </button>
      </h5>
   </div>
   <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
      <div class="card-body">
         <div class="wpb_text_column wpb_content_element ">
            <div class="wpb_wrapper cal">
               <p class="text-justify">
                It will be conducted virtually. All the process will be taken virtually right from Inauguration, Milestone Checks, Solution Submissions, Presentation & Valedictory
             </p>
          </div>
       </div>
    </div>
 </div>
</div>
<div class="card cal card-hover-shadow">
   <div class="card-header" id="headingEight">
      <h5 class="mb-0">
         <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
            <h4 class="text-yellow"> Who can apply?</h4>
         </button>
      </h5>
   </div>
   <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
      <div class="card-body">
         <div class="wpb_text_column wpb_content_element ">
            <div class="wpb_wrapper cal">
               <p class="text-justify">
                Students from Computer Science and Information Science students can participate, and who knows coding can apply for virtual hackathon.
             </p>
          </div>
       </div>
    </div>
 </div>
</div>
<div class="card cal card-hover-shadow">
   <div class="card-header" id="headingNine">
      <h5 class="mb-0">
         <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
            <h4 class="text-yellow"> Do we get certificates?</h4>
         </button>
      </h5>
   </div>
   <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
      <div class="card-body">
         <div class="wpb_text_column wpb_content_element ">
            <div class="wpb_wrapper cal">
               <p class="text-justify">
                Yes, participants are provided with the E-certificates for participating in the event.
             </p>
          </div>
       </div>
    </div>
 </div>
</div>
<div class="card cal card-hover-shadow">
   <div class="card-header" id="headingTen">
      <h5 class="mb-0">
         <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
            <h4 class="text-yellow"> What should I have?</h4>
         </button>
      </h5>
   </div>
   <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
      <div class="card-body">
         <div class="wpb_text_column wpb_content_element ">
            <div class="wpb_wrapper cal">
               <p class="text-justify">
               Participants need to have a laptop, dongle or modem for better access to the high-speed internet, and connected with your team members for coordination.
             </p>
          </div>
       </div>
    </div>
 </div>
</div>
<div class="card cal card-hover-shadow">
   <div class="card-header" id="headingEleven">
      <h5 class="mb-0">
         <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
            <h4 class="text-yellow"> How do we get problem statements?</h4>
         </button>
      </h5>
   </div>
   <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordion">
      <div class="card-body">
         <div class="wpb_text_column wpb_content_element ">
            <div class="wpb_wrapper cal">
               <p class="text-justify">
                Problem statements will be emailed to the Team leader by the organizers during the Hackathon inaugral.
             </p>
          </div>
       </div>
    </div>
 </div>
</div>
<div class="card cal card-hover-shadow">
   <div class="card-header" id="headingTwelve">
      <h5 class="mb-0">
         <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
            <h4 class="text-yellow"> How do we reach organizers for any queries?</h4>
         </button>
      </h5>
   </div>
   <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordion">
      <div class="card-body">
         <div class="wpb_text_column wpb_content_element ">
            <div class="wpb_wrapper cal">
               <p class="text-justify">
                Reach us out at, M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a> / E:<a href="mailto:yuvaprogram&#064;dfmail&#046;org"> yuvaprogram&#064;dfmail&#046;org</a>
             </p>
          </div>
       </div>
    </div>
 </div>
</div>
</div>
</div>
<br>
<br>

<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>