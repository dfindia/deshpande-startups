<!DOCTYPE html>
<html lang="en">
<head>
	<title>Shreyansh Singhal - Venture Capital, Ankur Capital</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/shreyansh"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/shreyansh">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/edge/shreyansh.png">
	<meta property="og:description" content="Shreyansh works with the investment team of Ankur Capital, an early stage fund investing in transformative technologies in unconventional markets that unlock opportunities for the next billion Indians."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Shreyansh works with the investment team of Ankur Capital, an early stage fund investing in transformative technologies in unconventional markets that unlock opportunities for the next billion Indians."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Makers Lab Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Shreyansh Singhal - Venture Capital, Ankur Capital">
	<link rel="canonical" href="https://www.deshpandestartups.org/shreyansh">
	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	/*p{text-align:justify;}*/
	.cal{
		font-family: calibri;
	}
	.modal-title {
		text-align: left;
		margin: 0;
		line-height: 1;
	}
	.follow-us{
		margin:20px 0 0;
	}
	.follow-us li{ 
		display:inline-block; 
		width:auto; 
		margin:0 5px;
	}
	.follow-us li .fa{ 
		font-size:25px; 
		/*color:#767676;*/
		color: #e7572a;
	}
	.follow-us li .fa:hover{ 
		color:#025a8e;
	}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>

	<div class="container cal pt-4">
		<div class="row wow fadeInLeft" data-wow-duration="1s" data-wow-offset="50">
			<div class="col-md-12 team-main">
				<div>
					<div class="modal-header">
						<!-- <a type="button" class="close" data-dismiss="modal" aria-label="Close" href="#"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> back </a> -->
						<h4 class="modal-title modal-title-cust text-yellow" id="myModalLabel">SHREYANSH SINGHAL<br><small class="text-muted">Venture Capital - Ankur Capital</small></h4>
						<ul class="follow-us clearfix">
							<li><a href="https://twitter.com/ansh_shrey" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="https://www.linkedin.com/in/shreyanshsinghal/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="modal-body">
						<p class="text-justify">
							<img src="img/edge/shreyansh.png" width="165" height="160" class="img img-fluid mr-2 rounded-circle" alt="Shreyansh, Venture Capital-Ankur Capital, EDGE Mentor" align="left">
							Shreyansh works with the investment team of Ankur Capital, an early stage fund investing in transformative technologies in unconventional markets that unlock opportunities for the next billion Indians. Having half a decade of experience in and around startups solving for India, he has a keen interest in early stage opportunities to help and scale the startups. In the past, he has donned various hats as an entrepreneur, investment banker and consultant and market researcher. In his latest role, he has evaluated companies across sectors and invested in digital inclusion technologies, alternative proteins, new age consumer food products and others.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>