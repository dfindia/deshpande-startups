<!DOCTYPE html>
<html lang="en">
<head>
	<title>Events | THENEXTLINK - A Blockchain Hackathon</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/blockchain"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/blockchain">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/blockchain-bg.jpg">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/blockchain.png">
	<meta property="og:description" content="An Initiative of Deshpande Startups is extending an opportunity to the current and alumni engineers to be a part of exclusive National Level 24 Hours Hackathon event, it includes 8 hours training with hands-on coding experience followed by 16 hours Hacking on Blockchain."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="An Initiative of Deshpande Startups is extending an opportunity to the current and alumni engineers to be a part of exclusive National Level 24 Hours Hackathon event, it includes 8 hours training with hands-on coding experience followed by 16 hours Hacking on Blockchain."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="THENEXTLINK - A Blockchain Hackathon">
	<link rel="canonical" href="https://www.deshpandestartups.org/blockchain">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/blockchain-bg.jpg" width="1349" height="198" alt="Deshpande Startups, events,Blockchain hackathon">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">A Blockchain Hackathon</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">THENEXTLINK</span><br> A BLOCKCHAIN HACKATHON</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
          <div class="col-md-4 p-4">
            <div class="card-deck">
              <div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
                <img class="card-img-top img-fluid wow zoomIn" src="img/events/blockchain.jpg" width="474" height="237" alt="Deshpande startups THENEXTLINK Blockchain hackathon">
                <div class="card-body">
                  <h5 class="card-title text-yellow text-center text-truncate">THENEXTLINK <br> A Blockchain Hackathon</h5>
                  <p><b>Date : </b>February 02<sup>nd</sup> & 03<sup>rd</sup> 2019</p>
                  <!-- <p><b>Time : </b>08:30 AM</p> -->
                  <!-- <p><b>Last date to apply : </b>January<br> 04<sup>th</sup> 2019</p> -->
                  <p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
                  </p>
                  <p class="text-truncate"><b>Contact details:</b><br>
                    M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
                    E:<a href="mailto:makerslab&#064;dfmail&#046;org"> makerslab&#064;dfmail&#046;org</a>
                  </p>
                </div>
                <div class="card-footer">
                  <p class="text-yellow">The registrations has been closed.</p>
                  <!-- <a href="blockchain-form" class="btn p-1 btn-primary" target="_blank">Register Now</a> -->
                  <!-- <a href="https://thenextlink.typeform.com/to/gBzpcE" class="btn p-1 btn-primary" target="_blank">Register Now</a> -->
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-8">
            <p class="pt-4 text-yellow"><b>Event description:</b></p>
            <!-- <br> -->
            <p class="text-justify wow slideInRight"><b>Blockchain Hackathon & Startup Dialogue 2019,</b> an initiative of Deshpande Startups is extending an opportunity to the current and alumni engineers to be a part of exclusive <b>national level 24 hours hackathon</b> event, it includes <b>8 hours training</b> with hands-on coding experience followed by <b>16 hours hacking on blockchain.</b></p>
            <br>
            <p class="text-justify wow slideInRight">We cordially invite you to build transparent tomorrow & develop the social impact solutions, propelling India's true growth story, and sense the vibrant startup ecosystem in a tier 2 city like hubblli during hackathon 2019.
            </p>
            <br>
            <p class="text-justify wow slideInRight"><b>Participate in THENEXTLINK Blockchain Hackathon and solve real world problems. Learn the cutting edge technology by industry experts and win exciting prizes.</b>
            </p>
            <p class="text-yellow"><b>Why should you join?</b></p>
            <ul class="wow slideInRight text-justify">
             <li>Network with exciting startups, high enthu developers & explore further opportunities</li>
             <li>Add real value to your resume with certification from Deshpande Startups</li>
             <li>A platform for placement/hiring by techurate and tech startups</li>
             <li>Get trained by experts from IBM and free access to IBM cloud platform</li>
           </ul>
         </div>
       </div>
     </div>
   </div>
 </div>
 <!-- <br> -->
 <div class="container">
  <!-- <h3>Frequently Asked Questions</h3> -->
  <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
   <h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">FREQUENTLY</span> ASKED QUESTIONS</h2>
   <div class="divider b-y text-yellow content-middle"></div>
 </div>
 <br>
 <div id="accordion">
<div class="card cal card-hover-shadow">
 <div class="card-header" id="headingNine">
  <h5 class="mb-0">
   <button class="btn btn-link" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
    <h4 class="text-yellow"> What can i benefit from THENEXTLINK - A Blockchain Hackathon?</h4>
  </button>
</h5>
</div>
<div id="collapseNine" class="collapse show" aria-labelledby="headingNine" data-parent="#accordion">
  <div class="card-body">
   <div class="wpb_text_column wpb_content_element ">
    <div class="wpb_wrapper cal">
     <ul class="text-justify">
      <li>Participate in THENEXTLINK Blockchain Hackathon and solve real world problems. Learn the cutting edge technology by industry experts and win exciting prizes.</li>
      <li>Network with exciting startups, high enthu developers & explore further opportunities</li>
      <li>Add real value to your resume with certification from Deshpande Startups</li>
      <li>A platform for placement/hiring by techurate and tech startups</li>
      <li>Get trained by experts from IBM and free access to IBM cloud platform</li>
      <li>Innovate and develop solutions of real world problems specifically focused on tier 2 & 3 cities of India</li>
      <li>Knowledge sharing by prominent figures and a showcase by startups about their products/services in blockchain domain</li>
    </ul>
  </div>
</div>
</div>
</div>
</div>
   <div class="card cal card-hover-shadow">
    <div class="card-header" id="headingOne">
     <h5 class="mb-0">
      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" ">
       <h4 class="text-yellow">What is a problem statement?</h4>
     </button>
   </h5>
 </div>
 <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
   <div class="card-body">
    <div class="wpb_wrapper cal">
     <p class="text-justify">
      A problem statement is a concise description of an issue to be addressed or a condition to be improved upon. It identifies the gap between the current (problem) state and desired (goal) state of a process or product. Blockchain Hackathon problem statements will be revealed post training sessions on the first day of the event. Problem statements will be targeting more than one sector. 
    </p>
  </div>
</div>
</div>
</div>
<div class="card cal card-hover-shadow">
 <div class="card-header" id="headingTwo">
  <h5 class="mb-0">
   <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
    <h4 class="text-yellow">Who can participate?</h4>
  </button>
</h5>
</div>
<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
  <div class="card-body">
   <!-- <br> -->
   <div class="wpb_wrapper cal">
    <p class="text-justify">
    100-120 (Pre-selected out of 500+) students, graduates and professionals from different engineering colleges from south india.</p>
  </div>
</div>
</div>
</div>
<div class="card cal card-hover-shadow">
 <div class="card-header" id="headingThree">
  <h5 class="mb-0">
   <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
    <h4 class="text-yellow"> How does it work?</h4>
  </button>
</h5>
</div>
<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
  <div class="card-body">
   <div class="wpb_text_column wpb_content_element ">
    <div class="wpb_wrapper cal">
     <p class="text-justify">
      A national level 24 hours hackathon event, it includes 8 hours training with hands-on coding experience by IBM and other experts followed by 16 hours hacking on blockchain.
    </p>
  </div>
</div>
</div>
</div>
</div>
<div class="card cal card-hover-shadow">
 <div class="card-header" id="headingFour">
  <h5 class="mb-0">
   <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
    <h4 class="text-yellow"> What should i bring?</h4>
  </button>
</h5>
</div>
<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
  <div class="card-body">
   <div class="wpb_text_column wpb_content_element ">
    <div class="wpb_wrapper cal">
     <p class="text-justify">
      Participants need to provide their own laptops and winter wearing clothes, we will be providing the basic things needed which is available at Deshpande Startups. The facilities include: power cords supply, internet and other logistics.
    </p>
  </div>
</div>
</div>
</div>
</div>
<!-- <div class="card cal card-hover-shadow">
 <div class="card-header" id="headingFive">
  <h5 class="mb-0">
   <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
    <h4 class="text-yellow"> What do i make?</h4>
  </button>
</h5>
</div>
<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
  <div class="card-body">
   <div class="wpb_text_column wpb_content_element ">
    <div class="wpb_wrapper cal">
     <p class="text-justify">
      Frame your team min 2 max 5 members. Learn and hack for the given problem statements.
    </p>
  </div>
</div>
</div>
</div>
</div> -->
<div class="card cal card-hover-shadow">
 <div class="card-header" id="headingSix">
  <h5 class="mb-0">
   <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
    <h4 class="text-yellow"> Do we get food facility?</h4>
  </button>
</h5>
</div>
<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
  <div class="card-body">
   <div class="wpb_text_column wpb_content_element ">
    <div class="wpb_wrapper cal">
     <p class="text-justify">
      Yes,
      <ul>
      <li>Day 1 (2nd Feb) starts with lunch, evening tea, dinner.</li>
      <li>Day 2 (3rd Feb) breakfast, lunch and high tea. Refreshments over night.</li>
    </ul>
    </p>
  </div>
</div>
</div>
</div>
</div>
<div class="card cal card-hover-shadow">
 <div class="card-header" id="headingSeven">
  <h5 class="mb-0">
   <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
    <h4 class="text-yellow"> Do we get the certificates?</h4>
  </button>
</h5>
</div>
<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
  <div class="card-body">
   <div class="wpb_text_column wpb_content_element ">
    <div class="wpb_wrapper cal">
     <p class="text-justify">
      Yes, upon completing the participation you will be given certificate.
    </p>
  </div>
</div>
</div>
</div>
</div>
<div class="card cal card-hover-shadow">
 <div class="card-header" id="headingEight">
  <h5 class="mb-0">
   <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
    <h4 class="text-yellow"> How much does this cost?</h4>
  </button>
</h5>
</div>
<div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
  <div class="card-body">
   <div class="wpb_text_column wpb_content_element ">
    <div class="wpb_wrapper cal">
     <p class="text-justify">
      Registration fee of 500 INR per participant, based on selection. This would cover the costs of food.
    </p>
  </div>
</div>
</div>
</div>
</div>
<div class="card cal card-hover-shadow">
 <div class="card-header" id="headingTen">
  <h5 class="mb-0">
   <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
    <h4 class="text-yellow"> Resources?</h4>
  </button>
</h5>
</div>
<div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
  <div class="card-body">
   <div class="wpb_text_column wpb_content_element ">
    <div class="wpb_wrapper cal">
     <p class="text-justify">
      We’ll be sharing online resources from time to time with participants once we get more details from IBM.
    </p>
  </div>
</div>
</div>
</div>
</div>

</div>
</div>
<br>
<br>
<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>