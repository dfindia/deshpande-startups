	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Apply now for makers lab membership</title>
		<?php
		require_once 'essentials/meta.php';
		?>
		<meta name="linkage" content="https://www.deshpandestartups.org/membership-form"/>
		<meta property="og:site_name" content="Deshpande Startups"/>
		<meta property="og:type" content="website">
		<meta property="og:url" content="https://www.deshpandestartups.org/membership-form">
		<!-- <meta property="og:image" content="https://www.deshpandestartups.org/img/events/alexathon.jpg"> -->
		<meta property="og:description" content="Membership in Makers Lab offers a direct connection to many of our services, programs and technological help for your startups."/>
		<meta name="author" content="Deshpande Startups"/>
		<meta name="description" content="Membership in Makers Lab offers a direct connection to many of our services, programs and technological help for your startups."/>
		<!-- <meta name="keywords" content=""/> -->
		<meta property="og:title" content="Apply now for Makers Lab Membership">
		<link rel="canonical" href="https://www.deshpandestartups.org/membership-form">
		<?php
		// $title = 'Deshpande Startups';
		require_once 'essentials/bundle.php';
		?>
	</head>
	<body>
		<?php
		require_once 'essentials/title_bar.php';
		require_once 'essentials/menus.php';
		?>
		<br>
		<div class="container text-center">
			<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">APPLY</span> FOR MEMBERSHIP</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="container">
			<div class="row">
				<div class="col-md-8 offset-lg-2">
					<iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
					</iframe>
					<div class="p-3 w3-card">
						<form role="form" action="https://docs.google.com/forms/d/e/1FAIpQLScQIUZT1MV-Td2mRB16Iu56Pirhi5BHi555fGNKu1zOpuDwdw/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">
							<div class="row">
								<div class="col-md-12 pad">
									<!-- <div class="row"> -->
										<div class="col-md-12">
											<div class="row">
												<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
													<label for="input1"><b>Name<span class="text-yellow">*</span></b></label>
													<input type="text" name="entry.1711740987" class="box2 form-control" maxlength="50" pattern="[A-Za-z\s]{1,50}" placeholder="Mention your name" title="Mention your name" required="required">
												</div>
												<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
													<label for="input3"><b>Mobile number<span class="text-yellow">*</span></b></label>
													<input type="phone" name="entry.1699590184" class="box2 form-control" pattern="\d*" min="12" placeholder="Mention your mobile number" maxlength="10" minlength="10" title="Your mobile number" required="required">
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="row">
												<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
													<label for="input2"><b>Email-Id<span class="text-yellow">*</span></b></label>
													<input type="email" name="entry.1612237477" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
												</div>
												<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
													<label for="input4"><b>College/Organization/Company<span class="text-yellow">*</span></b></label>
													<input type="text" name="entry.5247992" class="box2 form-control" placeholder="College/Organization/Company" required="required">
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="row">
												<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
													<label for="input5"><b>City<span class="text-yellow">*</span></b></label>
													<input type="text" name="entry.1252123054" class="box2 form-control" placeholder="Your location" required="required">
												</div>
												<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
													<label for="input5"><b>State<span class="text-yellow">*</span></b></label>
													<input type="text" name="entry.1002125042" class="box2 form-control" placeholder="Your State" required="required">
												</div>
											</div>
										</div>

										<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
											<label for="input7"><b>Are You?<span class="text-yellow">*</span></b></label><br>
										</div>
										<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
											<div class="row">
												<div class="form-group col-md-4">
													<label for="student"><input type="radio" name="entry.86334776" value="student" required="required"> Student</label>
												</div>
												<div class="form-group col-md-4">
													<label for="startup"><input type="radio" name="entry.86334776" value="Startup"> Startup</label>
												</div>
												<div class="form-group col-md-4">
													<label for="Professional"><input type="radio" name="entry.86334776" value="professional"> Professional</label>
												</div>
											</div>

											<div class="row txbx1" style="display: none">
												<div class="form-group col-md-12 m-0">
													<div class="row">
														<div class="form-group col-md-6">
															<label for="input45"><b>Academic Qualification<span class="text-yellow">*</span></b></label>
															<select class="form-control" name="entry.169719288">
																<option value="Diploma">Diploma</option>
																<option value="B.E">B.E</option>
																<option value="M.Tech">M.Tech</option>
																<option value="BBA">BBA</option>
																<option value="MBA">MBA</option>
																<option value="BCA">BCA</option>
																<option value="MCA">MCA</option>
																<option value="B.Com">B.Com</option>
																<option value="M.Com">M.Com</option>
																<option value="B.Sc">B.Sc</option>
																<option value="M.Sc">M.Sc</option>
																<option value="ITI">ITI</option>
															</select>
															<!-- <input type="text" name="entry.169719288" class="box2 form-control" title="Mention your Academic Qualification" placeholder="Your Academic Qualification"> -->
														</div>
														<div class="form-group col-md-6">
															<label for="input45"><b>Stream/ Department<span class="text-yellow">*</span></b></label>
															<select class="form-control" name="entry.621564618">
																<option value="CS" selected>CS</option>
																<option value="IS">IS</option>
																<option value="E&C">E&C</option>
																<option value="E&E">E&E</option>
																<option value="Mechanical">Mechanical</option>
																<option value="Civil">Civil</option>
																<option value="Architecture">Architecture</option>
																<option value="Bio Technology">Bio Technology</option>
																<option value="Chemical">Chemical</option>
																<option value="Aeronautical">Aeronautical</option>
																<option value="Biochemical">Biochemical</option>
																<option value="Other">Other</option>
															</select>
															<!-- <input type="text" name="entry.621564618" class="box2 form-control" title="Mention your Stream/ branch" placeholder="Your Stream/ branch"> -->
														</div>
													</div>
												</div>
											</div>

											<div class="row txbx2" style="display: none">
												<div class="form-group col-md-6">
													<label for="input45"><b>Year<span class="text-yellow">*</span></b></label>
													<select class="form-control" name="entry.1612539730">
														<option value="-">Select...</option>
														<option value="1st">1<sup>st</sup></option>
														<option value="2nd">2<sup>nd</sup></option>
														<option value="3rd">3<sup>rd</sup></option>
														<option value="4th">4<sup>th</sup></option>
													</select>
													<!-- <input type="text" name="entry.1612539730" class="box2 form-control" title="Mention your year" placeholder="Your year"> -->
												</div>
											</div>

											<div class="row txbx3" style="display: none">
												<div class="form-group col-md-12">
													<label for="input45"><b>Startup Name<span class="text-yellow">*</span></b></label>
													<input type="text" name="entry.519865218" class="box2 form-control" title="Your startup name" placeholder="Your startup name">
												</div>
											</div>

											<div class="row txbx4" style="display: none">
												<div class="form-group col-md-12">
													<label for="input45"><b>Professional In?<span class="text-yellow">*</span></b></label>
													<input type="text" name="entry.346605355" class="box2 form-control" title="You are professional in?" placeholder="You are professional in?">
												</div>
											</div>
										</div>
										<div class="form-group col-lg-12">
									<!-- <span class="text-yellow"><b>*</b></span>
										<div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div><br> -->
										<div class="form-group">
											<!-- <label for="agreement"><input type="checkbox" name="entry.788069306" value="I agree to make payment according to the team size" required="required"> I agree to make payment according to the team size for the Alexathon Participation.<span class="text-yellow"><b>*</b></span></label> -->
										</div>
										<span class="text-yellow"><h6><b>*</b> Fields are mandatory</h6></span>
										<input type="submit" class="btn btn-warning" id="ss-submit" name="submit" value="Submit">
									</div>
									<!-- </div> -->
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<br>

		<script src='https://www.google.com/recaptcha/api.js'></script>
		<?php
		require_once 'essentials/footer.php';
		require_once 'essentials/copyright.php';
		require_once 'essentials/js.php';
		?>
		<script type="text/javascript">
			$(function() {
				$('[name="entry.86334776"]').on('click', function (e) {
					var val = $(this).val();
					if (val == "student") {
						$('.txbx1').show('fade');
						$('.txbx2').show('fade');
						$('.txbx3').hide();
						$('.txbx4').hide();
					}
					else if (val == "Startup") {
						$('.txbx1').show('fade');
						$('.txbx2').hide();
						$('.txbx3').show('fade');
						$('.txbx4').hide();
					}else {
						$('.txbx1').show('fade');
						$('.txbx2').hide();
						$('.txbx3').hide();
						$('.txbx4').show('fade');
					};
				});
			});
		</script>
		<script>
			window.onload = function() {
				var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
				recaptcha.required = true;
				recaptcha.oninvalid = function(e) {
	 // do something
	 alert("Please complete the captcha");
	}
}
</script>
</body>
</html>