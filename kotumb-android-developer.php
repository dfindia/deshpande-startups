<!DOCTYPE html>
<html lang="en">
<head>
	<title>Senior Full stack Android Developer - KOTUMB</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/kotumb-android-developer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/kotumb-android-developer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/kotumb-big.png">
	<meta property="og:description" content="We are looking for Senior Full stack Android Developer. Job Position: Senior Full stack Android Developer, Experience:  5+ years."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Senior Full stack Android Developer. Job Position: Senior Full stack Android Developer, Experience:  5+ years."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Senior Full stack Android Developer, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/kotumb-android-developer">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Senior Full stack </span>Android Developer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Senior Full stack Android Developer<br>
					<strong>Startup:</strong> KOTUMB<br>
					<!-- <strong>Qualification:</strong> B.E graduates<br> -->
					<strong>Experience:</strong> 5+ years of experience required <br>
					<strong>Job Location:</strong> Hubballi<br>
				</p>
			</div>
			<div class="col-md-6">
				<a href="https://www.kotumb.in/" target="_blank" rel="nofollow"><img src="img/career/kotumb-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, KOTUMB"></a>
			</div>
		</div>
		<!-- <br> -->

		<p class="text-justify pt-2">KOTUMB is a fast growing - professional networking app. KOTUMB has been mentored at IIMB and now being incubated at Deshpande Startups.</p>
		<div class="row pt-2">
			<div class="col-md-12">
				<h3 class="text-yellow">Skills and Expertise:</h3>
				<ul class="text-justify">
					<li>A highly skilled full stack developer who is comfortable with both front and back end  programming for andorid applications</li>
					<li>The person will be leading a couple of junior programmers to complete the work and will need to meet the targets and quality of delivery</li>
					<li>Will be responsible for maintaining our android mobile application and in the future development </li>
					<li>Ensuring the responsiveness of applications, developing and maintaining new features and updates to the application ranging from but not limited to Object recognition, Chat integrations, API integrations, Back end servers, Dashboards for merchants, Admin dashboards, Analytics and influencers panels, Algorithms for data collection and tailored actionable feedback in the front end and user interfaces</li>
					<li>The application has video integration and bots for voice assistance. They need to be developed and maintained</li>
					<li>Full stack developers will be required to see out a project from conception to final product and be required to maintain and make these projects scalable, requiring good organizational skills and attention to detail</li>
				</ul>
			</div>
		</div>

		<div class="row pt-2">
			<div class="col-md-12">
				<h3 class="text-yellow">Roles and Responsibilities:</h3>
				<ul>
					<li>Developing front end in mobile architecture</li>
					<li>Designing and developing user interactions on mobile applications</li>
					<li>Developing back end for mobile applications</li>
					<li>Creating servers and databases for functionality</li>
					<li>Ensuring cross-platform optimization for mobile phones</li>
					<li>Ensuring responsiveness of applications</li>
					<li>Working alongside graphic designers for design and features</li>
					<li>Seeing through a project from conception to finished product</li>
					<li>Designing and developing and integrating APIs</li>
					<li>Meeting both technical and consumer needs</li>
					<li>Staying abreast of developments in E-commerce applications and related programming languages</li>
					<li>Developing, Maintaining, Updating and Optimizing the back end in AWS</li>
				</ul>
			</div>
		</div>

	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to<br>E:<a href="mailto:hemanthmathad&#064;kotumb&#046;com"> hemanthmathad&#064;kotumb&#046;com</a></b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>