<!DOCTYPE html>
<html lang="en">
<head>
	<title>Apply now - Workshop at Yuva Entrepreneurship program</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/workshop-form"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/workshop-form">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/3d-arduino-workshop-bg.jpg">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/makers/workshop-bg.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/3d-arduino-workshop.jpg">
	<meta property="og:description" content="Yuva Entrepreneurship program organizes workshops on the topics of IoT and Electronics, 3D Printing. Placed in a tier-2 city Deshpande Startups provides world-class infrastructure for Entrepreneurs with creative environment to host a vibrant execution-based ecosystem."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Yuva Entrepreneurship program organizes workshops on the topics of IoT and Electronics, 3D Printing. Placed in a tier-2 city Deshpande Startups provides world-class infrastructure for Entrepreneurs with creative environment to host a vibrant execution-based ecosystem."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Apply now for workshop">
	<link rel="canonical" href="https://www.deshpandestartups.org/workshop-form">
	<?php
			// $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">

	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<!-- <img class="carousel-inner img-fluid" src="img/events/arduino-and-3d-bg.png" width="1349" height="198" alt="Deshpande Startups, events, Startup Boot Camp"> -->
	<br>
	<br>
	<div class="container text-center">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">APPLY FOR</span> WORKSHOP</h2>
		<div class="divider b-y text-yellow content-middle"></div>
		<!-- <h2 class="text-yellow Pt-5">The registration has been closed</h2> -->
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-lg-2">
				<!-- <h5 class="text-center">Request base registration contact us: M: +91-951-331-5791  E: makerslab@dfmail.org</h5><br> -->
				<iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
				</iframe>
				<form role="form" action="https://docs.google.com/forms/d/e/1FAIpQLSeIvODwYiTbIsG5sRB7cTCheqBvzcUa_znClN_MTjIRjZvR4w/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">

					<div class="row w3-card p-3">
						<div class="col-md-12 pad">
							<div class="row">
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
									<label for="input1"><b>Name<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.925953799" class="box2 form-control" maxlength="50" pattern="^(?![ .]+$)[a-zA-Z .]*$" placeholder="Mention your name" title="Mention your name" required="required">
								</div>
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
									<label for="input3"><b>Mobile number<span class="text-yellow">*</span></b></label>
									<input type="phone" name="entry.1334738182" class="box2 form-control" pattern="\d*" min="12" placeholder="Mention your mobile number" maxlength="10" minlength="10" title="Your mobile number" required="required">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
									<label for="input2"><b>Email-Id<span class="text-yellow">*</span></b></label>
									<input type="email" name="entry.450398318" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
								</div>
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input4"><b>College/Institute/Company/Organization<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.1762996437" class="box2 form-control" placeholder="Name of College/Institute/Company/Organization" required="required">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input5"><b>City<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.1772295719" pattern="^(?![ .]+$)[a-zA-Z .]*$" class="box2 form-control" placeholder="Your location" required="required">
								</div>
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input5"><b>State<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.1695319444" pattern="^(?![ .]+$)[a-zA-Z .]*$" class="box2 form-control" placeholder="Your State" required="required">
								</div>
							</div>
						</div>

						<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input7"><b>Are You?<span class="text-yellow">*</span></b></label><br>
						</div>
						<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<div class="row">
								<div class="form-group col-md-6">
									<label for="student"><input type="radio" name="entry.1574222932" value="student" required="required"> Student</label>
								</div>
								<div class="form-group col-md-6">
									<label for="Graduate"><input type="radio" name="entry.1574222932" value="Graduate"> Graduate</label>
								</div>
								<!-- <div class="form-group col-md-4">
									<label for="Professional"><input type="radio" name="entry.1574222932" value="professional"> Professional</label>
								</div> -->
							</div>

							<div class="row txbx1" style="display: none">
								<div class="form-group col-md-12 m-0">
									<div class="row">
										<div class="form-group col-md-6">
											<label for="input45"><b>Academic Qualification<span class="text-yellow">*</span></b></label>
											<select class="form-control" name="entry.536708748">
												<option value="Diploma">Diploma</option>
												<option value="B.E">B.E</option>
												<option value="M.Tech">M.Tech</option>
												<option value="BBA">BBA</option>
												<option value="MBA">MBA</option>
												<option value="BCA">BCA</option>
												<option value="MCA">MCA</option>
												<option value="B.Com">B.Com</option>
												<option value="M.Com">M.Com</option>
												<option value="B.Sc">B.Sc</option>
												<option value="M.Sc">M.Sc</option>
												<option value="ITI">ITI</option>
											</select>
										</div>

										<div class="form-group col-md-6">
											<label for="input45"><b>Stream/ Branch<span class="text-yellow">*</span></b></label>
											<select class="form-control" name="entry.616735809">
												<option value="CS" selected>CS</option>
												<option value="IS">IS</option>
												<option value="E&C">E&C</option>
												<option value="E&E">E&E</option>
												<option value="Mechanical">Mechanical</option>
												<option value="Civil">Civil</option>
												<option value="Architecture">Architecture</option>
												<option value="Bio Technology">Bio Technology</option>
												<option value="Chemical">Chemical</option>
												<option value="Aeronautical">Aeronautical</option>
												<option value="Biochemical">Biochemical</option>
												<option value="Other">Other</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="row txbx2" style="display: none">
								<div class="form-group col-md-12">
									<div class="row">
										<div class="form-group col-md-12">
											<label for="input45"><b>Year<span class="text-yellow">*</span></b></label>
											<select class="form-control" name="entry.237470881">
												<option value="-">Select...</option>
												<option value="1st Year">1<sup>st</sup> Year</option>
												<option value="2nd Year">2<sup>nd</sup> Year</option>
												<option value="3rd Year">3<sup>rd</sup> Year</option>
												<option value="4th Year">4<sup>th</sup> Year</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<!-- <div class="row txbx3" style="display: none">
								<div class="form-group col-md-12">
									<div class="row">
										<div class="form-group col-md-12">
											<label for="input45"><b>Professional In?<span class="text-yellow">*</span></b></label>
											<input type="text" name="entry.951258556" class="box2 form-control" title="You are professional in?" placeholder="You are professional in?">
										</div>
									</div>
								</div>
							</div> -->

									<!-- <select class="form-control" name="">
								<option value=""></option>
								<option value=""></option>
								<option value=""></option>
								<option value=""></option>
							</select> -->
						</div>

						<!-- <div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input50"><b>Select the workshop you want to apply for<span class="text-yellow">*</span></b></label>
						</div>
						<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<div class="row">
								<div class="col-md-6 form-group">
									<label for="3dprinting"><input type="checkbox" name="entry.1290358863" id="one" value="3D Printing" checked="checked"> 3D Printing</label>
								</div>
								<div class="col-md-6 form-group">
									<label for="iot"><input type="checkbox" name="entry.1290358863" id="two" value="Arduino workshop"> Arduino workshop</label>
								</div>
							</div>
						</div> -->

						<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input50"><b>Select the workshop you want to apply for<span class="text-yellow">*</span></b></label>
						</div>
						<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<div class="row">
								<div class="col-md-6">
									<label for="3dprinting"><input type="radio" name="entry.1290358863" id="one" value="3D Printing" checked="checked"> 3D Printing</label>
								</div>
								<div class="col-md-6">
									<label for="iot"><input type="radio" name="entry.1290358863" id="two" value="Arduino workshop"> Arduino workshop</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label for="mech"><input type="radio" name="entry.1290358863" id="three" value="Mechanical & Civil"> Mechanical & Civil</label>
								</div>
								<div class="col-md-6">
									<label for="machinelearning"><input type="radio" name="entry.1290358863" id="four" value="Machine Learning"> Machine Learning</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label for="android"><input type="radio" name="entry.1290358863" id="five" value="Web & Android Development"> Web & Android Development</label>
								</div>
							</div>


							<!-- <div class="row txbx4" style="display: none">
								<div class="form-group col-md-12">
										<div class="form-group col-md-12">
											<input type="text" class=" box2 form-control" name="entry.405453607" title="mention name" pattern="[A-Za-z\s]{1,50}" placeholder="Name of participant 2"/>
										</div>
										<div class="form-group col-md-12">
											<input type="text" class="box2 form-control" pattern="\d*" maxlength="10" minlength="10" name="entry.2060293040" placeholder="Mobile number of participant 2"/>
										</div>
										<div class="form-group col-md-12">	
											<input type="email" class="box2 form-control" name="entry.1423835170" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email-Id of participant 2"/>
										</div>
										<div class="form-group col-md-12">
											<input type="text" class="box2 form-control" name="entry.1848816327" title="mention name" pattern="[A-Za-z\s]{1,50}" placeholder="Name of participant 3"/>
										</div>
										<div class="form-group col-md-12">
											<input type="text" class="box2 form-control" name="entry.1767658446" pattern="\d*" maxlength="10" minlength="10" placeholder="Mobile number of participant 3"/>
										</div>
										<div class="form-group col-md-12">	
											<input type="email" class="box2 form-control" name="entry.763081010" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email-Id of participant 3"/>
										</div>
										<div class="form-group col-md-12">
											<input type="text" class="box2 form-control" name="entry.524591563" title="mention name" pattern="[A-Za-z\s]{1,50}" placeholder="Name of participant 4"/>
										</div>
										<div class="form-group col-md-12">
											<input type="text" class="box2 form-control" name="entry.1530918235" pattern="\d*" maxlength="10" minlength="10" placeholder="Mobile number of participant 4"/>
										</div>
										<div class="form-group col-md-12">	
											<input type="email" class="box2 form-control" name="entry.2092216976" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email-Id of participant 4"/>
										</div>
									</div>
								</div> -->

								</div>

								<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input6"><b>How did you come to know about workshop?<span class="text-yellow">*</span></b></label><br>
									<div class="row">
										<div class="col-md-6">
											<label for="Personal"><input type="radio" name="entry.533495035" value="Personal Reference" checked="checked"> Personal reference</label>
										</div>
										<div class="col-md-6">
											<label for="Newsletters"><input type="radio" name="entry.533495035" value="Email News Letter"> Email news letter</label>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<label for="facebook"><input type="radio" name="entry.533495035" value="Facebook"> Facebook</label>
										</div>
										<div class="col-md-6">
											<label for="whatsapp"><input type="radio" name="entry.533495035" value="Whatsapp"> Whatsapp</label>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<label for="SocialMedia"><input type="radio" name="entry.533495035" value="Other Social Media"> Other social media</label>
										</div>
										<div class="col-md-6">
											<label for="PrintMedia"><input type="radio" name="entry.533495035" value="Print Media"> Print media</label>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-12">
									<!-- <span class="text-yellow"><b>*</b></span>
										<div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div><br> -->
										<div class="form-group">
											<label for="agreement"><input type="checkbox" name="entry.553338795" value="I agree to make payment for the workshop" required="required"> I agree to make payment according to the workshop selected.<span class="text-yellow"><b>*</b></span></label>
										</div>
										<span class="text-yellow"><h6><b>*</b> Fields are mandatory</h6></span>
										<input type="submit" class="btn custom-btn2 btn-warning" id="ss-submit" name="submit" value="Submit">
									</div>
								</div>

							</form>
						</div>
					</div>
				</div>
				<!-- </div> -->
				<br>
				<br>
				<!-- </div> -->

				<script src='https://www.google.com/recaptcha/api.js'></script>
				<?php
				require_once 'essentials/footer.php';
				require_once 'essentials/copyright.php';
				require_once 'essentials/js.php';
				?>

				<!-- <script defer> 
					function ShowHideDiv() {
						var other = document.getElementById("other");
						var dvtext = document.getElementById("dvtext");
						dvtext.style.display = other.checked ? "block" : "none";
					}
				</script> -->
				<!-- <script type="text/javascript">
					$(function() {
						$('[name="entry.1574222932"]').on('click', function (e) {
							var val = $(this).val();
							if (val == "student") {
								$('.txbx1').show('fade');
								$('.txbx2').show('fade');
								$('.txbx3').hide();
							}else if (val == 'Graduate') {
								$('.txbx1').show('fade');
								$('.txbx2').hide();
								$('.txbx3').hide();
							}else {
								$('.txbx1').show('fade');
								$('.txbx2').hide();
								$('.txbx3').show('fade');
							};
						});
					});
				</script> -->
				<script type="text/javascript">
					$(function() {
						$('[name="entry.1574222932"]').on('click', function (e) {
							var val = $(this).val();
							if (val == "student") {
								$('.txbx1').show('fade');
								$('.txbx2').show('fade');
							}else {
								$('.txbx1').show('fade');
								$('.txbx2').hide();
							};
						});
					});
				</script>
				<!-- <script type="text/javascript">
					$(function() {
						$('[name="entry.1290358863"]').on('click', function (e) {
							var val = $(this).val();
							if (val == "Arduino workshop") {
								$('.txbx4').show('fade');
							}else {
								$('.txbx4').hide();
							};
						});
					});
				</script> -->
				<script>
					window.onload = function() {
						var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
						recaptcha.required = true;
						recaptcha.oninvalid = function(e) {
	            // do something
	            alert("Please complete the captcha");
	        }
	    }
	</script>
</body>
</html>