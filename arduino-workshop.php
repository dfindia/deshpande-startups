	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Register now for Arduino Workshop</title>
		<?php
		require_once 'essentials/meta.php';
		?>
		<meta name="linkage" content="https://www.deshpandestartups.org/arduino-workshop"/>
		<meta property="og:site_name" content="Deshpande Startups"/>
		<meta property="og:type" content="website">
		<meta property="og:url" content="https://www.deshpandestartups.org/arduino-workshop">
		<meta property="og:image" content="https://www.deshpandestartups.org/img/events/arduino.png">
		<!-- <meta property="og:description" content="An initiative of Makers Lab of Deshpande Startups, hosting an ALEXATHON a workshop on Alexa for 8 hours and 16 Hours Hackathon the competition."/> -->
		<meta name="author" content="Deshpande Startups"/>
		<!-- <meta name="description" content="An initiative of Makers Lab of Deshpande Startups, hosting an ALEXATHON a workshop on Alexa for 8 hours and 16 Hours Hackathon the competition."/> -->
		<!-- <meta name="keywords" content=""/> -->
		<meta property="og:title" content="Register now for Arduino Workshop">
		<!-- <link rel="canonical" href="https://www.deshpandestartups.org/arduino-workshop"> -->
		<?php
			// $title = 'Deshpande Startups';
		require_once 'essentials/bundle.php';
		?>
		<style type="text/css">
		.tdwidth
		{width: 200px;}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<br>
	<br>
	<br>
	<br>
	<div class="container text-center">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">Arduino workshop</span> registration has been closed</h2>
		<div class="divider b-y text-yellow content-middle"></div>
		<!-- <h4 class="text-yellow">Thank You for Registration.</h4> -->
		<h6>Note: For more information <a href="contact-us">Contact us</a>.</h6>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>

	<script src='https://www.google.com/recaptcha/api.js'></script>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
	
	<script type="text/javascript">
		$(function() {
			$('[name="entry.1603671595"]').on('click', function (e) {
				var val = $(this).val();
				if (val == 1) {
						// $('.txbx').show('fade');
						$('.txbx1').hide();
						$('.txbx2').hide();
						$('.txbx3').hide();
					}
					else if (val == 2) {
						// $('.txbx').hide();
						$('.txbx1').show('fade');
						$('.txbx2').hide();
						$('.txbx3').hide();
					} else if (val == 3) {
						// $('.txbx').hide();
						$('.txbx1').hide();
						$('.txbx2').show('fade');
						$('.txbx3').hide();
					} else {
						// $('.txbx').hide();
						$('.txbx1').hide();
						$('.txbx2').hide();
						$('.txbx3').show('fade');
					};
				});
		});
	</script>
	<script>
		window.onload = function() {
			var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
			recaptcha.required = true;
			recaptcha.oninvalid = function(e) {
	 // do something
	 alert("Please complete the captcha");
	}
}
</script>
</body>
</html>