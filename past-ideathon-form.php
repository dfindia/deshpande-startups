<!DOCTYPE html>
<html lang="en">
<head>
	<title>Apply now for IDEATHON</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/ideathon-form"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/ideathon-form">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/ideathon-bg.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/ideathon.png">
	<meta property="og:description" content="Do you have an innovative idea that can translate into a cutting-edge product or service? Participate in the IDEATHON, a Platform to Present Your Idea to Solve Real World Problems and get selected to YUVA ENTREPRENEURSHIP PROGRAM to Kick Start Your Startup Journey with the startup ecosystem and Win 1 Lakh Worth Rewards."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Do you have an innovative idea that can translate into a cutting-edge product or service? Participate in the IDEATHON, a Platform to Present Your Idea to Solve Real World Problems and get selected to YUVA ENTREPRENEURSHIP PROGRAM to Kick Start Your Startup Journey with the startup ecosystem and Win 1 Lakh Worth Rewards."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Apply now for Ideathon">
	<link rel="canonical" href="https://www.deshpandestartups.org/ideathon-form">
	<?php
			// $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	.tdwidth
	{width: 200px;}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<br>
	<div class="container text-center">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">APPLY FOR</span> IDEATHON</h2>
		<div class="divider b-y text-yellow content-middle"></div>
		<!-- <h2 class="text-yellow Pt-5">The registration has been closed</h2> -->
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-lg-2">
				<!-- <h5 class="text-center">Request base registration contact us: M: +91-951-331-5791  E: makerslab@dfmail.org</h5><br> -->
				<iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
				</iframe>
				<form role="form" action="https://docs.google.com/forms/d/e/1FAIpQLSfOt9vzyh78zQRR91EFiKHSa_9-I6cWzo0unsClBPeHOTFUUg/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">

					<div class="row w3-card p-3">
						<div class="col-md-12 pad">
							<div class="row">
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
									<label for="input1"><b>Full Name<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.1744202240" class="box2 form-control" maxlength="50" pattern="[A-Za-z\s]{1,50}" placeholder="Mention your name" title="Mention your name" required="required">
								</div>
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
									<label for="input2"><b>Mobile Number<span class="text-yellow">*</span></b></label>
									<input type="phone" name="entry.1846441455" class="box2 form-control" pattern="\d*" min="12" placeholder="Mention your mobile number" maxlength="10" minlength="10" title="Your mobile number" required="required">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
									<label for="input3"><b>Email-Id<span class="text-yellow">*</span></b></label>
									<input type="email" name="entry.1354550476" placeholder="johndoe@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
								</div>
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input4"><b>City/Place<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.1739903176" class="box2 form-control" placeholder="Your location" required="required">
								</div>
							</div>
						</div>
						<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input5"><b>College/Institute/Organization/Company<span class="text-yellow">*</span></b></label>
							<input type="text" name="entry.1174543694" class="box2 form-control" placeholder="Name of College/Institute/Organization/Company" required="required">
						</div>

						<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input6"><b>Are You?<span class="text-yellow">*</span></b></label><br>
						</div>
						<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<div class="row">
								<div class="form-group col-md-4">
									<label for="student"><input type="radio" name="entry.106366330" value="student" required="required"> Student</label>
								</div>
								<div class="form-group col-md-4">
									<label for="Graduate"><input type="radio" name="entry.106366330" value="Graduate"> Graduate</label>
								</div>
								<div class="form-group col-md-4">
									<label for="Professionals"><input type="radio" name="entry.106366330" value="professional"> Professional</label>
								</div>
							</div>

							<div class="row txbx1" style="display: none">
								<div class="form-group col-md-12 m-0">
									<div class="row">
										<div class="form-group col-md-6">
											<label for="input7"><b>Academic Qualification<span class="text-yellow">*</span></b></label>
											<input type="text" name="entry.351159227" class="box2 form-control" title="Mention your Academic Qualification" placeholder="Mention your Degree">
										</div>
										<div class="form-group col-md-6">
											<label for="input8"><b>Year<span class="text-yellow">*</span></b></label>
											<input type="text" name="entry.147606824" class="box2 form-control" title="Mention your course year" placeholder="Your course year">
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-12">
											<label for="input9"><b>Stream/ Branch<span class="text-yellow">*</span></b></label>
											<input type="text" name="entry.2019618986" class="box2 form-control" title="Mention your Stream/ branch" placeholder="Mention your Stream/ branch">
										</div>
									</div>
								</div>
							</div>

							<div class="row txbx2" style="display: none">
								<div class="form-group col-md-12">
									<div class="row">
										<div class="form-group col-md-6">
											<label for="input10"><b>Academic Qualification<span class="text-yellow">*</span></b></label>
											<input type="text" name="entry.351159227" class="box2 form-control" placeholder="Mention your degree" title="Mention your degree">
										</div>
										<div class="form-group col-md-6">
											<label for="input11"><b>Stream/ Branch<span class="text-yellow">*</span></b></label>
											<input type="text" name="entry.2019618986" class="box2 form-control" title="Mention your Stream/ branch" placeholder="Mention your Stream/ branch">
										</div>
									</div>
								</div>
							</div>

							<div class="row txbx3" style="display: none">
								<div class="form-group col-md-12">
									<div class="row">
										<div class="form-group col-md-6">
											<label for="input12"><b>Academic Qualification<span class="text-yellow">*</span></b></label>
											<input type="text" name="entry.351159227" class="box2 form-control" placeholder="Mention your degree" title="Academic qualification">
										</div>
										<div class="form-group col-md-6">
											<label for="input13"><b>Stream/ Branch<span class="text-yellow">*</span></b></label>
											<input type="text" name="entry.2019618986" class="box2 form-control" title="Mention your Stream/ branch" placeholder="Mention your Stream/ branch">
										</div>
									</div>
									<label for="input14"><b>Professional In?<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.1828571844" class="box2 form-control" title="You are professional in?" placeholder="You are professional in?">
								</div>
							</div>
						</div>

						<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input15"><b>Team Size<span class="text-yellow">*</span></b></label>
						</div>
						<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<div class="row" >
								<div class="col-md-4 form-group">
									<label for="one"><input type="radio" name="entry.1649461349" id="one" value="1" checked="checked"> 1</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="two"><input type="radio" name="entry.1649461349" id="two" value="2"> 2</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="three"><input type="radio" name="entry.1649461349" id="three" value="3"> 3</label>
								</div>
							</div>
							<div class="row txbx4" style="display: none">
								<div class="form-group col-md-12">
									<input type="text" class=" box2 form-control" name="entry.8390891" title="mention name" pattern="[A-Za-z\s]{1,50}" placeholder="Name of participant 2" />
								</div>
								<div class="form-group col-md-12">
									<input type="text" class="box2 form-control" pattern="\d*" maxlength="10" minlength="10" name="entry.1520740115" placeholder="Mobile number of participant 2"/>
								</div>
								<div class="form-group col-md-12">	
									<input type="email" class="box2 form-control" name="entry.362422578" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email-Id of participant 2"/>
								</div>
							</div>
							<div class="row txbx5" style="display: none">
								<div class="form-group col-md-12">
									<input type="text" class="box2 form-control" name="entry.771221565" title="mention name" pattern="[A-Za-z\s]{1,50}" placeholder="Name of participant 3" />
								</div>
								<div class="form-group col-md-12">
									<input type="text" class="box2 form-control" name="entry.118645765" pattern="\d*" maxlength="10" minlength="10" placeholder="Mobile number of participant 3"/>
								</div>
								<div class="form-group col-md-12">	
									<input type="email" class="box2 form-control" name="entry.119403986" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email-Id of participant 3"/>
								</div>
							</div>
						</div>

						<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input16"><b>Problem Addressing Sector<span class="text-yellow">*</span></b></label>
						</div>
						<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<div class="row">
								<div class="col-md-4 form-group">
									<label for="smartcity"><input type="radio" name="entry.718144415" value="Smart City" required="required"> Smart City</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="agriculture"><input type="radio" name="entry.718144415" value="Agriculture"> Agriculture</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="renewable"><input type="radio" name="entry.718144415" value="renewable energy"> Renewable Energy</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 form-group">
									<label for="healthcare"><input type="radio" name="entry.718144415" value="Healthcare"> Healthcare</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="edutech"><input type="radio" name="entry.718144415" value="Edutech"> Edutech</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="electronics"><input type="radio" name="entry.718144415" value="Electronis & IoT"> Electronics & IoT</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 form-group">
									<label for="WaterManagement"><input type="radio" name="entry.718144415" value="Water Management"> Water Management</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="waste management"><input type="radio" name="entry.718144415" value="waste management"> Waste Management</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="Safety & Security"><input type="radio" name="entry.718144415" value="Safety & Security"> Safety & Security</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 form-group">
									<label for="Software development"><input type="radio" name="entry.718144415" value="Software development"> Software Development</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="Automation"><input type="radio" name="entry.718144415" value="Automation"> Automation</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="Transportation"><input type="radio" name="entry.718144415" value="Transportation"> Transportation</label>
								</div>
							</div>
						</div>

						<div class="form-group col-lg-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input17"><b>Brief about your idea<span class="text-yellow">*</span></b></label>
							<textarea name="entry.2130276016" class="box2 form-control" rows="5" minlength="5" maxlength="1000" title="Brief about your idea" placeholder="Brief about your idea" required="required"></textarea>
						</div>

						<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input18"><b>How did you come to know about this event?<span class="text-yellow">*</span></b></label><br>
							<div class="row">
								<div class="col-md-6">
									<label for="Personal"><input type="radio" name="entry.346859205" value="Personal Reference" required="required"> Personal Reference</label>
								</div>
								<div class="col-md-6">
									<label for="Newsletters"><input type="radio" name="entry.346859205" value="Email News Letter"> Email News Letter</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label for="CollegeNotice"><input type="radio" name="entry.346859205" value="College Notice"> College Notice</label>
								</div>
								<div class="col-md-6">
									<label for="whatsapp"><input type="radio" name="entry.346859205" value="Whatsapp"> Whatsapp</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label for="PrintMedia"><input type="radio" name="entry.346859205" value="Print Media"> Print Media</label>
								</div>
								<div class="col-md-6">
									<label for="SocialMedia"><input type="radio" name="entry.346859205" value="Other Social Media"> Other Social Media</label>
								</div>
							</div>
						</div>
						<div class="form-group col-lg-12">
									<!-- <span class="text-yellow"><b>*</b></span>
										<div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div><br> -->
										<!-- <div class="form-group">
											<label for="agreement"><input type="checkbox" name="entry.782003103" value="I agree to make payment for the IDEATHON Participation" required="required"> I agree to make payment of Rs.500/- for the IDEATHON Participation.<span class="text-yellow"><b>*</b></span></label>
										</div> -->
										<span class="text-yellow"><h6><b>*</b> Fields are mandatory</h6></span>
										<input type="submit" class="btn custom-btn2 btn-warning" id="ss-submit" name="submit" value="Submit">
									</div>
								</div>

							</form>
						</div>		
					</div>
				</div>
				<br>
				<br>

				<script src='https://www.google.com/recaptcha/api.js'></script>
				<?php
				require_once 'essentials/footer.php';
				require_once 'essentials/copyright.php';
				require_once 'essentials/js.php';
				?>

				<!-- <script defer> 
					function ShowHideDiv() {
						var other = document.getElementById("other");
						var dvtext = document.getElementById("dvtext");
						dvtext.style.display = other.checked ? "block" : "none";
					}
				</script> -->
				<script type="text/javascript">
					$(function() {
						$('[name="entry.106366330"]').on('click', function (e) {
							var val = $(this).val();
							if (val == "student") {
								$('.txbx1').show('fade');
								$('.txbx2').hide();
								$('.txbx3').hide();
							}
							else if (val == "Graduate") {
								$('.txbx1').hide();
								$('.txbx2').show('fade');
								$('.txbx3').hide();
							}else {
								$('.txbx1').hide();
								$('.txbx2').hide();
								$('.txbx3').show('fade');
							};
						});
					});
				</script>
				<script type="text/javascript">
					$(function() {
						$('[name="entry.1649461349"]').on('click', function (e) {
							var val = $(this).val();
							if (val == 1) {
								$('.txbx4').hide();
								$('.txbx5').hide();
							}
							else if (val == 2) {
								$('.txbx4').show('fade');
								$('.txbx5').hide();
							} else {
								$('.txbx4').show('fade');
								$('.txbx5').show('fade');
							};
						});
					});
				</script>
				<!-- <script>
					window.onload = function() {
						var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
						recaptcha.required = true;
						recaptcha.oninvalid = function(e) {
							alert("Please complete the captcha");
						}
					}
				</script> -->
			</body>
			</html>