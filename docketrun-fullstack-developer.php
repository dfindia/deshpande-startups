<!DOCTYPE html>
<html lang="en">
<head>
	<title>Full Stack Developer & Machine Learning - DocketRun Tech Private Limited</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/docketrun-fullstack-developer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/docketrun-fullstack-developer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/docketrun-big.png">
	<meta property="og:description" content="We are looking for Senior Full Stack Developer & Machine Learning. Job Position: Senior Full Stack Developer & Machine Learning, Experience:  3+ years, Qualification: BE graduate preferably CS, ISE & EC, EE & MCA graduates."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Full Stack Developer & Machine Learning. Job Position: Full Stack Developer & Machine Learning, Experience:  3+ years, Qualification: BE graduate preferably CS, ISE & EC, EE & MCA graduates."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Full Stack Developer & Machine Learning, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/docketrun-fullstack-developer">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Full Stack Developer &</span> Machine Learning</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Full Stack Developer & Machine Learning<br>
					<strong>Startup:</strong> DocketRun Tech Private Limited<br>
					<strong>Qualification:</strong> BE graduate preferably CS, ISE & EC, EE & MCA graduates<br>
					<strong>Experience:</strong> 3+ years of experience required<br>
					<strong>Job Location:</strong> Hubballi - Dharwad<br>
					<strong>Skills and Expertise:</strong> We are looking for a motivated, intelligent, hardworking and creative full-stack engineer to join our Docketrun Team<br>
					<strong>Good to have:</strong> Ionic, Bootstrap, Android,ML Algorithms<br>
				</p>
			</div>
			<div class="col-md-6">
				<a href="https://www.docketrun.com/" target="_blank" rel="nofollow"><img src="img/career/docketrun-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, DocketRun Tech Private Limited"></a>
			</div>
		</div>
		<!-- <br> -->

		<div class="row pt-2">
			<div class="col-md-7">
				<h3 class="text-yellow">Key Responsibilities:</h3>
				<ul>
					<li>Excellent and Working knowledge in Python, AngularJS, MongoDB,Web Development</li>
					<li>Should have knowledge of cloud based services like Azure and AWS,Tensorflow,OpenCV,Keras, Caffe, API's development</li>
					<li>Excellent problem solving skills</li>
					<li>Ability to grasp new challenges and business requirements and come up with optimum solutions</li>
					<li>Develop and evaluate data analytics models, algorithms and solutions</li>
					<li>Analyze the information, identify patterns and trends</li>
					<li>Understand/implement ML algorithms, performance tuning and reporting</li>
				</ul>
			</div>
			<div class="col-md-5">
				<h3 class="text-yellow">Candidate should be able to:</h3>
				<ul>
					<li>Contribute in all phases of the development lifecycle</li>
					<li>Write well designed, testable, efficient code</li>
					<li>Design and implementation of low-latency, high-availability, and performant applications</li>
					<li>Work well under pressure and meet deadlines without sacrificing quality</li>
				</ul>
			</div>
		</div>

		</div>
		<br>

		<div class="container cal">
			<p class="text-center"><b>Interested candidates email Resumes to <br><a href="mailto: career&#064;docketrun&#046;com"> career&#064;docketrun&#046;com</a> or <a href="mailto: ajay&#064;docketrun&#046;com"> ajay&#064;docketrun&#046;com</a><br> with subject as job title you are applying. </b></p>
		</div>
		<br>
		<?php
		require_once 'essentials/footer.php';
		require_once 'essentials/copyright.php';
		require_once 'essentials/js.php';
		?>
	</body>
	</html>