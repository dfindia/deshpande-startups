<!DOCTYPE html>
<html lang="en">
<head>
	<title>Senior Software Developer - LinkEZ Technologies</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/linkez-software-developer2"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/linkez-software-developer2">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/linkez-big.png">
	<meta property="og:description" content="We are looking for Senior Senior Software Developer. Job Position: Senior Senior Software Developer, Experience:  5-10 years."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Senior Software Developer. Job Position: Senior Software Developer, Experience:  5-10 years."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Senior Software Developer, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/linkez-software-developer2">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Senior Software</span> Developer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Senior Software Developer<br>
					<strong>Startup:</strong> LinkEZ Technologies<br>
					<!-- <strong>Qualification:</strong> B.E graduates<br> -->
					<strong>Experience:</strong> 5-10 years of experience required <br>
					<strong>Job Location:</strong> Hubballi<br>
				</p>
			</div>
			<div class="col-md-6">
				<a href="https://www.linkeztech.com/" target="_blank" rel="nofollow"><img src="img/career/linkez-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, LinkEZ Technologies"></a>
			</div>
		</div>
		<!-- <br> -->

		<p class="text-justify pt-1">LinkEZ began its journey 6 years ago with a vision to make the manufacturing process transparent at all levels so that the unit members know what's happening and have a better ability to manage and achieve desired output and efficiency.</p>
		<p class="text-justify pt-1">After several years of research, working with large scale manufacturers, we at LinkEZ developed and successfully implemented our solutions that improved the Operational Efficiency and Operational Equipment Effectiveness by 10 times... all this during the first year of operations.</p>
		<p class="text-justify pt-1">A combination of cutting edge hardware and software solutions, combined with powerful analytics makes us a one-stop partner to realize the full manufacturing potential of organizations.</p>

		<div class="row pt-2">
			<div class="col-md-12">
				<h3 class="text-yellow">Skills and Expertise:</h3>
				<ul>
					<li>A technical bachelor’s degree with 5+ years web development, javascript, react.js, HTML5, CSS3, SASS and LESS experience</li>
					<li>2+ years experience with modern web frameworks, React.js preferred</li>
					<li>Develop web-based, front-end applications using React.js</li>
					<li>2+ years JavaScript applications and shared components development</li>
					<li>Deep understanding of API-powered JavaScript applications</li>
					<li>Extensive experience with web browsers as well as mobile-responsive apps</li>
					<li>Strong JavaScript skills and an understanding of functional programming, closures, ES2015/2016, asynchronous programming and handling large amounts of data</li>
					<li>Excellent planning/execution skills including estimating and scheduling</li>
					<li>Excellent written and verbal communication skills</li>
					<li>Strong team player</li>
					<li>A constant desire to grow, learn and explore new things</li>
				</ul>
			</div>
		</div>

	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to<br>E:<a href="mailto:shirish&#064;linkeztech&#046;com"> shirish&#064;linkeztech&#046;com</a></b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>