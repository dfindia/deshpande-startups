<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Yuva Entrepreneur Program</title>
      <?php
         require_once 'essentials/meta.php';
         ?>
      <meta name="linkage" content="https://www.deshpandestartups.org/yuva-entrepreneurship"/>
      <meta property="og:site_name" content="Deshpande Startups"/>
      <meta property="og:type" content="website">
      <meta property="og:url" content="https://www.deshpandestartups.org/yuva-entrepreneurship">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/yuvaentrepreneurship-bg.png">
      <meta property="og:description" content="Yuva Entrepreneurship is a unique program, where student communities are trained to build their Entrepreneurial mind set by creating their ideas into product. "/>
      <meta name="author" content="Deshpande Startups"/>
      <meta name="description" content="Yuva Entrepreneurship is a unique program, where student communities are trained to build their Entrepreneurial mind set by creating their ideas into product. "/>
      <!-- <meta name="keywords" content=""/> -->
      <meta property="og:title" content="Yuva Entrepreneur Program">
      <link rel="canonical" href="https://www.deshpandestartups.org/yuva-entrepreneurship">
      <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
      <!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script> -->
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <?php
         // $title = 'Deshpande Startups';
         require_once 'essentials/bundle.php';
         ?>
      <style type="text/css">
         .parallax {
         background-image: url("img/makers/deshpande2.jpg");
         min-height: 500px; 
         background-attachment: fixed;
         background-position: center;
         background-repeat: no-repeat;
         background-size: cover;
         }
         .parallax2 {
         background-image: url("img/makers/deshpande2.jpg");
         min-height: 300px; 
         background-attachment: fixed;
         background-position: center;
         background-repeat: no-repeat;
         background-size: cover;
         }
         .pt-6 {
         padding-top: 4rem!important;
         }
         .featured-bg-container {
         color: #eee;
         background: linear-gradient(45deg, rgba(35, 36, 37, 0.59) 25%, rgba(40, 50, 74, 0.3) 25%, rgba(60, 61, 62, 0.35) 90%, rgba(45, 52, 66, 0.7) 90%), linear-gradient(-45deg, rgba(236, 240, 247, 0.28) 30%, rgba(29, 29, 39, 0.41) 30%, rgba(58, 58, 88, 0.26) 60%, rgba(33, 40, 84, 0.03) 60%, rgba(57, 57, 58, 0.64) 60%);
         background-color: #48494a;
         }
         .counter {
         /*background-color:#f5f5f5;*/
         padding: 20px 0;
         border-radius: 5px;
         }
         .count-title {
         font-size: 40px;
         font-weight: bold;
         margin-top: 10px;
         margin-bottom: 0;
         text-align: center;
         color: #ffffff;
         }
         .count-text {
         font-size: 30px;
         /*font-weight: bold;*/
         margin-top: 10px;
         margin-bottom: 0;
         text-align: center;
         color: #ffffff;
         /*color: #eb7c26;*/
         }
         .fa-clr {
         margin: 0 auto;
         float: none;
         display: table;
         color: #eb7c26;
         }
      </style>
   </head>
   <body>
      <?php
         require_once 'essentials/title_bar.php';
         require_once 'essentials/menus.php';
         ?>
      <!-- <img data-u="image" src="img/makers/yuvaentrepreneurship-bg.png" width="1349" height="512" class="img img-fluid carousel-inner" alt="India's largest incubation center Deshpande startups building, Makers Lab"> -->
      <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
         <div class="carousel-inner">
            <div class="carousel-item active img-hover-zoom-img">
               <img class="d-block w-100 img img-fluid" src="img/makers/yuvaentrepreneurship-bg.png" width="1349" height="512" alt="Yuva Entrepreneurship Program, Deshpande Startups">
            </div>
            <div class="carousel-item img-hover-zoom-img">
               <img class="d-block w-100 img img-fluid" src="img/makers/yuva-benefits.png" width="1349" height="512" alt="Benefits of Yuva Entrepreneurship Program, Deshpande Startups">
            </div>
            <!-- <div class="carousel-item img-hover-zoom-img">
               <img class="d-block w-100 img img-fluid" src="img/makers/yuva-impact-image.png" width="1349" height="512" alt="Impact of Yuva Entrepreneurship Program, Deshpande Startups">
               </div> -->
         </div>
         <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
         </a>
      </div>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb justify-content-end">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Yuva Entrepreneurship</li>
         </ol>
      </nav>
      <div class="container">
         <!-- <br> -->
         <!-- <div class="center wow fadeInDown">
            <h2 class="text-yellow text-center">YUVA ENTREPRENEURSHIP<br> <span class="text-muted">PROGRAM</span> </h2>
            <div class="divider b-y text-yellow content-middle"></div>
            </div> -->
         <div class="col-md-12">
            <h3 class="text-center pt-2 font-weight-bold text-yellow">"Create and Nurture product building mind set among the youth in the region.<br> 
               A platform wherein one can envision to build their entrepreneurial journey"
            </h3>
         </div>
         <br>
         <div class="row justify-content-md-center">
            <div class="col-md-9">
               <p class="text-justify wow slideInLeft"><b>Yuva Entrepreneurship</b> program provides a platform to nurture promising ideas brought up by young minds to build their respective ideas into prototypes/products. A unique program where student communities are trained to build their entrepreneurial mindset along with a dedicated team of mentors and resources available in the ecosystem.</p>
            </div>
            <div class="col-md-2 text-center">
               <div class="row pl-4">
                  <a href="survey-form" class="btn btn-rotate" target="_blank">Apply Now</a>
               </div>
            </div>
         </div>
      </div>
      <div class="text-center">
         <h2 class="text-yellow text-center">IMPACT OF <span class="text-muted"><br>YUVA ENTREPRENEURSHIP PROGRAM</span></h2>
         <div class="divider b-y text-yellow content-middle"></div>
      </div>
      <br>
      <div class="parallax2 inverse-text" data-parallax-img="img/makers/deshpande2.jpg" data-parallax-img-width="1920" data-parallax-img-height="800">
         <br>
         <br>
         <div class="container">
            <div class="row text-center">
               <div class="col-lg-4 col-md-4">
                  <div class="counter">
                     <!-- <i class="fa fa-code fa-3x fa-clr"></i> -->
                     <img src="img/makers/exposure-impact.png" class="img img-fluid" width="60" height="55" alt="yuva entrepreneurship program, exposure to students" class="img img-fluid">
                     <h2 class="timer count-title count-number" data-to="6900" data-speed="10000"></h2>
                     <p class="count-text">Exposure to Students</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4">
                  <div class="counter">
                     <img src="img/makers/engage-impact.png" class="img img-fluid" width="60" height="55" alt="yuva entrepreneurship program, Students Engaged" class="img img-fluid">
                     <!-- <i class="fa fa-users fa-3x fa-clr"></i> -->
                     <h2 class="timer count-title count-number" data-to="2600" data-speed="10000"></h2>
                     <p class="count-text">Students Engaged</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4">
                  <div class="counter">
                     <img src="img/makers/elevate-impact.png" class="img img-fluid" width="60" height="55" alt="yuva entrepreneurship program, Elevated Young Aspirants" class="img img-fluid">
                     <!-- <i class="fa fa-rocket fa-3x fa-clr" aria-hidden="true"></i> -->
                     <h2 class="timer count-title count-number" data-to="500" data-speed="10000"></h2>
                     <p class="count-text">Young Aspirants Elevated</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <br>
      <div class="container">
         <h4 class="text-yellow"><b>We envision to democratize entrepreneurship by following three levels:</b></h4>
         <div class="row text-center">
            <div class="col-md-4">
               <img src="img/makers/expose.png" class="img img-fluid wow zoomIn" width="200" height="200" alt="yuva entrepreneurship program, expose" class="img img-fluid">
               <p class="text-center"><b>We give exposure to the Deshpande Startups ecosystem, introduction to entrepreneurship and insights from successful entrepreneurs.</b></p>
            </div>
            <div class="col-md-4">
               <img src="img/makers/engage.png" class="img img-fluid wow zoomIn" width="200" height="200" alt="yuva entrepreneurship program, engage" class="img img-fluid">
               <p class="text-center"><b>We engage students from different institutes and create a platform to Ideate, Design and Build your product. Aspirants solve real-world problems by developing social impact solutions.</b></p>
            </div>
            <div class="col-md-4">
               <img src="img/makers/elevate.png" class="img img-fluid wow zoomIn" width="200" height="200" alt="yuva entrepreneurship program, elevate" class="img img-fluid">
               <p class="text-center"><b>We support passionate individuals to elevate their ideas towards their Entrepreneurial journey.</b></p>
            </div>
         </div>
      </div>
      <div class="parallax inverse-text" data-parallax-img="img/makers/deshpande2.jpg" data-parallax-img-width="1920" data-parallax-img-height="1078">
         <br>
         <!-- facilities -->
         <div id="ml-activities">
            <div class="container">
               <div class="row text-center">
                  <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                     <div class="row justify-content-md-center">
                        <div class="col-md-3 col-sm-6 col-lg-3 col-xs-12">
                           <img src="img/makers/exposure-visit.png" width="255" height="195" class="blog-img img-fluid img pb-1 wow zoomIn" alt="Exposure visit, Makers Lab, Activities" width="255" height="195">
                           <div class="p-2">
                              <a href="exposure-visit" class="btn p-1 btn-warning" target="_blank">Know More</a>
                              <a href="fssi-visit-form" class="btn p-1 btn-warning" target="_blank">Apply Now</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <br>
         <div id="ml-events">
            <div class="container">
               <div class="row featurette text-center">
                  <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 wow fadeInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
                     <h2 class=" text-white">ENGAGEMENTS</h2>
                     <div class="divider b-y text-yellow content-middle"></div>
                  </div>
               </div>
               <div class="row text-center">
                  <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                     <!-- <div class="row justify-content-md-center">
                        <div class="col-md-3 col-sm-6 col-lg-3 col-xs-12">
                        	<img src="img/makers/internship.png" width="255" height="195" class="blog-img img-fluid img pb-1 wow zoomIn" alt="Internship,Makers Lab, Activities" width="255" height="195">
                        	<div class="p-2">
                        		<a href="internship" class="btn p-1 btn-warning" target="_blank">Know More</a>
                        		<a href="internship-form" class="btn p-1 btn-warning" target="_blank">Apply Now</a>
                        	</div>
                        </div>
                        </div>
                        <br> -->
                     <div class="row justify-content-md-center">
                        <div class="col-md-3 col-sm-6 col-lg-3 col-xs-12">
                           <img src="img/makers/hackathon.png" width="255" height="195" class="blog-img img-fluid img pb-1 img-thumbnail" alt="Hackathon, Makers Lab, Events" width="255" height="195">
                           <div class="p-2">
                              <a href="hackathon" class="btn p-1 btn-warning" target="_blank">Know More</a>
                              <!-- <a href="hackathon" class="btn p-1 btn-warning" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-lg-3 col-xs-12">
                           <img src="img/makers/developthon.png" width="255" height="195" class="blog-img img-fluid img pb-1 img-thumbnail" alt="Developthon, Makers Lab, Events" width="255" height="195">
                           <div class="p-2">
                              <a href="developthon" class="btn p-1 btn-warning" target="_blank">Know More</a>
                              <!-- <a href="#" class="btn p-1 btn-warning" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-lg-3 col-xs-12">
                           <img src="img/makers/makeathon.png" width="255" height="195" class="blog-img img-fluid img pb-1 img-thumbnail" alt="Makeathon, Makers Lab, Events" width="255" height="195">
                           <div class="p-2">
                              <a href="makeathon" class="btn p-1 btn-warning" target="_blank">Know More</a>
                              <!-- <a href="#" class="btn p-1 btn-warning" target="_blank">Apply Now</a> -->
                           </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-lg-3 col-xs-12">
                           <img src="img/makers/workshops.png" width="255" height="195" class="blog-img img-fluid img pb-1 img-thumbnail" alt="3d printing, Makers Lab, Workshops" width="255" height="195">
                           <div class="p-2">
                              <a href="workshop" class="btn p-1 btn-warning" target="_blank">Know More</a>
                              <a href="workshop-form" class="btn p-1 btn-warning" target="_blank">Apply Now</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <br>
         <div id="ml-workshop">
            <div class="container">
               <div class="row text-center">
                  <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                     <div class="row justify-content-md-center">
                        <!-- <div class="col-md-3 col-sm-6 col-lg-3 col-xs-12">
                           <img src="img/makers/workshops.png" width="255" height="195" class="blog-img img-fluid img pb-1 img-thumbnail" alt="3d printing, Makers Lab, Workshops" width="255" height="195">
                           <div class="p-2">
                           	<a href="workshop" class="btn p-1 btn-warning" target="_blank">Know More</a>
                           	<a href="workshop" class="btn p-1 btn-warning" target="_blank">Apply Now</a>
                           </div>
                           </div> -->
                        <div class="col-md-3 col-sm-6 col-lg-3 col-xs-12">
                           <img src="img/makers/internship.png" width="255" height="195" class="blog-img img-fluid img pb-1" alt="Internship,Makers Lab, Activities" width="255" height="195">
                           <div class="p-2">
                              <a href="internship" class="btn p-1 btn-warning" target="_blank">Know More</a>
                              <a href="internship-form" class="btn p-1 btn-warning" target="_blank">Apply Now</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <br>
         <div class="container">
            <div class="row featurette text-center">
               <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 wow fadeInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
                  <h2 class=" text-white">ELEVATE</h2>
                  <div class="divider b-y text-yellow content-middle"></div>
               </div>
            </div>
            <div class="row text-center">
               <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                  <div class="row justify-content-md-center">
                     <div class="col-md-3 col-sm-6 col-lg-3 col-xs-12">
                        <img src="img/makers/ideathon.png" width="255" height="195" class="blog-img img-fluid img pb-1 img-thumbnail" alt="Ideathon, Makers Lab, Events" width="255" height="195">
                        <div class="p-2">
                           <a href="ideathon" class="btn p-1 btn-warning" target="_blank">Know More</a>
                           <a href="ideathon-form" class="btn p-1 btn-warning" target="_blank">Apply Now</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <br>
         <br>
      </div>
      <!-- <img class="img img-fluid" src="img/makers/yuva-benefits.png" width="1349" height="512" alt="Benefits of Yuva Entrepreneurship Program, Deshpande Startups"> -->
      <div class="featured-bg-container">
         <div class="row">
            <p>India needs Entrepreneurs to solve real world problems and we believe these problems can be solved by the budding entrepreneurs.</p>
         </div>
         <div class="row valign-wrapper justify-content-md-center">
            <div class="col-md-6">
               <h4 class="text-yellow"><b>Why Yuva Entrepreneurship Program?</b></h4>
               <ul class="text-justify wow slideInLeft">
                  <li>Lack of awareness & exposure on startups & entrepreneurship</li>
                  <li>India needs entrepreneurs to solve real world problems</li>
                  <li>Dearth of platform to build their ideas to products</li>
                  <li>Effective engagement & mentorship from domain experts</li>
               </ul>
            </div>
            <div class="col-md-5">
               <h4 class="text-yellow"><b>Objectives:</b></h4>
               <ul class="text-justify wow slideInLeft">
                  <li>Nurture problem solving mind set</li>
                  <li>Inculcate product building mind set</li>
                  <li>Creating entrepreneurial mind set</li>
                  <li>Promote ideas, innovation & entrepreneurship</li>
               </ul>
            </div>
         </div>
      </div>
      <!-- <img data-u="image" src="img/makers/yuva-benefits.png" width="1349" height="512" class="img img-fluid carousel-inner" alt="India's largest incubation center Deshpande startups building, Makers Lab"> -->
      <!-- <div class="featured-bg-container">
         <div class="row valign-wrapper">
         	<div class="col-md-4">
         		<img src="img/makers/yuva-entrepreneurship-small.png" width="380" height="213" alt="Deshpande startups, yuva entrepreneurship program" class="img img-fluid img-thumbnail">
         	</div>
         	<div class="col-md-6">
         		<h4 class="text-yellow"><b>Objectives:</b></h4>
         		<ul class="text-justify wow slideInLeft">
         			<li>To spread awareness among students on startup and entrepreneurship</li>
         			<li>Creating budding entrepreneurs for our country</li>
         			<li>To promote and engage students with peer-peer learning</li>
         			<li>To inculcate product building mind set among students</li>
         			<li>Promote ideas, innovation and entrepreneurship</li>
         		</ul>
         	</div>
         	<div class="col-md-2">
         		<div class="row">
         			<a href="survey-form" class="btn btn-md btn-rotate" target="_blank">Apply Now</a>
         		</div>
         	</div>
         </div>
         </div> -->
      <script type="text/javascript">
         (function ($) {
         	$.fn.countTo = function (options) {
         		options = options || {};
         
         		return $(this).each(function () {
         // set options for current element
         var settings = $.extend({}, $.fn.countTo.defaults, {
         	from:            $(this).data('from'),
         	to:              $(this).data('to'),
         	speed:           $(this).data('speed'),
         	refreshInterval: $(this).data('refresh-interval'),
         	decimals:        $(this).data('decimals')
         }, options);
         
         // how many times to update the value, and how much to increment the value on each update
         var loops = Math.ceil(settings.speed / settings.refreshInterval),
         increment = (settings.to - settings.from) / loops;
         
         // references & variables that will change with each update
         var self = this,
         $self = $(this),
         loopCount = 0,
         value = settings.from,
         data = $self.data('countTo') || {};
         
         $self.data('countTo', data);
         
         // if an existing interval can be found, clear it first
         if (data.interval) {
         	clearInterval(data.interval);
         }
         data.interval = setInterval(updateTimer, settings.refreshInterval);
         
         // initialize the element with the starting value
         render(value);
         
         function updateTimer() {
         	value += increment;
         	loopCount++;
         	
         	render(value);
         	
         	if (typeof(settings.onUpdate) == 'function') {
         		settings.onUpdate.call(self, value);
         	}
         	
         	if (loopCount >= loops) {
         		// remove the interval
         		$self.removeData('countTo');
         		clearInterval(data.interval);
         		value = settings.to;
         		
         		if (typeof(settings.onComplete) == 'function') {
         			settings.onComplete.call(self, value);
         		}
         	}
         }
         
         function render(value) {
         	var formattedValue = settings.formatter.call(self, value, settings);
         	$self.html(formattedValue);
         }
         });
         	};
         
         	$.fn.countTo.defaults = {
         from: 0,               // the number the element should start at
         to: 0,                 // the number the element should end at
         speed: 1000,           // how long it should take to count between the target numbers
         refreshInterval: 100,  // how often the element should be updated
         decimals: 0,           // the number of decimal places to show
         formatter: formatter,  // handler for formatting the value before rendering
         onUpdate: null,        // callback method for every time the element is updated
         onComplete: null       // callback method for when the element finishes updating
         };
         
         function formatter(value, settings) {
         return value.toFixed(settings.decimals);
         }
         }(jQuery));
         
         jQuery(function ($) {
         // custom formatting example
         $('.count-number').data('countToOptions', {
         formatter: function (value, options) {
         	return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
         }
         });
         
         // start all the timers
         $('.timer').each(count);  
         
         function count(options) {
         var $this = $(this);
         options = $.extend({}, options || {}, $this.data('countToOptions') || {});
         $this.countTo(options);
         }
         });
      </script>
      <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once dirname(__FILE__).'/essentials/js.php';
         ?>
   </body>
</html>