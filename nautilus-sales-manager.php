<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sales Manager - Nautilus</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/nautilus-sales-manager"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/nautilus-sales-manager">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/nautilus-big.png">
	<meta property="og:description" content="We are looking for Sales Manager. Job Position: Sales Manager, Experience: Minimum 5 years’ experience in Healthcare Industry Selling devices/ software to medical fraternity, Qualification: Any Graduate, preferably Bachelor’s in Science/Pharmacy. An MBA will be an added advantage."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Sales Manager. Job Position: Sales Manager, Experience: Minimum 5 years’ experience in Healthcare Industry Selling devices/ software to medical fraternity, Qualification: Any Graduate, preferably Bachelor’s in Science/Pharmacy. An MBA will be an added advantage."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Sales Manager, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/nautilus-sales-manager">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Sales</span> Manager</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Sales Manager<br>
					<strong>Startup:</strong> Nautilus Hearing Solutions Pvt. Ltd.<br>
					<strong>Qualification:</strong> Any Graduate, preferably Bachelor’s in Science/Pharmacy. An MBA will be an added advantage<br>
					<strong>Experience:</strong> Minimum 5 years’ experience in Healthcare Industry Selling devices/ software to medical fraternity<br>
					<!-- <strong>Career Level:</strong> Senior<br> -->
					<strong>Work Shift:</strong> Standard Monday to Saturday ( 9am to 5Pm )<br>
					<strong>Travel:</strong> More frequently to achieve the sales budgets - Within India<br>
					<!-- <strong>Role Category:</strong> Programming & design<br> -->
					<strong>Job Location:</strong> Karnataka
				</p>
			</div>
			<div class="col-md-6">
				<a href="http://www.nautilushearing.com/" target="_blank" rel="nofollow"><img src="img/career/nautilus-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, Nautilus Hearing Solutions Pvt. Ltd."></a>
			</div>
		</div>
		<h3 class="text-yellow">Job Summary:</h3>
		<p class="text-justify pt-1">Responsible to achieve the Regional objectives which include sales - value wise & product wise.Should Lead the sales activity. Motivate and provide directions to team members to meet or exceed respective sales budget. To ensure that the Sales practice is in accordance with the compliance and values of the organisation. To understand the market pulse & drafting the appropriate local strategies. Periodically maintain & analyse the data.</p>

		<h3 class="text-yellow">Job Description:</h3>
		<div class="row pt-2">
			<div class="col-md-6">
				<ul class="text-justify">
					<li>Should have experience in Territory Sales with Doctors in all therapy areas</li>
					<li>Capability to build and handle team</li>
					<li>Manage team, track sales performance and achieve results as desired and targeted for the specified region</li>
					<li>Reaching the targets and goals set for your territory</li>
					<li>Establishing, maintaining and expanding your doctor base</li>
					<li>Servicing the needs of companies existing customers</li>
					<li>Increasing business opportunities through various routes to market</li>
					<li>Recruiting and training sales executives</li>
				</ul>
			</div>
			<div class="col-md-6">
				<ul class="text-justify">
					<li>Setting targets for individual sales executive and your team as a whole</li>
					<li>Allocating area to sales executives</li>
					<li>Developing sales strategies and setting targets</li>
					<li>Monitoring your team's performance and motivating them to reach targets</li>
					<li>Compiling and analysing sales figures</li>
					<li>Collecting doctor’s feedback and reporting to management about same</li>
					<li>Keeping up to date with products and competitors</li>
				</ul>
			</div>
		</div>

		<h3 class="text-yellow">Job Responsibilities:</h3>
		<div class="row pt-2">
			<div class="col-md-6">
				<ul class="text-justify">
					<li>Direct the activities of the Sales Team for the achievement of short and long term business objectives, increased profit and market share within the organisation</li>
					<li>Translating sales data/analysis into day to day working sales and marketing strategy for the Business Unit product portfolios and driving these through the sales force</li>
					<li>Maintain contact with identified Key Opinion Leaders within the region to increase Nautilus Hearing Solutions brand visibility</li>
					<li>Maintain segmentation of market to drive business objectives</li>
					<li>High level of training and industry knowledge to be maintained with all members of the team</li>
					<li>Develop and manage the expense budgets within plan by allocating monies appropriately and regularly monitoring performance</li>
					<li>Interface with other functions like Marketing, Regulatory and other Support functions to leverage their expertise and get their support in achieving business objectives</li>
				</ul>
			</div>
			<div class="col-md-6">
				<ul class="text-justify">
					<li>Comply with all relevant company Occupational Health, Safety and Environmental policies, procedures and work practices with the intent of preventing or minimising accidental exposures to self, colleagues and/or the environment</li>
					<li>Supervisory/Management Responsibilities</li>
					<li>Compliance with all people related process and manage people related issues. Responsible growth, development and coaching of team members</li>
					<li>Demonstrate and ensure adherence to the core values of the organisation and ethical practices as per compliance policies</li>
					<li>Lead by Example - Demonstrate high level of ethical and moral conduct to build value based culture</li>
					<li>Attend regular departmental meetings with manager/supervisor to build in continuous feedback mechanisms</li>
					<!-- <li></li> -->
				</ul>
			</div>
		</div>

		<h3 class="text-yellow">Skills Required:</h3>
		<div class="row pt-2">
			<div class="col-md-6">
				<ul class="text-justify">
					<li>Sales & marketing skills</li>
					<li>Excellent communication skills in English, Hindi & Kannada</li>
					<li>Leadership & interpersonal skills</li>
					<li>Accustomed to frequent traveling</li>
					<li>Should be result oriented & have good business judgment skills</li>
					<li>Self motivated and should be a go getter</li>
					<li>Knows startup culture</li>
					<li>Customer centricity & service orientation</li>
					<li>Super passionate</li>
				</ul>
			</div>
			<div class="col-md-6">
				<ul class="text-justify">
					<li>High energy</li>
					<li>Detailed oriented</li>
					<li>Strategic thinking & strong business acumen</li>
					<li>Very good in data & analytics</li>
					<li>Can plan & organising things</li>
					<li>Strong sense of ownership</li>
					<li>Strong sense of accountability</li>
					<li>Empower processes & policies</li>
					<li>Super strong in execution</li>
				</ul>
			</div>
		</div>
		
	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to<br> E:<a href="mailto: uday&#064;nautilushearing&#046;com"> uday&#064;nautilushearing&#046;com</a></b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>