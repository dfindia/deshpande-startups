<!--Bootstrap core JavaScript ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<!-- js bundle code -->
<?php
require_once dirname(__FILE__).'/../include/vendor/autoload.php';
$path = dirname(__FILE__).'/../';
$options = array(
	'force'=> false,
	'doc_root' => $path,
	'css_cache_path' => 'css',
	'js_cache_path' => 'js',
);

$bundle = new \DotsUnited\BundleFu\Bundle($options);
?>
<?php $bundle->start(); ?>
<!-- <script src="js/vendor/jquery-slim.min.js" defer></script> -->
<script src="js/vendor/jquery-3.2.1.min.js" defer></script>
<!-- <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script> -->
<script src="js/vendor/popper.min.js" defer></script>
<script src="js/bootstrap.min.js" defer></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<!-- <script src="js/vendor/holder.min.js"></script> -->
<script src="js/custom.js" defer></script>
<script src="js/wow.js" defer></script>
<script src="js/youtube.js" defer></script>
<?php $bundle->end(); ?>
<?php
echo $bundle->renderJs();
?>

<script>
	wow = new WOW(
	{
		animateClass: 'animated',
		offset:       100,
		callback:     function(box) {
			console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
		}
	}
	);
	wow.init();  
</script>

<script src="js/jquery-1.7.1.min.js" defer></script>

<!--popup-->
<script>
	$('#overlay').modal('show');

	// setTimeout(function() {
	// 	$('#overlay').modal('hide');
	// }, 5000);
</script>

<script>
//Get the button
var mybutton = document.getElementById("myBtn-scroll");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
 <script async src="https://www.googletagmanager.com/gtag/js?id=UA-105794986-1"></script>
 <script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());
 gtag('config', 'UA-105794986-1');
</script>

<script>
window.aikcSettings = {app_id: 'D34143'};
	!function(){var t=window,a=t.aikc;if("function"==typeof a)a("reattach_activator"),a("update",t.aikcSettings);else{var e=document,n=function(){n.c(arguments)};n.q=[],n.c=function(t){n.q.push(t)},t.aikc=n;var c=function(){var a=e.createElement("script");a.type="text/javascript",a.async=!0,a.src="https://app.konectchat.com/web/widgets/loader/"+t.aikcSettings.app_id;var n=e.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n)};t.attachEvent?t.attachEvent("onload",c):t.addEventListener("load",c,!1)}}();
</script>