<nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light ">
	<a class="navbar-brand" href="./">
		<img src="../img/deshpande-startup-logo.png" class="img-responsive" width="208" height="59" alt="Deshpande startups logo">
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse justify-content-end wow fadeInDown" id="navbarCollapse">
		<ul class="navbar-nav">
			<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'index.php') ? ' active' : '';?>">
				<a class="nav-link" href="./"><i class="fa fa-home"></i> Home</a>
			</li>
			<li class="dropdown nav-item">
				<a href="#" class="dropdown-toggle nav-link<?php echo (in_array(strtolower(basename($_SERVER['SCRIPT_NAME'])), array('about-us.php','mentors.php'))) ? ' active' : '';?>" data-toggle="dropdown">
					<i class="fa fa-university " aria-hidden="true"></i> About Us</a>
					<ul class="dropdown-content px-0">
						<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'about-us.php') ? ' active' : '';?>"><a href="about-us" class="nav-link"><i class="fa fa-ils"></i> Who We Are</a></li>
						<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'mentors.php') ? ' active' : '';?>"><a href="mentors" class="nav-link"><i class="fa fa-users"></i> Mentors</a></li>
						<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'portfolio.php') ? ' active' : '';?>"><a href="portfolio" class="nav-link"><i class="fa fa-suitcase"></i> Portfolio</a></li>
						<!-- <li class="nav-item">
							<a class="nav-link" href="#"><i class="fa fa-snowflake-o" aria-hidden="true"></i> Portfolio</a>
						</li> -->
					</ul>
				</li>
				<li class="dropdown nav-item">
					<a href="#" class="dropdown-toggle nav-link<?php echo (in_array(strtolower(basename($_SERVER['SCRIPT_NAME'])), array('edge.php','incubation-support.php','funding.php','yuva-entrepreneurship.php','co-working.php'))) ? ' active' : '';?>" data-toggle="dropdown">
						<i class="fa fa-tasks" aria-hidden="true"></i> Programs</a>
						<ul class="dropdown-content px-0">
							<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'incubation-support.php') ? ' active' : '';?>"><a href="incubation-support" class="nav-link"><i class="fa fa-universal-access" aria-hidden="true"></i> Incubation</a></li>
							<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'edge.php') ? ' active' : '';?>"><a href="edge" class="nav-link"><i class="fa fa-users"></i> EDGE</a></li>
							<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'yuva-entrepreneurship.php') ? ' active' : '';?>"><a href="yuva-entrepreneurship" class="nav-link"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Yuva Entrepreneurship</a></li>
							<!-- <li class="nav-item<?php //echo (basename($_SERVER['SCRIPT_NAME']) == 'co-working.php') ? ' active' : '';?>"><a href="co-working" class="nav-link"><i class="fa fa-linode" aria-hidden="true"></i> Co-working</a></li> -->
							<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'funding.php') ? ' active' : '';?>"><a href="funding" class="nav-link"><i class="fa fa-money" aria-hidden="true"></i> Funding</a></li>
						</ul>
					</li>
					<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'esdm-cluster.php') ? ' active' : '';?>">
						<a class="nav-link" href="esdm-cluster"><i class="fa fa-microchip" aria-hidden="true"></i> ESDM Cluster</a>
					</li>

					<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'makers-lab.php') ? ' active' : '';?>">
						<a class="nav-link" href="makers-lab"><i class="fa fa-cogs" aria-hidden="true"></i> Makers Lab</a>
					</li>

					<li class="dropdown nav-item">
						<a href="#" class="dropdown-toggle nav-link<?php echo (in_array(strtolower(basename($_SERVER['SCRIPT_NAME'])), array('news/index.php'))) ? ' active' : '';?>" data-toggle="dropdown">
							<i class="fa fa-film" aria-hidden="true"></i> Media<!-- <i class="fa fa-long-arrow-down faa-vertical animated" aria-hidden="true"></i> --></a>
							<ul class="dropdown-content px-0 nav-item">
								<li class="nav-item">
									<a class="nav-link" href="news/"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Media Coverage</a>
								</li>
								<!-- <li class="nav-item<?php //echo (basename($_SERVER['SCRIPT_NAME']) == 'news.php') ? ' active' : '';?>"><a href="news" class="nav-link"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Media Coverage</a></li> -->
								<li class="nav-item"><a href="https://justaskdesh.com/" class="nav-link" rel="nofollow" target="_blank"><i class="fa fa-info-circle" aria-hidden="true"></i> Insights from Desh</a></li>
							</ul>
						</li>

						<li class="nav-item<?php echo (in_array(strtolower(basename($_SERVER['SCRIPT_NAME'])), array('events.php','illuminate.php','hackathon.php','inauguration.php','pitch-your-idea.php','uplift.php','alexathon.php'))) ? ' active' : '';?>"><a href="events" class="nav-link blink_me"><i class="fa fa-calendar"></i> Events <!-- <i class="fa fa-long-arrow-left faa-horizontal animated" aria-hidden="true"></i> --></a>
						</li>

						<li class="dropdown nav-item">
							<a href="#" class="dropdown-toggle nav-link<?php echo (in_array(strtolower(basename($_SERVER['SCRIPT_NAME'])), array('career.php','career-startup.php'))) ? ' active' : '';?>" data-toggle="dropdown">
								<i class="fa fa-user" aria-hidden="true"></i> Career</a>
								<ul class="dropdown-content px-0 nav-item">

									<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'career.php') ? ' active' : '';?>"><a href="career" class="nav-link"><i class="fa fa-user" aria-hidden="true"></i> At Deshpande Startups</a></li>
									<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'career-startup.php') ? ' active' : '';?>"><a href="career-startup" class="nav-link"><i class="fa fa-user" aria-hidden="true"></i> At Incubated Startups</a></li>
								</ul>
							</li>

							<li class="nav-item<?php echo (basename($_SERVER['SCRIPT_NAME']) == 'contact-us.php') ? ' active' : '';?>">
								<a class="nav-link" href="contact-us"><i class="fa fa-phone" aria-hidden="true"></i> Contact Us</a>
							</li>
						</ul>
					</div>
				</nav>
				<main role="main">