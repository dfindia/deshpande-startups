	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="msvalidate.01" content="C9BFF13894C06831E96D7998851842A6" />
	<meta http-equiv="Copyright" content="Copyright (c) 2020 Deshpande Startups"/>
	<meta name="distribution" content="global"/>
	<meta name="rating" content="safe for kids"/>
	<meta name="web_author" content="Deshpande Startups"/>
	<meta name="resource-type" content="document"/>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta name="robots" content="index, follow"/>
	<meta name="classification" content="internet"/>
	<meta name="doc-type" content="public"/>
	<meta http-equiv="cahe-control" content="cache"/>
	<meta name="contactName" content="C. M Patil"/>
	<meta name="contactOrganization" content="Deshpande Startups"/>
	<meta name="contactCity" content="Hubballi"/>
	<meta name="contactState" content="Karnataka"/>
	<meta name="contactCountry" content="India"/>
	<meta name="contact NetworkAddress" content="seir@dfmail.org"/>
