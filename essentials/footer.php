<hr class="featurette-divider-sm p-1 m-0 bg-yellow">
<div class="container-fluid marketing bg-dark text-secondary pt-4 p-3">
   <div class="row featurette">
      <div class="col-md-2 details wow animated slideInUp">
         <h3 class="featurette-heading-sm font-weight-normal text-white">Quick links</h3>
         <div class="divider b-y text-yellow w-75"></div>
         <ul class="list-unstyled">
            <li><a href="./">| Home</a></li>
            <li><a href="about-us">| About Us</a></li>
            <!-- <li><a href="#">| Startup-Stories</a></li> -->
            <li><a href="events">| Events</a></li>
            <li><a href="news">| Media Coverage</a></li>
            <li><a href="career">| Career</a></li>
            <li><a href="career-startup">| Career at Startups</a></li>
         </ul>
      </div>
      <div class="col-md-2 wow animated slideInUp">
         <h3 class="featurette-heading-sm font-weight-normal text-white">Useful links</h3>
         <div class="divider b-y text-yellow w-75"></div>
         <ul class="list-unstyled">
            <li><a href="survey-form">| Yuva Survey Form</a></li>
            <!-- <li><a href="fssi-visit-form">| Exposure Visit</a></li> -->
            <li><a href="media" target="_blank">| Media</a></li>
            <li><a href="https://justaskdesh.com/" rel="nofollow" target="_blank">| Insights from Desh</a></li>
            <li><a href="http://www.startupdialogue.org/" target="_blank">| Startup Dialogue</a></li>
            <li><a href="site-map" target="_blank">| Sitemap</a></li>
            <!-- <li><a href="http://hrms.sandboxstartups.org" rel="nofollow" target="_blank">| FSSI HRMS</a></li> -->
         </ul>
      </div>
      <div class="col-md-2 wow animated slideInUp">
         <h3 class="featurette-heading-sm font-weight-normal text-white">Engagements</h3>
         <div class="divider b-y text-yellow w-75"></div>
         <ul class="list-unstyled">
            <li><a href="internship">| Internship</a></li>
            <li><a href="hackathon">| Hackathon</a></li>
            <li><a href="developthon">| Developthon</a></li>
            <li><a href="makeathon">| Makeathon</a></li>
            <li><a href="ideathon">| Ideathon</a></li>
            <li><a href="workshop">| Workshop</a></li>
         </ul>
      </div>
      <div class="col-md-2 wow animated slideInUp">
         <h3 class="featurette-heading-sm font-weight-normal text-white">Programs</h3>
         <div class="divider b-y text-yellow w-75"></div>
         <ul class="list-unstyled">
            <!-- <li><a href="survey-form">| YEP Survey Form</a></li> -->
            <li><a href="yuva-entrepreneurship">| Yuva Entrepreneurship</a></li>
            <li><a href="edge">| EDGE</a></li>
            <li><a href="incubation-support">| Incubation</a></li>
            <li><a href="funding">| Funding</a></li>
            <li><a href="esdm-cluster">| ESDM Cluster</a></li>
            <li><a href="makers-lab">| Makers Lab</a></li>
         </ul>
      </div>
      <div class="col-md-4 wow animated slideInUp">
         <h3 class="featurette-heading-sm font-weight-normal text-white">Subscribe us</h3>
         <div class="divider b-y text-yellow w-75"></div>
         <p class="lead pb-2">Subscribe to our newsletter to get all our news in your Inbox.</p>
         <iframe name=hidden_iframe id=hidden_iframe class="disp" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('thank-you').reset();}">
         </iframe>
         <form method="POST" id="thank-you" action="https://docs.google.com/forms/d/e/1FAIpQLSforoL3W315d-1Nuw5iU_BNJ397XVaIqwXEmr-ykdMP5Qpb6w/formResponse" target=hidden_iframe onSubmit="submitted=true;">
            <div class="form-group pb-2">
               <input type="email" class="form-control" name="entry.1884931546" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email id" required>
               <input type="hidden" class="form-control" name="entry.840748439" value="www.deshpandestartups.org">
               <small id="emailHelp" class="form-text">We'll never share your email with anyone else.</small>
            </div>
            <button class="btn btn-warning" type="submit">SUBSCRIBE US</button>
         </form>
      </div>
   </div>
   <hr class="featurette-divider-sm p-0 m-0 bg-yellow">
   <div class="row featurette pt-4">
      <div class="col-md-3 details wow slideInUp">
         <h3 class="featurette-heading-sm font-weight-normal text-white">We are located at</h3>
         <div class="divider b-y text-yellow w-75"></div>
         <p><i class="fa fa-map-marker"></i> &nbsp; Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka. </p>
      </div>
      <div class="col-md-3 details wow slideInUp">
         <h3 class="featurette-heading-sm font-weight-normal text-white">EDGE & Incubation</h3>
         <div class="divider b-y text-yellow w-75"></div>
         <p><i class="fa fa-phone"></i><a class="text-white" href="tel:+91-951-331-5287"> &nbsp;+91-951-331-5287</a></p>
         <!-- <p><i class="fa fa-phone"></i><a class="text-white" href="tel:+91-968-665-4749"> &nbsp;+91-968-665-4749</a></p> -->
         <p><i class="fa fa-envelope text-white"></i><a class="text-white" href="mailto:seir&#064;dfmail&#046;org"> &nbsp;seir&#064;dfmail&#046;org</a></p>
      </div>
      <div class="col-md-3 details wow slideInUp">
         <h3 class="featurette-heading-sm font-weight-normal text-white">Makers Lab</h3>
         <div class="divider b-y text-yellow w-75"></div>
         <p><i class="fa fa-phone"></i><a class="text-white" href="tel:+91-951-331-5791"> &nbsp;+91-951-331-5791</a></p>
         <p><i class="fa fa-envelope text-white"></i><a class="text-white" href="mailto:makerslab&#064;dfmail&#046;org"> &nbsp;makerslab&#064;dfmail&#046;org</a></p>
      </div>
      <div class="col-md-3 details wow slideInUp">
         <h3 class="featurette-heading-sm font-weight-normal text-white">ESDM Cluster</h3>
         <div class="divider b-y text-yellow w-75"></div>
         <p><i class="fa fa-phone"></i><a class="text-white" href="tel:+91-951-331-5793"> &nbsp;+91-951-331-5793</a></p>
         <p><i class="fa fa-envelope text-white"></i><a class="text-white" href="mailto:veeru&#046;sandbox&#064;dfmail&#046;org"> &nbsp;veeru&#046;sandbox&#064;dfmail&#046;org</a></p>
      </div>
   </div>
   <div class="row featurette pt-4">
      <div class="details wow slideInUp">
         <p><i class="fa fa-globe"></i> &nbsp;<a class="text-white" href="https://www.deshpandestartups.org/">www.deshpandestartups.org</a> | <i class="fa fa-globe"></i> &nbsp;<a class="text-white" href="https://www.startupdialogue.org/">www.startupdialogue.org</a> | <i class="fa fa-bus"></i> &nbsp;<a class="text-white" href="https://www.startupdialogue.org/tourist-attraction"target="_blank">Tourist Attraction</a> </p>
      </div>
   </div>
   <!-- /END THE FEATURETTES -->
</div>