<!doctype html>
<html lang="en">
<head>
	<?php require_once 'meta.php'; ?>
	<title><?php echo $title; ?></title>
	<!-- css bundle code -->
	<?php
	require_once dirname(__FILE__).'/../include/vendor/autoload.php';
	$path = dirname(__FILE__).'/../';
	$options = array(
		'force'=> false,
		'doc_root' => $path,
		'css_cache_path' => 'css',
		'js_cache_path' => 'js',
	);
	
	$bundle = new \DotsUnited\BundleFu\Bundle($options);
	?>
	<?php $bundle->start(); ?>
	<!-- font-awesome CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Bootstrap core CSS -->
	<!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css"> -->
	<!-- Custom styles for this template -->
	<link href="css/carousel.css" rel="stylesheet">
	<link href="css/slider.css" rel="stylesheet">
	<link href="css/custom.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">
	<?php $bundle->end(); ?>
	<?php
	echo $bundle->renderCss();
	?>
	<!-- css bundle code -->
	<link type="text/css" href="css/lightbox.css" rel="stylesheet" />
	<link href="css/jquery.onebyone.css" rel="stylesheet">
	<link href="css/default.css" rel="stylesheet">
	<link href="css/animate-new.css" rel="stylesheet">
	