<!DOCTYPE html>
<html lang="en">
<head>
	<title>Media Coverage - Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/news"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/news">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/deshpande-bg-news.jpg">
	<meta property="og:description" content="Tech Incubator Sandbox Startups Provide DST-NIDHI Funding to Three Hubballi Based Companies."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Tech Incubator Sandbox Startups Provide DST-NIDHI Funding to Three Hubballi Based Companies."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Deshpande Startups Media Coverage">
	<link rel="canonical" href="https://www.deshpandestartups.org/news">
	<?php
         // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	.bg-light1{
		background-color: #fd7e141a;
	}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/deshpande-bg-news.jpg" width="1349" height="198" alt="Deshpande Startups, media coverage">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">News</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">MEDIA</span> COVERAGE</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
	</div>
	<br>
	<div class="container-fluid">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">05</span>
							<span class="post-month">August</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/05august2019-udayavani.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Sandbox Startups Recognized in Elevate</p><br><br>
								<h6 class="text-yellow">UDAYAVANI</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">15</span>
							<span class="post-month">July</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/15july2019-forbesindia.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.forbesindia.com/article/startup-hubs-special/beyond-bengaluru-north-karnatakas-startup-opportunities/54349/1" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Beyond Bengaluru: North Karnataka's startup opportunities</p><br><br>
								<h6 class="text-yellow">Forbes INDIA</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">05</span>
							<span class="post-month">June</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/05june2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/06/startup-bharat-healthcare-healthtech-tier-ii-iii-towns" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] How healthcare startups in Tier II and III cities are solving the problem of access in their hometowns</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">31</span>
							<span class="post-month">May</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/31may2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/05/startup-bharat-aasalabs-vyavasahaaya-agritech-open-innovation" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] With the likes of Bayer as clients, Mysuru’s Aasalabs is connecting agritech startups with corporates</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">29</span>
							<span class="post-month">May</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/29may2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/05/startup-bharat-prime-minister-narendra-modi-expectations" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] Here are the four things ‘Startup Bharat’ wants Prime Minister Narendra Modi to focus on</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">27</span>
							<span class="post-month">May</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/27may2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/05/startup-bharat-beyond-metros-deep-tech-solutions-real-india-problems" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] Away from the metros, a new breed of deep-tech startups is solving ‘real India’ problems</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">17</span>
							<span class="post-month">May</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/17may2019-krishijagran.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://krishijagran.com/agriculture-world/inviting-digital-solutions-for-combating-fall-armyworm-in-india/" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Inviting Digital Solutions for Combating Fall Armyworm in India</p><br><br>
								<h6 class="text-yellow">Krishijagran.com</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">23</span>
							<span class="post-month">April</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/23april2019-inc42.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://inc42.com/features/india-govt-backed-startup-incubation-centres/" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>India’s State-Backed Incubators Are Proving That Government Support Can Lift The Startup Ecosystem</p><br>
								<h6 class="text-yellow">Inc42</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">10</span>
							<span class="post-month">April</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/10april2019-prajavani2.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Battery power replaces fossil fuel in newly invented paddy sowing machine</p><br>
								<h6 class="text-yellow">PRAJAVANI</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">10</span>
							<span class="post-month">April</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/10april2019-prajavani.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>A machine for pickle business</p><br><br><br>
								<h6 class="text-yellow">PRAJAVANI</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">09</span>
							<span class="post-month">April</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/09april2019-theindianexpress.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href=https://indianexpress.com/article/cities/mumbai/maharashtra-government-picks-13-start-ups-in-push-to-mumbai-fintech-hub-5478851/?fbclid=IwAR27hysWYWnJhPCcNoZzEFE3FPGdOCY8CjH1q0MQ9ZstosRZI_wkPLB7bbw" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Maharashtra government picks 13 start-ups in push to Mumbai fintech hub</p><br>
								<h6 class="text-yellow">The Indian EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">22</span>
							<span class="post-month">March</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/22march2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/03/startup-bharat-nautilus-hubli-portable-hearing-loss-lpv72epgza?__sta=pbh.uosvdzxrhwzjsq.lvvn%7CUIY&__stm_medium=email&__stm_source=smartech" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] Made in Hubli: a portable device that makes hearing tests 80 percent cheaper</p><br>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">20</span>
							<span class="post-month">March</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/20march2019-prajavani.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Auto Startup for the smooth functioning of pumpset</p><br><br>
								<h6 class="text-yellow">PRAJAVANI</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">14</span>
							<span class="post-month">March</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/14march2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/03/startup-bharat-healthcare-startups-medtech-vfoy563gjz" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] With these startups, healthcare services are getting better in non-metros</p><br>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">13</span>
							<span class="post-month">March</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/13march2019-prajavani.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Freshboxx: For organic vegitables and fruits</p><br><br>
								<h6 class="text-yellow">PRAJAVANI</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">08</span>
							<span class="post-month">March</span>
							<span class="post-date">2019</span>
							<!-- <a href="https://yourstory.com/2019/03/microchip-payments-digital-transactions-voice-network-tnbm/amp" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
							<a href="img/pdf/08march2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] No internet, no problem: this Hubli-based startup is enabling digital payments over voice network</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">21</span>
							<span class="post-month">February</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/21feb2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/02/startupbharat-hubli-sandbox-startups" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>[Startup Bharat] From portable machines that can save newborns to AI startups, these Sandbox Hubli startups are solving real-world problems</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">20</span>
							<span class="post-month">February</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/20feb2019-cnbctv18.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Narayana Murthy lists key things small-town Hubli needs to become startup powerhouse</p><br><br>
								<h6 class="text-yellow">CNBC TV19</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">15</span>
							<span class="post-month">February</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/15feb2019-yourstory.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/02/startupbharat-nr-narayana-murthy-sandbox-hubli-h1j95o6hk4" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>StartupBharat: NR Narayana Murthy reveals what it would take for Hubli startups to be No. 1 in India at Sandbox Hubli’s Startup Dialogue 2019 event</p>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">04</span>
							<span class="post-month">February</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/04feb2019-udayavani.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.readwhere.com/read/2011151/Hubli-Edition/04-Feb-2019#page/2/2" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>"Failure is not the end of entrepreneurship journey" <br>- Mr. NR Narayana Murthy </p><br>
								<h6 class="text-yellow">UDAYAVANI</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">29</span>
							<span class="post-month">January</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/29jan2019-thenewindianexpress.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://epaper.newindianexpress.com/2001930/The-New-Indian-Express-Hubbali/29-JAN-2019#page/4/2" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Judicial Magistrates, Public Prosecutors sensitised on animal laws</p><br><br>
								<h6 class="text-yellow">THE NEW INDIAN EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">23</span>
							<span class="post-month">January</span>
							<span class="post-date">2019</span>
							<a href="img/pdf/23jan2019-yourstory.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2019/01/save-babies-jaundice-portable-machine/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>This doctor built a portable machine to save newborn babies from dying of jaundice across rural India</p><br>
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">12</span>
							<span class="post-month">December</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/12dec2018-vijayavani.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://epapervijayavani.in/ArticlePage/APpage.php?edn=Hubli&articleid=VVAANINEW_HUB_20181212_6_18&artwidth=315px" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Amit Sharma from Startup India have shared the details of initiatives of Startup India to support small, micro & medium entreprises</p>
								<h6 class="text-yellow">VIJAYAVANI</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">11</span>
							<span class="post-month">December</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/11dec2018-newindianexpress.pdf" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://epaper.newindianexpress.com/c/34771337" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>This startup helps farmers know what is good for them</p><br><br>
								<h6 class="text-yellow">THE NEW INDIAN EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">11</span>
							<span class="post-month">December</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/startup-india.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Amit Sharma from Startup India have shared the details of initiatives of Startup India to support small, micro & medium entreprises</p>
								<h6 class="text-yellow">PRAJAVANI</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">29</span>
							<span class="post-month">October</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/the-new-indian-express-hubballi-291018.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://epaper.newindianexpress.com/1874287/The-New-Indian-Express-Hubbali/29-OCT-2018#page/5/1" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>ESDM facility could help Hubballi become next electronic hub of K'taka</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE NEW INDIAN EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">28</span>
							<span class="post-month">October</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/UV-hubballi-281018.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a><!-- <a href="http://epaper.udayavani.com/home.php?edition=Hubli&date=2018-10-28&pageno=2&pid=UVANI_HUB#Article/UVANI_HUB_20181028_2_3/363px" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Hubballi will be the electronic center in future : Hegde</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">UDAYAVANI</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">28</span>
							<span class="post-month">October</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/28oct2018-sayuktakarnataka.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.samyukthakarnataka.com/1872900/Samyukta-Karnataka-Hubballi-%E0%B2%B8%E0%B2%82%E0%B2%AF%E0%B3%81%E0%B2%95%E0%B3%8D%E0%B2%A4-%E0%B2%95%E0%B2%B0%E0%B3%8D%E0%B2%A8%E0%B2%BE%E0%B2%9F%E0%B2%95-%E0%B2%B9%E0%B3%81%E0%B2%AC%E0%B3%8D%E0%B2%AC%E0%B2%B3%E0%B3%8D%E0%B2%B3%E0%B2%BF-10-06-2017/28-10-2018#page/5/2" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>If there is fear, life is not successful</p><br><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">SAMYUKTA KARNATAKA</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">28</span>
							<span class="post-month">October</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/kp-hubballi-281018.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>The minister of state for skill development & entrepreneurship, Anant Kumar Hegde said: The state's next electronic station is Hubballi.</p>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">KANNADA PRABHA</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">24</span>
							<span class="post-month">September</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/24sep2018-thenewindianexpress.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://epaper.newindianexpress.com/1828432/The-New-Indian-Express-Hubbali/24-SEP-2018#issue/2/1" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Admin to extend all necessary support to budding entrepreneurs</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE NEW INDIAN EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">25</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/25july2018-timesofindia.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://timesofindia.indiatimes.com/city/hubballi/startup-designs-portable-phototherapy-unit-to-tackle-neonatal-jaundice/articleshow/65124261.cms" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Startup designs portable device to tackle neonatal jaundice</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE TIMES OF INDIA</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">24</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/24july2018-thenewenglandnews.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://indianewengland.com/2018/07/deshpande-foundation-launches-indias-largest-startup-incubation-in-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Deshpande Foundation Launches India’s Largest Startup Incubation in Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">INDIA NEW ENGLAND NEWS</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">20</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/20july2018-KNNknowledge.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://knnindia.co.in/news/newsdetails/sectors/indias-first-largest-startup-incubation-centre-launched-in-hubballi-city-to-promote-entrepreneurship" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>India’s first largest startup Incubation Centre launched in Hubballi city to promote entrepreneurship</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">KNN Knowledge & News Network</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">19</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/19july2018-indianweb2.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.indianweb2.com/2018/07/19/sandbox-startups-unveils-indias-largest-startup-incubation-centre-in-hubballi-city/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Sandbox Startups Unveils India’s Largest Startup Incubation Centre in Hubballi City</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">indianWeb2</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">19</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/19july2018-thehindu.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.thehindu.com/todays-paper/tp-national/tp-karnataka/niti-aayog-ceo-sees-need-for-overhauling-education/article24457227.ece" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>NITI Aayog CEO sees need for overhauling education</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE HINDU</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">18</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/18july2018-thehindu-BusinessLine.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.thehindubusinessline.com/news/niti-aayog-ceo-farms-health-education-problems-need-disruptive-innovations/article24454516.ece" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>NITI Aayog CEO: Farms, health, education problems need disruptive innovations</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE HINDU - BusinessLine</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">18</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/18july2018-thehindu.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.thehindu.com/news/national/karnataka/esdm-cluster-to-benefit-young-entrepreneurs/article24446740.ece" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>ESDM Cluster to benefit young entrepreneurs</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE HINDU</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">18</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/18july2018-yourstory.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2018/07/indias-largest-startup-incubation-centre-inaugurated-in-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>India’s largest startup incubation centre inaugurated in Hubballi</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">18</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/18july2018-thetimesofindia.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://timesofindia.indiatimes.com/city/hubballi/device-helps-local-stores-stave-off-e-commerce-sites/articleshow/65029927.cms?from=mdr" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Device helps local stores stave off e-commerce sites</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE TIMES OF INDIA</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">18</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/18july2018-thetimesofindia2.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://timesofindia.indiatimes.com/city/hubballi/ap-telangana-may-poach-our-investors/articleshow/65030005.cms" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>'AP, Telangana may poach our investors'</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE TIMES OF INDIA</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">17</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/17july2018-canindia.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.canindia.com/electronics-hardware-cluster-unveiled-in-karnataka-city/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Electronics hardware cluster unveiled in Karnataka city</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">CanIndia</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">17</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/17july2018-thenewindianexpress.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.newindianexpress.com/states/karnataka/2018/jul/17/startups-moving-from-crowded-metros-to-connected-economical-hubballi-1844466.html" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Startups moving from crowded metros to connected, economical Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE NEW INDIAN EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">12</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/12july2018-indiathishour.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://indiathishour.in/sandbox-startups-is-set-to-induct-indias-largest-incubation-centre-in-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Sandbox Startups Is Set To Induct  India’s Largest Incubation Centre In Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">INDIA THIS HOUR</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">12</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/12july2018-dailynewspost.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://dailynewspost.live/sandbox-startups-is-set-to-induct-indias-largest-incubation-centre-in-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Sandbox Startups Is Set To Induct  India’s Largest Incubation Centre In Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">DailyNewsPost</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">11</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/11july2018-indianweb2.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.indianweb2.com/2018/07/11/indias-largest-startup-incubator-to-be-inaugurated-on-18-july-in-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>India’s Largest Startup Incubator To Be Inaugurated on 18 July in Hubballi</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">indianWeb2</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">11</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/11july2018-theweek.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.theweek.in/news/biz-tech/2018/07/11/india-largest-startup-incubator-inaugurated-hubballi-next-week.html" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>India's largest startup incubator to be inaugurated in Hubballi next week</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE WEEK</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">10</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/10july2018-bangalorenewsnetwork.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.bangalorenewsnetwork.com/m/news_detail.php?f_news_id=1156" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Minister K J George to inaugurate state-of-the-art Sandbox ESDM Cluster in Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">BNN - Bangalore News Network</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">10</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/10july2018-thehindu-businessline.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.thehindubusinessline.com/news/national/electronics-design-cluster-for-startups-to-be-opened-shortly-in-hubballi/article24380166.ece" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Electronics design cluster for startups to be opened shortly in Hubballi</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE HINDU - BusinessLine</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">10</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/10july2018-thenewindianexpress.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://epaper.newindianexpress.com/1729666/The-New-Indian-Express-Hubbali/10-JUL-2018#issue/4/1" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Hubballi gets electronic system design and manufacturing cluster</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE NEW INDIAN EXPRESS</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">09</span>
							<span class="post-month">July</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/09july2018-theeconomictimes.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://economictimes.indiatimes.com/small-biz/startups/newsbuzz/deshpande-foundation-to-launch-sandbox-startups-esdm-cluster-in-hubballi/articleshow/64920808.cms" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Deshpande Foundation to launch Sandbox Startups ESDM Cluster in Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE ECONOMIC TIMES - ETRISE</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">19</span>
							<span class="post-month">April</span>
							<span class="post-date">2018</span>
							<a href="https://m.siliconindia.com/news/business/Tech-Incubator-Sandbox-Startups-Provide-DSTNIDHI-Funding-to-Three-Hubli-Based-Companies-nid-204142-cid-3.html" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Tech Incubator Sandbox Startups Provide DST-NIDHI Funding to Three Hubli Based Companies</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">SiliconIndia</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">23</span>
							<span class="post-month">April</span>
							<span class="post-date">2018</span>
							<a href="img/pdf/23april2018-socialstory.pdf" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://yourstory.com/2018/04/deshpande-foundation-sandbox-startup-nidhisss-funding/" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Technology incubator Sandbox Startups invests in three Hubballi-based companies</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">YOURSTORY</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">29</span>
							<span class="post-month">January</span>
							<span class="post-date">2017</span>
							<a href="img/pdf/29jan2017-indianweb2.pdf" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://www.indianweb2.com/2017/01/29/deshpande-set-indias-largest-startup-incubator-hubli/" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Deshpande To Set Up India’s Largest Startup Incubator at Hubli</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">indianWeb2</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">28</span>
							<span class="post-month">January</span>
							<span class="post-date">2017</span>
							<a href="img/pdf/28jan2017-businessstandard.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.business-standard.com/article/companies/deshpande-foundation-to-set-up-india-s-largest-start-up-incubator-117012700912_1.html" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>'Desh' to set up India's largest start-up incubator at Hubballi</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">Business Standard</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">28</span>
							<span class="post-month">January</span>
							<span class="post-date">2017</span>
							<a href="img/pdf/28jan2017-theeconomictimes.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://economictimes.indiatimes.com/small-biz/startups/hubballi-to-get-countrys-largest-startup-incubator/articleshow/56829858.cms?from=mdr" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Hubballi to get country's largest startup incubator</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE ECONOMIC TIMES - ETRISE</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">28</span>
							<span class="post-month">January</span>
							<span class="post-date">2017</span>
							<a href="img/pdf/28jan2017-khaleejmag.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.khaleejmag.com/entrepreneurship/india-launch-largest-startup-incubation-center-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>India to launch its Largest Startup Incubation Center in Hubballi</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">Khaleej Mag</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">28</span>
							<span class="post-month">January</span>
							<span class="post-date">2017</span>
							<a href="img/pdf/28jan2017-thetimesofindia.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="https://timesofindia.indiatimes.com/business/india-business/hubballi-to-host-indias-biggest-startup-incubation/articleshow/56822425.cms?intenttarget=no&utm_source=newsletter&utm_medium=email&utm_campaign=Top_Headlines" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Hubballi to host India's biggest startup incubation</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE TIMES OF INDIA</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">29</span>
							<span class="post-month">January</span>
							<span class="post-date">2017</span>
							<a href="img/pdf/29jan2017-newskart.pdf" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.newskart.com/gururaj-deshpande-settingup-indias-biggest-incubator-startups-hubballi/" rel="nofollow" target="_blank" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Gururaj Deshpande Setting Up India’s Biggest Incubator For Startups In Hubballi</p><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">NEWSKART</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 bg-light1 card-hover-shadow">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 post-data-left">
							<span class="post-month">25</span>
							<span class="post-month">November</span>
							<span class="post-date">2017</span>
							<a href="img/pdf/25nov2017-thehindu.pdf" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a>
							<!-- <a href="http://www.thehindu.com/news/national/karnataka/hubballi-set-to-house-countrys-biggest-startup-incubation-centre/article17105461.ece" target="_blank" rel="nofollow" class="btn btn-warning button4 lg-block">View</a> -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 post-data-right">
							<div class="pt-3">
								<p>Hubballi set to house country’s biggest startup incubation centre</p><br><br>
								<!-- <small class="text-yellow"><b>Published by</b></small> -->
								<h6 class="text-yellow">THE HINDU</h6>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<br>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>