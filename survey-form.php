<!DOCTYPE html>
<html lang="en">
<head>
	<title>Yuva Entrepreneurship Form</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/survey-form"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/survey-form">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/makers/yuvaentrepreneurship-bg.png">
	<meta property="og:description" content="This forms is aimed at understanding the problems of young budding entrepreneurs in tier-2 and 3 cities. Based on your responses, Deshpande Startup is going to design a program addressing the issues and create an opportunity for young bright minds to start a company of their own."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="This forms is aimed at understanding the problems of young budding entrepreneurs in tier-2 and 3 cities. Based on your responses, Deshpande Startup is going to design a program addressing the issues and create an opportunity for young bright minds to start a company of their own."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Yuva Entrepreneurship Form">
	<link rel="canonical" href="https://www.deshpandestartups.org/survey-form">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		.smalllable{
			font-size: 15px;
			padding-top: 0;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<br>
	<div class="container text-center">
		<h2 class=" text-yellow text-center Pt-5 wow animated slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">YUVA ENTREPRENEURSHIP</span><br> FORM</h2>
		<div class="divider b-y text-yellow content-middle"></div>
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="text-justify">This forms is aimed at understanding the problems of young budding entrepreneurs in tier-2 and 3 cities. Based on your responses, Deshpande Startup is going to design a program addressing the issues and create an opportunity for young bright minds to start a company of their own.</p><br>
				<iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(typeof submitted != 'undefined' && submitted){alert('Thank you we received your request'); document.getElementById('ss-form').reset();}">
				</iframe>
				<form role="form" action="https://docs.google.com/forms/d/e/1FAIpQLSegFQZXOD_NduRqEt3o_SVHxJ78bQxiWsMmCniryYTVVLONvw/formResponse" method="post" target="hidden_iframe" id="ss-form" onSubmit="submitted=true;">

					<div class="row w3-card p-3">
						<div class="col-md-12 pad">
							<div class="row">
								<div class="form-group col-md-4 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0s">
									<label for="input1"><b>Full Name<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.236573406" class="box2 form-control" maxlength="50" pattern="^(?![ .]+$)[a-zA-Z .]*$" placeholder="Mention your name" title="Mention your name" required="required">
								</div>
								<div class="form-group col-md-4 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.2s">
									<label for="input2"><b>Mobile Number<span class="text-yellow">*</span></b></label>
									<input type="phone" name="entry.1564253503" class="box2 form-control" pattern="\d*" min="12" placeholder="Mention your mobile number" maxlength="10" minlength="10" title="Your mobile number" required="required">
								</div>
								<div class="form-group col-md-4 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
									<label for="input3"><b>Email-Id<span class="text-yellow">*</span></b></label>
									<input type="email" name="entry.1324139691" placeholder="johndoe@gmail.com" title="Your email-id" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="box2 form-control" required="required">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-4 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input4"><b>City/ Place<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.1652456813" pattern="^(?![ .]+$)[a-zA-Z .]*$" class="box2 form-control" placeholder="Your location" required="required">
								</div>
								<div class="form-group col-md-4 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
									<label for="input3"><b>State<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.846002520" pattern="^(?![ .]+$)[a-zA-Z .]*$" placeholder="Your state" class="box2 form-control" required="required">
								</div>
								<div class="form-group col-md-4 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input4"><b>Gender<span class="text-yellow">*</span></b></label>
									<select class="form-control" name="entry.240877432">
										<option value="Male">Male</option>
										<option value="Female">Female</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input5"><b>College/ Institute<span class="text-yellow">*</span></b></label>
									<input type="text" name="entry.1686896241" class="box2 form-control" placeholder="Name of college/institute" required="required">
								</div>
								<div class="form-group col-md-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input7"><b>Academic Qualification<span class="text-yellow">*</span></b></label>
									<select class="form-control" name="entry.458084109">
										<option value="Diploma">Diploma</option>
										<option value="B.E">B.E</option>
										<option value="M.Tech">M.Tech</option>
										<option value="BBA">BBA</option>
										<option value="MBA">MBA</option>
										<option value="BCA">BCA</option>
										<option value="MCA">MCA</option>
										<option value="B.Com">B.Com</option>
										<option value="M.Com">M.Com</option>
										<option value="B.Sc">B.Sc</option>
										<option value="M.Sc">M.Sc</option>
										<option value="ITI">ITI</option>
									</select>
									<!-- <input type="text" name="entry.458084109" class="box2 form-control" title="Mention your Academic Qualification" placeholder="Mention your Degree"> -->
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-lg-6 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label><b>Stream/ Department<span class="text-yellow">*</span></b></label>
									<select class="form-control" name="entry.1423301987">
										<option value="CS" selected>CS</option>
										<option value="IS">IS</option>
										<option value="E&C">E&C</option>
										<option value="E&E">E&E</option>
										<option value="Mechanical">Mechanical</option>
										<option value="Civil">Civil</option>
										<option value="Architecture">Architecture</option>
										<option value="Bio Technology">Bio Technology</option>
										<option value="Chemical">Chemical</option>
										<option value="Aeronautical">Aeronautical</option>
										<option value="Biochemical">Biochemical</option>
										<option value="Other">Other</option>
									</select>
								</div>
								<div class="form-group col-md-6 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input4"><b>Which year are you in? <span class="text-yellow">*</span></b></label>
									<select class="form-control" name="entry.2050028900">
										<option value="1st">1<sup>st</sup></option>
										<option value="2nd">2<sup>nd</sup></option>
										<option value="3rd">3<sup>rd</sup></option>
										<option value="4th">4<sup>th</sup></option>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input6"><b>Select which ever best describes your thoughts about entrepreneurship/startup and job?</b></label><br>
						</div>
						<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<div class="row">
								<div class="form-group col-md-6">
									<label for="risky"><input type="radio" name="entry.1882344902" value="Startups are a risky affair so I want to take up a job" checked="checked"> Startups are a risky affair so i want to take up a job</label>
								</div>
								<div class="form-group col-md-6">
									<label for="neverheard"><input type="radio" name="entry.1882344902" value="I never heard about the term startup or entrepreneurship"> I never heard about the term startup or entrepreneurship</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6">
									<label for="ownstartup"><input type="radio" name="entry.1882344902" value="I want to start my own Startup but i don't have an idea"> I want to start a startup but i don't have an Idea</label>
								</div>
								<div class="form-group col-md-6">
									<label for="haveanidea"><input type="radio" name="entry.1882344902" value="I have an Idea, but i don't know where to begin"> I have a startup Idea, but i don't know where to begin</label>
								</div>
							</div>

							<div class="row txbx1" style="display: none">
								<div class="form-group col-md-12 m-0">
									<div class="form-group col-md-12 m-0">
										<label for="input16"><b>Problem addressing sector<span class="text-yellow">*</span></b></label>
									</div>
									<div class="col-md-12 form-group m-0">
										<div class="row">
											<div class="col-md-4 form-group">
												<label for="smartcity"><input type="radio" name="entry.155707691" value="Smart City"> Smart City</label>
											</div>
											<div class="col-md-4 form-group">
												<label for="agriculture"><input type="radio" name="entry.155707691" value="Agriculture"> Agriculture</label>
											</div>
											<div class="col-md-4 form-group">
												<label for="renewable"><input type="radio" name="entry.155707691" value="renewable energy"> Renewable Energy</label>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4 form-group">
												<label for="healthcare"><input type="radio" name="entry.155707691" value="Healthcare"> Healthcare</label>
											</div>
											<div class="col-md-4 form-group">
												<label for="edutech"><input type="radio" name="entry.155707691" value="Edutech"> Edutech</label>
											</div>
											<div class="col-md-4 form-group">
												<label for="electronics"><input type="radio" name="entry.155707691" value="Electronis & IoT"> Electronics & IoT</label>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4 form-group">
												<label for="WaterManagement"><input type="radio" name="entry.155707691" value="Water Management"> Water Management</label>
											</div>
											<div class="col-md-4 form-group">
												<label for="waste management"><input type="radio" name="entry.155707691" value="waste management"> Waste Management</label>
											</div>
											<div class="col-md-4 form-group">
												<label for="Safety & Security"><input type="radio" name="entry.155707691" value="Safety & Security"> Safety & Security</label>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4 form-group">
												<label for="Software development"><input type="radio" name="entry.155707691" value="Software development"> Software Development</label>
											</div>
											<div class="col-md-4 form-group">
												<label for="Automation"><input type="radio" name="entry.155707691" value="Automation"> Automation</label>
											</div>
											<div class="col-md-4 form-group">
												<label for="Transportation"><input type="radio" name="entry.155707691" value="Transportation"> Transportation</label>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label for="input9"><b>Brief about your idea<span class="text-yellow">*</span></b></label>
										<textarea name="entry.302853416" class="box2 form-control" rows="5" minlength="5" maxlength="742" title="Brief about your idea" placeholder="Brief about your idea (Max 500 words)"></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input18"><b>What is stopping you start your own startup journey or challenges you are facing?</b></label><br>
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-4 form-group">
										<p class="p-0 m-0"><b>Social</b><span class="text-yellow">*</span></p>
										<p class="smalllable pb-4"><b>(Ex- Get support from family/abandon a job to start company)</b></p>
										<select class="form-control" name="entry.1760026360">
											<option value="Not at all a problem">Not at all a problem</option>
											<option value="A little problem but I can overcome this">A little problem but I can overcome this</option>
											<option value="A big problem">A big problem</option>
											<option value="Far too big problem">Far too big problem</option>
										</select>
									</div>
									<div class="col-md-4 form-group">
										<p class="p-0 m-0"><b>Financial</b><span class="text-yellow">*</span></p>
										<p class="smalllable pb-5"><b>(Access to funding)</b></p>
										<select class="form-control" name="entry.1492992120">
											<option value="Not at all a problem">Not at all a problem</option>
											<option value="A little problem but I can overcome this">A little problem but I can overcome this</option>
											<option value="A big problem">A big problem</option>
											<option value="Far too big problem">Far too big problem</option>
										</select>
									</div>
									<div class="col-md-4 form-group">
										<p class="p-0 m-0"><b>Business/Entrepreneurial</b><span class="text-yellow">*</span></p>
										<p class="smalllable"><b>(Lack of background knowledge, inexperience, marketing strategy, choosing the right team members)</b></p>
										<select class="form-control" name="entry.853413782">
											<option value="Not at all a problem">Not at all a problem</option>
											<option value="A little problem but I can overcome this">A little problem but I can overcome this</option>
											<option value="A big problem">A big problem</option>
											<option value="Far too big problem">Far too big problem</option>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group col-md-12 wow fadeInLeft m-0" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<label for="input16"><b>Would you like to become member?</b></label>
						</div>
						<div class="col-md-12 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
							<div class="row">
								<div class="col-md-4 form-group">
									<label for="makerslab"><input type="checkbox" name="entry.549595345" value="Makers Lab" checked="checked"> Makers Lab</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="yuva"><input type="checkbox" name="entry.549595345" value="Yuva entrepreneurship program"> Yuva Entrepreneurship Program</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="internship"><input type="checkbox" name="entry.549595345" value="Internship"> Internship</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 form-group">
									<label for="Events"><input type="checkbox" name="entry.549595345" value="Events"> Events</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="workshop"><input type="checkbox" name="entry.549595345" value="Workshops"> Workshops</label>
								</div>
								<div class="col-md-4 form-group">
									<label for="notinterested"><input type="checkbox" name="entry.549595345" value="Not Interested in Any"> Not interested in any</label>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-4 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input16"><b>Would you like to use the facilities at Makers Lab to build your projects more often?<span class="text-yellow">*</span></b></label>
									<select class="form-control" name="entry.884248663">
										<option value="Yes">Yes</option>
										<option value="No">No</option>
										<option value="Maybe">Maybe</option>
									</select>
								</div>
								<div class="form-group col-md-4 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input16"><b>If the answer is yes, how frequently would you want to come to Makers Lab?<span class="text-yellow">*</span></b></label>
									<select class="form-control" name="entry.657523506">
										<option value="Once a week">Once a week</option>
										<option value="Twice a week">Twice a week</option>
										<option value="Once in two weeks">Once in two weeks</option>
										<option value="Not interested">Not interested</option>
									</select>
								</div>
								<div class="form-group col-md-4 wow fadeInLeft" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s">
									<label for="input16"><b>Would you like to recommend/refer your friend to Yuva Entrepreneurship Program? <span class="text-yellow">*</span></b></label><br>
									<select class="form-control" name="entry.1993890758">
										<option value="Yes">Yes</option>
										<option value="No">No</option>
										<option value="Maybe">Maybe</option>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group col-md-12">
							<label for="input90"><b>Feedback or suggestion<span class="text-yellow">*</span></b></label>
							<textarea name="entry.2086941058" class="box2 form-control" rows="3" minlength="3" maxlength="742" title="Feedback or any suggestion" placeholder="Feedback or any suggestion" required="required"></textarea>
						</div>

						<div class="form-group col-lg-12">
							<!-- <span class="text-yellow"><b>*</b></span>
							<div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div><br> -->
							<span class="text-yellow"><h6><b>*</b> Fields are mandatory</h6></span>
							<input type="submit" class="btn custom-btn2 btn-warning" id="ss-submit" name="submit" value="Submit">
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	<br>
	<br>

	<script src='https://www.google.com/recaptcha/api.js'></script>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>

	<script type="text/javascript">
		$(function() {
			$('[name="entry.1882344902"]').on('click', function (e) {
				var val = $(this).val();
				if (val == "I have an Idea, but i don't know where to begin") {
					$('.txbx1').show('fade');
				}else {
					$('.txbx1').hide();
				};
			});
		});
	</script>
	<script>
		window.onload = function() {
			var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
			recaptcha.required = true;
			recaptcha.oninvalid = function(e) {
				alert("Please complete the captcha");
			}
		}
	</script>
</body>
</html>