<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sandbox Illuminate | Events, Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/sandbox-illuminate"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/sandbox-illuminate">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/illuminate.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/illuminate-bg.png">
	<meta property="og:description" content="Sandbox Illuminate invites entrepreneurs across India to come present their business ideas & win support from Deshpande Startups."/>
	<meta name="author" content="SaDeshpandendbox Startups"/>
	<meta name="description" content="Sandbox Illuminate invites entrepreneurs across India to come present their business ideas & win support from Deshpande Startups."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Sandbox Illuminate">
	<link rel="canonical" href="https://www.deshpandestartups.org/sandbox-illuminate">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/illuminate-bg.png" width="1349" height="198" alt="Deshpande Startups, events illuminate">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Sandbox Illuminate</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">SANDBOX </span>ILLUMINATE</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/illuminate.png" width="474" height="237" alt="Deshpande startups, illuminate-idea to execution">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center">Sandbox Illuminate</h5>
									<p><b>Date : </b>May 10<sup>th</sup> 2019</p>
									<!-- <p><b>Last Date to Register : </b>May 02<sup>nd</sup> 2019</p> -->
									<p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village,<br> Gokul Road, Hubballi, Karnataka.
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br>
										E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
									</p>
								</div>
								<div class="card-footer">
									<p class="text-yellow">The registrations has been closed.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-5 text-yellow"><b>Event Description:</b></p>
						<p class="text-justify wow slideInRight">Sandbox Illuminate invites entrepreneurs across India to come present their business ideas & win support from Deshpande Startups.</p>

						<p class="text-justify pt-3 wow slideInRight">We at Deshpande Startups provide a nurturing environment with active execution approach that challenges you to think quickly, critically and innovative about your business executions and resolve any problem with the help of expert advices.<br><br> If you have an idea or a solution to a problem and want to launch and grow your business, get access to a potential buffet of capital choices, along with a host of intangible benefits/supports including mentorship, expertise and networking. We are available to help you at every step of the entrepreneurial journey.</p>

						<p class="text-justify pt-3 wow slideInRight">Are you an aspiring entrepreneur and/or an early-stage startup looking for support in your business venture? Apply now to join Deshpande ecosystem.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<br>

	<br>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>