<!DOCTYPE html>
<html lang="en">
<head>
	<title>Embedded Engineer - Shopgro</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/shopgro-embedded-engineer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/shopgro-embedded-engineer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/shopgro-big.png">
	<meta property="og:description" content="We are looking for Embedded Engineer. Job Position: Embedded Engineer, Experience:  2+ years, Qualification: BE graduate preferably Electronics & Communication."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Embedded Engineer. Job Position: Embedded Engineer, Experience:  2+ years, Qualification: BE graduate preferably Electronics & Communication."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Embedded Engineer, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/shopgro-embedded-engineer">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Embedded</span> Engineer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Embedded Engineer<br>
					<strong>Startup:</strong> Shopgro<br>
					<strong>Qualification:</strong> BE graduate preferably Electronics & Communication<br>
					<strong>Experience:</strong> 2+ years of experience required<br>
					<strong>Job Location:</strong> Hubballi<br>
				</p>
			</div>
			<div class="col-md-6">
				<a href="https://www.shopgro.in/" target="_blank" rel="nofollow"><img src="img/career/shopgro-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, Shopgro" /></a>
			</div>
		</div>
		<!-- <br> -->

		<!-- <p class="text-justify pt-1"></p> -->

		<div class="row pt-2">
			<div class="col-md-12">
				<h3 class="text-yellow">Skills and Expertise:</h3>
				<ul>
					<li>comprehensive design and development ability</li>
					<li>Work experience in full lifecycle product development (Design, Develop, Test prototypes, Co-ordinate manufacture and after sales support)</li>
					<li>Work experience in Hardware Design and schematics development and debugging</li>
					<li>Work experience on Nordic SoC is must</li>
					<li>Knowledge of BLE (Bluetooth Low Energy) device must</li>
					<li>Work experience to use power supply aspects</li>
					<li>Knowledge of SPI and I2C must</li>
					<li>Hands-on experience in prototyping platforms like Arduino and Raspberry Pi will be added advantage</li>
					<li>Experience in working with PIC microcontrollers</li>
					<li>Solid understanding of core concepts including Power Electronics, Analogue Circuits, Microcontrollers, Embedded C & C ++ etc.</li>
					<li>Creativity and analytical skills ability to communicate technical knowledge in a clear and understandable manner</li>
					<li>Organized and focused on delivering tasks within defined deadlines</li>
					<li>Strive for excellence. Perform at the highest levels of accuracy and efficiency</li>
					<li>Strong commitment to improve things every day. Seek continuous feedback</li>
				</ul>
			</div>
		</div>

	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to <br>E: <a href="mailto: uday&#064;shopgro&#046;in"> uday&#064;shopgro&#046;in</a></b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>