<!DOCTYPE html>
<html lang="en">
<head>
	<title>Career | Deshpande Startups - Current openings at Deshpande startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/career"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/career">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/career-bg-img.png">
	<meta property="og:description" content="We at Deshpande Startups are on a mission to support Entrepreneurs across domain. We are building an ecosystem that enables ideas and entrepreneurs to complement each other to create large impact."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content= "We are building an ecosystem that enables ideas and entrepreneurs to complement each other to create large impact."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Business development manager, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Deshpande Startups career opportunities">
	<link rel="canonical" href="https://www.deshpandestartups.org/career">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		.gray{background-color: #403b3b;}
		.white{color:#fff;}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<a href="career-opportunities" target="_blank"><img class="carousel-inner img-fluid" src="img/career/career-bg-img.png" width="1349" height="200" alt="Deshpande Startups deshpande foundation"></a>
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Career at Deshpande Stratups</li>
		</ol>
	</nav>

	<div class="container">
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">CURRENT</span> OPENINGS</h2>
			<div class="divider b-y text-yellow content-middle"></div>
			<br>
			<p>
				We at Deshpande Startups are on a mission to support Entrepreneurs across domain.  We are building an ecosystem that enables ideas and entrepreneurs to complement each other to create large impact. We started the Hubballi Deshpande Startups to support mission-driven Startups to come up with sustainable and scalable business.
			</p>
		</div>
	</div>
	
	<div class="container pt-3" id="contact-info">
		<div class="row">
			<div class="col-md-12">
				<table class="table table-responsive-md table-responsive-sm w3-card">
					<tbody>
						<tr class="tit gray" >
							<td class="white">#</td>
							<td class="white">Program</td>
							<td class="white">Current Openings</td>
							<td class="white">No. of Positions</td>
							<td class="white">Experience</td>
							<td class="white">Location</td>
							<td class="white">Job Description</td>
							<!-- <td class="white">Application Form</td> -->
						</tr>
						<tr class="red box card-hover-shadow">
							<td>1</td>
							<td>Deshpande Startups</td>
							<td>CEO</td>
							<td class="text-center">1</td>
							<td></td>
							<td>Hubballi</td>
							<td><a href="fssi-ceo" class="btn btn-warning btn-sm" target="_blank">View Details</a></td>
							<!-- <td><a href="career-opportunities" class="btn btn-warning btn-md" target="_blank">Apply Now</a></td> -->
						</tr>
						<tr class="red box card-hover-shadow">
							<td>2</td>
							<td>Deshpande Startups</td>
							<td>Sr. Director</td>
							<td class="text-center">1</td>
							<td>12+ years</td>
							<td>Hubballi</td>
							<td><a href="sr-director" class="btn btn-warning btn-sm" target="_blank">View Details</a></td>
							<!-- <td><a href="career-opportunities" class="btn btn-warning btn-md" target="_blank">Apply Now</a></td> -->
						</tr>
						
						<!-- <tr class="red box card-hover-shadow">
							<td>3</td>
							<td>ESDM Cluster</td>
							<td>Technical Sales Engineer</td>
							<td class="text-center">1</td>
							<td>2 - 3 years</td>
							<td>Bengaluru</td>
							<td><a href="sales-engineer" class="btn btn-warning btn-sm" target="_blank">View Details</a></td>
						</tr> -->
						
						<!-- <tr class="red box card-hover-shadow">
							<td>4</td>
							<td>Makers Lab</td>
							<td>Makers Lab Associate</td>
							<td class="text-center">1</td>
							<td>1 - 2 years</td>
							<td>Hubballi</td>
							<td><a href="makerslab-associate" class="btn btn-warning btn-sm" target="_blank">View Details</a></td>
						</tr> -->
						
					</tbody>
				</table>
				<br>
			</div>
		</div>
		<br><br>
		<br>
	</div>

	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>