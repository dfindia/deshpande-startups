<!DOCTYPE html>
<html lang="en">
<head>
	<title>Android Developer - Nautilus</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/nautilus-android-developer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/nautilus-android-developer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/nautilus-big.png">
	<meta property="og:description" content="We are looking for Android Developer. Job Position: Android Developer, Experience: 3-4 years, Qualification: Bachelor’s degree or equivalent."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Android Developer. Job Position: Android Developer, Experience: 3-4 years, Qualification: Bachelor’s degree or equivalent."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Android Developer, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/nautilus-android-developer">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Android</span> Developer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Android Developer<br>
					<strong>Startup:</strong> Nautilus Hearing Solutions Pvt. Ltd.<br>
					<strong>Qualification:</strong> Bachelor’s degree or equivalent<br>
					<strong>Experience:</strong> 3-4 years of experience required<br>
					<strong>Career Level:</strong> Senior<br>
					<strong>Job Location:</strong> Hubballi
				</p>
			</div>
			<div class="col-md-6">
				<a href="http://www.nautilushearing.com/" target="_blank" rel="nofollow"><img src="img/career/nautilus-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, Nautilus Hearing Solutions Pvt. Ltd."></a>
			</div>
		</div>
		<p class="text-justify pt-1">We are hiring an android and/or hybrid mobile application developer for full time occupation to join our software development department. Contribute on continues development and support of mobile applications for our core company products.</p>
		<div class="row pt-2">
			<div class="col-md-12">
				<h3 class="text-yellow">Job Responsibilities:</h3>
				<ul class="text-justify">
					<li>Write effective, scalable code following coding patterns</li>
					<li>Test and debug mobile applications</li>
					<li>Cooperate with product owners coordinate with internal teams to understand user requirements and provide technical solutions</li>
					<li>Working directly with developers and product managers to conceptualize, build, test and release products</li>
					<li>Gather requirements around functionality and translate those requirements into elegant functional solutions</li>
					<li>Build prototypes at tech scoping stage of projects</li>
					<li>Working with the front end developers to build the interface with focus on usability features</li>
					<li>Create compelling device specific user interfaces and experiences</li>
					<li>Standardizing the platform and in some cases apps to deliver across multiple brands with minimal duplication of effort</li>
					<li>Optimizing performance for the apps Keep up to date on the latest industry trends in the mobile technologies</li>
					<li>Explain technologies and solutions to technical and non-technical stakeholders, attend industry events/conference - both attending and presenting</li>
				</ul>
			</div>
		</div>
		<!-- <br> -->
		<div class="row pt-2">
			<div class="col-md-6">
				<h3 class="text-yellow">Skills and Expertise:</h3>
				<ul>
					<li>Strong OO design and programming skills in Java</li>
					<li>Familiar with the Android SDK</li>
					<li>Knowledge of Firebase, SQLite, MySQL or similar database management system</li>
					<li>Excellent knowledge in core Java</li>
					<li>Hands on experience in the full life cycle of Android application development</li>
					<li>Knowledge of Android SDK's</li>
					<li>Push Notifications & Toast</li>
					<li>Experience in using Android Studio</li>
					<li>Experience in Gradle</li>
				</ul>
			</div>
			<div class="col-md-6">
				<h3 class="text-yellow">Good to Have:</h3>
				<ul>
					<li>Experience in payment gateway integration in mobile apps is a big plus</li>
					<li>A portfolio of Android apps in the play store/app store is a plus</li>
					<li>Hands on experience with Android studio & Gradle build system</li>
					<li>UI & UX experience</li>
					<li>Locations and Map API's</li>
					<li>Knowledge of using Version Control</li>
					<li>Excellent debugging and optimization skills</li>
					<li>Knowledge about JSON, Python, AI & ML systems etc</li>
				</ul>
			</div>
		</div>

	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to<br> E:<a href="mailto: uday&#064;nautilushearing&#046;com"> uday&#064;nautilushearing&#046;com</a></b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>