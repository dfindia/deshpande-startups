<!DOCTYPE html>
<html lang="en">
<head>
	<title>Makers Lab Associate | Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/makerslab-associate"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/makerslab-associate">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/team-image.png">
	<meta property="og:description" content="Job Position: Makers Lab Associate, Experience:  1 - 2 years, Education: BE (Mechanical), Job Location: Hubballi"/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Job Position: Makers Lab Associate, Experience:  1 - 2 years, Education: BE (Mechanical), Job Location: Hubballi"/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Makers Lab Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Makers Lab Associate, Current openings at Deshpande startups">
	<link rel="canonical" href="https://www.deshpandestartups.org/makerslab-associate">
	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Makers Lab</span> Associate</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="pull-right"><a href="career-opportunities" class="btn btn-warning btn-md" target="_blank">Apply Now</a></div>
		<p class="text-justify"><strong>Job Position:</strong> Makers Lab Associate<br>
			<strong>Organization:</strong> Deshpande Startups<br>
			<strong>Department Name:</strong> Makers Lab<br>
			<strong>Experience:</strong> 1 - 2 years of experience<br>
			<strong>Education:</strong> BE (Mechanical)<br>
			<strong>Job Location:</strong> Hubballi<br>
			<strong>Tentative Date of Joining:</strong> Immediate
		</p>
		<h3  class="text-yellow">Job Description:</h3>
		<ul>
			<li>Mechanical Designing, Operating & Handling 3D Printers (Accountable for 3D Printing Section), supporting in operating other machinery's of makers lab</li>
			<li>Training on Designing, 3D Printing & Mechanical, coordinate other workshops and managing events of program</li>
			<li>Identify potential aspiring young startups to create & nurture from Idea to PoC</li>
			<li>Engaging engineering students & building makers community (Students & Professionals)</li>
			<li>Technical support to early product startups</li>
			<li>Manage projects to assure on time execution and delivery</li>
			<li>Monitor machines and equipment performance and develop improvements to enhance machines & equipment's reliability and scalability</li>
			<li>Sharing of periodic reports</li>
			<li>Perform such other duties as required and appropriate to the objectives of this position and the needs of the organization</li>
		</ul>
		<br>
		<h3  class="text-yellow">Knowledge, Skills & Abilities:</h3>
		<ul>
			<li>Skills on designing and machine operations (Solid works, Edge & CATIA)</li>
			<li>Sound knowledge of MS-Office (Power point, XL and Word)</li>
			<li>Excellent communication skills (Verbal & Written, English/Kannada/Hindi)</li>
			<li>Organized with good attention to detail</li>
			<li>A self-starter who can spot new opportunities</li>
			<li>Able to work as part of wide and varied team</li>
			<li>Focus on continuous quality improvement with creative approach</li>
			<li>Knowledge on machine maintenance like preventive, breakdown maintenance & predictive maintenance</li>
		</ul>
	</div>
	<br>

	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>