<!DOCTYPE html>
<html lang="en">
<head>
	<title>Incubation Support - Are you looking for Incubation support?</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/incubation-support"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/incubation-support">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/incubation/incubation-support-bg.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/incubation/incubation-services.jpg">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/incubation/incubation-impact.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/incubation/journey.png">
	<meta property="og:description" content="We invite Entrepreneurs to test, pilot and validate their ideas and build successful ventures. Startups can leverage an ecosystem of resources, world-class infrastructure, mentorship from experts, networking, knowledge from industry leaders and peers, talent, and fund."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We invite Entrepreneurs to test, pilot and validate their ideas and build successful ventures. Startups can leverage an ecosystem of resources, world-class infrastructure, mentorship from experts, networking, knowledge from industry leaders and peers, talent, and fund."/>
	<!-- <meta name="keywords" content="Networking, knowledge, talent, finance and mentors, Market validation platform, Makers Lab &amp; ESDM Cluster,Industry connects, validate Business Solutions or Productsm, Mentors connects, In-House seed funding, Access to VCs, Our journey and impact"/> -->
	<meta property="og:title" content="Are you looking for Incubation support">
	<link rel="canonical" href="https://www.deshpandestartups.org/incubation-support">

	<style type="text/css">
		.vl {
			border-right: 1px solid #ec9520;
			/* border-right: 3px solid #df2d2c; */
			height: auto;
			/* height: 265px; */
		}
		#feedback a {
			display: block;
			background: #E66425;
			height: 40px;
			padding-top: 5px;
			width: 155px;
			text-align: center;
			color: #fff;
			font-family: Arial, sans-serif;
			/*font-size: 17px;*/
			font-weight: bold;
			text-decoration: none;
		}

		#feedback {
			height: 0px;
			width: 0px;
			/*width: 85px;*/
			position: fixed;
			/*right: 0;*/
			top: 50%;
			z-index: 998;
			transform: rotate(-90deg);
			-webkit-transform: rotate(-90deg);
			-moz-transform: rotate(-90deg);
			-o-transform: rotate(-90deg);
		}
		ul{
			margin:0;
			padding:0;
			list-style:none;
			line-height: 32px
		}
		.lh{
			line-height: 45px;
		}
		.parallax {
			background-image: url("img/makers/deshpande.jpg");
			min-height: 300px; 
			background-attachment: fixed;
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
		}


		/*sectors*/
		.wrap {
			display: flex;
			background: white;
			padding: 1rem 1rem 1rem 1rem;
			border-radius: 0.5rem;
			box-shadow: 7px 7px 30px -5px rgba(0,0,0,0.1);
			margin-bottom: 2rem;
		}
		.wrap:hover {
			/*background: linear-gradient(135deg,#6394ff 0%,#0a193b 100%);*/
			background: linear-gradient(135deg,#f9ebe0 0%,#d5ddef 100%);
			color: #eb7c26;
			border: 1px solid #dad9d9;
		}
		.ico-wrap {
			margin: auto;
		}

		.mbr-iconfont {
			font-size: 4.5rem !important;
			color: #313131;
			margin: 1rem;
			padding-right: 1rem;
		}
		.mbr-section-title3 {
			text-align: left;
		}



		/*engage with us - mentors*/
		.single-service {
			border: 1px solid #e6e3e3;
			/*border: 1px solid #eee;*/
			padding: 30px 10px;
			position: relative;
			text-align: center;
			margin-bottom: 30px;
			background-color: #fff;
		}
		.single-service i.fa {
			width: 60px;
			height: 60px;
			/*background-color: #e66329;*/
			background-color: #5a5a5a;
			font-size: 25px;
			color: #fff;
			line-height: 60px;
			text-align:center;
			margin-bottom:20px;
		}
		.single-service i.fa {
			-webkit-transition: .4s;
			transition: .4s;
		}
		.single-service:hover i.fa {
			border-radius: 50%;
		}
		.single-service h4 {
			text-transform: capitalize;
			font-size: 22px;
			margin-bottom: 10px;
			font-weight: 500;
			color: #5a5a5a;
		}
		.single-service ul{
			color: #5a5a5a;
			text-align: left;
		}


		/*engage with us - statups*/

		.text{
			/*color: rgba(31,181,172,.9);*/
			text-align: center;
		}
		.folded-corner:hover .text{
			visibility: visible;
			color: #000000;;
		}
		/* nav link items  */
		.folded-corner{
			padding: 25px 25px;
			position: relative;
			font-size: 90%;
			text-decoration: none;
			color: #5a5a5a; 
			background: #ffffff;
			transition: all ease .5s;
			/*border: 1px solid rgba(31,181,172,.9);*/
			border: 1px solid rgb(88, 88, 90);
			height: 231px;
		}
		.folded-corner a{
			color: #5a5a5a;
		}
		.folded-corner a:hover{
			text-decoration: none;
		}
		.folded-corner:hover{
			/*background-color: rgba(31,181,172,.9);*/
			background-color: rgb(239, 235, 235);
		}
		/* paper fold corner */
		.folded-corner:before {
			content: "";
			position: absolute;
			top: 0;
			right: 0;
			border-style: solid;
			border-width: 0 0px 0px 0;
			border-color: #ddd #000;
			transition: all ease .3s;
		}
		/* on li hover make paper fold larger */
		.folded-corner:hover:before {
			background-color: #D00003;
			border-width: 0 50px 50px 0;
			border-color: #c5bdbd #000;
			/*border-color: #c5bdbd #eb7c26;*/
		}
		.service_tab_1:hover .fa-icon-image{
			color: #000;
			transform: scale(1.5);
			/*transform: rotate(360deg) scale(1.5);*/
		}
		.fa-icon-image{
			color: rgb(235, 124, 38);
			/*color: rgba(31,181,172,.9);*/
			text-align: center;
			transition: all 1s cubic-bezier(.99,.82,.11,1.41);
		}

	</style>
	<?php
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/incubation/incubation-support-bg.png" width="1349" height="400" alt="Deshpande Startups, Incubation Support">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Incubation Support</li>
		</ol>
	</nav>
	
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col-md-12">
				<i class="fa fa-quote-left fa-4x pb-2"></i>
				<h3 class="text-yellow wow zoomIn"><b>Deshpande Startups offers startups a strategic gateway to Bharat – rural and<br> semi-urban India which accounts for 70% of India’s population.</b></h3>
				<h3 class="wow zoomIn">Our incubation is driven by our vision and commitment to co-create solutions<br> and bring global solutions for local problems.</h3>
				<div class="divider b-y pt-2 text-yellow content-middle"></div>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<p class="text-justify wow slideInLeft">The Incubation program at Deshpande Startups aligned with the vision to promote Entrepreneurship in Non-metros, the incubation program nurtures and supports startups that address the real problems and aspire to impact 1.1 billion lives in Bharat.</p>
				<p class="text-justify wow slideInLeft">Deshpande Startups works with startups from Bharat and Startups that are building for Bharat. The ecosystem nurtured by the foundation gives startups a ready launch pad to access farmers, students, micro entrepreneurs and networks that can enable quicker growth.</p>
				<p class="text-justify wow slideInLeft">The program has been designed keeping in mind three critical needs of startups Mentor, Market and Money. The program enables startups to progress from stage to stage by receiving timely advice, market penetration support, funding connects and access to infrastructure.</p>

				<div class="row">
					<div class="col-md-5 pt-3 wow zoomIn">
						<h4 class="text-center text-yellow pb-2">
							Incubation at Deshpande Startups
						</h4>
						<!-- <br> -->
						<ul class="lh">
							<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Deep connects in Bharat ecosystem</li>
							<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Business enablement support</li>
							<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Access to experienced domain and business mentors</li>
							<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Strong networks at field, national, international levels</li>
							<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Coaching to be funding ready </li>
							<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> World class infrastructure</li>
							<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Receive benefits from our partner institutions</li>
						</ul>
					</div>
					<div class="col-md-7 p-3 wow zoomIn bg-grey1">
						<h4 class="text-center text-yellow pb-2">
							Sectors
						</h4>
						<div class="row mbr-justify-content-center">

							<div class="col-lg-6 mbr-col-md-10">
								<div class="wrap">
									<div class="ico-wrap">
										<span class="mbr-iconfont fa fa-leaf"></span>
									</div>
									<div class="text-wrap ico-wrap">
										<h4 class="mbr-fonts-style font-weight-bold mbr-section-title3">Agri-tech</h4>
										<!-- <p class="mbr-fonts-style text1 mbr-text display-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p> -->
									</div>
								</div>
							</div>
							<div class="col-lg-6 mbr-col-md-10">
								<div class="wrap">
									<div class="ico-wrap">
										<span class="mbr-iconfont fa fa-medkit"></span>
									</div>
									<div class="text-wrap ico-wrap">
										<h4 class="mbr-fonts-style font-weight-bold mbr-section-title3">Rural <br>Health-tech</h4>
										<!-- <p class="mbr-fonts-style text1 mbr-text display-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p> -->
									</div>
								</div>
							</div>
							<div class="col-lg-6 mbr-col-md-10">
								<div class="wrap">
									<div class="ico-wrap">
										<span class="mbr-iconfont fa-book fa"></span>
									</div>
									<div class="text-wrap ico-wrap">
										<h4 class="mbr-fonts-style font-weight-bold mbr-section-title3">Edu-tech</h4>
										<!-- <p class="mbr-fonts-style text1 mbr-text display-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p> -->
									</div>
								</div>
							</div>
							<div class="col-lg-6 mbr-col-md-10">
								<div class="wrap">
									<div class="ico-wrap">
										<span class="mbr-iconfont fa-cogs fa"></span>
									</div>
									<div class="text-wrap ico-wrap">
										<h4 class="mbr-fonts-style font-weight-bold mbr-section-title3">Rural Innovations</h4>
										<!-- <p class="mbr-fonts-style text1 mbr-text display-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p> -->
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<br>

	<h2 class=" text-yellow text-center wow slideInDown pt-2" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted"> THE INCUBATION</span> MODEL</h2>
	<div class="divider b-y text-yellow content-middle"></div>
	<img src="img/incubation/incubation-model.png" class="img-fluid img" width="100%" height="400" alt="Deshpande startups, Incubation Model">
	<!-- <img src="img/incubation/testimonial/model-demo.png" class="img-fluid img" width="100%" height="400" alt="Deshpande startups, Benefits of Incubation"> -->
	<br>

<div class="featured-bg-container">
	<div class="container pb-4">
		<h2 class=" text-yellow text-center wow slideInDown pt-2" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-white"> ENGAGE</span> WITH US</h2>
		<div class="divider b-y text-yellow content-middle"></div>
		<div class="row">
			<div class="col-md-12 mx-auto text-center">
				<h4 class="text-yellow">Startups</h4>
				<p>Have an idea to impact Bharat? Join us to accelerate your enterprise</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mt-3">
				<div class="folded-corner service_tab_1">
					<a href="incubation-support-form" target="_blank">
						<div class="text">
							<i class="fa fa-pencil-square-o fa-5x fa-icon-image"></i>
							<h3 class="pt-4 pb-3"> Apply online</h3>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mt-3">
				<div class="folded-corner service_tab_1">
					<div class="text">
						<i class="fa fa-paper-plane fa-5x fa-icon-image" ></i>
						<h3 class="pt-4 pb-3"> Get selected to pitch</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mt-3">
				<div class="folded-corner service_tab_1">
					<div class="text">
						<i class="fa fa-laptop fa-5x fa-icon-image"></i>
						<h3 class="pt-4 pb-3"> Pitch to experts</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mt-3">
				<div class="folded-corner service_tab_1">
					<div class="text">
						<i class="fa fa-rocket fa-5x fa-icon-image"></i>
						<h3 class="pt-4 pb-3"> Get onboarded</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
			<div class="col-md-12 mx-auto text-center">
				<h4 class="text-yellow">Mentors</h4>
				<p>We welcome experts and mentor engage with our startups to support their script their growth stories. </p>
			</div>
			<div class="row justify-content-md-center">

				<div class="col-lg-4 col-md-4 col-sm-6">
					<div class="single-service pt-3 pl-4">
					<h4>We look for</h4>
					<ul class="mb-0">
						<li>•	Domain mentors</li>
						<li>•	Business experts</li>
						<li>•	Successful entrepreneurs</li>
						<li>•	Technology and product experts</li>
						<li>•	Thought leaders</li>
					</ul>
				</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="single-service">
						<i class="fa fa-user" aria-hidden="true"></i>
						<h4>Express interest to be a mentor</h4>
						<!-- <p></p> -->
						<p><a href="expertise" class="text-yellow font-weight-bold" target="_blank">Know more >></a></p>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="single-service">
						<i class="fa fa-users"></i>
						<h4>Our current <br>mentors</h4>
						<!-- <p></p> -->
						<p><a href="mentors" class="text-yellow font-weight-bold" target="_blank">Know more >></a></p>
					</div>
				</div>
			</div>
		</div>
	</div>



	<!-- <section class="bg-grey1 pt-4 pb-3" id="services">
		<div class="container">
			<div class="col-md-12 mx-auto text-center">
				<h4 class="text-yellow">Mentors</h4>
				<p>We welcome experts and mentor engage with our startups to support their script their growth stories. </p>
			</div>
			<div class="row justify-content-md-center">
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="single-service">
						<i class="fa fa-user" aria-hidden="true"></i>
						<h4>Express interest to be a mentor</h4>
						<p><a href="expertise" class="text-yellow font-weight-bold" target="_blank">Know more >></a></p>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="single-service">
						<i class="fa fa-users"></i>
						<h4>Our current <br>mentors</h4>
						<p><a href="mentors" class="text-yellow font-weight-bold" target="_blank">Know more >></a></p>
					</div>
				</div>
			</div>
		</div>
	</section>
 -->


	
	<!-- <br> -->
	<div class="parallax inverse-text" data-parallax-img="img/makers/deshpande.jpg" data-parallax-img-width="1920" data-parallax-img-height="1078">

		<h2 class="text-yellow wow slideInDown text-center pt-4" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">TESTIMONIALS</h2>
	<div class="divider b-y text-yellow content-middle"></div>
		<!-- <br> -->
		<div id="carousel">
			<div class="container text-yellow pt-4 pb-5">
				<div class="row justify-content-md-center">
					<div class="col-md-10">
						<!-- <div class="quote text-white"><i class="fa fa-quote-left fa-4x"></i></div> -->
						<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
							<!-- Carousel indicators -->
							<ol class="carousel-indicators">
								<li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>
								<li data-target="#fade-quote-carousel" data-slide-to="1"></li>
								<li data-target="#fade-quote-carousel" data-slide-to="2"></li>
								<li data-target="#fade-quote-carousel" data-slide-to="3"></li>
									<li data-target="#fade-quote-carousel" data-slide-to="4"></li>
							</ol>
							<!-- Carousel items -->
							<div class="carousel-inner">
								<div class="item carousel-item active">
									<div class="profile-circle">
										<img src="img/incubation/testimonial/sandeep.png" width="100" height="100" class="img img-fluid" alt="Sandeep Kondaji Founder and CEO, Krishitantra ">
									</div>
									<p class="text-center">Sandeep Kondaji <br><span class="text-yellow">Founder and CEO, Krishitantra</span></span></p>
									<blockquote>
										<p> The backing from Deshpande Startups has helped Krishitantra emerge as a brand and first-hand experience of customer by being our pilot customer. It was a very big support for the initial customers acquisition, through it’s wide networking around Farmer Producer Organizations. This helped us to take off the product very quickly to the market from the prototype stage. The program has helped us grow reach out to investors and mentored us through our GTM.</p>
									</blockquote>
								</div>
								<div class="item carousel-item">
									<div class="profile-circle">
										<img src="img/incubation/testimonial/uday.png" width="100" height="100" class="img img-fluid" alt="Uday Raga Kiran Founder, Nautilus Hearing">
									</div>
									<p class="text-center">Uday Raga Kiran <br><span class="text-yellow">Founder, Nautilus Hearing</span></p>
									<blockquote>
										<p>My Entrepreneurship Journey began at Deshpande Startups, I had great learnings from inspiring individuals, I have been able to refine my idea and made our offering stronger. Through the support our Nautilus has developed product, raise seed round funding and launch our product in the market. What I love most about the Deshpande Startups is that it is like someone from your own team. DS provides all the support required for an aspiring Entrepreneur to build his company from idea to product. The facilities like 3d Printing , PCB assembly services, Makers Lab etc helped me speed up the process. Add to all the support is the mentorship which has helped me and can help many more startups grow.</p>
									</blockquote>
								</div>
								<div class="item carousel-item">
									<div class="profile-circle">
										<img src="img/incubation/testimonial/rohini.png" width="100" height="100" class="img img-fluid" alt="Rohini Co-Founder, Widemobility">
									</div>
									<p class="text-center">Rohini<br><span class="text-yellow"> Co-founder, Widemobility</span></p>
									<blockquote>
										<p>The incubation program of DS is a must for all startups who are looking at making a difference with their idea/PoC/Product! The help/guidance/attention/connections provided to us, the Account Manager assigned to us, have given a direction to what we must do!.</p>
									</blockquote>
								</div>
								<div class="item carousel-item">
									<div class="profile-circle">
										<img src="img/incubation/testimonial/printalytics.png" width="100" height="100" class="img img-fluid" alt="Printalytix">
									</div>
									<p class="text-center">Team<br><span class="text-yellow">Printalytix</span></p>
									<blockquote>
										<p>The truth of entrepreneurship can be found in the aspects of an individual's passion for his venture, discipline to build the company, ability to innovate solutions through which the venture can flourish - all of this with an invaluable support system in terms of mentorship, startups ecosystem connect, industry connect, funding opportunities and much more, makes for an unique incubation center - The Deshpande Startups in its true essence is more than <br> A Living Laboratory of Entrepreneurs</p>
									</blockquote>
								</div>
								<div class="item carousel-item">
									<div class="profile-circle">
										<img src="img/incubation/testimonial/navjeet.png" width="100" height="100" class="img img-fluid" alt="Navajith Co-Founder, Rapture Innovations">
									</div>
									<p class="text-center">Navajith<br><span class="text-yellow"> Co-founder, Rapture Innovations</span></p>
									<blockquote>
										<p width="">I'm absolutely delighted to be a part of the incubation program at DS. Being a tech innovator it is vital for me to learn and get involved in business, customer and market aspects to succeed. Having been guided and supported by a diverse, well versed, highly experienced mentors was the best part of the program. The way they analyze, breakdown, and give suggestions for business problems is always enlightening, inspiring and has greatly improved my thought process and perspective. There's a rich pool of startup enablers and domain specific mentors which is otherwise hard to access. Hardware is hard but thanks to the MakersLabs and ESDM facility things become easier due to access to Rapid prototyping resources like 3D printers, electronics & mechanical tools, equipment and services. There's a great opportunity for peer collaboration and learning from fellow entrepreneurs. All in all DS is very supportive and nurturing ecosystem driven by amazing people who always go an extra mile to support me in my entrepreneurial journey.</p>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container --> 
	</div>
	<br>

<h2 class=" text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">OUR</span> JOURNEY</h2>
	<div class="divider b-y text-yellow content-middle"></div>
	<img src="img/incubation/our-journey.png" class="img-fluid img" width="100%" height="400" alt="Deshpande startups journey">
	<br>
	<br>

	<!--<h2 class=" text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">OUR INCUBATED</span> STARTUPS</h2>
	<div class="divider b-y text-yellow content-middle"></div>
	<div class="text-center">
	<img src="img/incubation/testimonial/startups-demo.png" width="100%" height="400" class="img img-fluid" alt="Deshpande Startup, Incubated Startups">
</div>-->
	<!-- <img src="img/incubation/incubation-impact.png" class="img-fluid img" width="100%" height="400" alt="Deshpande startups impact"> -->
	<!--<br>-->
	<!-- <br> -->

	<!--<br>-->


	<div id="feedback">
		<a href="incubation-support-form" target="_blank">Apply Now</a>
	</div>
	<!-- <br> -->

	<!-- <hr class="featurette-divider-sm"> -->

	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>