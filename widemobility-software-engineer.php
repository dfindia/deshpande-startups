<!DOCTYPE html>
<html lang="en">
<head>
	<title>Software Engineer - Widemobility</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/widemobility-software-engineer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/widemobility-software-engineer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/widemobility-big.png">
	<meta property="og:description" content="We are looking for Software Engineer. Job Position: Software Engineer, Experience:  2+ years, Qualification: B.E in Computer Science/MCA or equivalent."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Software Engineer. Job Position: Software Engineer (Fullstack), Experience:  2+ years, Qualification: B.E in Computer Science/MCA or equivalent."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Software Engineer, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/widemobility-software-engineer">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Software</span> Engineer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class="text-justify"><strong>Job Position:</strong> Software Engineer<br>
					<strong>Startup:</strong> Widemobility<br>
					<strong>Qualification:</strong> B.E in Computer Science/MCA or equivalent<br>
					<strong>Experience:</strong> 2+ years of software development experience required<br>
					<strong>Job Location:</strong> Hubballi<br>
				</p>
			</div>
			<div class="col-md-6">
				<a href="http://widemobility.com/" target="_blank" rel="nofollow"><img src="img/career/widemobility-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, Widemobility"></a>
			</div>
		</div>
		<!-- <br> -->
		<div class="row pt-2">
			<div class="col-md-6">
				<h3 class="text-yellow">Job Requirements:</h3>
				<ul>
					<li>Proficiency in C, C++, Python, Linux and Windows</li>
					<li>Passion for best design and coding practices</li>
					<li>Attitude towards learning and bettering one’s performance</li>
					<li>Willingness to work in a Startup</li>
					<li>Self-motivated</li>
				</ul>
			</div>
			<div class="col-md-6">
				<h3 class="text-yellow">Job Responsibilities:</h3>
				<ul>
					<li>Understanding requirements and functional specifications</li>
					<li>Ensuring foolproof performance of the deliverable</li>
					<li>Taking up responsibility of the task given and ensuring completion</li>
					<li>Innovate and contribute to continuous improvement of best practices in areas of software development and testing</li>
					<li>Regular updating on the project status</li>
				</ul>
			</div>
		</div>

		</div>
		<br>

		<div class="container cal">
			<!-- <p class="text-center"><b>Interested candidates email Resumes to <a href="mailto: rohini&#064;widemobility&#046;com"> rohini&#064;widemobility&#046;com</a> with subject as job title you are applying. </b></p> -->
			<p class="text-center"><b>Interested candidates email Resumes to<br> E:<a href="mailto: rohini&#064;widemobility&#046;com"> rohini&#064;widemobility&#046;com</a></b></p>
		</div>
		<br>
		<?php
		require_once 'essentials/footer.php';
		require_once 'essentials/copyright.php';
		require_once 'essentials/js.php';
		?>
	</body>
	</html>