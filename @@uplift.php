<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Events | Uplift</title>
      <?php
         require_once 'essentials/meta.php';
         ?>
      <meta name="linkage" content="https://www.deshpandestartups.org/uplift"/>
      <meta property="og:site_name" content="Deshpande Startups"/>
      <meta property="og:type" content="website">
      <meta property="og:url" content="https://www.deshpandestartups.org/uplift">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/events/uplift1.jpg">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/events/uplift-bg-2.jpg">
      <meta property="og:description" content="We are hosting a platform for all the mission driven entrepreneurs across India to pitch their business idea to access support from Deshpande Startups in scaling their business ventures."/>
      <meta name="author" content="Deshpande Startups"/>
      <meta name="description" content="We are hosting a platform for all the mission driven entrepreneurs across India to pitch their business idea to access support from Deshpande Startups in scaling their business ventures."/>
      <!-- <meta name="keywords" content=""/> -->
      <meta property="og:title" content="Sandbox Uplift">
      <!-- <link rel="canonical" href="https://www.deshpandestartups.org/uplift"> -->
      <?php
         require_once 'essentials/bundle.php';
         ?>
   </head>
   <body>
      <?php
         require_once 'essentials/title_bar.php';
         require_once 'essentials/menus.php';
         ?>
      <img class="carousel-inner img-fluid" src="img/events/uplift-bg-2.jpg" width="1349" height="198" alt="Deshpande Startups, events Sandbox Uplift">
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb justify-content-end">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item"><a href="events">Events</a></li>
            <li class="breadcrumb-item active" aria-current="page">Sandbox Uplift</li>
         </ol>
      </nav>
      <div class="container">
         <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
            <h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted"> SANDBOX</span> UPLIFT</h2>
            <div class="divider b-y text-yellow content-middle"></div>
         </div>
         <div class="row">
            <div class="col-md-12 px-5">
               <div class="row">
                  <div class="row">
                     <div class="col-md-12">
                        <!-- <div class="col-md-12"> -->
                        <!-- <p class="text-justify wow slideInLeft">An initiative of <b>“Yuva Entrepreneurship Program”</b> of Deshpande Startups present <b>“Ideathon”</b>, a platform to present your idea. An opportunity for young minds to pitch their ideas and meet young aspiring entrepreneurs. Be a part of this exclusive event and sense the vibrant startup ecosystem to become India's propelling true growth story and get selected for <b>Yuva Entrepreneurship Program</b> to kick start your startup journey with likeminded entrepreneurial catalysts.</p> -->
                        <!-- <p class="pt-5 text-yellow text-justify wow slideInLeft"><b>Why Uplift? </b></p> -->
						<h4 class="pt-5 text-yellow text-justify wow slideInLeft pb-2">Why Uplift?</h4>
                        <p class="text-justify wow slideInLeft">Sandbox UPLIFT is an initiation of Deshpande Startups to identify and support innovators and entrepreneurs aspiring to impact the bottom 1.1 billion people of Bharat. Deshpande Startups offers entrepreneurs a strategic gateway to Bharat - non-metro India, which accounts for over 70% of India’s population. Our population program is driven by our vision and commitment to co-create global solutions for local problems. Our support is across stages and sectors through an ecosystem of mentors, industry experts, influencers, entrepreneurs, and investors to help startups become sustainable.</p>
                        <!-- <p class="text-justify wow slideInLeft"><b>Event dates and deadlines:</b></p> -->
                        <!-- <p class="text-justify wow slideInRight"><i class="fa fa-arrow-right text-primary" aria-hidden="true"></i> Event Date : <b>31<sup>st</sup> December 2020</b><br> -->
                        <!-- <i class="fa fa-arrow-right text-primary" aria-hidden="true"></i> Last date to apply : <b>31<sup>st</sup> December 2020</b> -->
                        </p>
                        <a href="uplift-form" class="btn btn-warning button4" target="_blank">Register Now</a>
                        <!-- <p class="text-justify wow slideInRight"><b class="text-yellow">Note :</b><br> Once the form is submitted, please send your business deck format at <a href="mailto:seir&#064;dfmail&#046;org">seir&#064;dfmail&#046;org</a> (Confirmation mail will contain the business deck format).</p> -->
                        <!-- <p class="text-justify wow slideInRight"><b class="text-yellow">*</b> Applications will be selected on first come first serve basis.</p> -->
                        
                     </div>
                     <!-- <div class="col-md-2">
                        <div class="row pl-4">
                           <a href="uplift-form" class="btn btn-rotate" target="_blank">Register Now</a>
                        </div>
                     </div> -->
                  </div>
				                     
						   
                      
                     </div>
               </div>
            </div>
         </div>
      </div>
      <br><br>
      <div class="container">
         <div class="row">
            <div class="col-sm-8">
               <h4 class="text-center text-yellow pb-2">
                  Process:
               </h4>
               <!-- <img src="img/test-.jpg" alt=""> -->
               <img class="card-img-top img-fluid wow zoomIn" src="img/events/uplift_process.jpg" width="474" height="237" alt="Deshpande startups, events, Sandbox Uplift">
               <!-- <ul class="lh">
                  <li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> Application process (<a href="">Apply online</a> with all the required information and documents)</li>
                  <li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i>  Online screening ( If eligible, you will receive a selection email)</li>
                  <li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i>  Pitch to experts (Pitch to a panel of jury inclusive of mentors and industry experts)</li>
                  <li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i>  Onboarding (If selected, Get on boarded into our programs and achieve your goals faster)</li>
                  
                  </ul> -->
            </div>
            <div class="col-sm-4">
               <h4 class="text-center text-yellow pb-2">
                  Sectors
               </h4>
               <div class="row mbr-justify-content-center">
                  <div class="col-lg-12 mbr-col-md-10">
                     <div class="wrap">
                        <div class="ico-wrap">
                           <span class="mbr-iconfont fa fa-leaf"></span>
                        </div>
                        <div class="text-wrap ico-wrap">
                           <h4 class="mbr-fonts-style font-weight-bold mbr-section-title3">Agri-tech</h4>
                           <!-- <p class="mbr-fonts-style text1 mbr-text display-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p> -->
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 mbr-col-md-10">
                     <div class="wrap">
                        <div class="ico-wrap">
                           <span class="mbr-iconfont fa fa-medkit"></span>
                        </div>
                        <div class="text-wrap ico-wrap">
                           <h4 class="mbr-fonts-style font-weight-bold mbr-section-title3">Medtech</h4>
                           <!-- <p class="mbr-fonts-style text1 mbr-text display-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p> -->
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 mbr-col-md-10">
                     <div class="wrap">
                        <div class="ico-wrap">
                           <span class="mbr-iconfont fa-book fa"></span>
                        </div>
                        <div class="text-wrap ico-wrap">
                           <h4 class="mbr-fonts-style font-weight-bold mbr-section-title3">Edu-tech</h4>
                           <!-- <p class="mbr-fonts-style text1 mbr-text display-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p> -->
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 mbr-col-md-10">
                     <div class="wrap">
                        <div class="ico-wrap">
                           <span class="mbr-iconfont fa-cogs fa"></span>
                        </div>
                        <div class="text-wrap ico-wrap">
                           <h4 class="mbr-fonts-style font-weight-bold mbr-section-title3">Rural Innovations</h4>
                           <!-- <p class="mbr-fonts-style text1 mbr-text display-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p> -->
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 mbr-col-md-10">
                     <div class="wrap">
                        <div class="ico-wrap">
                           <span class="mbr-iconfont fa-shopping-cart fa"></span>
                        </div>
                        <div class="text-wrap ico-wrap">
                           <h4 class="mbr-fonts-style font-weight-bold mbr-section-title3">Rural Retail/<br>Consumption</h4>
                     
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 mbr-col-md-10">
                     <div class="wrap">
                        <div class="ico-wrap">
                           <span class="mbr-iconfont fa-money fa"></span>
                        </div>
                        <div class="text-wrap ico-wrap">
                           <h4 class="mbr-fonts-style font-weight-bold mbr-section-title3">Rural Fintech</h4>
                     
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <br><br>
      <div class="featured-bg-container">
         <h2 class="text-yellow text-center wow slideInDown pt-2" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.1s; animation-name: slideInDown;">Offerings</h2>
         <div class="divider b-y text-yellow content-middle"></div>
         <div class="row">
            <div class="col-sm-3">
               <h4 class="text-yellow"><b>Mentorship:</b></h4>
               <ul class="wow slideInLeft">
                  <li>One-one mentorship sessions</li>
                  <li>Internal mentor to build the Startups Founder Entrepreneurial mindset</li>
                  <li>External mentor-an industrial expert/business mentor </li>
               </ul>
            </div>
            <div class="col-sm-3">
               <h4 class="text-yellow"><b>Market access:</b></h4>
               <ul class="wow slideInLeft">
                  <li>Access to Deshpande foundation ecosystem connect & partnerships</li>
                  <li>Connect to Early adopters</li>
                  <li>Connect to Progressive thinking customers</li>
               </ul>
            </div>
            <div class="col-sm-3">
               <h4 class="text-yellow"><b>Money:</b></h4>
               <ul class="wow slideInLeft">
                  <li>Building capacity of Entrepreneur & Startup to make them Investment Ready</li>
                  <li>Opportunity to pitch your business idea to Deshpande Startups Investment Committee</li>
                  <li>Creating entrepreneurial mind set</li>
                  <li>Connect with leading angel & VC firms across India & the globe</li>
               </ul>
            </div>
            <div class="col-md-3">
               <h4 class="text-yellow"><b>Value-added services:</b></h4>
               <p><b>Free cloud services:</b></p>
               <ul class="wow slideInLeft">
                  <li>AMAZON - $5000 (Extendable upto $100000*)</li>
                  <li>Google - $3000 (Extendable upto $100000*)</li>
               </ul>
               <p><b>Software services:</b></p>
               <ul class="wow slideInLeft">
			        <li>One year free subscription of ZOHO ONE, Mathworks, Solidworks, etc.</li>			   
                 </ul>
            </div>
         </div>
         <!--row end-->
         <div class="row">
            <div class="col-sm-3">
               <h4 class="text-yellow"><b>Co-working space:</b></h4>
               <ul class="wow slideInLeft">
                  <li>World-class infrastructure for a vibrant startup ecosystem</li>
                  <li>24/7 open co-working space</li>
                  <li>Full-fledged 300+ Plug and Play work stations etc</li>
                  <li>Auditorium & seminar halls</li>
               </ul>
            </div>
            <div class="col-sm-3">
               <h4 class="text-yellow"><b>State of the art architecture laboratories:</b></h4>
               <ul class="wow slideInLeft">
                  <li>Modern-day technology to facilitate manufacturing and testing of Electronic systems</li>
                  <li>3D printing & scanners lab</li>
                  <li>IoT & Electronics</li>
                  <li>Mechanical section with advanced CNC lathe, VMC & laser cutting, conventional machines, etc</li>
               </ul>
            </div>
            <div class="col-sm-3">
               <h4 class="text-yellow"><b>One-stop-services:</b></h4>
               <ul class="wow slideInLeft">
                  <li>IP/legal services</li>
                  <li>Product/solution Design Services</li>
                  <li>Valuation & ESOP’s</li>
                  <li>Govt. Regulatory & Certification Services</li>
                  <li>HR and CA services</li>
               </ul>
            </div>
            <div class="col-sm-3">
               <h4 class="text-yellow"><b>GST Exemption:</b></h4>
               <ul class="wow slideInLeft">
                  <li>No tax for revenue up to INR 50 lakhs per year for 3 years for startups</li>
               </ul>
            </div>
         </div>
         <!-- end row-->
      </div>
      <div class="container-fluid text-center p-3">
         <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
            <h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">Journey at</span> Deshpande Startups:</h2>
            <div class="divider b-y text-yellow content-middle"></div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <!-- <i class="fa fa-quote-left fa-4x pb-2"></i> -->
               <p class="wow zoomIn">
                  <!-- <b>Onboard - meet the team - assess needs - set milestones - meet mentors - avail resources - connect with the ecosystem - engage -  Achieve Goals.</b> -->
                  <!-- <img src="img/events/journey-at-DS.jpg" alt=""> -->
                  <img class="carousel-inner img-fluid" src="img/events/road-map-ds.jpg" width="1349" height="300" alt="Deshpande Startups, events Sandbox Uplift">                    
               </p>
            </div>
         </div>
         <!-- <div class="text-center">
            <a href="edge-form" class="btn btn-warning" target="_blank">Apply Now</a>
            </div> -->
      </div>
      <div class="container">
         <div class="row">
            <!-- <div class="col-md-6">
               <h4 class="text-center text-yellow pb-2">
               				Application:
               				</h4>
               				<p class="wow zoomIn">
               			To be part of India’s largest incubator and its ever-growing ecosystem, register here and we will get back to you in a day or two.</p>
               				<br>
               				<h4>Selection criteria:</h4>
               				<ul class="lh">
               					<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> <b>Innovation:</b> The solution should be innovative and create an impact on the non-metro market.</li>
               					<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> <b>Entrepreneur:</b> The founder should be committed to solving the problem with a learning attitude. The team should be complementary.</li>
               					<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> <b>Impact:</b> The Startup should have some. </li>
               					<li><i class="fa fa-arrow-right text-yellow" aria-hidden="true"></i> <b>Business Feasibility & Scalability:</b> The solution should have business feasibility and scalability.</li>
               				</ul>
               
               </div>end col-md-6 -->
            <div class="col-md-12">
               <!-- <h4 class="text-center text-yellow pb-2">FAQ's</h4> -->
			   <h2 class=" text-yellow text-center wow slideInDown"> FAQ's</h2>
               <div id="accordion">
                  <div class="card">
                     <div class="card-head" id="headingOne">
                        <h5 class="mb-0">
                           <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                           <b>What are the programs at Deshpande Startups to support entrepreneurs?</b>
                           </button>
                        </h5>
                     </div>
                     <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                           Deshpande Startups has various structured programs to support and elevate startups from PoC to Revenue.
                           To know more about these programs, visit <a href="edge" target="_blank">www.deshpandestartups.org/edge</a>  and <a href="incubation-support" target="_blank">www.deshpandestartups.org/incubation-support</a> 
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-head" id="headingTwo">
                        <h5 class="mb-0">
                           <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                           <b>Who are the mentors at Deshpande Startups?</b>
                           </button>
                        </h5>
                     </div>
                     <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                           To know the mentors at Deshpande Startups, visit <a href="mentors" target="_blank">www.deshpandestartups.org/mentors</a>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-head" id="headingThree">
                        <h5 class="mb-0">
                           <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                           <b>Is physical presence for the programs required?</b>
                           </button>
                        </h5>
                     </div>
                     <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                           No, the programs are made virtually available for startups across India.
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-head" id="headingFour">
                        <h5 class="mb-0">
                           <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                           <b>What is the lab support available for product development?</b>
                           </button>
                        </h5>
                     </div>
                     <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                           Deshpande Startups has full-pledged laboratories to support startups in product development.
                           For electronics systems support, visit: <a href="esdm-cluster" target="_blank">www.deshpandestartups.org/esdm-cluster</a>
                           For other lab facilities, visit <a>www.deshpandestartups.org/makers-lab</a>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-head" id="headingFive">
                        <h5 class="mb-0">
                           <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                           <b>What is the funding support provided?</b>
                           </button>
                        </h5>
                     </div>
                     <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                           To know the funding support at Deshpande Startups, visit <a href="funding" target="_blank">www.deshpandestartups.org/funding</a>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-head" id="headingSix">
                        <h5 class="mb-0">
                           <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                           <b>What are the facilities available in co-working space?</b>
                           </button>
                        </h5>
                     </div>
                     <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                           <ul>
		 						<li>24/7 open space</li>
		 					<li>High-Speed Wifi network</li>
							 <li>Clean and Sanitized spaces</li>
							 <li>Clean Drinking Water Facility</li>
							 <li>Well equipped Meeting rooms</li>
						   
						   </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--end col-md-6-->
      </div>
      <!-- <div class="container text-center p-3">
         <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
         	<h2 class=" text-yellow text-center wow slideInDown">Application:</h2>
         	<div class="divider b-y text-yellow content-middle"></div>
         </div>
         <div class="row">
         	<div class="col-md-12">
         		 <i class="fa fa-quote-left fa-4x pb-2"></i> -->
      <!-- <p class="wow zoomIn">
         <b>To be part of India’s largest incubator and its ever-growing ecosystem, register here and we will get back to you in a day or two.</b>
         </p>				
         </div>
         
         </div> --> 
      <!-- <div class="text-center">
         <a href="edge-form" class="btn btn-warning" target="_blank">Apply Now</a>
         </div> -->
      <!-- </div> -->
      <br>
      <br>
      <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once 'essentials/js.php';
         ?>
   </body>
</html>