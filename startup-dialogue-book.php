<!DOCTYPE html>
<html lang="en">
<head>
	<title>Startup Dialogue Book | Startup Dialogue 2020</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/startup-dialogue-book"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/startup-dialogue-book">
	<!-- <meta property="og:image" content="https://www.deshpandestartups.org/img/events/uplift.jpg">
		<meta property="og:image" content="https://www.deshpandestartups.org/img/events/uplift-bg.jpg"> -->
		<meta property="og:description" content="Startup Dialogue the annual conference of Deshpande Startups intends to create an exciting platform for all the stakeholders (Enablers, Startups, VC's/Investors, Startup enthusiasts, Leaders) of the Indian startup ecosystem to come together and celebrate the spirit of entrepreneurship."/>
		<meta name="author" content="Deshpande Startups"/>
		<meta name="description" content="Startup Dialogue the annual conference of Deshpande Startups intends to create an exciting platform for all the stakeholders (Enablers, Startups, VC's/Investors, Startup enthusiasts, Leaders) of the Indian startup ecosystem to come together and celebrate the spirit of entrepreneurship."/>
		<!-- <meta name="keywords" content=""/> -->
		<meta property="og:title" content="Startup Dialogue Book">
		<!-- <link rel="canonical" href="https://www.deshpandestartups.org/uplift"> -->

		<?php
		require_once 'essentials/bundle.php';
		?>
		<style type="text/css">
			.gray{background-color: #403b3b !important;}
			.white{color:#fff;}
		</style>
	</head>
	<body>
		<?php
		require_once 'essentials/title_bar.php';
		require_once 'essentials/menus.php';
		?>
		<div class="container pt-5">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2 text-center pb-4">
				<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">STARTUP DIALOGUE</span> BOOK</h2>
				<div class="divider b-y text-yellow content-middle"></div>
				<a href="startup-dialogue-book.pdf" download="startup-dialogue-book" class="btn btn-md btn-warning px-5"><i class="fa fa-download" aria-hidden="true"></i> Download Book</a>
			</div>
				<div class="col-md-12 pl-5">
					<iframe src="startup-dialogue-book.pdf" width="100%" height="800px" frameborder="0"></iframe>
				</div>
			</div>
		</div>
		<br>
		<br>
		<?php
		require_once 'essentials/footer.php';
		require_once 'essentials/copyright.php';
		require_once 'essentials/js.php';
		?>
	</body>
	</html>