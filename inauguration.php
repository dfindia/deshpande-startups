<!DOCTYPE html>
<html lang="en">
<head>
	<title>Deshpande Startups | Inauguration of India's largest Incubation center</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/inauguration"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/inauguration">
	<meta property="og:title" content="Deshpande Startups inauguration">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/inauguration/inauguration-slider.png">
	<meta property="og:description" content="Come celebrate, visit, and experience Deshpande Startups’ inaugural opening of “India’s largest Incubation Center” on July 18th, 2018 with Chief Guests Mr. Amitabh Kant, CEO of NITI Aayog as well as Dr. Desh Deshpande, Founder of Deshpande Foundation."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Come celebrate, visit, and experience Deshpande Startups’ inaugural opening of “India’s largest Incubation Center” on July 18th, 2018 with Chief Guests Mr. Amitabh Kant, CEO of NITI Aayog as well as Dr. Desh Deshpande, Founder of Deshpande Foundation."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Business development manager, technical manager, Hubballi Karnatak India."/> -->
	<link rel="canonical" href="https://www.deshpandestartups.org/inauguration">
	<?php
         // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/inauguration/inauguration-slider.png" width="1349" height="320" alt="Deshpande Startups, inaugural">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Inauguration</li>
		</ol>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="text-justify wow fadeInDown">Come celebrate, visit, and experience Deshpande Startups’ inaugural of <b>“India’s largest Incubation Center”</b> on <b>July 18th, 2018</b> with <br>Chief Guests <b>Mr. Amitabh Kant</b>, CEO of NITI Aayog as well as <b>Dr. Gururaj "Desh" Deshpande</b>, Founder of Deshpande Foundation will be inaugurating our new Startup Incubation Centre.</p>
				<p class="text-justify wow fadeInDown">This new center is intended to foster an inclusive startup ecosystem by providing a platform to Validate Startup Ideas & create linkages between startups and all of the Stakeholders like Mentors, Investors, early customers etc.</p>
				<p class="text-justify wow fadeInDown">More than 70% of Indian population lives in semi-urban and rural areas in India – the Hubballi incubator is targeted to support the startups which serve this population. With access to this center, startups will also have the ability to expand their businesses to a wider global market.</p>
			</div>
		</div>
	</div>
	<!-- <div class="container">
		<div class="row justify-content-end pr-3">
				<div class="pr-2">
					<a href="inauguration-form" class="btn btn-warning" target="_blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Register Now</a>
				</div>
				<a href="agenda" class="btn btn-success" target="_blank"><i class="fa fa-download"></i> Download Agenda</a>
			</div>
		</div> -->
		<!-- <br> -->
		<!-- <hr class="featurette-divider-sm"> -->
		<h2 class="text-yellow text-center pt-2 wow fadeInDown"><span class="text-muted">CHIEF</span> GUESTS</h2>
		<div class="divider b-y text-yellow content-middle"></div>
		<!-- <br> -->
		<div class="container">
			<!-- row1 -->
			<div class="row pt-1">
				<div class="col-lg-2"></div>
				<div class="col-lg-4">
					<div class="text-center">
						<img class="img-fluid wow zoomIn rounded-circle" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" src="img/mentors/gururaj.png" width="165" height="160"  alt="Dr. Gururaj Desh Deshpande, Founder, Deshpande Foundation"><br>
						<p class="text-yellow"><b>Dr. Gururaj "Desh" Deshpande</b><br>
							<span class="text-muted">Founder, Deshpande Foundation</span>
						</p>
					</div>
					<br>
				</div>
				<div class="col-lg-4">
					<div class="text-center">
						<img class="img-fluid wow zoomIn rounded-circle " data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" src="img/inauguration/amitabh-kant.png" width="165" height="160"  alt="Dr. Amitabh Kant, CEO of NITI Aayog"><br>
						<p class="text-yellow"><b>Dr. Amitabh Kant</b><br>
							<span class="text-muted">CEO of NITI Aayog</span>
						</p>
					</div>
					<br>
				</div>
			</div>
		</div>
		<!-- <br> -->
		<!-- <hr class="featurette-divider-sm"> -->
		<h2 class="text-yellow text-center wow fadeInDown"><span class="text-muted">GUEST</span> SPEAKERS</h2>
		<div class="divider b-y text-yellow content-middle"></div>
		<br>
		<div class="container">
			<div class="row text-center">
				<!---first row-->
				<!--  <div class="col-md-1"></div> -->
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" >
					<img src="img/speakers/k-j-george.jpg" width="160" height="165" class="img-fluid rounded-circle"alt="Mr. K J George, Honorable IT BT minister, Govt. of Karnataka">
					<p class="text-center text-yellow"><b>K J George</b>
						<br>
						<small class="text-muted">Honorable IT BT minister,<br> Govt. of Karnataka</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s">
					<img src="img/speakers/gaurav-gupta.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Gaurav Gupta, Principal Secretary, Department of IT BT, Govt. of Karnataka">
					<p class="text-center text-yellow"><b>Gaurav Gupta</b>
						<br>
						<small class="text-muted">Principal Secretary, Department of IT BT, Govt. of Karnataka</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s">
					<img src="img/speakers/raj-kumar.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Raj Kumar Srivastav, Advisor, IT BT Minister">
					<p class="text-center text-yellow"><b>Raj Kumar S</b>
						<br>
						<small class="text-muted">Advisor, IT BT Minister</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s">
					<img src="img/speakers/phani.jpg" width="160px" height="560px" class="img-fluid rounded-circle" alt="Phanindra Sama, Co-Founder Redbus & Chief Innovation Officer, Telangana">
					<p  class="text-yellow text-center"><b>Phanindra Sama</b>
						<br>
						<small class="text-muted">Co-Founder Redbus & Chief Innovation Officer, Telangana</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s">
					<img src="img/speakers/sushil-vachani.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Sushil Vachani, Former Director, IIM B">
					<p class="text-center text-yellow"><b>Sushil Vachani</b>
						<br>
						<small class="text-muted">Former Director, IIM B</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="1.1s">
					<img src="img/speakers/anand-sankeshwar.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Anand sankeshwar, CEO, VRL">
					<p class="text-center text-yellow"><b>Anand sankeshwar</b>
						<br>
						<small class="text-muted">CEO, VRL</small>
					</p>
				</div>
			</div>
			<br>
			<div class="row text-center">
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s">
					<img src="img/speakers/suhas-gopinath.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Suhas Gopinath, CEO, Global Inc.">
					<p class="text-center text-yellow"><b>Suhas Gopinath</b>
						<br>
						<small class="text-muted">CEO, Global Inc.</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s">
					<img src="img/speakers/ravi-narayan.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Ravi Narayana, Mentor Microsoft">
					<p  class="text-yellow text-center"><b>Ravi Narayana</b>
						<br>
						<small class="text-muted">Mentor, Microsoft</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s">
					<img src="img/speakers/champa.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Champa, GM, KBITS, Govt. of Karnataka">
					<p class="text-center text-yellow"><b>Champa</b>
						<br>
						<small class="text-muted">GM, KBITS, Govt. of Karnataka</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s">
					<img src="img/speakers/aravind-chincure.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Aravind Chinchure, Director, Deshpande Startups">
					<p class="text-center text-yellow"><b>Aravind Chinchure</b>
						<br>
						<small class="text-muted">Director, Deshpande Startups</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s">
					<img src="img/speakers/shyam-vasudevrao.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Shyam Vasudevarao, Founder, Director Forus Health Pvt. Ltd.">
					<p class="text-center text-yellow"><b>Shyam V</b>
						<br>
						<small class="text-muted">Founder, Director Forus Health Pvt. Ltd</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="1.1s">
					<img src="img/speakers/manoj-kumar.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Manoj Kumar, Trustee, TATA Trust">
					<p class="text-center text-yellow"><b>Manoj Kumar</b>
						<br>
						<small class="text-muted">Trustee, TATA Trust</small>
					</p>
				</div>
			</div>
			<br>
			<!---fourth row-->
			<div class="row text-center">
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s">
					<img src="img/speakers/manish-chowdary.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Manish Chowdhary, CEO, Tally Education Pvt. Ltd">
					<p class="text-center text-yellow"><b>Manish Chowdhary</b>
						<br>
						<small class="text-muted">CEO, Tally Education Pvt. Ltd</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s">
					<img src="img/speakers/vijay-kumar.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Vijay Kumar, CEO, Mygate">
					<p class="text-center text-yellow"><b>Vijay Kumar</b>
						<br>
						<small class="text-muted">CEO, Mygate</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s">
					<img src="img/speakers/ram-ramanathan.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Ram Ramanathan, CEO and Managing Director, 4R Recycling">
					<p class="text-center text-yellow"><b>Ram Ramanathan</b>
						<br>
						<small class="text-muted">CEO and Managing Director, 4R Recycling</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s">
					<img src="img/speakers/anil-muniswami.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Anil Kumar Muniswamy, Chairman & MD, IESA & SLN Technologies Pvt. Ltd.">
					<p  class="text-yellow text-center"><b>Anil Kumar M</b>
						<br>
						<small class="text-muted">Chairman & MD, IESA & SLN Technologies Pvt. Ltd</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s">
					<img src="img/speakers/jitender-chadda.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Jitendra Chaddah, Sr Director, Operations and Strategy, Intel India">
					<p class="text-center text-yellow"><b>Jitendra Chaddah</b>
						<br>
						<small class="text-muted">Sr. Director, Operations and Strategy, Intel India</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="1.1s">
					<img src="img/speakers/reema.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Rema Subramaniam, Founding partner, Ankur Capital">
					<p class="text-center text-yellow"><b>Rema S</b>
						<br>
						<small class="text-muted">Founding Partner, Ankur Capital</small>
					</p>
				</div>
			</div>
			<!--first row-->
			<br/>
			<!---second row-->
			<div class="row text-center">
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s">
					<img src="img/speakers/Ramanathan-Ramachandran.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Ramanathan R, CEO, Inthree Access Services Private Limited">
					<p class="text-center text-yellow"><b>Ramanathan R</b>
						<br>
						<small class="text-muted">CEO, Inthree Access Services Pvt. Ltd</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s">
					<img src="img/speakers/raj-belgaumkar.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Raj Belgaumkar, Chairman, KLS IMER">
					<p class="text-center text-yellow"><b>Raj Belgaumkar</b>
						<br>
						<small class="text-muted">Chairman, KLS IMER</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s">
					<img src="img/speakers/prathiba.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Pratibha, Representative, KBITS, Govt. of Karnataka">
					<p class="text-center text-yellow"><b>Pratibha</b>
						<br>
						<small class="text-muted">Representative, KBITS, Govt. of Karnataka</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s">
					<img src="img/speakers/rajeev-prakash.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Rajeev Prakash, Director, Deshpande Startups">
					<p class="text-center text-yellow"><b>Rajeev Prakash</b>
						<br>
						<small class="text-muted">Director, Deshpande Startups</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s">
					<img src="img/speakers/santosh.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Santosh Hurlikopi, Investor Astrac Ventures">
					<p class="text-center text-yellow"><b>Santosh Hurlikopi</b>
						<br>
						<small class="text-muted">Investor, Astrac Ventures</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="1.1s">
					<img src="img/speakers/kv-anand.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Anand K V, Ex. Senior Vice President, Flipkart">
					<p class="text-center text-yellow"><b>Anand K V</b>
						<br>
						<small class="text-muted">Ex. Senior Vice President, Flipkart</small>
					</p>
				</div>
			</div>
			<!--second row-->
			<br>
			<div class="row text-center">
				<!---third row-->
				<!--  <div class="col-md-1"></div> -->
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s">
					<img src="img/speakers/seshu.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Dr. Seshu, Director IIT Dharwad">
					<p class="text-center text-yellow"><b>Dr. Seshu</b>
						<br>
						<small class="text-muted">Director IIT, Dharwad</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s">
					<img src="img/speakers/rajiv-upadyay.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Rajiv Updahyay, Utilis Capital">
					<p class="text-center text-yellow"><b>Rajiv Updahyay</b>
						<br>
						<small class="text-muted">Utilis Capital</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s">
					<img src="img/speakers/sandeep-b.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Sandeep Bidarsia, President, TIE, Hubballi">
					<p class="text-center text-yellow"><b>Sandeep Bidarsia</b>
						<br>
						<small class="text-muted">President, TIE, Hubballi</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s">
					<img src="img/speakers/kuppulakshmi.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Kuppulakshmi K, Evangelist, Zoho">
					<p class="text-center text-yellow"><b>Kuppulakshmi K</b>
						<br>
						<small class="text-muted">Evangelist, Zoho</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s">
					<img src="img/speakers/bala-girisaballa.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Bala Girisbala, President, Techstars">
					<p class="text-center text-yellow"><b>Bala Girisbala</b>
						<br>
						<small class="text-muted">President, Techstars</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="1.1s">
					<img src="img/speakers/chetan-anand.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Chetan Anand, Director, PWC">
					<p class="text-center text-yellow"><b>Chetan Anand</b>
						<br>
						<small class="text-muted">Director, PWC</small>
					</p>
				</div>
			</div>
			<!--third row-->
			<br/>
			<div class="row text-center">
				<!---second row-->
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s">
					<img src="img/speakers/sandeep-agarwal.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Sandeep Agarwal, VP, Happiest Minds">
					<p class="text-center text-yellow"><b>Sandeep Agarwal</b>
						<br>
						<small class="text-muted">VP, Happiest Minds</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s">
					<img src="img/speakers/pratishta.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Prathistha Jain, Director, Vibazone Private Limited">
					<p class="text-center text-yellow"><b>Prathistha Jain</b>
						<br>
						<small class="text-muted">Director, Vibazone Private Limited</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s">
					<img src="img/speakers/parag.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Parag Naik, CEO at Saankhya Labs Pvt. Ltd">
					<p class="text-center text-yellow"><b>Parag Naik</b>
						<br>
						<small class="text-muted">CEO, Saankhya Labs Pvt. Ltd.</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s">
					<img src="img/speakers/jayant.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Jayant Hummarwadi, MD, Akshoka IRON Works">
					<p class="text-center text-yellow"><b>Jayant H</b>
						<br>
						<small class="text-muted">MD, Akshoka IRON Works</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s">
					<img src="img/speakers/lr-bhat.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="L R Bhat, Executive Director, VRL">
					<p class="text-center text-yellow"><b>L R Bhat</b>
						<br>
						<small class="text-muted">Executive Director, VRL</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="1.1s">
					<img src="img/speakers/vivek-nayak.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Vivek Nayak, MD, Ken Agritech Pvt. Ltd">
					<p class="text-center text-yellow"><b>Vivek Nayak</b>
						<br>
						<small class="text-muted">MD, Ken Agritech Pvt. Ltd</small>
					</p>
				</div>
			</div>
			<br>
			<!---fifth row-->
			<div class="row text-center">
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s">
					<img src="img/speakers/ravi-linganuri.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Ravi Linganuri, Early Stage Investor & Advisory">
					<p class="text-center text-yellow"><b>Ravi Linganuri</b>
						<br>
						<small class="text-muted">Early Stage Investor & Advisory</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s">
					<img src="img/speakers/chirant-patil.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Chiranth Patil, Investor Mentor">
					<p class="text-center text-yellow"><b>Chiranth Patil</b>
						<br>
						<small class="text-muted">Investor Mentor</small>
					</p>
					<!-- <p class="mb-10">Co-founder, redBus.in </p> -->
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s">
					<img src="img/speakers/nayani-nasa.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Nayani Nasa, Invest India">
					<p class="text-center text-yellow"><b>Nayani Nasa</b>
						<br>
						<small class="text-muted">Invest India</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s">
					<img src="img/speakers/muthu-singaram.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Muthu Singaram, CEO, IIT Madras HTIC Incubator">
					<p class="text-center text-yellow"><b>Muthu Singaram</b>
						<br>
						<small class="text-muted">CEO, IIT Madras HTIC Incubator</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s">
					<img src="img/speakers/prasanna.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Prassanna, Dean, IIT Dharwad">
					<p class="text-center text-yellow"><b>Prassanna</b>
						<br>
						<small class="text-muted">Dean, IIT Dharwad</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="1.1s">
					<img src="img/speakers/rajeev-brigade.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Rajeev Nair, Head Operations, brigade REAP">
					<p class="text-center text-yellow"><b>Rajeev Nair</b>
						<br>
						<small class="text-muted">Head Operations, brigade REAP</small>
					</p>
				</div>
			</div>
			<!--second row-->
			<br/>
			<!---third row-->
			<div class="row text-center">
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s">
					<img src="img/speakers/suchitra.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Suchitra, CEO, nanoPix ISS">
					<p class="text-center text-yellow"><b>Suchitra</b>
						<br>
						<small class="text-muted">CEO, nanoPix ISS</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s">
					<img src="img/speakers/anand-talwai.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Anand Talwai, Co-Founder & Executive Director, NextWealth Entreprenuers Pvt. Ltd">
					<p class="text-center text-yellow"><b>Anand Talwai</b>
						<br>
						<small class="text-muted">Executive Director, NextWealth Entreprenuers Pvt. Ltd</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s">
					<img src="img/speakers/naveen-asrani.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Naveen Asrani, Director, Startups at Microsoft">
					<p  class="text-yellow text-center"><b>Naveen Asrani</b>
						<br>
						<small class="text-muted">Director, Startups at Microsoft</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s">
					<img src="img/speakers/girendra-kasmalkar.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Gireendra Kasmalkar, Founder Director, Ideas to Impact Innovations Pvt. Ltd">
					<p class="text-center text-yellow"><b>Gireendra K</b>
						<br>
						<small class="text-muted">Founder Director, Ideas to Impact Innovations Pvt. Ltd</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.9s">
					<img src="img/speakers/kartik-padmanabhan.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Karthik Padmananbhan, Relationship Manager, Google">
					<p class="text-center text-yellow"><b>Karthik P</b>
						<br>
						<small class="text-muted">Relationship Manager, Google</small>
					</p>
				</div>
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="1.1s">
					<img src="img/speakers/sivarama.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Shivarama Murthy, Startup Advisor">
					<p class="text-center text-yellow"><b>Sivarama Moorthy</b>
						<br>
						<small class="text-muted">Startup Advisor</small>
					</p>
				</div>
			</div>
			<!--third row-->
			<div class="row text-center">
				<div class="col-md-2 wow zoomIn" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s">
					<img src="img/speakers/shreyansh-singhal.jpg" width="160" height="165" class="img-fluid rounded-circle" alt="Shreyansh Singhal, Investment banker, Ankur Capital">
					<p class="text-center text-yellow"><b>Shreyansh Singhal</b>
						<br>
						<small class="text-muted">Investment banker, Ankur Capital</small>
					</p>
				</div>
			</div>
		</div>
		<div class="container-fluid bg-grey1">
			<div class="row pt-3">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<h2 class="text-yellow pb-2 wow fadeInDown text-center"><b>Our Inaugural event will offer</b></h2>
					<!-- <br> -->
					<ul>
						<li class="wow slideInRight">Unparalleled access to networks within the <b>Entrepreneurial Ecosystem</b> in Karnataka and all over India</li>
						<br>
						<li class="wow slideInRight">Networking opportunities with a wide variety of <b>Mentors, Investors, Central and State government Officials</b> and <b>Partners</b> <br>who are currently supporting our associated startups</li>
						<br>
						<li class="wow slideInRight">Exciting Panel Sessions by <b>Industry Leaders/Domain Experts</b></li>
						<br>
						<li class="wow slideInRight">Exposure to our world-class <b>full-fledged facility</b> dedicated to support Entrepreneurs at Deshpande Startups</li>
						<br>
						<li class="wow slideInRight"> Access to <b>Makers Lab & ESDM Cluster</b> with <b>Automated SMT</b> line with various testing equipments</li>
						<br>
						<li class="wow slideInRight">A comprehensive understanding of the <b>Semi-urban & Rural Market</b></li>
						<br>
					</ul>
					<!-- <br> -->
				</div>
			</div>
		</div>
		<?php
		require_once 'essentials/footer.php';
		require_once 'essentials/copyright.php';
		require_once 'essentials/js.php';
		?>
	</body>
	</html>