<!DOCTYPE html>
<html lang="en">
<head>
 <title>Makers Lab | Arduino & 3D Printing Workshops</title>
 <?php
 require_once 'essentials/meta.php';
 ?>
 <meta name="linkage" content="https://www.deshpandestartups.org/makerslab-workshop"/>
 <meta property="og:site_name" content="Deshpande Startups"/>
 <meta property="og:type" content="website">
 <meta property="og:url" content="https://www.deshpandestartups.org/makerslab-workshop">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/events/arduino-and-3d-bg.png">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/3d-printers-expo.png">
 <meta property="og:description" content=">Makers Lab organizes workshops on the topics of IoT and Electronics, 3D Printing, Mechanical and Civil, Web and Android Development and Machine Learning. Placed in a tier-2 city Deshpande Startups provides world-class infrastructure for Entrepreneurs with creative environment to host a vibrant execution-based ecosystem."/>
 <meta name="author" content="Deshpande Startups"/>
 <meta name="description" content="Makers Lab organizes workshops on the topics of IoT and Electronics, 3D Printing, Mechanical and Civil, Web and Android Development and Machine Learning. Placed in a tier-2 city Deshpande Startups provides world-class infrastructure for Entrepreneurs with creative environment to host a vibrant execution-based ecosystem."/>
 <!-- <meta name="keywords" content=""/> -->
 <meta property="og:title" content="Arduino & 3D Printing Workshops">
 <link rel="canonical" href="https://www.deshpandestartups.org/makerslab-workshop">
 <?php
 require_once 'essentials/bundle.php';
 ?>

 <style type="text/css">
</style>

</head>
<body>
 <?php
 require_once 'essentials/title_bar.php';
 require_once 'essentials/menus.php';
 ?>

<img class="carousel-inner img-fluid" src="img/events/arduino-and-3d-bg.png" width="1349" height="198" alt="Deshpande Startups, workshops">
 <nav aria-label="breadcrumb">
  <ol class="breadcrumb justify-content-end">
    <li class="breadcrumb-item"><a href="./">Home</a></li>
    <li class="breadcrumb-item"><a href="events">Events</a></li>
    <li class="breadcrumb-item active" aria-current="page">Workshops</li>
  </ol>
</nav> 

<div class="container">
  <!-- <br> -->
  <div class="center wow fadeInDown">
   <h2 class="text-yellow text-center"><span class="text-muted">ARDUINO & 3D PRINTING</span><br> WORKSHOPS</h2>
   <div class="divider b-y text-yellow content-middle"></div>
 </div><br>
 <!-- <div class="pull-right"><a href="makerslab-events" class="btn btn-warning btn-md" target="_blank">Apply Now</a></div><br> -->
 <!-- <h4  class="text-yellow">Workshop description:</h4> -->
 <div class="row">
   <div class="col-md-12">
     <p class="text-justify wow slideInLeft">Makers Lab organizes workshops on the topics of <b>IoT and Electronics, 3D Printing, Mechanical and Civil, Web and Android Development and Machine Learning</b>. Placed in a tier-2 city Deshpande Startups provides world-class infrastructure for Entrepreneurs with creative environment to host a vibrant execution-based ecosystem.</p>
   </div>
     <!-- <div class="row justify-content-md-center"> -->
   <!-- <div class="col-md-2">
      <div class="row pl-4">
       <a href="workshop" class="btn btn-rotate" target="_blank">Apply Now</a>
     </div>
   </div> -->
 </div>
</div>
<br>
<!-- <br> -->

<div id="3d-printing">
  <div class="featured-bg-container">
    <div class="row valign-wrapper">
     <div class="col-md-4">
      <img src="img/makers/3d-printers-expo.png" width="380" height="213" alt="makers lab, workshop, 3d printing" class="img img-fluid img-thumbnail wow zoomIn">
    </div>
    <div class="col-md-8">
     <h4 class="text-yellow">3D Printing:</h4>
     <p class="text-justify wow slideInRight">
       3D Printing is an <b>Additive Manufacturing technique</b> that creates a physical object from a virtual 3D CAD model by depositing successive layers of material. They work like the traditional inkjet printers, but instead of ink, a 3D printer deposits desired material to manufacture an object from its digital format.
     </p>
     <p class="text-justify wow slideInRight">This workshop on 3D Printing helps the participants understand The Basics of 3D printing, understand the design, functioning and operation of basic 3D printer.</p>
     <p class="text-justify wow slideInRight">Students of any background with interest in 3D printing, preferably engineering students can attend this workshop.</p>
   </div>
 </div>
</div>
<br>
</div>

<div id="iot-electronics">
  <div class="container-fluid px-5">
    <div class="row valign-wrapper">
      <div class="col-md-8">
        <h4 class="text-yellow">Arduino Workshop:</h4>
        <!-- <p class="text-justify wow slideInLeft">IoT is bringing more and more things into the digital fold every day, which will likely make the IoT technology a very popular and demanding industry in the near future.</p> -->
        <ul class="text-justify wow slideInLeft">
         <li><b class="text-yellow">Arduino for Beginners:</b> A one-day workshop conducted mainly for the beginners introduces them to the amazing world of IoT and its fascinating applications. <br><br>Students will get exposed to basics of programming and few hands-on projects. <br><br>Students of any background with an interest in Arduino can attend this workshop.</li>
       </ul>
     </div>
     <div class="col-md-4">
      <img src="img/makers/iot-electronics-img.png" width="398" height="270" alt="makers lab, workshop, IoT and Electronics" class="img img-fluid img-thumbnail wow zoomIn">
    </div>
  </div>
</div>
<br>
</div>
<br>

<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>