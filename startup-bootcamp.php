<!DOCTYPE html>
<html lang="en">
<head>
	<title>Events | Startup Boot Camp</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/startup-bootcamp"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/startup-bootcamp">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/global-entrepreneur.png">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/global-entrepreneur-bg.png">
	<meta property="og:description" content="We at Deshpande Startups are glad to share that, as a part of Global Entrepreneurship Week, we are hosting a “Startup boot camp” in Hubballi."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We at Deshpande Startups are glad to share that, as a part of Global Entrepreneurship Week, we are hosting a “Startup boot camp” in Hubballi."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Startup Boot Camp, Deshpande Startups Event">
	<link rel="canonical" href="https://www.deshpandestartups.org/startup-bootcamp">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">


	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/global-entrepreneur-bg.png" width="1349" height="198" alt="Deshpande Startups, events, Startup Boot Camp">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">Startup Boot Camp</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">Startup </span>Boot Camp</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/global-entrepreneur.png" width="474" height="237" alt="Deshpande startups, events, Startup Boot Camp">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center text-truncate">Startup Boot Camp</h5>
									<p><b>Date : </b>November 24<sup>th</sup> & 25<sup>th</sup> 2019</p>
									<!-- <p><b>Fees : </b>Rs. 500/- per person</p> -->
									<p><b>Last date to register : </b>November 22<sup>nd</sup> 2019</p>
									<p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
									</p>
									<p class="text-truncate"><b>Contact details:</b><br>
										M:<a href="tel:+91-968-665-4749"> +91-968-665-4749</a><br>
										E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
									</p>
								</div>
								<div class="card-footer">
									<!-- <a href="#" class="btn btn-warning" target="_blank">Apply Now</a> -->
									<p class="text-yellow">The registrations has been closed.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-5 text-yellow mb-0"><b>Event description:</b></p>
						<p class="text-justify wow slideInRight">We at Deshpande Startups are glad to share that, as a part of <b>Global Entrepreneurship Week</b>, we are hosting a <b>“Startup boot camp”</b> in Hubballi.<br><br> The boot camp will focus on helping entrepreneurs solving the problems of TIER-II & TIER-III cities to minimize barriers and obstacles in their startup journey to maintain sustainability at different phases of their Startup Journey.</p>
						<p class="mb-0"><b class="text-yellow">What is happening in “Startup Boot Camp”?</b></p>
						<ul class="wow slideInRight">
							<li>Mentor meetup</li>
							<li>Workshops by industry experts</li>
							<li>Knowledge sessions by successful entrepreneurs</li>
							<li>B-plan competition to access support from Deshpande Startups</li>
						</ul>
						<p class="mb-0"><b class="text-yellow">Business deck format:</b></p>
						<ul class="wow slideInRight">
							<li>Introduction of the company & founding team - 1 slide</li>
							<li>Problem statement - 1 slide</li>
							<li>Your business deck - 1 to 3 slide</li>
							<li>Uniqueness about your ideas - 1 to 2 slide</li>
							<li>Business competition/ existing solution providers - 1 slide</li>
							<li>Market size/ opportunity - 1 to 2 slide</li>
							<li>Revenue model</li>
						</ul>
					</div>
				</div>

				<p class="text-yellow font-weight-bold">What does Startup Boot Camp has to offer to entrepreneurs:</p>
				<ul class="wow slideInRight text-justify">
					<li>Network, connect and receive inputs from industry-leading mentors, industry experts, investors</li>
					<li>Workshops and knowledge sessions by successful entrepreneurs</li>
					<li>Opportunity to pitch your business plan to panel of experts, entrepreneurs, industry veteran & angel investor</li>
				</ul>
				<p class="text-justify wow slideInRight"><h4 class="text-danger font-weight-bold">Note:</h4> After submitting the form, Please send your business deck to <a href="mailto:seir&#064;dfmail&#046;org">seir&#064;dfmail&#046;org</a></p>

				<!-- <div class="row">
					<div class="col-md-7 p-4 mt-2">
						<p><b class="text-yellow">Business deck format:</b></p>
						<ul class="wow slideInLeft">
							<li>Introduction of the company & founding team - 1 slide</li>
							<li>Problem statement - 1 slide</li>
							<li>Your business deck - 1 to 3 slide</li>
							<li>Uniqueness about your ideas - 1 to 2 slide</li>
							<li>Business competition/existing solution providers - 1 slide</li>
							<li>Market size/opportunity - 1 to 2 slide</li>
							<li>Revenue model</li>
						</ul>
					</div>
					<div class="col-md-5 p-4 mt-4">
						<p class="text-justify wow slideInRight"><h4 class="text-danger font-weight-bold">Note:</h4> After submitting the Form, Please send your business deck to <a href="mailto:seir&#064;dfmail&#046;org">seir&#064;dfmail&#046;org</a></p>
					</div>
				</div> -->

			</div>
		</div>
	</div>
	<br>

	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>