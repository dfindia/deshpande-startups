<!DOCTYPE html>
<html lang="en">
<head>
   <title>National Initiative for Developing and Harnessing Innovations (NIDHI)</title>
   <?php
   require_once 'essentials/meta.php';
   ?>
   <meta name="linkage" content="https://www.deshpandestartups.org/dst-nidhi-sss"/>
   <meta property="og:site_name" content="Deshpande Startups"/>
   <meta property="og:type" content="website">
   <meta property="og:url" content="https://www.deshpandestartups.org/dst-nidhi-sss">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/funded-startups/microchip-payments.jpg">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/funded-startups/widemobility.jpg">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/funded-startups/freshboxx.jpg">
   <meta property="og:description" content="The Entrepreneurship Development Center (Venture Center) is hosting a seed fund with a corpus of Rs 10 crore under the scheme National Initiative for Developing and Harnessing Innovations - Seed Support System (NIDHI-SSS) of the Department of Science and Technology."/>
   <meta name="author" content="Deshpande Startups"/>
   <meta name="description" content="The Entrepreneurship Development Center (Venture Center) is hosting a seed fund with a corpus of Rs 10 crore under the scheme National Initiative for Developing and Harnessing Innovations - Seed Support System (NIDHI-SSS) of the Department of Science and Technology."/>
   <!-- <meta name="keywords" content=""/> -->
   <meta property="og:title" content="National Initiative for Developing and Harnessing Innovations (NIDHI)">
   <link rel="canonical" href="https://www.deshpandestartups.org/dst-nidhi-sss">
   <?php
         // $title = 'Deshpande Startups';
   require_once 'essentials/bundle.php';
   ?>
   
</head>
<body>
   <?php
   require_once 'essentials/title_bar.php';
   require_once 'essentials/menus.php';
   ?>

   <div class="container">
      <br>
      <div class="center wow fadeInDown">
         <h2 class="text-yellow text-center">Department of Science and Technology<br> <span class="text-muted">National Initiative for Developing and Harnessing Innovations <br>(NIDHI)</span> </h2>
         <div class="divider b-y text-yellow content-middle"></div>
      </div><br>
      <h4  class="text-yellow cal">NIDHI-Seed Support System (NIDHI-SSS)</h4>
      <p class="text-justify wow slideInLeft">Deshpande Startups is hosting a seed fund with a corpus of Rs 10 crore under the scheme <b>"National Initiative for Developing and Harnessing Innovations - Seed Support System (NIDHI-SSS)"</b> of the Department of Science and Technology.</p>

      <p class="text-justify wow slideInLeft">The basic idea of NIDHI seed support is providing financial assistance to potential startups with promising ideas, innovations and technologies. This would enable some of these incubatee startups with innovative ideas/technologies to graduate to a level where they will be able to raise investments from angel/Venture capitalist or they will reach a position to seek loans from commercial banks/financial institutions. Thus the proposed seed support disbursed by an incubator to an incubatee is positioned to act as a bridge between development and commercialization of innovative technologies/products/services in a relatively hassle-free manner.</p>

      <p class="text-yellow"><b>Broad Areas to be covered under the financial assistance include:</b></p>
      <ul class="text-justify wow slideInLeft">
         <li>Product development</li>
         <li>Testing and trials</li>
         <li>Test Marketing</li>
         <li>Mentoring</li>
         <li>Professional Consultancy (To attract Professors of institutions to work with small firms)</li>
         <li>IPR issues</li>
         <li>Manpower for day to day operations</li>
         <li>Any other area as deemed necessary and recommended by the Management Committee</li>
      </ul>
   </div>


   <div class="container">
      <br>
      <div class="center wow fadeInDown">
         <h2 class="text-yellow text-center">FUNDED<span class="text-muted"> STARTUPS</span> </h2>
         <div class="divider b-y text-yellow content-middle"></div>
      </div>
      <!-- <br> -->

      <!-- <div class="row">
         <div class="col-md-12">
            <div class="row text-center text-yellow">
               <div class="col-md-4">
                  <img src="img/funded-startups/microchip-payments.jpg" class="img img-fluid img-thumbnail card-hover-shadow wow zoomIn" data-wow-duration="0.9s" width="380" height="214" data-wow-offset="50" data-wow-delay="0.1s" alt="funded startups, Microchip Payments Pvt. Ltd.">
                  <p class="pt-2"><b>Microchip Payments Pvt. Ltd.</b></p>
               </div>
               <div class="col-md-4">
                  <img src="img/funded-startups/widemobility.jpg" class="img img-fluid img-thumbnail card-hover-shadow wow zoomIn" data-wow-duration="0.9s" width="380" height="214" data-wow-offset="50" data-wow-delay="0.3s" alt="funded startups, Wide Mobility Mechatronics Pvt. Ltd.">
                  <p class="pt-2"><b>Wide Mobility Mechatronics Pvt. Ltd. </b></p>
               </div>
               <div class="col-md-4">
                  <img src="img/funded-startups/freshboxx.jpg" class="img img-fluid img-thumbnail card-hover-shadow wow zoomIn" data-wow-duration="0.9s" width="380" height="214" data-wow-offset="50" data-wow-delay="0.5s" alt="funded startups, Freshboxx Services Pvt. Ltd.">
                  <p class="pt-2"><b>Freshboxx Services Pvt. Ltd.</b></p>
               </div>
            </div>
            <br>
            <div class="row text-center text-yellow">
               <div class="col-md-4">
                  <img src="img/funded-startups/nautilus.png" class="img img-fluid img-thumbnail card-hover-shadow wow zoomIn" data-wow-duration="0.9s" width="380" height="214" data-wow-offset="50" data-wow-delay="0.1s" alt="funded startups, Nautilus Hearing Solutions Pvt. Ltd.">
                  <p class="pt-2"><b>Nautilus Hearing Solutions Pvt. Ltd.</b></p>
               </div>
               <div class="col-md-4">
                  <img src="img/funded-startups/shopgro.png" class="img img-fluid img-thumbnail card-hover-shadow wow zoomIn" data-wow-duration="0.9s" width="380" height="214" data-wow-offset="50" data-wow-delay="0.3s" alt="funded startups, Shopgro Pvt. Ltd.">
                  <p class="pt-2"><b>Shopgro Pvt. Ltd. </b></p>
               </div>
               <div class="col-md-4">
                  <img src="img/funded-startups/public-next.png" class="img img-fluid img-thumbnail card-hover-shadow wow zoomIn" data-wow-duration="0.9s" width="380" height="214" data-wow-offset="50" data-wow-delay="0.5s" alt="funded startups, ElectReps Pvt. Ltd.">
                  <p class="pt-2"><b>ElectReps Pvt. Ltd.</b></p>
               </div>
            </div>
         </div>
      </div> -->
   </div>
   <!-- <br>
   <br>
-->

<section class="container pb-4">
   <div class="row justify-content-md-center">
      <div class="col-xs-12 col-sm-6 col-md-4">
         <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
            <div class="mainflip">
               <div class="frontside">
                  <div class="card">
                     <div class="card-body text-center">
                        <img class="pt-5 img-fluid" src="img/funded-startups/microchip-payments.jpg" alt="Microchip Payments, Funded Startup">
                     </div>
                  </div>
               </div>
               <div class="backside">
                  <div class="card">
                     <div class="card-body">
                        <h5 class="pt-5 text-yellow"><b>Microchip Payments Pvt. Ltd.</b></h5>
                        <p class="">
                           Microchip Payments enables Payments without Internet Connectivity to boost the digital transaction even in Internet untapped regions.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4">
         <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
            <div class="mainflip">
               <div class="frontside">
                  <div class="card">
                     <div class="card-body text-center">
                        <img class="pt-5 img-fluid" src="img/funded-startups/widemobility.jpg" alt="Widemobility, Funded Startup">
                     </div>
                  </div>
               </div>
               <div class="backside">
                  <div class="card">
                     <div class="card-body">
                        <h5 class="pt-3 text-yellow"><b>Wide Mobility Mechatronics Pvt. Ltd.</b></h5>
                        <p class="">
                          Wide mobility offers digital radiography based solutions by enterprise class of image processing software and applications to ensure high quality vegetables and fruits inspection.
                       </p>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
     <div class="col-xs-12 col-sm-6 col-md-4">
      <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
         <div class="mainflip">
            <div class="frontside">
               <div class="card">
                  <div class="card-body text-center">
                     <img class="pt-5 img-fluid" src="img/funded-startups/freshboxx.jpg" alt="Freshboxx, Funded Startup">
                  </div>
               </div>
            </div>
            <div class="backside">
               <div class="card">
                  <div class="card-body">
                     <h5 class="pt-4 text-yellow"><b>Freshboxx Services Pvt. Ltd.</b></h5>
                     <p class="lh">
                        Freshboxx works to better value chain in agri sector by bridging the gap between farmers and consumers through their tech enabled distribution model.
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row justify-content-md-center">
   <div class="col-xs-12 col-sm-6 col-md-4">
      <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
         <div class="mainflip">
            <div class="frontside">
               <div class="card">
                  <div class="card-body text-center">
                     <img class="pt-5 img-fluid" src="img/funded-startups/nautilus.png" alt="Nautilus, Funded Startup">
                  </div>
               </div>
            </div>
            <div class="backside">
               <div class="card">
                  <div class="card-body">
                     <h5 class="pt-5 text-yellow"><b>Nautilus Hearing Solutions Pvt. Ltd.</b></h5>
                     <p class="">
                        Nautilus Hearing is a Medtech company using best technologies to make Hearing Care Simple, Accessible and Reliable.
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-xs-12 col-sm-6 col-md-4">
      <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
         <div class="mainflip">
            <div class="frontside">
               <div class="card">
                  <div class="card-body text-center">
                     <img class="pt-5 img-fluid" src="img/funded-startups/shopgro.png" alt="Shopgro, Funded Startup">
                  </div>
               </div>
            </div>
            <div class="backside">
               <div class="card">
                  <div class="card-body">
                     <h5 class="pt-4 text-yellow"><b>Shopgro Pvt. Ltd.</b></h5>
                     <p class="">
                        Simple, Automated hyper-local search marketing platform for businesses and brands. The technology ensures retailer and brands to build their digital presence.
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-xs-12 col-sm-6 col-md-4">
      <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
         <div class="mainflip">
            <div class="frontside">
               <div class="card">
                  <div class="card-body text-center">
                     <img class="pt-5 img-fluid" src="img/funded-startups/public-next.png" alt="Public Next, Funded Startup">
                  </div>
               </div>
            </div>
            <div class="backside">
               <div class="card">
                  <div class="card-body">
                     <h5 class="pt-4 text-yellow"><b>ElectReps Pvt. Ltd.</b></h5>
                     <p class="lh">
                        A Hyper local news sharing platform where its envision this to be the biggest local social and news media platform to ensure community knowledge and relationship.
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</section>

<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>