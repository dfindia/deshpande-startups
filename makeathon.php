<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Makeathon - Yuva Events</title>
      <?php
         require_once 'essentials/meta.php';
         ?>
      <meta name="linkage" content="https://www.deshpandestartups.org/makeathon"/>
      <meta property="og:site_name" content="Deshpande Startups"/>
      <meta property="og:type" content="website">
      <meta property="og:url" content="https://www.deshpandestartups.org/makeathon">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/makeathon-bg-img.png">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/events/makeathon-event.png">
      <meta property="og:description" content="An initiative of “Yuva Entrepreneurship Program” of Deshpande Startups present “MAKEATHON”, a platform for engineers and non-engineers, students and professionals to collaborate to build innovative solutions to challenging problems held at India’s Largest Incubation Center – The Maker’s Lab, next to Hubballi Airport."/>
      <meta name="author" content="Deshpande Startups"/>
      <meta name="description" content="An initiative of “Yuva Entrepreneurship Program” of Deshpande Startups present “MAKEATHON”, a platform for engineers and non-engineers, students and professionals to collaborate to build innovative solutions to challenging problems held at India’s Largest Incubation Center – The Maker’s Lab, next to Hubballi Airport."/>
      <!-- <meta name="keywords" content=""/> -->
      <meta property="og:title" content="Makeathon - Makers Lab Events">
      <link rel="canonical" href="https://www.deshpandestartups.org/makeathon">
      <?php
         require_once 'essentials/bundle.php';
         ?>
      <style type="text/css">
         .parallax {
         background-image: url("img/makers/deshpande2.jpg");
         min-height: 400px; 
         background-attachment: fixed;
         background-position: center;
         background-repeat: no-repeat;
         background-size: cover;
         }
         section.home{
         padding: 4rem 1rem;
         }
      </style>
   </head>
   <body>
      <?php
         require_once 'essentials/title_bar.php';
         require_once 'essentials/menus.php';
         ?>
      <img class="carousel-inner img-fluid" src="img/makers/makers-bg.png" width="1349" height="198" alt="Deshpande Startups, events makeathon">
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb justify-content-end">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item"><a href="yuva-entrepreneurship">Yuva Entrepreneurship</a></li>
            <li class="breadcrumb-item active" aria-current="page">Makeathon</li>
         </ol>
      </nav>
      <div class="container">
         <!-- <br> -->
         <div class="center wow fadeInDown">
            <h2 class="text-yellow text-center">MAKEATHON</h2>
            <div class="divider b-y text-yellow content-middle"></div>
         </div>
         <br>
         <div class="row">
            <!-- <div class="col-md-10"> -->
            <div class="col-md-10">
               <p class="text-justify wow slideInLeft">An initiative of <b>“Yuva Entrepreneurship Program”</b> of Deshpande Startups present <b>“Makeathon”</b>, a platform for engineers and non-engineers, students and professionals to collaborate to build innovative solutions to challenging problems held at India’s Largest Incubation Center – The Deshpande Startups, next to Hubballi Airport.<br><br><b>“Makeathon”</b> is an exciting opportunity for all creative thinkers to meet at one single platform, and develop Social Impact solutions and witness boosting India’s true growth story.</p>
               <p class="text-justify wow slideInLeft">Applicants will have to <b>build a product to solve one of our problem statements</b> with your team and guidance from expertise mentors. Participants will get to learn about the product design process and pitching through the inspirational breaks and conversations with peers and industry professionals.</p>
            </div>
            <div class="col-2">
               <a href="mm-register" class="btn btn-rotate" target="_blank">Apply Now</a>
            </div>
            <!-- <div class="col-md-2">
               <div class="row pl-4">
                <a href="#" class="btn btn-rotate" target="_blank">Apply Now</a>
               </div>
               </div> -->
         </div>
      </div>
      <br>
      <!-- <br> -->
      <div class="featured-bg-container">
         <div class="row valign-wrapper">
            <div class="col-md-4">
               <img src="img/makers/makeathon-img.png" width="398" height="270" alt="makers lab, makeathon event" class="img img-fluid img-thumbnail wow zoomIn">
            </div>
            <div class="col-md-8">
               <h4 class="text-yellow"><b>8 Reasons why you should participate/key highlights of the event:</b></h4>
               <ul class="text-justify wow slideInRight">
                  <li>Get to be a part of vibrant community of makers and build your network</li>
                  <li>Internship opportunities at Deshpande Startups</li>
                  <li>Gain practical exposer to new technologies and hands on experience on utilizing cutting edge machines available at the Makers Lab</li>
                  <li>Learn how to prototype innovative ideas efficiently while reducing the product development cycle</li>
                  <li>Network with exciting startups, enthusiastic makers/developers and explore further opportunities</li>
                  <li>Win awards/recognition among makers</li>
                  <li>Inspire the younger generation by sharing your practical knowledge and nurture them in developing their skills, we are certain that giving back is as joyous as learning</li>
                  <li>Add real value to your resume with certification from Deshpande Startups</li>
               </ul>
            </div>
         </div>
      </div>
      <br>
      <div class="container">
         <!-- <h3>Frequently Asked Questions</h3> -->
         <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
            <h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">FREQUENTLY</span> ASKED QUESTIONS</h2>
            <div class="divider b-y text-yellow content-middle"></div>
         </div>
         <br>
         <div id="accordion">
            <div class="card cal card-hover-shadow">
               <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                     <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" ">
                        <h4 class="text-yellow">What is a problem statement?</h4>
                     </button>
                  </h5>
               </div>
               <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body">
                     <div class="wpb_wrapper cal">
                        <p class="text-justify">
                           A problem statement is a concise description of an issue to be addressed or a condition to be improved upon. It identifies the gap between the current (problem) state and desired (goal) state of a process or product. Makers Makeathon problem statements will be revealed on the first day of the event. Problem statements will be targeting more than one sector. 
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="card cal card-hover-shadow">
               <div class="card-header" id="headingTwo">
                  <h5 class="mb-0">
                     <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <h4 class="text-yellow">Who can participate?</h4>
                     </button>
                  </h5>
               </div>
               <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                  <div class="card-body">
                     <!-- <br> -->
                     <div class="wpb_wrapper cal">
                        <p class="text-justify">
                           Students from all different backgrounds are welcome to register. We believe that multidisciplinary teams can tackle problems from different angles, which may result in better quality product solutions.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="card cal card-hover-shadow">
               <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                     <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <h4 class="text-yellow"> How does it work?</h4>
                     </button>
                  </h5>
               </div>
               <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                  <div class="card-body">
                     <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper cal">
                           <p class="text-justify">
                              Over the course of 3 days, you will work in teams of 4 to create a solution for one of the problem statements revealed at the opening ceremony of the Makers Makeathon. Teams will be formed on the 1st day of the event after the problem statements are announced. There is no registration fees for attending the launch of Makers Makeathon, but participants who wish to continue with the event have to pay a fee of 200 INR per participant. Only 25 product ideas will be shortlisted to continue for the event. Team formation time will be open after announcing the problem statements and will be closed on the second day. Teams need to be approved by mentors and be as multidisciplinary as possible. Participants will get badges showing their skills and areas of interests to make it easier to recognize talent to form teams.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="card cal card-hover-shadow">
               <div class="card-header" id="headingFour">
                  <h5 class="mb-0">
                     <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        <h4 class="text-yellow"> What should I bring?</h4>
                     </button>
                  </h5>
               </div>
               <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                  <div class="card-body">
                     <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper cal">
                           <p class="text-justify">
                              Participants need to provide their own laptops. We will be providing the tools needed to operate the facilities available at Deshpande startups.<br> The facilities include: fully loaded Makers Lab, 3D Printers, IoT tools, electronic lab, CNC, laser cutting machine, carpentry tools, PCB design and printing and working space. Facilitators will be operating some of the machines to insure the safety of all the participants.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="card cal card-hover-shadow">
               <div class="card-header" id="headingFive">
                  <h5 class="mb-0">
                     <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        <h4 class="text-yellow"> What do I make?</h4>
                     </button>
                  </h5>
               </div>
               <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                  <div class="card-body">
                     <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper cal">
                           <p class="text-justify">
                              Potentially, any product that could solve any of the problem statements. The product could be hardware or software or a mix of both. Participants have the freedom to explore any technologies that may be used to solve a problem statement.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="card cal card-hover-shadow">
               <div class="card-header" id="headingSix">
                  <h5 class="mb-0">
                     <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                        <h4 class="text-yellow"> How much does this cost?</h4>
                     </button>
                  </h5>
               </div>
               <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                  <div class="card-body">
                     <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper cal">
                           <p class="text-justify">
                              The Entree of the first day is for free, but individuals who choose to officially register for Makers Makeathon and continue with the event have to pay a registration fee of 200 INR. 
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="card cal card-hover-shadow">
               <div class="card-header" id="headingSeven">
                  <h5 class="mb-0">
                     <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                        <h4 class="text-yellow"> What can I benefit from Makers Makeathon?</h4>
                     </button>
                  </h5>
               </div>
               <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                  <div class="card-body">
                     <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper cal">
                           <ul class="text-justify">
                              <li>Get to meet multidisciplinary people, Peer to Peer Learning</li>
                              <li>Practical Exposure to Technology/Hands on Learning</li>
                              <li>Platform for creative/innovative ideas or products</li>
                              <li>Opportunity to learn and utilize the makers lab for all kind of professionals and local community</li>
                              <li>Support ideation product startups in experimenting and building prototypes</li>
                              <li>Build prototypes much faster & reduce the product development cycle</li>
                              <li>Developing technical skills and building innovative ideas/products</li>
                              <li>Be a part of makers community ecosystem</li>
                              <li>For technology aspirants to gets hands-on experience on the latest industry machineries and quench their thrust of knowledge</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <br>
      <br>
      <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once 'essentials/js.php';
         ?>
   </body>
</html>