<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Ideathon - Makers Lab Events</title>
      <?php
         require_once 'essentials/meta.php';
         ?>
      <meta name="linkage" content="https://www.deshpandestartups.org/ideathon"/>
      <meta property="og:site_name" content="Deshpande Startups"/>
      <meta property="og:type" content="website">
      <meta property="og:url" content="https://www.deshpandestartups.org/ideathon">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/bg-ideathon.png">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/ideathon-img.png">
      <meta property="og:description" content="Do you have an innovative idea that can translate into a cutting-edge product or service? Participate in the IDEATHON, a Platform to Present Your Idea to Solve Real World Problems and get selected to YUVA ENTREPRENEURSHIP PROGRAM to Kick Start Your Startup Journey with the startup ecosystem and Win 1 Lakh Worth Rewards."/>
      <meta name="author" content="Deshpande Startups"/>
      <meta name="description" content="Do you have an innovative idea that can translate into a cutting-edge product or service? Participate in the IDEATHON, a Platform to Present Your Idea to Solve Real World Problems and get selected to YUVA ENTREPRENEURSHIP PROGRAM to Kick Start Your Startup Journey with the startup ecosystem and Win 1 Lakh Worth Rewards."/>
      <!-- <meta name="keywords" content=""/> -->
      <meta property="og:title" content="Ideathon - Makers Lab Events">
      <!-- <link rel="canonical" href="https://www.deshpandestartups.org/ideathon"> -->
      <?php
         // $title = 'Deshpande Startups';
         require_once 'essentials/bundle.php';
         ?>
   </head>
   <body>
      <?php
         require_once 'essentials/title_bar.php';
         require_once 'essentials/menus.php';
         ?>
      <img class="carousel-inner img-fluid" src="img/makers/bg-ideathon.jpg" width="1349" height="198" alt="Deshpande Startups, events IDEATHON">
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb justify-content-end">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item"><a href="yuva-entrepreneurship">Yuva Entrepreneurship</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ideathon</li>
         </ol>
      </nav>
      <div class="container">
         <!-- <br> -->
         <div class="center wow fadeInDown">
            <h2 class="text-yellow text-center">IDEATHON</h2>
            <div class="divider b-y text-yellow content-middle"></div>
         </div>
         <br>
         <div class="row">
            <div class="col-md-10">
               <!-- <div class="col-md-12"> -->
               <p class="text-justify wow slideInLeft">An initiative of <b>“Yuva Entrepreneurship Program”</b> of Deshpande Startups present <b>“Ideathon”</b>, a platform to present your idea. An opportunity for young minds to pitch their ideas and meet young aspiring entrepreneurs. Be a part of this exclusive event and sense the vibrant startup ecosystem to become India's propelling true growth story and get selected for <b>Yuva Entrepreneurship Program</b> to kick start your startup journey with likeminded entrepreneurial catalysts.</p>
            </div>
            <div class="col-md-2">
               <div class="row pl-4">
                  <a href="ideathon-form" class="btn btn-rotate" target="_blank">Apply Now</a>
               </div>
            </div>
         </div>
         <!-- <p class="text-justify">Any student/graduates aged between 15-23 are eligible to take part in this event.</p>
            <p><b>Event date:</b> 21<sup>st</sup> March 2020</p>
            <p><b>Last date to apply:</b> 18<sup>th</sup> March 2020</p> -->
      </div>
      <br>
      <div class="featured-bg-container">
         <div class="row valign-wrapper justify-content-md-center">
            <div class="col-md-5 text-center">
               <img src="img/makers/ideathon-img.png" width="398" height="270" alt="makers lab, ideathon event" class="img img-fluid img-thumbnail wow zoomIn">
            </div>
            <div class="col-md-7">
               <h4 class="text-yellow"><b>6 Reasons why you should participate:</b></h4>
               <ul class="text-justify wow slideInRight">
                  <li>Access to makers lab & support to build the prototype</li>
                  <li>Furnished co-working space with Wi-Fi facility</li>
                  <li>Connect with mentors</li>
                  <li>Networking opportunity with industry experts</li>
                  <li>Training & workshops</li>
                  <li>Knowledge sessions</li>
               </ul>
               <!-- <ul class="text-justify wow slideInRight">
                  <li>As a part of winning team, you will receive support from Makers Lab - a platform to ideate, design and build your product</li>
                  <li>Opportunity to work in full-fledged ESDM lab</li>
                  <li>Get to be a part of vibrant community of developers and build your network</li>
                  <li>Furnished co-working space with Wi-Fi facility</li>
                  <li>Mentorship and coaching by professionals</li>
                  <li>Training and workshops</li>
                  <li>Knowledge sessions</li>
                  <li>Unique chance to meet and network with exciting startups and team with high-enthu developers and explore further opportunities</li>
                  </ul> -->
            </div>
         </div>
      </div>
      <br>
      <section id="pdf">
         <br>
         <div class="text-center">
            <!-- <a href="contact-us" class="btn btn-warning button4" target="_blank">Contact Us </a> -->
            <a href="img/pdf/ideathon/ideathon-presentation-format.pptx" class="btn btn-warning button4" download><i class="fa fa-download blink" aria-hidden="true"></i> Ideathon Presentation Format</a>
            <!-- <a href="img/pdf/ideathon/ideathon-presentation-template.pptx" class="btn btn-warning button4" download><i class="fa fa-download blink" aria-hidden="true"></i> Ideathon Presentation Template</a> -->
            <!-- <a href="" class="btn btn-warning button4" data-toggle="modal" data-target="#quotation-modal" target="_blank">Get Your PCB Assembly Quotation</a>
               <a href="contact-us" class="btn btn-warning button4" target="_blank">Contact Us </a>   -->
         </div>
      </section>
      <br>
      <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once 'essentials/js.php';
         ?>
   </body>
</html>