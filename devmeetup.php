<!DOCTYPE html>
<html lang="en">
<head>
	<title>#devmeetup | Events, Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/devmeetup"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/devmeetup">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/events/developtn.jpg">
	<meta property="og:description" content="Makers Lab Presents, DevMeetup + Google I/O Extended,Devmeetup + Google I/O Extended, An initiative of Makers Lab, Deshpande Startups."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Makers Lab Presents, DevMeetup + Google I/O Extended,Devmeetup + Google I/O Extended, An initiative of Makers Lab, Deshpande Startups."/>
	<!-- <meta name="keywords" content=""/> -->
	<meta property="og:title" content="Deshpande Startups #devmeetup">
	<link rel="canonical" href="https://www.deshpandestartups.org/devmeetup">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<img class="carousel-inner img-fluid" src="img/events/devmeetup.jpg" width="1349" height="198" alt="Deshpande Startups, events devmeetup">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
			<li class="breadcrumb-item"><a href="events">Events</a></li>
			<li class="breadcrumb-item active" aria-current="page">#devmeetup</li>
		</ol>
	</nav>
	<div class="container">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pt-2">
			<h2 class=" text-yellow text-center wow slideInDown"><span class="text-muted">#</span>DEVMEETUP</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<div class="row">
			<div class="col-md-12 px-5">
				<div class="row">
					<div class="col-md-5 p-4 mt-4">
						<div class="card-deck">
							<div class="card card-wrapper col-lg-11 col-md-11 col-sm-11 col-xs-12 card-hover-shadow">
								<img class="card-img-top img-fluid wow zoomIn" src="img/events/developtn.jpg" width="474" height="237" alt="Deshpande startups nanopix logo">
								<div class="card-body">
									<h5 class="card-title text-yellow text-center">#devmeetup</h5>
									<p><b>Date : </b>May 10<sup>th</sup> 2018</p>
									<!-- <p><b>Last Date to Register : </b>May 05<sup>th</sup> 2018</p> -->
									<p><b>Venue :</b> Deshpande Startups,<br> Next to Airport, Opp to Gokul Village, Gokul Road, Hubballi, Karnataka.
									</p>
									<p><b>Contact details:</b><br>
										M:<a href="tel:+91-951-331-5791"> +91-951-331-5791</a><br>
										E:<a href="mailto:seir&#064;dfmail&#046;org"> seir&#064;dfmail&#046;org</a>
									</p>
								</div>
								<div class="card-footer">
									<p class="text-yellow">The registrations has been closed.</p>
									<!-- <a href="http://bit.ly/Devmeetupgoogle" class="btn btn-primary" target="_blank">Apply Now</a> -->
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<p class="pt-5 text-yellow"><b>Event Description:</b></p>
						<p class="text-justify">An initiative of Deshpande Startups, Makers Lab, a workspace for experimentation, fabrication and
							prototyping, as well as provides co-working and incubation space in makers lab, which has 12,000
							(Sq.ft) square feet in Hubballi, it is the first facility of its kind in North Karnataka.
						</p>
						<p class="text-justify pt-3">Conceived to train passionate/enthusiastic and early product Startups, Makers Lab gives its
						members an opportunity to build ideas into products, prototypes and experimentation.</p><br>
						<p class="text-justify"><b>Makers Lab Presents, DevMeetup + Google I/O Extended,</b> An initiative of Makers Lab, Deshpande Startups, for the first
							time hosting Google I/O 2018 Live Stream, a gathering of curious and like minded 100 Hackers,
							which is a 24-hours Hackathon, will be held at the India&#39;s Largest Incubation Center, next to
						International Airport, Hubballi, Also sense the vibrant startup ecosystem in a tier 2 city.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="text-justify">Google I/O is an annual developer conference where Google announces its new hardware,
					software, and various updates for its existing apps and services. This takes place at the Shoreline
					Amphitheatre in Mountain View, California on 8th,9th and 10th of May. Google I/O Extended
					events help developers from around the world take part in the I/O experience. Meanwhile Makers
					Lab is hosting a Hackathon to build the technical skill among the budding developers and want to
					create a hub for developers to encourage the art of developing and building products in Makers
					Lab Hubblli.
				</p>
				<p class="text-justify"><b>1. 24 Hours Hackathon :</b> Develop the Social Impact Solutions and sense the vibrant startup
				ecosystem in a tier 2 city during Hackathon.</p>
				<p class="text-justify"><b>2. 3D Printing Workshop:</b> Learn Designing, 3D Scanning and 3D Printers and Printing.</p>
				<p class="text-justify"><b>3. Workshop on Electronics of Drones:</b> Drone Design/Developers delivered to you from the
				experienced folks. Rights from the basics till the flight of it.</p>
				<p class="text-justify"><b>4. Google Apps script workshop:</b> Come to know the cool products of Google.</p>
				<p class="text-justify"><b>5. Women Techmakers:</b> Google women techmakers <a href="https://www.womentechmakers.com/">www.womentechmakers.com</a> Is a program introduced by Google to empower and encourage women in tech, by building a global community and providing scholarships, conducting monthly meetups to keep things in cycle.</p>
				<p class="text-justify"><b>6. Google I/O Extended 2018 Live Stream:</b> A place where the event is taking place at San
					Francisco will be shown must be arranged. Walk-ins can attend this live stream and also
				the show n tell events only.</p>
				<p class="text-justify"><b>7. Show ‘n’ tell :</b> A 5 minutes opportunity for local tech enthusiasts to showcase present their Hacks or innovative/unique products among the gathered developers.</p>
			</div>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>