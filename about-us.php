<!DOCTYPE html>
<html lang="en">
   <head>
      <title>About Us | Deshpande Startups</title>
      <?php
         require_once 'essentials/meta.php';
         ?>
      <meta name="linkage" content="https://www.deshpandestartups.org/about-us"/>
      <meta property="og:site_name" content="Deshpande Startups"/>
      <meta property="og:type" content="website">
      <meta property="og:url" content="https://www.deshpandestartups.org/about-us">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/team/deshpande-bg-who-we-are.jpg">
      <meta property="og:image" content="https://www.deshpandestartups.org/img/mentors/gururaj.png">
      <meta property="og:description" content="We envision an environment where anyone can realize their entrepreneurial aspiration, with free and full access to our startup ecosystem, regardless of one’s socio-cultural, geographic, education or financial background or venture domain one that will meet the realities of real India."/>
      <meta name="author" content="Deshpande Startups"/>
      <meta name="description" content="We envision an environment where anyone can realize their entrepreneurial aspiration, with free and full access to our startup ecosystem, regardless of one’s socio-cultural, geographic, education or financial background or venture domain one that will meet the realities of real India."/>
      <!-- <meta name="keywords" content="Makers lab, Best Ecosystem, Board members, Dr. Gururaj, Desh, Deshpande Founder, Deshpande foundation, Vivek pawar chairman, Deshpande startups, Dr. Aravind Chinchure chair professor of innovation and entrepreneurship, Ramanathan Narayan CEO and Managing Director, 4R Recycling"/> -->
      <meta property="og:title" content="Deshpande Startups About Us">
      <!-- <link rel="canonical" href="https://www.deshpandestartups.org/about-us"> -->
      <?php
         // $title = 'Deshpande Startups';
         require_once 'essentials/bundle.php';
         ?>
      <style type="text/css">
         .button4 {border-radius: 20px;}
      </style>
   </head>
   <body>
      <?php
         require_once 'essentials/title_bar.php';
         require_once 'essentials/menus.php';
         ?>
      <img class="carousel-inner img-fluid" src="img/team/about-us-bg.png" width="1349" height="400" alt="About us, Deshpande Startups, deshpande foundation">
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb justify-content-end">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Who We Are</li>
         </ol>
      </nav>
      <div class="container">
         <h2 class="text-yellow text-center wow slideInDown pt-2" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">WHO</span> WE ARE</h2>
         <div class="divider b-y text-yellow content-middle"></div>
      </div>
      <br>
      <div class="container-fluid">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-yellow">
               <h3 class="wow slideInLeft text-center"  data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.3s"><b>India’s largest platform to penetrate into the 70% (TIER-II &amp; TIER-III) untapped India's market.</b></h3>
               <h3 class="wow slideInLeft text-center"  data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.4s"><b>World-class facilities to build product startups with an access to Makers Lab &amp; ESDM Cluster.</b></h3>
               <div class="divider b-y text-yellow content-middle p-4"></div>
            </div>
         </div>
      </div>
      <div class="container">
         <!-- <h4 class="text-center"><b class="text-yellow">"Our vision is to enable Entrepreneurship for all members of the society"</b></h4> -->
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <p class="wow slideInLeft text-justify" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
                  We envision an environment where anyone can realize their <b>entrepreneurial aspiration,</b> with free and full access to our startup ecosystem, regardless of one’s socio-cultural, geographic, education or financial background or venture domain one that will meet the realities of real India.
               </p>
               <p class="wow slideInLeft text-justify" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
                  We see a wide spectrum of <b>startups</b> emerging in India to meet the heterogeneous structure of the Indian economy and society.<br> Venture-funded technology startups, whilst important, is just one such form and even there, the model still needs to evolve for India. We see the vibrancy of such entrepreneurship at scale drive the future of <b>livelihoods, economic growth, global identity and social infrastructure development for India.</b>
               </p>
               <p class="wow slideInLeft text-justify" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
                  The purpose of Deshpande Startups is to catalyze such an environment.  The world has <b>7 billion</b> people.  About the top 2 billion are now served well by what we now know as the startup ecosystem in metros in India and abroad.  The economic activity for the bottom 5 billion is picking up and that growing economy is underserved by startups.  There are startups coming up in big metros to serve this population in India.  However, they suffer being far removed from the customer base.  The audacious goal for the incubation centre in <b>Hubballi</b> is to create the <b>Best Ecosystem</b> for startups to serve the bottom 5 billion people in the world and the associated businesses.
               </p>
               <p class="wow slideInLeft text-justify" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
                  India has over 1.3 billion people.  About 200 million of them live in cities and over 1.1 billion live in villages and semi-urban areas.  Hubballi is a small city surrounded by villages.  The incubator in Hubballi offers access to a rural and semi-urban customer base, motivated cost-effective talent and compassionate mentors who can nurture startups. The startups can draw on the expertise and support of 500 professionals of the <b>Deshpande Foundation’s Hubballi sandbox</b> who are actively engaged in programs for <b>skill development, agriculture, livelihoods and healthcare</b> for these rural and semi-urban customers. With the assistance of these seasoned professionals the startups can refine business models designed to serve this important customer segment.
               </p>
               <p class="wow slideInLeft text-justify" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s">
                  If startups in Hubballi can serve villages and semi-urban areas in India, they can then expand their businesses to global markets to serve the 5 billion people at the base of the economic pyramid across the world.
               </p>
            </div>
         </div>
      </div>
      <hr class="featurette-divider-sm">
      <h2 class="text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">BOARD</span> MEMBERS</h2>
      <div class="divider b-y text-yellow content-middle"></div>
      <!-- /.container --> 
      <div class="container">
         <!-- row1 -->
         <div class="row pt-4">
            <div class="col-lg-4">
               <div class="text-center">
                  <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" src="img/mentors/gururaj.png" width="165" height="160"  alt="Dr. Gururaj Desh Deshpande, Advisory Board Deshpande Startups"><br>
                  <p class="text-yellow pb-0 mb-1"><b>Dr. Gururaj "Desh" Deshpande</b><br>
                     <span class="text-muted">Advisor, Deshpande Startups</span>
                  </p>
               </div>
               <!-- <br> -->
               <div class="text-center">
                  <a href="https://www.linkedin.com/in/deshdeshpande" rel="nofollow" class="btn btn-warning button4" target="_blank">View Profile</a>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="text-center">
                  <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" src="img/mentors/aravind-chinchure.jpg" width="165" height="160"  alt="Dr. Aravind Chinchure, Director, Deshpande startups"><br>
                  <p class="text-yellow pb-0 mb-1"><b>Dr. Aravind Chinchure</b><br>
                     <span class="text-muted">Director, Deshpande Startups</span>
                  </p>
               </div>
               <div class="text-center">
                  <a href="https://www.linkedin.com/in/aravindchinchure" rel="nofollow" class="btn btn-warning button4" target="_blank">View Profile</a>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="text-center">
                  <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" src="img/team/ramnathan.jpg" width="165" height="160"  alt="Ramanathan Narayanan, Director, Deshpande startups"><br>
                  <p class="text-yellow pb-0 mb-1"><b>Ramanathan Narayanan</b><br>
                     <span class="text-muted">Director, Deshpande Startups</span>
                  </p>
               </div>
               <div class="text-center">
                  <a href="https://www.linkedin.com/in/ramanathann" rel="nofollow" class="btn btn-warning button4" target="_blank">View Profile</a>
               </div>
            </div>
         </div>
         <div class="row pt-4">
            <div class="col-lg-2"></div>
            <div class="col-lg-4">
               <div class="text-center">
                  <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" src="img/team/rajiv-prakash.jpg" width="165" height="160"  alt="Rajiv Prakash, Director, Deshpande startups"><br>
                  <p class="text-yellow pb-0 mb-1"><b>Rajiv Prakash</b><br>
                     <span class="text-muted">Director,<br> Deshpande Startups</span>
                  </p>
                  <!-- <br> -->
               </div>
               <div class="text-center">
                  <a href="https://www.linkedin.com/in/rajivprakashnextin/" rel="nofollow" class="btn btn-warning button4" target="_blank">View Profile</a>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="text-center">
                  <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s" src="img/team/c-m-patil.jpg" width="165" height="160"  alt="C. M. Patil, Chief Executive Officer, Deshpande startups"><br>
                  <p class="text-yellow pb-0 mb-1"><b>C. M. Patil</b><br>
                     <span class="text-muted">Chief Executive Officer,<br> Deshpande Startups</span>
                  </p>
               </div>
               <div class="text-center">
                  <a href="https://www.linkedin.com/in/cmpatil/" rel="nofollow" class="btn btn-warning button4" target="_blank">View Profile</a>
               </div>
            </div>
         </div>
      </div>
      <!-- row1 -->
      <br>
      <hr class="featurette-divider-sm">
      <h2 class="text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">TEAM</span> MEMBERS</h2>
      <div class="divider b-y text-yellow content-middle"></div>
      <!-- <div class="wpb_wrapper text-yellow p-2">
         <h2 class="text-center text-muted">“Talent wins games, but <span class="text-yellow">teamwork</span> and <span class="text-yellow">intelligence</span><br> win <span class="text-yellow">championships”</span></h2>
         </div> -->
      <!-- /.container --> 
      <!--</div>  -->
      <div class="container">
         <div class="row pt-2">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" src="img/team/gopalkrishnan.png" width="165" height="160" alt="B Gopala Krishnan, Director, Incubation, Deshpande Startups">
               <p class="text-yellow"><b>B Gopala Krishnan</b><br>
                  <span class="text-muted">Director - Incubation</span>
               </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" src="img/team/golden.jpg" width="165" height="160" alt="Golden Frankly, Deputy Director - Incubation, Deshpande Startups">
               <p class="text-yellow"><b>Golden Frankly</b><br>
                  <span class="text-muted">Deputy Director - Incubation</span>
               </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" src="img/team/amrut.jpg" width="165" height="160" alt="Amrut Patil, Deputy Director - Yuva Entrepreneurship, Deshpande Startups">
               <p class="text-yellow"><b>Amrut Patil</b><br>
                  <span class="text-muted">Deputy Director - Yuva Entrepreneurship</span>
               </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" src="img/team/manjunath-gogi.jpg" width="165" height="160" alt="Manjunath Gogi, Sr. Manager, Deshpande Startups">
               <p class="text-yellow"><b>Manjunath Gogi</b><br>
                  <span class="text-muted">Sr. Manager</span>
               </p>
            </div>
            <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" src="img/team/sharanu-mugali.jpg" width="165" height="160" alt="Sharanu Mugali, Manager - IT, Deshpande Startups">
               <p class="text-yellow"><b>Sharanu Mugali</b><br>
               	<span class="text-muted">Manager - IT</span>
               </p>
               </div> -->
         </div>
         <!-- <br> -->
         <div class="row p-2">
            <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" src="img/team/manjunath-gogi.jpg" width="165" height="160" alt="Manjunath Gogi, Startup evangelist, Deshpande Startups">
               <p class="text-yellow"><b>Manjunath Gogi</b><br>
               	<span class="text-muted">Startup evangelist</span>
               </p>
               </div> -->
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" src="img/team/veer.png" width="165" height="160" alt="Veeranagouda S, Business Development - ESDM Cluster, Deshpande startups">
               <p class="text-yellow"><b>Veeranagouda S</b><br>
                  <span class="text-muted">Business Development Manager-ESDM</span>
               </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" src="img/team/neelappa.png" width="165" height="160" alt="Neelappa H, Manager-ESDM, Deshpande Startups">
               <p class="text-yellow"><b>Neelappa H</b><br>
                  <span class="text-muted">Manager-ESDM</span>
               </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" src="img/team/vijaylaxmi.png" width="165" height="160" alt="Vijaylaxmi Patil, Sr. Executive-Incubation, Deshpande Startups">
               <p class="text-yellow"><b>Vijaylaxmi Patil</b><br>
                  <span class="text-muted">Sr. Executive-Incubation</span>
               </p>
            </div>
            <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" src="img/team/snehal-dixit.png" width="165" height="160" alt="Snehal H Dixit, Incubation Executive, Deshpande Startups">
               <p class="text-yellow"><b>Snehal H Dixit</b><br>
               	<span class="text-muted">Incubation Executive</span>
               </p>
               </div> -->
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" src="img/team/kedar.png" width="165" height="160" alt="Kedar Kulkarni, Sr. Executive-Incubation, Deshpande Startups">
               <p class="text-yellow"><b>Kedar Kulkarni</b><br>
                  <span class="text-muted">Sr. Executive-Incubation</span>
               </p>
            </div>
            <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" src="img/team/azhar-yakkundi.jpg" width="165" height="160" alt="Azhar Yakkundi, Incubation Executive, Deshpande Startups">
               <p class="text-yellow"><b>Azhar Yakkundi</b><br>
               	<span class="text-muted">Incubation Executive</span>
               </p>
               </div>			 -->
         </div>
         <div class="row p-2 justify-content-md-center">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" src="img/team/yashvi-mehta.png" width="165" height="160" alt="Yashvi Mehta, Executive-Yuva Entrepreneurship, Deshpande Startups">
               <p class="text-yellow"><b>Yashvi Mehta</b><br>
                  <span class="text-muted">Executive-Yuva Entrepreneurship</span>
               </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" src="img/team/snehal-dixit.png" width="165" height="160" alt="Snehal H Dixit, Incubation Executive, Deshpande Startups">
               <p class="text-yellow"><b>Snehal H Dixit</b><br>
                  <span class="text-muted">Incubation-Executive</span>
               </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" src="img/team/azhar-yakkundi.jpg" width="165" height="160" alt="Azhar Yakkundi, Incubation-Executive, Deshpande Startups">
               <p class="text-yellow"><b>Azhar Yakkundi</b><br>
                  <span class="text-muted">Incubation-Executive</span>
               </p>
            </div>
            <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" src="img/team/vijaylaxmi.png" width="165" height="160" alt="Vijaylaxmi Patil, Senior Incubation Associate, Deshpande Startups">
               <p class="text-yellow"><b>Vijaylaxmi Patil</b><br>
               	<span class="text-muted">Sr. Executive-Incubation</span>
               </p>
               </div> -->
            <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" src="img/team/kedar.png" width="165" height="160" alt="Kedar Kulkarni, Senior Incubation Associate, Deshpande Startups">
               <p class="text-yellow"><b>Kedar Kulkarni</b><br>
               	<span class="text-muted">Sr. Executive-Incubation</span>
               </p>
               </div>	 -->
         </div>
         <div class="row p-2 justify-content-md-center">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" src="img/team/soundhar.png" width="165" height="160" alt="Soundhar B, Incubation Associate, Deshpande Startups">
               <p class="text-yellow"><b>Soundhar B</b><br>
                  <span class="text-muted">Incubation Associate</span>
               </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" src="img/team/harish.png" width="165" height="160" alt="Harish Gadagin, Technical Executive, Makers Lab, Deshpande Startups">
               <p class="text-yellow"><b>Harish Gadagin</b><br>
                  <span class="text-muted">Technical Executive, Makers Lab</span>
               </p>
            </div>
            <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" src="img/team/habiba.jpg" width="165" height="160" alt="Umme Habiba Arlikatti, Web Developer, Deshpande Startups">
               <p class="text-yellow"><b>Umme Habiba Arlikatti</b><br>
               	<span class="text-muted">Web Developer</span>
               </p>
               </div> -->
         </div>
      </div>
      <hr class="featurette-divider-sm">
      <h2 class="text-yellow text-center wow slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">SUPPORTING</span> TEAM</h2>
      <div class="divider b-y text-yellow content-middle"></div>
      <div class="container">
         <div class="row p-2 justify-content-md-center">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" src="img/team/sharanu-mugali.jpg" width="165" height="160" alt="Sharanu Mugali, Manager - IT, Deshpande Startups">
               <p class="text-yellow"><b>Sharanu Mugali</b><br>
                  <span class="text-muted">Manager, IT</span>
               </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.7s" src="img/team/sanmati.png" width="165" height="160" alt="Sanmati Jakkannavar,  Asst. Manager - Human Resources, Deshpande Startups">
               <p class="text-yellow"><b>Sanmati Jakkannavar</b><br>
                  <span class="text-muted">Asst. Manager, HR</span>
               </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.3s" src="img/team/siddu-parannavar.jpg" width="165" height="160" alt="Siddu Parannavar, Manager, Admin & Operations, Deshpande Startups">
               <p class="text-yellow"><b>Siddu Parannavar</b><br>
                  <span class="text-muted">Manager, Admin & Operation </span>
               </p>
            </div>
         </div>
         <div class="row p-2 justify-content-md-center">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.5s" src="img/team/raj-reddy.jpg" width="165" height="160" alt="Raju Mudhireddy,Graphic Designer, Deshpande Startups">
               <p class="text-yellow"><b>Raju Mudhireddy</b><br>
                  <span class="text-muted">Graphic Designer</span>
               </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
               <img class="img-fluid wow zoomIn rounded-circle img-hover-shadow" data-wow-duration="0.9s" data-wow-offset="50" data-wow-delay="0.1s" src="img/team/nikhil.png" width="165" height="160" alt="Nikhil Bellad, Sr. Executive, Accounts, Deshpande Startups">
               <p class="text-yellow"><b>Nikhil Bellad</b><br>
                  <span class="text-muted">Sr. Executive, Accounts</span>
               </p>
            </div>
         </div>
         <br>
      </div>
      <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once 'essentials/js.php';
         ?>
   </body>
</html>