<!DOCTYPE html>
<html lang="en">
<head>
 <title>Internship | Yuva Entrepreneurship</title>
 <?php
 require_once 'essentials/meta.php';
 ?>
 <meta name="linkage" content="https://www.deshpandestartups.org/internship"/>
 <meta property="og:site_name" content="Deshpande Startups"/>
 <meta property="og:type" content="website">
 <meta property="og:url" content="https://www.deshpandestartups.org/internship">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/internship-bg.png">
 <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/internship-img.png">
 <meta property="og:description" content="Yuva Entrepreneurship Program is providing an opportunity to Learn, Ideate, Design and Build your product while sharpening the skills needed to succeed in the rapidly changing technological fields."/>
 <meta name="author" content="Deshpande Startups"/>
 <meta name="description" content="Yuva Entrepreneurship Program is providing an opportunity to Learn, Ideate, Design and Build your product while sharpening the skills needed to succeed in the rapidly changing technological fields."/>
 <!-- <meta name="keywords" content=""/> -->
 <meta property="og:title" content="Internship">
 <link rel="canonical" href="https://www.deshpandestartups.org/internship">
 <?php
 require_once 'essentials/bundle.php';
 ?>
 <style type="text/css">
   /*section.home.dark{
      background-color:#343a40;
      color:white;
      }*/
      section.home{
       padding: 4rem 1rem;
     }
     .parallax {
      background-image: url("img/makers/deshpande2.jpg");
      min-height: 500px; 
      background-attachment: fixed;
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
    }
  </style>
</head>
<body>
 <?php
 require_once 'essentials/title_bar.php';
 require_once 'essentials/menus.php';
 ?>


 <img class="carousel-inner img-fluid" src="img/makers/internship-bg.png" width="1349" height="198" alt="Deshpande Startups, internship">
 <nav aria-label="breadcrumb">
  <ol class="breadcrumb justify-content-end">
   <li class="breadcrumb-item"><a href="./">Home</a></li>
   <li class="breadcrumb-item"><a href="yuva-entrepreneurship">Yuva Entrepreneurship</a></li>
   <li class="breadcrumb-item active" aria-current="page">Internship</li>
 </ol>
</nav>

<div class="container">
  <!-- <br> -->
  <div class="center wow fadeInDown">
   <h2 class="text-yellow text-center">INTERNSHIP</h2>
   <div class="divider b-y text-yellow content-middle"></div>
 </div><br>
 <h4 class="text-yellow">About internship:</h4>
 <div class="row">
  <!-- <div class="col-md-12"> -->
  <div class="col-md-10">
    <p class="text-justify wow slideInLeft">Yuva Entrepreneurship Program is providing an opportunity to <b>learn, ideate, design and build your product</b> while sharpening the skills needed to succeed in the rapidly changing technological fields. The Makers Lab provides the facility for <b>experimentation, fabrication and prototyping</b> and also a space which is spread over <b>12,000 square feet</b> in Hubballi. The program caters to the ever-increasing needs of technical brilliance in the given areas.<br><br>This internship is mainly designed for <b>aspiring engineers currently pursuing their 3<sup>rd</sup> or final year</b> of their engineering and engineer graduates (3 to 6 months internship for interested graduates).</p>
  </div>
  <div class="col-md-2">
      <div class="row pl-4">
        <a href="internship-form" class="btn btn-rotate" target="_blank">Apply Now</a>
      </div>
    </div>
  </div>
  <br>
</div>
<!-- <section class="home dark"> -->
  <div class="featured-bg-container">
   <!-- <div class="container"> -->
    <div class="row valign-wrapper">
     <div class="col-md-4">
      <img src="img/makers/internship-img.png" width="398" height="297" class="img img-fluid img-thumbnail wow zoomIn" alt="Deshpande startups, internship at makers lab">
    </div>
    <div class="col-md-8">
      <h4 class="text-yellow">Key highlights of the internship:</h4>
      <ul class="text-justify wow slideInRight">
       <li>Assigned internee must be strongly communicative - communication being a major component typically required to work in any combination. <b>The available combination in Makers Lab are 3D printing, IoT and electronics field and mechanical</b></li>
       <li>Be a part of vibrant <b>developers, network and community</b></li>
       <li>Internee will be able to acquire knowledge and industry standard practices</li>
       <li>Internee will be able to apply any component of technical knowledge in analysing the industrial problems</li>
       <li>Exposure to Startup Ecosystem, Makers Lab, Yuva Entrepreneurship & ESDM and build entrepreneurial mindset</li>
       <li>Network with exciting startups, join the enthusiastic multidisciplinary makers and developer’s community, explore career opportunities</li>
     </ul>               
   </div>
 </div>
</div>
<!-- </section> -->
<!-- <br> -->

 <div class="parallax inverse-text" data-parallax-img="img/makers/deshpande2.jpg" data-parallax-img-width="1920" data-parallax-img-height="1078">
   <section class="home text-white">
    <div class="container text-white">
     <!-- <div class="col-md-12"> -->
      <div class="row">
       <div class="col-md-6">
        <h4 class="text-yellow">Internship benefits:</h4>
        <ul class="text-justify wow slideInLeft">
         <li>Practical exposure to technology</li>
         <li>Substantial interaction with professional staff and mentors</li>
         <li>Rapid prototyping</li>
         <li>Develop and refine skills</li>
         <li>Grow your network with developers</li>
         <li>By the end of the internship, internee will be able to overcome career development issues</li>
         <li>By the end of the internship, internee will be able to prototype innovative ideas efficiently by reducing the product development cycle</li>
         <li>A certificate from Deshpande Startups</li>
       </ul>
     </div>
     <div class="col-md-6">
       <h4 class="text-yellow">Intern requirements:</h4>
       <ul class="text-justify wow slideInLeft">
        <li>Internee must be available for a specified duration</li>
        <li>Have relevant skills and interest in the mentioned domain</li>
        <li>Internees must be in their 3rd year or final year engineering</li>
        <li>Engineer graduates can apply as well (3 to 6 months internship for interested graduates)</li>
      </ul>
          </div>
        </div>
        <!-- </div> -->
      </div>
    </section>
  </div>

  <?php
  require_once 'essentials/footer.php';
  require_once 'essentials/copyright.php';
  require_once 'essentials/js.php';
  ?>
</body>
</html>