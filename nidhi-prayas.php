<!DOCTYPE html>
<html lang="en">
<head>
   <title>NIDHI PRAYAS</title>
   <?php
   require_once 'essentials/meta.php';
   ?>
   <meta name="linkage" content="https://www.deshpandestartups.org/nidhi-prayas"/>
   <meta property="og:site_name" content="Deshpande Startups"/>
   <meta property="og:type" content="website">
   <meta property="og:url" content="https://www.deshpandestartups.org/nidhi-prayas">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/funded-startups/nidhi-prayas-benefits.jpg">
   <meta property="og:image" content="https://www.deshpandestartups.org/img/funded-startups/nidhi-focus-area.jpg">
   <meta property="og:description" content="A NIDHI-PRomotion and Acceleration of Young and Aspiring technology entrepreneur (NIDHI-PRAYAS) – Support from Idea to Prototype, is the scheme under NIDHI, aimed at addressing the gap in the very early stage of an idea/proof of concept funding."/>
   <meta name="author" content="Deshpande Startups"/>
   <meta name="description" content="A NIDHI-PRomotion and Acceleration of Young and Aspiring technology entrepreneur (NIDHI-PRAYAS) – Support from Idea to Prototype, is the scheme under NIDHI, aimed at addressing the gap in the very early stage of an idea/proof of concept funding."/>
   <!-- <meta name="keywords" content=""/> -->
   <meta property="og:title" content="NIDHI PRAYAS">
   <link rel="canonical" href="https://www.deshpandestartups.org/nidhi-prayas">
   <?php
         // $title = 'Deshpande Startups';
   require_once 'essentials/bundle.php';
   ?>
   <style type="text/css">
     .cal{
      font-family: calibri;
   }
</style>

</head>
<body>
   <?php
   require_once 'essentials/title_bar.php';
   require_once 'essentials/menus.php';
   ?>

<!-- <a href="nidhi-prayas" target="_blank"> --><img class="carousel-inner img-fluid" src="img/events/nidhi-prayas-bg.jpg" width="1349" height="400" alt="NIDHI Prayas, Deshpande Startups deshpande foundation">
<!-- </a> -->
   <div class="container">
      <br>
      <div class="center wow fadeInDown">
         <h2 class="text-yellow text-center">NIDHI <span class="text-muted">PRAYAS</span> </h2>
         <div class="divider b-y text-yellow content-middle"></div>
      </div><br>
      <!-- <h4  class="text-yellow">NIDHI-Seed Support System (NIDHI-SSS)</h4> -->
      <p class="text-justify wow slideInLeft">A NIDHI-PRomotion and Acceleration of Young and Aspiring technology entrepreneur (NIDHI-PRAYAS) - Support from Idea to Prototype, is the scheme under NIDHI, aimed at addressing the gap in the very early stage of an idea/proof of concept funding. The NIDHI-PRAYAS program focuses on addressing the idea to prototype funding gaps.</p>

      <p class="text-justify wow slideInLeft">Deshpande Startups is one of the approved NIDHI-PRAYAS centers by DST, Govt of India. The innovators selected under this scheme by Deshpande Startups, i.e, the Prayasee will primarily utilise the funds to convert their ideas into prototype.</p>

      <h4 class="text-yellow mb-2"><b>Objectives of NIDHI-PRAYAS:</b></h4>
      <ul class="text-justify wow slideInLeft">
         <li>To enable translation of an innovative idea to a prototype</li>
         <li>To support faster experimentation and the idea to market journey</li>
         <li>To generate innovative solutions relevant to the local and global problems</li>
         <li>To attract a large number of youth who demonstrates problem solving zeal and abilities</li>
         <li>To work on their new technology/knowledge/innovation based startups</li>
         <li>To build a vibrant innovation ecosystem, by establishing a network between innovators, academia, mentors and incubators</li>
      </ul>
      <!-- <div class="text-center">
         <a href="nidhi-prayas" target="_blank" class="btn btn-warning btn-lg" >Apply now for NIDHI Prayas</a>
      </div> -->
   </div>
   <br>

   <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
         <div class="carousel-item active img-hover-zoom-img">
            <img class="d-block w-100 img img-fluid" src="img/funded-startups/nidhi-prayas-benefits.jpg" width="1349" height="400" alt="NIDHI Prayas Benefits, Deshpande Startups">
         </div>
         <div class="carousel-item img-hover-zoom-img">
            <img class="d-block w-100 img img-fluid" src="img/funded-startups/nidhi-focus-area.jpg" width="1349" height="400" alt="NIDHI Prayas Focus Areas, Deshpande Startups">
         </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
      </a>
   </div>

   <!-- <img src="img/funded-startups/nidhi-prayas-benefits.jpg" class="img-fluid img" width="100%" height="400" alt="Deshpande startups, Offerings"> -->
   <!-- <br> -->
   <div class="container">
      <br>
      <h4 class="text-yellow"><b>Guidelines:</b></h4>
      <p class="text-justify wow slideInLeft mb-3">The PRAYAS program is focused to provide funding support to individual innovators. Innovators should use the PRAYAS grant, primarily to convert their ideas into prototype. The IP generated if any, should vest with the innovator/startup.</p>

      <!-- <p class="text-justify wow slideInLeft mb-2"><b>The eligibility criteria for the applicants for PRAYAS program are as follows:</b></p> -->
      <p class="text-justify wow slideInLeft mb-2"><b>Are you eligible? (Eligibility criteria)</b></p>
      <p class="text-justify wow slideInLeft mb-2">Any individual applying for PRAYAS has to be an Indian citizen with a government approved proof of nationality such as a valid passport, voter’s id etc. The applicant should be in the age group of minimum of 18 years, as on date of application.</p>

      <p class="text-justify wow slideInLeft mb-2"><b>1.</b><span class="text-yellow"> Individual Innovator or Team of innovators:</span></p>
      <p class="text-justify wow slideInLeft mb-2">The innovator should have the knowhow of the PRAYAS project. For team of innovators there has to be </p>
      <ul class="text-justify wow slideInLeft">
         <li>An agreement among the team about the lead innovator who would apply, and the funds will be transferred to the lead innovator’s account (after selection)</li>
         <li>IP generated would vest with the innovator/team and it would be further taken for commercialization</li>
      </ul>

      <p class="text-justify wow slideInLeft mb-2"><b>2.</b><span class="text-yellow"> In case of founders, co-founders of startup applying for PRAYAS:</span></p>
      <ul class="text-justify wow slideInLeft">
         <li>At the time of application, the period of existence of a startup and its operations should not exceed 7 years from the date of incorporation</li>
         <li>Wants to develop the prototype for a new product, for which they may have not received similar kind of support</li>
         <li>Company should have annual turnover not exceeding Rs. 25 lakhs for any of the financial year since its inception</li>
         <li>IP generated would vest with the startup</li>
      </ul>

      <!-- <p class="text-justify wow slideInLeft mb-2"><b>3.</b><span class="text-yellow"> IP generated would vest with the startup</span></p> -->

      <p class="text-justify wow slideInLeft mb-2"><b>3.</b><span class="text-yellow"> Innovators in employment or students with any R&D organisation/academic institution:</span></p>
      <ul class="text-justify wow slideInLeft">
         <li>a.   It is mandatory that the innovators pursuing PRAYAS should have the project know how and own the IP</li>
         <li>b.   The innovator has to seek the NOC from their organisation/institution. The NOC should have the following<br>
            <b>(1)</b> Permission to apply for PRAYAS and the innovator will be granted sufficient time to work on the PRAYAS project<br>
            <b>(2)</b> IP generated would vest with the innovator/team and it would be further taken for commercialization
         </li>
      </ul>


      <p class="text-justify wow slideInLeft mb-2 font-weight-bold">It cannot, however, be used for supporting:</p>
      <ul class="text-justify wow slideInLeft">
         <li>Innovators/startup who do not/will not own the project know how/IP</li>
         <li>Funding research/student internship in academic institution or R&D organisation</li>
         <li>Innovators/team/startup who is already supported once under NIDHI PRAYAS</li>
         <li>Innovators who do not have the necessary NOC on IP, granting sufficient time to work on PRAYAS project from their respective organisation/institution</li>
         <li>Innovator supported under NIDHI-EIR in parallel with NIDHI-PRAYAS</li>
         <li>Innovators who do not have a roadmap/will towards commercialization</li>
         <li>Ideas purely relating to Software/App development</li>
      </ul>

   </div>
   <br>
   <br>
   

   <?php
   require_once 'essentials/footer.php';
   require_once 'essentials/copyright.php';
   require_once 'essentials/js.php';
   ?>
</body>
</html>