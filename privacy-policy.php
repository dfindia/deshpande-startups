<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Privacy Policy | Deshpande Startups</title>
      <?php
         require_once 'essentials/meta.php';
         ?>
      <meta name="linkage" content="https://www.deshpandestartups.org"/>
      <meta property="og:site_name" content="Deshpande Startups"/>
      <meta property="og:type" content="website">
      <meta property="og:url" content="https://www.deshpandestartups.org/privacy">
      <meta property="og:url" content="https://www.deshpandestartups.org/contact-us">
      <!-- <meta property="og:image" content="https://www.deshpandestartups.org/img/makers/bg-ideathon.png"> -->
      <!-- <meta property="og:description" content="Do you have an innovative idea that can translate into a cutting-edge product or service? Participate in the IDEATHON, a Platform to Present Your Idea to Solve Real World Problems and get selected to YUVA ENTREPRENEURSHIP PROGRAM to Kick Start Your Startup Journey with the startup ecosystem and Win 1 Lakh Worth Rewards."/> -->
      <meta name="author" content="Deshpande Startups"/>
      <meta name="description" content="Privacy Policy Deshpande Startups"/>
      <!-- <meta name="keywords" content=""/> -->
      <meta property="og:title" content="Deshpande Startups Privacy Policy">
      <!-- <link rel="canonical" href="https://www.deshpandestartups.org/ideathon"> -->
      <?php
         // $title = 'Deshpande Startups';
         require_once 'essentials/bundle.php';
         ?>
   </head>
   <body>
      <?php
         require_once 'essentials/title_bar.php';
         require_once 'essentials/menus.php';
         ?>
      <br>
      <!-- <div class="container"> -->
      <!-- <br> -->
      <div class="text-center">
         <h2 class="text-yellow  wow fadeInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted"> Privacy </span> Policy </h2>
         <div class="divider b-y text-yellow content-middle"></div>
      </div>
      <br>
      <div class="container">
         <div class="col-lg-12 text-justify">
            <p><b>What is this Privacy Policy for?</b></p>
            <p>This privacy policy is for this website www.deshpandestartups.org and served by Deshpande Foundation India and governs the privacy of its users who choose to use it. </p>
            <p>The policy sets out the different areas where user privacy is concerned and outlines the obligations & requirements of the users, the website and website owners. Furthermore the way this website processes, stores and protects user data and information will also be detailed within this policy.
            </p>
            <p><b>The Website</b></p>
            <p>This website and its owners take a proactive approach to user privacy and ensure the necessary steps are taken to protect the privacy of its users throughout their visiting experience. This website complies to all national laws and requirements for user privacy. </p>
            <p><b>Use of Cookies</b></p>
            <p>This website uses cookies to better the users experience while visiting the website. Where applicable this website uses a cookie control system allowing the user on their first visit to the website to allow or disallow the use of cookies on their computer / device. This complies with recent legislation requirements for websites to obtain explicit consent from users before leaving behind or reading files such as cookies on a user's computer / device. </p>
            <p>Cookies are small files saved to the user's computers hard drive that track, save and store information about the user's interactions and usage of the website. This allows the website, through its server to provide the users with a tailored experience within this website.
               Users are advised that if they wish to deny the use and saving of cookies from this website on to their computers hard drive they should take necessary steps within their web browsers security settings to block all cookies from this website and its external serving vendors.
            </p>
            <p>This website uses tracking software to monitor its visitors to better understand how they use it. This software is provided by Google Analytics which uses cookies to track visitor usage. The software will save a cookie to your computers hard drive in order to track and monitor your engagement and usage of the website, but will not store, save or collect personal information. You can read Google's privacy policy here for further information [<a href="http://www.google.com/privacy.html" target="_blank"> www.google.com/privacy.html </a>].
            </p>
            <p>Other cookies may be stored to your computers hard drive by external vendors when this website uses referral programs, sponsored links or adverts. Such cookies are used for conversion and referral tracking and typically expire after 30 days, though some may take longer. No personal information is stored, saved or collected.
            </p>
            <p><b>Contact & Communication</b></p>
            <p>Users contacting this website and/or its owners do so at their own discretion and provide any such personal details requested at their own risk. Your personal information is kept private and stored securely until a time it is no longer required or has no use, as detailed in the Data Protection Act 1998. Every effort has been made to ensure a safe and secure form to email submission process but advise users using such form to email processes that they do so at their own risk.</p>
            <p>This website and its owners use any information submitted to provide you with further information about the products / services they offer or to assist you in answering any questions or queries you may have submitted. This includes using your details to subscribe you to any email newsletter program the website operates but only if this was made clear to you and your express permission was granted when submitting any form to email process. Or whereby you the consumer have previously purchased from or enquired about purchasing from the company a product or service that the email newsletter relates to. This is by no means an entire list of your user rights in regard to receiving email marketing material. Your details are not passed on to any third parties. </p>
            <p><b>Email Newsletter</b></p>
            <p>This website operates an email newsletter program, used to inform subscribers about products and services supplied by this website. Users can subscribe through an online automated process should they wish to do so but do so at their own discretion. Some subscriptions may be manually processed through prior written agreement with the user.</p>
            <p>Subscriptions are taken in compliance with UK Spam Laws detailed in the Privacy and Electronic Communications Regulations 2003. All personal details relating to subscriptions are held securely and in accordance with the Data Protection Act 1998. No personal details are passed on to third parties nor shared with companies / people outside of the company that operates this website. Under the Data Protection Act 1998 you may request a copy of personal information held about you by this website's email newsletter program. A small fee will be payable. If you would like a copy of the information held on you please write to the business address at the bottom of this policy.</p>
            <p>Email marketing campaigns published by this website or its owners may contain tracking facilities within the actual email. Subscriber activity is tracked and stored in a database for future analysis and evaluation. Such tracked activity may include; the opening of emails, forwarding of emails, the clicking of links within the email content, times, dates and frequency of activity [this is by no far a comprehensive list].
               This information is used to refine future email campaigns and supply the user with more relevant content based around their activity.
            </p>
            <p><b>External Links</b></p>
            <p>Although this website only looks to include quality, safe and relevant external links, users are advised adopt a policy of caution before clicking any external web links mentioned throughout this website. (External links are clickable text / banner / image links to other websites, similar to; <a href="https://www.google.co.in/"> www.google.com </a>or <a href="http://www.newportholidaycottages.co.uk/" rel="nofollow">Cottages in Pembrokeshire</a>.)</p>
            <p>The owners of this website cannot guarantee or verify the contents of any externally linked website despite their best efforts. Users should therefore note they click on external links at their own risk and this website and its owners cannot be held liable for any damages or implications caused by visiting any external links mentioned.</p>
            <p><b>Adverts and Sponsored Links</b></p>
            <p>This website may contain sponsored links and adverts. These will typically be served through our advertising partners, to whom may have detailed privacy policies relating directly to the adverts they serve. </p>
            <p>Clicking on any such adverts will send you to the advertisers website through a referral program which may use cookies and will track the number of referrals sent from this website. This may include the use of cookies which may in turn be saved on your computers hard drive. Users should therefore note they click on sponsored external links at their own risk and this website and its owners cannot be held liable for any damages or implications caused by visiting any external links mentioned.</p>
            <p><b>Social Media Platforms</b></p>
            <p>Communication, engagement and actions taken through external social media platforms that this website and its owners participate on are custom to the terms and conditions as well as the privacy policies held with each social media platform respectively. </p>
            <p>Users are advised to use social media platforms wisely and communicate / engage upon them with due care and caution in regard to their own privacy and personal details. This website nor its owners will ever ask for personal or sensitive information through social media platforms and encourage users wishing to discuss sensitive details to contact them through primary communication channels such as by telephone or email.</p>
            <p>This website may use social sharing buttons which help share web content directly from web pages to the social media platform in question. Users are advised before using such social sharing buttons that they do so at their own discretion and note that the social media platform may track and save your request to share a web page respectively through your social media platform account.</p>
            <p><b>Shortened Links in Social Media</b></p>
            <p>This website and its owners through their social media platform accounts may share web links to relevant web pages. By default some social media platforms shorten lengthy urls [web addresses].</p>
            <p>Users are advised to take caution and good judgement before clicking any shortened urls published on social media platforms by this website and its owners. Despite the best efforts to ensure only genuine urls are published many social media platforms are prone to spam and hacking and therefore this website and its owners cannot be held liable for any damages or implications caused by visiting any shortened links.</p>
            <br>
         </div>
      </div>
      <!-- </div> -->
      <br>
      <?php
         require_once 'essentials/footer.php';
         require_once 'essentials/copyright.php';
         require_once 'essentials/js.php';
         ?>
   </body>
</html>