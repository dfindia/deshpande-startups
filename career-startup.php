<!DOCTYPE html>
<html lang="en">
<head>
	<title>Career at our incubated startups - Current openings</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/career-startup"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/career-startup">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/career-startup-bg.png">
	<meta property="og:description" content="Applications are invited for the following eligible candidates at our incubated startups."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content= "Applications are invited for the following eligible candidates at our incubated startups."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Business development manager, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="career at our incubated startups, Deshpande Startups">
	<link rel="canonical" href="https://www.deshpandestartups.org/career-startup">

	<?php
	 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		ul {
			list-style: none;
			margin: 0;
			padding: 0;
		}
		.jp_job_post_main_wrapper_cont {
			float: left;
			width: 100%;
			margin-top: 30px;
			transition: all 0.5s;
		}
		.jp_job_post_main_wrapper {
			float: left;
			width: 100%;
			background: #ffffff;
			padding-left: 30px;
			padding-right: 30px;
			padding-top: 30px;
			padding-bottom: 30px;
			border: 1px solid #e9e9e9;
			border-bottom: 1px solid #e9e9e9;
			transition: all 0.5s;
		}
		.jp_job_post_side_img {
			float: left;
			/*width: 105px;*/
		}
		.jp_job_post_right_cont {
			float: left;
			width: calc(100% - 105px);
			padding-left: 30px;
			padding-top: 10px;
		}
		.jp_job_post_right_cont h4 {
			font-size: 15px;
			color: #000000;
			font-weight: bold;
		}
		.jp_job_post_right_cont p {
			font-size: 14px;
			font-weight: 500;
			color: #ea7c26;
			padding-top: 5px;
		}
		.jp_job_post_right_cont li:first-child {
			margin-left: 0;
			color: #000000;
			font-size: 16px;
			font-weight: bold;
		}
		.jp_job_post_right_cont li:last-child {
			color: #797979;
			font-size: 16px;
		}
		.jp_job_post_right_btn_wrapper li:first-child a {
			float: left;
			width: 100%;
			height: 30px;
			line-height: 30px;
			text-align: center;
			background: #868e96;
			/*background: #37d09c;*/
			color: #ffffff;
			font-size: 12px;
			text-transform: uppercase;
			border-radius: 10px;
			text-decoration: none;
			margin-bottom: 10px;
		}
		.jp_job_post_right_btn_wrapper li:last-child {
			/*margin-left: 50px;*/
			margin-top: 20px;
			float: none;
		}
		.jp_job_post_right_btn_wrapper li:last-child a {
			float: left;
			width: 100%;
			/*padding-right: 20px;*/
			/*padding-left: 20px;*/
			height: 30px;
			/*width: 100px;*/
			line-height: 30px;
			text-align: center;
			background: #ea7c26;
			/*background: #f36969;*/
			color: #ffffff;
			font-size: 12px;
			text-transform: uppercase;
			border-radius: 10px;
			text-decoration: none;
		}
		.featured-wrap {
			background: #efefef;
			padding: 25px 0;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
<!-- <img class="carousel-inner img-fluid" src="img/career/career-startup-bg.png" width="1349" height="400" alt="Deshpande Startups deshpande foundation">
<nav aria-label="breadcrumb">
	<ol class="breadcrumb justify-content-end m-0">
		<li class="breadcrumb-item"><a href="./">Home</a></li>
		<li class="breadcrumb-item active" aria-current="page">Career at Startups</li>
	</ol>
</nav> -->

<div class="featured-wrap">
	<div class="container">
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center slideInDown" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">CAREER AT OUR</span><br> INCUBATED STARTUPS</h2>
			<div class="divider b-y text-yellow content-middle"></div>
			<br>
			<p class="text-center"><b>
				Applications are invited for the following eligible candidates at our incubated startups.
			</b></p>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/kosha-logo.png" width="100" height="100" alt="Kosha Designs logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Full Stack Developer</h4>
									<p>4 Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="kosha-fullstack-developer" target="_blank">Full Time</a></li>
										<li><a href="kosha-fullstack-developer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/ingeniouspix-logo.png" width="100" height="100" alt="IngeniousPix logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Senior Web Developer</h4>
									<p>3-5 Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="ingeniouspix-web-developer" target="_blank">Full Time</a></li>
										<li><a href="ingeniouspix-web-developer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/nautilus-logo.png" width="100" height="100" alt="Nautilus logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Sales Manager</h4>
									<p>5 Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Karnataka</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="nautilus-sales-manager" target="_blank">Full Time</a></li>
										<li><a href="nautilus-sales-manager" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/shopgro-logo.png" width="100" height="100" alt="Shopgro logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Senior Web Developer</h4>
									<p>2+ Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="shopgro-web-developer" target="_blank">Full Time</a></li>
										<li><a href="shopgro-web-developer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/shopgro-logo.png" width="100" height="100" alt="Shopgro logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Sales Representative</h4>
									<p>2-3 Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="shopgro-sales-representative" target="_blank">Full Time</a></li>
										<li><a href="shopgro-sales-representative" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/shopgro-logo.png" width="100" height="100" alt="Shopgro logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Market Research Analyst</h4>
									<p>0-2 Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="shopgro-market-research" target="_blank">Full Time</a></li>
										<li><a href="shopgro-market-research" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/shopgro-logo.png" width="100" height="100" alt="Shopgro logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Embedded Engineer</h4>
									<p>2+ Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="shopgro-embedded-engineer" target="_blank">Full Time</a></li>
										<li><a href="shopgro-embedded-engineer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/docketrun-logo.png" width="100" height="100" alt="DocketRun Tech Private Limited, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Full Stack Developer & Machine Learning</h4>
									<p>3+ Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i> Hubballi - Dharwad</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="docketrun-fullstack-developer" target="_blank">Full Time</a></li>
										<li><a href="docketrun-fullstack-developer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/widemobility-logo.png" width="100" height="100" alt="widemobility logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Software Engineer</h4>
									<p>2+ Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="widemobility-software-engineer" target="_blank">Full Time</a></li>
										<li><a href="widemobility-software-engineer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/linkez-logo.png" width="100" height="100" alt="LinkEZ Technologies logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Senior Software Developer</h4>
									<p>5-10 Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="linkez-software-developer2" target="_blank">Full Time</a></li>
										<li><a href="linkez-software-developer2" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/kotumb-logo.png" width="100" height="100" alt="Kotumb logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Senior Full stack Android Developer</h4>
									<p>5+ Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="kotumb-android-developer" target="_blank">Full Time</a></li>
										<li><a href="kotumb-android-developer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/nautilus-logo.png" width="100" height="100" alt="Nautilus Hearing Solutions Pvt. Ltd. logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Android Developer</h4>
									<p>3-4 Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="nautilus-android-developer" target="_blank">Full Time</a></li>
										<li><a href="nautilus-android-developer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/nautilus-logo.png" width="100" height="100" alt=" Nautilus Hearing Solutions Pvt. Ltd. logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Android Developer</h4>
									<p>2 Years of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="nautilus-android-developer2" target="_blank">Full Time</a></li>
										<li><a href="nautilus-android-developer2" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/nautilus-logo.png" width="100" height="100" alt=" Nautilus Hearing Solutions Pvt. Ltd. logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Android UI UX Designer</h4>
									<p>1 Year of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="nautilus-android-designer" target="_blank">Full Time</a></li>
										<li><a href="nautilus-android-designer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/krishitantra-logo.png" width="100" height="100" alt="Krishitantra logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Sales Engineer</h4>
									<p>Freshers</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Hubballi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="krishitantra-sales-engineer" target="_blank">Full Time</a></li>
										<li><a href="krishitantra-sales-engineer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/krishitantra-logo.png" width="100" height="100" alt="Krishitantra logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Software Engineer</h4>
									<p>Freshers</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Udupi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="krishitantra-software-engineer" target="_blank">Full Time</a></li>
										<li><a href="krishitantra-software-engineer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="jp_job_post_main_wrapper_cont">
					<div class="jp_job_post_main_wrapper card-hover-shadow">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="jp_job_post_side_img">
									<img src="img/career/krishitantra-logo.png" width="100" height="100" alt="Krishitantra logo, Deshpande startups incubated startup" />
								</div>
								<div class="jp_job_post_right_cont">
									<h4>Assembly Engineer Trainee</h4>
									<p>0-1 Year of Experience</p>
									<ul>
										<li><i class="fa fa-map-marker"></i>&nbsp; Udupi</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="jp_job_post_right_btn_wrapper">
									<ul>
										<li><a href="krishitantra-assembly-engineer" target="_blank">Full Time</a></li>
										<li><a href="krishitantra-assembly-engineer" target="_blank">View Details</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<br>
</div>



<?php
require_once 'essentials/footer.php';
require_once 'essentials/copyright.php';
require_once 'essentials/js.php';
?>
</body>
</html>