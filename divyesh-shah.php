<!DOCTYPE html>
<html lang="en">
<head>
	<title>Divyesh Shah - Founder & CEO, LinkEZ Technologies</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/divyesh-shah"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/divyesh-shah">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/edge/divyeshshah.png">
	<meta property="og:description" content="Divyesh comes with a Masters degree from IIT Kanpur and 13 years of Industry Experience in Networking protocols, Wireless Architecture & System level solutions at TI and ST-Ericsson before Starting up in 2013."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="Divyesh comes with a Masters degree from IIT Kanpur and 13 years of Industry Experience in Networking protocols, Wireless Architecture & System level solutions at TI and ST-Ericsson before Starting up in 2013."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Makers Lab Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Divyesh Shah - Founder & CEO, LinkEZ Technologies">
	<link rel="canonical" href="https://www.deshpandestartups.org/divyesh-shah">
	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
	/*p{text-align:justify;}*/
	.cal{
		font-family: calibri;
	}
	.modal-title {
		text-align: left;
		margin: 0;
		line-height: 1;
	}
	.follow-us{
		margin:20px 0 0;
	}
	.follow-us li{ 
		display:inline-block; 
		width:auto; 
		margin:0 5px;
	}
	.follow-us li .fa{ 
		font-size:25px; 
		/*color:#767676;*/
		color: #e7572a;
	}
	.follow-us li .fa:hover{ 
		color:#025a8e;
	}
</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>

	<div class="container cal pt-4">
		<div class="row wow fadeInLeft" data-wow-duration="1s" data-wow-offset="50">
			<div class="col-md-12 team-main">
				<div>
					<div class="modal-header">
						<!-- <a type="button" class="close" data-dismiss="modal" aria-label="Close" href="#"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> back </a> -->
						<h4 class="modal-title modal-title-cust text-yellow" id="myModalLabel">DIVYESH KUMAR SHAH<br><small class="text-muted">Founder & CEO - LinkEZ Technologies</small></h4>
						<ul class="follow-us clearfix">
							<!-- <li><a href="#" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li> -->
							<li><a href="https://www.linkedin.com/in/divyesh-shah-143a1b20/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="modal-body">
						<p class="text-justify">
							<img src="img/edge/divyeshshah.png" width="165" height="160" class="img img-fluid mr-2 rounded-circle" alt="Divyesh Shah, Founder & CEO - LinkEZ Technologies, EDGE Mentor" align="left">
							Divyesh comes with a Masters degree from IIT Kanpur and 13 years of Industry Experience in Networking protocols, Wireless Architecture & System level solutions at TI and ST-Ericsson before Starting up in 2013.<br><br>
							He has filed 5 patents with USPTO & 2 patents in India.<br><br>

							His Heart always lied in the Application of Technology to Solve the ground level problems and with this Passion, started LinkEZ Technologies in October 2013 at Deshpande Foundation. Today LinkEZ is part of the Deshpande Startups and helping solve Connected Manufacturing needs, leveraging the power of IoT with cutting edge AI and ML based Analytics
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>