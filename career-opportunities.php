<?php
$msg = '';
if (array_key_exists('cv', $_FILES)) {
	try {
		require_once(dirname(__FILE__) ."/include/Framework/autoload.php");
		$Load = new Framework\Helper\Load();
		// $content = $Load->view('include/contents', $_POST, TRUE);
		$content = $Load->view('include/template', $_POST, TRUE);
		// echo $content;
		// die();
		$mail = new Framework\PHPMailer\PHPMailer(true);
		// $mail->SMTPAuth = true;
		$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		// $mail->SMTPSecure = "ssl";
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		// $mail->IsSMTP();

		// $mail->Username = "myemail@gmail.com";
		// $mail->Password = "**********";
		// $mail->Port = "465";

		// $mail->setFrom('umme.sandbox@dfmail.org', 'Sandbox Stratups Resume', FALSE);
		$mail->setFrom('talent.sandbox@dfmail.org', 'Deshpande Stratups Resume', FALSE);

		// $mail->addAddress('umme.sandbox@dfmail.org', 'Sandbox Info');
		$mail->addAddress('talent.sandbox@dfmail.org', 'Deshpande Info');

		$mail->addReplyTo('no-reply@dfmail.org', 'No Reply');

	    // $mail->addCC('');
		$mail->addBCC('sagar.sa@dfmail.org');
		$mail->addBCC('sanmati@dfmail.org');
		$mail->addBCC('umme.sandbox@dfmail.org');
		$mail->addBCC('talent.sandbox@dfmail.org');

	    // Attachments
		// $mail->addAttachment($tmpName, $filename);


	    // Content
		$mail->isHTML(true);
		$mail->Subject = 'Deshpande Startups Career Page Online Submission of Resume';


		// Read an HTML message body from an external file, convert referenced images to embedded,
		// convert HTML into a basic plain-text alternative body
		$mail->msgHTML($content);
		// Replace the plain text body with one created manually
		$mail->AltBody = 'This is a plain-text message body';
		// Attach an image file
		if(is_uploaded_file($_FILES["cv"]["tmp_name"])){
			$cv_formats = array("pdf", "doc", "docx", "jpg", "png");
			$cv_extension = pathinfo($_FILES["cv"]["name"], PATHINFO_EXTENSION);
			if(in_array(strtolower($cv_extension),$cv_formats)){
				$tempFile = $_FILES["cv"]["tmp_name"];
				$fileName = basename($_FILES["cv"]["name"], ".".$cv_extension);
				// $fileName = $_FILES["cv"]["name"];
				$cvDir = "resumes/";
				// $unique = md5( rand(0,1000) );
				// $fileTime =date("Ymdhis").$unique;
				$fileTime =date("Ymdhis");
				// $cv_name = "DFHRcv".$fileTime.".".$cv_extension;
				$cv_name = $fileName.'-'.$fileTime.".".$cv_extension;
				if(move_uploaded_file($tempFile, $cvDir . basename($cv_name))){
					$mail->addAttachment($cvDir . basename($cv_name));
					if (!$mail->send()) {
						$data['response'] = false;
						$data['message'] = "Mailer Error: " . $mail->ErrorInfo;
					} else {
						$data['response'] = true;
						$data['message'] = "Your application has been submitted!<br>We'll get back to you soon";

						if( $curl = curl_init() ){
							extract($_POST);
							$filepath = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]".dirname($_SERVER['PHP_SELF']).'/'. $cvDir . basename($cv_name);
							$params = array(
								'entry.307619064' => $firstname,
								'entry.1991067555' => $lastname,
								'entry.1777537464' => $emailaddress,
								'entry.1482857309' => $mobilenumber,
								'entry.982341909' => $select,
								'entry.697021108' => $comments,
								'entry.1070399743' => $filepath,
							);

							$url = 'https://docs.google.com/forms/d/e/1FAIpQLSchsDP_RR7BRZE02JZHLf-jLcl-VVjqQMYJf7kVAr8-OS_9Xg/formResponse';

							$ch = curl_init();
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
							$result = curl_exec($ch);
							$error  = curl_error($ch);
						}
					}
				} else {
					$data['response'] = false;
					$data['message'] = 'Not uploaded.' ;
				}
			} else {
				$data['response'] = false;
				$data['message'] = 'Not allowed format, you can upload with these formats - pdf, doc, docx, jpg, png.' ;
			}
		} else {
			$data['response'] = false;
			$data['message'] = 'Resume is Not uploaded, attach your resume and try again.' ;
		}
		// send the message, check for errors
		// $data['response'] = true;
		// $data['message'] = 'Here\'s your data.' ;
	} catch (Framework\Exceptions\FrameworkSDKException $e) {
		$data['response'] = false;
		$data['message'] = 'FrameworkSDK Error : '.$e->getMessage();
	} catch (PDOException $e) {
		$data['response'] = false;
		$data['message'] = 'PDO Error :'.$e->getMessage();
	} catch (Exception $e) {
		$data['response'] = false;
		$data['message'] = 'Php Error : '.$e;
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Career | current openings at Deshpande Startups</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/career-opportunities"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/career-opportunities">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/bg-home/team-image.png">
	<meta property="og:description" content="We at Deshpande Startups are on a mission to support Entrepreneurs across domain. We are building an ecosystem that enables ideas and entrepreneurs to complement each other to create large impact."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content= "We are building an ecosystem that enables ideas and entrepreneurs to complement each other to create large impact."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Business development manager, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Deshpande Startups career Opportunities">
	<link rel="canonical" href="https://www.deshpandestartups.org/career-opportunities">

	<?php
		 // $title = 'Deshpande Startups';
	require_once 'essentials/bundle.php';
	?>
	<script>
		function myFunction()
		{
			$('.box').hide();
			$('.clickme').each(function() {
				$(this).show(0).on('click', function(e) {
					e.preventDefault();
					$(this).next('.box').slideToggle('fast');
				});
			});
		}
	</script>

	<style type="text/css">
		.bgcolorogr{background-color:rgba(0,0,0,.05)}
		.mandatory{color:red;}
		#g-recaptcha-response {
			display: block !important;
			position: absolute;
			margin: -78px 0 0 0 !important;
			width: 302px !important;
			height: 76px !important;
			z-index: -999999;
			opacity: 0;
		}
	</style>
</head>
<body onload="myFunction()" id="page-top">
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	<!-- <img class="carousel-inner img-fluid" src="img/bg-home/team-image.png" width="1349" height="395" alt="Deshpande Startups deshpande foundation">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb justify-content-end">
			<li class="breadcrumb-item"><a href="./">Home</a></li>
         <li class="breadcrumb-item"><a href="career">Career</a></li>
			<li class="breadcrumb-item active" aria-current="page">Career</li>
		</ol>
	</nav> -->
	<br>
	<!-- Section Contact ================================================== -->
	<section id="contact" class="no-padding">
		<div class="container">
			<div class="center  wow fadeInDown">
				<h2 class="text-yellow text-center slideInDown pt-2" data-wow-duration="0.5s" data-wow-offset="50" data-wow-delay="0.1s"><span class="text-muted">APPLY</span> NOW</h2>
				<div class="divider b-y text-yellow content-middle"></div>
				<br>
			</div>
		</div>

		<?php if (empty($msg)) { ?>
			<div class="container">
				<?php
				if (isset($data)) {
					if (!$data['response']) {
						?>
						<div class="alert alert-warning alert-dismissible text-center" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>Warning!</strong> <?php echo $data['message']?>
						</div>
						<?php
					} else {
						?>
						<div class="alert alert-success alert-dismissible text-center" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>Success!</strong> <?php echo $data['message']?>
						</div>
						<?php
					}
				}
				?>
				<div class="col-md-12">
					<div class="row justify-content-md-center">
						<!-- <div class="col-md-6"> -->
							<div class="col-lg-8 p-4 w3-card">
								<form action='<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>' enctype="multipart/form-data" id="ss-form" role="form" method="POST">
									<div class="form-group">
										<label for="firstname"><b>First name <span class="mandatory">*</span></b></label>
										<input type="text" class="form-control" maxlength="50" pattern="^(?![ .]+$)[a-zA-Z .]*$" required="required" id="firstname" name="firstname" placeholder="Your name"  maxlength="40" title="No Space Starting, Between or After the Name or Should be only Character">
									</div>
									<div class="form-group">
										<label for="lastname"><b>Last name <span class="mandatory">*</span></b></label>
										<input type="text" class="form-control" maxlength="50" pattern="^(?![ .]+$)[a-zA-Z .]*$" required="required" id="lastname" name="lastname" placeholder="Your last name"  maxlength="40" title="No Space Starting, Between or After the Last Name or Should be only Character">
									</div>
									<div class="form-group">
										<label for="emailaddress"><b>Email address <span class="mandatory">*</span></b></label>
										<input type="email" class="form-control" id="emailaddress" name="emailaddress" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Enter email address"  maxlength="80" title="No Space Starting, Between or After the Email id">
									</div>
									<div class="form-group">
										<label for="mobilenumber"><b>Mobile number <span class="mandatory">*</span></b></label>
										<input type="text" class="form-control" id="mobilenumber" pattern="[789][0-9]{9}" maxlength="10" name="mobilenumber" placeholder="Enter 10 digits mobile number"  required="required" title="Number should be 10 digits starts with 7 or 8 or 9">
									</div>
									<div class="form-group">
										<label for="select" ><b>Applying for <span class="mandatory">*</span></b></label><br>
										<select name="select" class="form-control" required="required">
											<option value=""> Select... </option>
											<!-- <option value="CEO">CEO</option> -->
											<option value="CEO">CEO</option>
											<option value="Sr. Director"> Sr. Director</option>
											<option value="Graphic Designer">Graphic Designer</option>
										</select>
										<br>
										<div class="form-group">
											<label for="comments"><b>Comments </b></label>
											<textarea type="text" class="form-control" id="comments" name="comments"  cols="10" rows="4"  maxlength="500" placeholder="Message (remember, short & sweet please)"></textarea>
										</div>
										<div class="form-group">
											<label for="upload"><b>Upload CV <span class="mandatory">*</span></b></label>
											<input type="file" id="cv" name="cv" required="required"><br>
											<!-- <input name="file" type="file" id="file" required="required"><br> -->
											<p><b><span class="mandatory">*</span></b> All fields are mandatory.</p>
										</div>
										<!-- <div class="form-group">
											<div class="g-recaptcha" data-sitekey="6LfBZWIUAAAAAB6-K56qksxFSQvO5vLeluI7ykAI" required></div>
										</div> -->
										<input type="submit" name="submit" class="btn btn-warning" value="Submit">
									</div>
								</form>
								<!-- </div> -->
							</div>
							<br>
							<!-- </div> -->
						</div>
					</div>
				</div>
			<?php } else {
				echo $msg;
			} ?>
		</section>

		<div class="clearfix">
		</div><br>
		<!-- Section Footer ================================================== -->
		<?php
		require_once 'essentials/footer.php';
		require_once 'essentials/copyright.php';
		require_once 'essentials/js.php';
		?>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<!-- <script type="text/javascript">
			$(window).load(function(){
			var $recaptcha = document.querySelector('#g-recaptcha-response');

			if($recaptcha) {
				$recaptcha.setAttribute("required", "required");
			}
		});
	</script> -->
	<script>
		window.onload = function() {
			var recaptcha = document.forms["ss-form"]["g-recaptcha-response"];
			recaptcha.required = true;
			recaptcha.oninvalid = function(e) {
	 // do something
	 alert("Please complete the captcha");
	}
}
</script>
</body>
</html>