<!DOCTYPE html>
<html lang="en">
<head>
	<title>Assembly Engineer - Krishitantra</title>
	<?php
	require_once 'essentials/meta.php';
	?>
	<meta name="linkage" content="https://www.deshpandestartups.org/krishitantra-assembly-engineer"/>
	<meta property="og:site_name" content="Deshpande Startups"/>
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.deshpandestartups.org/krishitantra-assembly-engineer">
	<meta property="og:image" content="https://www.deshpandestartups.org/img/career/krishitantra-big.png">
	<meta property="og:description" content="We are looking for Assembly Engineer. Job Position: Assembly Engineer, Experience: 0-1 year of experience, Qualification: Diploma in Electrical and Electronics/ Electronics and Communication."/>
	<meta name="author" content="Deshpande Startups"/>
	<meta name="description" content="We are looking for Assembly Engineer. Job Position: Assembly Engineer, Experience: 0-1 year of experience, Qualification: Diploma in Electrical and Electronics/ Electronics and Communication."/>
	<!-- <meta name="keywords" content="Current openings, Business development executive, Incubation Associate, technical manager, Hubballi Karnatak India."/> -->
	<meta property="og:title" content="Assembly Engineer, Current openings at our incubated startup">
	<link rel="canonical" href="https://www.deshpandestartups.org/krishitantra-assembly-engineer">
	<?php
	require_once 'essentials/bundle.php';
	?>
	<style type="text/css">
		/*p{text-align:justify;}*/
		.cal{
			font-family: calibri;
		}
	</style>
</head>
<body>
	<?php
	require_once 'essentials/title_bar.php';
	require_once 'essentials/menus.php';
	?>
	
	<div class="container cal">
		<br>
		<div class="center  wow fadeInDown">
			<h2 class="text-yellow text-center"><span class="text-muted">Assembly</span> Engineer</h2>
			<div class="divider b-y text-yellow content-middle"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<p class=""><strong>Job Position:</strong> Assembly Engineer<br>
					<strong>Startup:</strong> Krishitantra (Klonec Automation Systems Pvt. Ltd.)<br>
					<strong>Qualification:</strong> Diploma in Electrical and Electronics/Electronics and Communication<br>
					<strong>Experience:</strong> 0-1 year of experience<br>
					<strong>Skills:</strong> Controls and Instrumentation, Equipment Maintenance, Manufacturing Experience, Manufacturing Quality, Mechanical Inspection Tools, Tooling, Safety Management, Quality Focus, Power Tools, Judgment, Decision Making Soldering, Electronics assembly, testing, calibration<br>
					<strong>Job Location:</strong> Udupi<br>
				</p>
			</div>
			<div class="col-md-6">
				<img src="img/career/krishitantra-big.png" class="img img-fluid" width="440" height="130" alt="Deshpande startups, incubated startup, Krishitantra"/>
			</div>
		</div>
		<!-- <br> -->

		<div class="row pt-2">
			<div class="col-md-12">
				<h3 class="text-yellow">Job Description:</h3>
				<ul>
					<li>Prepares work to be accomplished by studying assembly instructions, blueprint specifications, and parts lists; gathering parts, subassemblies, tools, and materials</li>
					<li>Positions parts and subassemblies by using templates or reading measurements</li>
					<li>Assembles components by examining connections for correct fit; fastening parts and subassemblies</li>
					<li>Verifies specifications by measuring completed component</li>
					<li>Resolves assembly problems by altering dimensions to meet specifications; notifying supervisor to obtain additional resources</li>
					<li>Keeps equipment operational by completing preventive maintenance requirements; following manufacturer's instructions; troubleshooting malfunctions; calling for repairs</li>
					<li>Maintains safe and clean working environment by complying with procedures, rules, and regulations</li>
					<li>Maintains supplies inventory by checking stock to determine inventory level; anticipating needed supplies; placing and expediting orders for supplies; verifying receipt of supplies</li>
					<li>Conserves resources by using equipment and supplies as needed to accomplish job results</li>
					<li>Documents actions by completing production and quality forms</li>
					<li>Contributes to team effort by accomplishing related results as needed</li>
				</ul>
			</div>
		</div>

		<!-- <br> -->
	</div>
	<br>

	<div class="container cal">
		<p class="text-center"><b>Interested candidates email Resumes to<br> E:<a href="mailto: sandeep&#046;kondaji&#064;krishitantra&#046;com"> sandeep&#046;kondaji&#064;krishitantra&#046;com</a></b></p>
	</div>
	<br>
	<?php
	require_once 'essentials/footer.php';
	require_once 'essentials/copyright.php';
	require_once 'essentials/js.php';
	?>
</body>
</html>